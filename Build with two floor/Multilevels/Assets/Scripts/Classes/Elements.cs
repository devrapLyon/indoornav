﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elements : MonoBehaviour {

	//Element class - Offices, employes
	public class elements_class {

		//Elements's position
		public float pos_element_x { get; set;}
		public float pos_element_y { get; set;}
		public float pos_element_z { get; set;}

		//Orientation
		public float rot_element_x { get; set;}
		public float rot_element_y { get; set;}
		public float rot_element_z { get; set;}


		//Level
		public int LevelID_elements {get;set;}

		public elements_class(){}

		public elements_class (float pos_element_x,float pos_element_y,float pos_element_z,float rot_element_x,float rot_element_y,float rot_element_z,int LevelID_elements)
		{
			this.pos_element_x = pos_element_x;
			this.pos_element_y = pos_element_y;
			this.pos_element_z = pos_element_z;
			this.rot_element_x = rot_element_x;
			this.rot_element_y = rot_element_y;
			this.rot_element_z = rot_element_z;
			this.LevelID_elements = LevelID_elements;
		}

	}
	public elements_class element_obj;
}
