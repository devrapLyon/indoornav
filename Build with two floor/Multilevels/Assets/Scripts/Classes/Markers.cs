﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Markers : MonoBehaviour {

	//Marker class
	public class Vumark_Class{

		//VuMark's position
		public float pos_vumark_x { get; set;}
		public float pos_vumark_y { get; set;}
		public float pos_vumark_z { get; set;}

		//Vumark's rotation
		public float Rot_vumark_x { get; set;}
		public float Rot_vumark_y { get; set;}
		public float Rot_vumark_z { get; set;}

		//Level
		public int LevelID_markers {get;set;}
		//VumarkID
		public string VuMarkID {get;set;}

		public Vumark_Class(){}

		public Vumark_Class(float pos_vumark_x,float pos_vumark_y,float pos_vumark_z,
			float Rot_vumark_x ,float Rot_vumark_y,float Rot_vumark_z,
			int LevelID_markers,string VuMarkID)
		{
			this.pos_vumark_x = pos_vumark_x;
			this.pos_vumark_y = pos_vumark_y;
			this.pos_vumark_z = pos_vumark_z;
			this.Rot_vumark_x = Rot_vumark_x;
			this.Rot_vumark_y = Rot_vumark_y;
			this.Rot_vumark_z = Rot_vumark_z;
			this.LevelID_markers = LevelID_markers;
			this.VuMarkID = VuMarkID;
		}
	}
	public Vumark_Class vm;
}
