﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;



public class Controler_agent : MonoBehaviour {
	private NavMeshAgent _agent;

	public Transform _destination;

	// Use this for initialization
	void Start () {
		
		//this.transform.position=GameObject.Find("Cible1").transform.position;
		//OnDrawGiz (_destination);
		//OnDrawGiz (_destination);
		//_agent = this.GetComponent<NavMeshAgent> ();
		//_agent.destination = _destination.position;

	}
	
	// Update is called once per frame
	void Update () {
		
		if (Input.GetKeyDown (KeyCode.A)) 
		{
			this.transform.position=GameObject.Find("Cible1").transform.position;
			OnDrawGiz (_destination);
		}
		if (Input.GetKeyDown (KeyCode.B)) 
		{
			this.transform.position=GameObject.Find("Cible2").transform.position;
			OnDrawGiz (_destination);
		}

	}

	void OnDrawGiz(Transform obj)
	{
		//Destroy GameObjects 
		Transform target = obj.GetComponent<Transform> ();
		NavMeshAgent nav;

		LineRenderer line;
		nav= this.GetComponent<NavMeshAgent>();
		if( nav == null || nav.path == null )
			return;
		line = this.GetComponent<LineRenderer>();
		line.material = new Material( Shader.Find( "Sprites/Default" ) ) { color = Color.yellow };

		line.startColor=Color.yellow;
		line.endColor = Color.green;

		nav.SetDestination (target.position);
		nav.isStopped=true;
		var path = nav.path;
		if (path.corners.Length < 2)
			return ;

		line.positionCount=path.corners.Length ;
		//line.alignment = LineAlignment.Local;
		//Draw the line
		for( int i = 0; i < path.corners.Length; i++ )
		{
			line.SetPosition( i, path.corners[i] );

		}

	}



}


