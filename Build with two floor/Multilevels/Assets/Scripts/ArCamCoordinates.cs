﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.AI;
//using System.Linq;
//using System;
///// <summary>
///// Script can be change in term of our objectif 
///// for example if i use the script only for get my position on the map, 
///// or in navigation mode to draw path from Vumark prefab to target
///// </summary>
//public class ArCamCoordinates : MonoBehaviour {
//	//This class is responsible for Get ArCamera position and rotation to set it to element class also get current level of element
//	//Use this for initialization
//	//Variables declaration
//	//Find_Second_Floor fsf_script;
//	private Elements element_class_script;
//	private Markers vumark_class_script;
//	List<Markers.Vumark_Class> markers_nodes = new List<Markers.Vumark_Class>();
//	VuMarkHandler vumarkhandler_Script;
//
//	void Start () {
//		//fsf_script = GameObject.Find ("Origin").GetComponent<Find_Second_Floor> ();
//
//		//Inialize the element_script
//		element_class_script=GameObject.Find("VuMark").GetComponent<Elements>();
//		vumark_class_script = GameObject.Find ("VuMark").GetComponent<Markers> ();
//
//		//Find all the vumarks objects in the scene
//		GameObject[] objs = GameObject.FindGameObjectsWithTag("vumark").ToArray();
//		foreach (GameObject go in objs) {
//			//Get vumark level number
//			int marker_level_num = go.GetComponent<vumark_levid> ().vumark_level_num;
//			//Get marker id
//			string marker_id = go.GetComponent<vumark_levid> ().marker_id;
//			//Instance of the vumark objects with different's level 
//			vumark_class_script.vm = new Markers.Vumark_Class (go.transform.position.x,
//				go.transform.position.y, go.transform.position.z,
//				go.transform.eulerAngles.x, go.transform.eulerAngles.y, go.transform.eulerAngles.z,
//				marker_level_num, marker_id);
//			//Add Vumark object in the list of Vumark_Class
//			markers_nodes.Add (vumark_class_script.vm);
//		}//foreach end 
//		//Access to Vumark object
//		vumarkhandler_Script=GameObject.Find("VuMark").GetComponent<VuMarkHandler>();
//
//	}
//	
//	// Update is called once per frame
//	void Update () {
//		//define or objectif ----> Navigation or Get Position and inset it to BDD
//		#region vumark_detection
//		if (vumarkhandler_Script.vuMarkId.Length != 0) {
//			//Call when we detect a maker on the scene, then find the Vumark objects in the list of markers 
//			//For example if i scan marker number 1, program will find the Vumark object with this number=>1 at the list.
//			var marker_temp = markers_nodes.Where (i => i.VuMarkID == vumarkhandler_Script.vuMarkId).FirstOrDefault ();
//			//if the prefab are not defined in our program  
//			if (marker_temp == null) {
//				Debug.LogError ("You don't define Vumark object with this id in the application");
//
//			}
//		//else set the position and localRotation of marker object to VuMark prefab in the Unity Scene.
//		 else {
//				//GameObject.Find ("VuMark").transform.position = new Vector3(marker_temp.pos_vumark_x,marker_temp.pos_vumark_y,marker_temp.pos_vumark_z);
//				//GameObject.Find ("VuMark").transform.eulerAngles=new Vector3(marker_temp.Rot_vumark_x,marker_temp.Rot_vumark_y,marker_temp.Rot_vumark_z);
//
//				//the element get ARCamera position and orientation and marker level
//				Transform _arCam = this.GetComponent<Transform> ();
//				element_class_script.element_obj = new Elements.elements_class (_arCam.transform.position.x, _arCam.transform.position.y, _arCam.transform.position.z,
//					_arCam.transform.eulerAngles.x, _arCam.transform.eulerAngles.y, _arCam.transform.eulerAngles.z, marker_temp.LevelID_markers);
//			
//				GameObject.Find ("Sphere").transform.position = new Vector3 (element_class_script.element_obj.pos_element_x, element_class_script.element_obj.pos_element_y, element_class_script.element_obj.pos_element_z); 
//				//Add to database 
//			}//end else
//			#endregion
//	
//		}//end if 
//	}//end update function
//}
