﻿using UnityEngine;
using System.Collections;
using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Newtonsoft.Json;
using LitJson;

public class UDPSend : MonoBehaviour
{
	//private string IP = "192.168.1.23"; // default local 10.188.106.212
	private string IP = "192.168.43.24";
	private int port = 5555; 
	IPEndPoint remoteEndPoint;
	UdpClient client;
	//string strMessage="";
	string json_str=null;
	VuMarkHandler scriptHandler;

	public void Start()
	{
		scriptHandler = GameObject.Find ("VuMark").GetComponent<VuMarkHandler> ();
		init();   
	}

	void Update()//OnGUI()
	{
		//Rect rectObj=new Rect(40,120,200,400);
		//GUIStyle style = new GUIStyle();
		//style.alignment = TextAnchor.UpperLeft;
		//GUI.Box(rectObj,"UDPSendData\n IP : "+IP+" Port : "+port,style);
		//
		//strMessage=GUI.TextField(new Rect(40,160,140,40),strMessage);
		//if (GUI.Button(new Rect(180,160,80,40),"Send"))
		//{
		Vector3 pos=this.GetComponent<Transform>().position;
//		float a = this.GetComponent<Transform> ().rotation.x;
//		Debug.Log ("DDDDDDDDDDDDDDDDDDDDDDDDDDDD"+a);
//
		Vector3 eulerAngles = transform.localRotation.eulerAngles;
		Vumark_Class vm = new Vumark_Class{cam_x=pos.x,cam_y=pos.y,cam_z=pos.z,
			camR_x=eulerAngles.x,camR_y=eulerAngles.y,camR_z=eulerAngles.z,
			VuMarkID=scriptHandler.vuMarkId};
			
		json_str=JsonConvert.SerializeObject(vm);
		//json_str=JsonUtility.ToJson(pos);
		Debug.Log (json_str);
		sendString(json_str);
		//}      
	}

	public void init()
	{
	    //remoteEndPoint = new IPEndPoint(IPAddress.Broadcast, port);
		 //remoteEndPoint = new IPEndPoint(IPAddress.Any, port);
		remoteEndPoint = new IPEndPoint(IPAddress.Parse(IP), port);
		client = new UdpClient();
	}

	private void sendString(string message)
	{
		try
		{
			//Debug.Log("Here");
			byte[] data = Encoding.UTF8.GetBytes(message);
			client.Send(data, data.Length, remoteEndPoint);
		}

		catch (Exception err)
		{
			print(err.ToString());
		}
	}

	void OnDisable()
	{
		if ( client!= null)   client.Close();
	}
	public class Vumark_Class{
		//Camera position
		public float cam_x{ get; set;}
		public float cam_y{ get; set;}
		public float cam_z{ get; set;}
		//Camera Rotation
		public float camR_x{ get; set;}
		public float camR_y{ get; set;}
		public float camR_z{ get; set;}
		//VuMarkID
		public string VuMarkID{ get; set;}
	}
}