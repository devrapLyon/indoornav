﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class Distance : MonoBehaviour {

	[SerializeField]
	GameObject VumarkObj;
	public float distance;
	private VuMarkHandler ScriptHandler;
	private PanelShowHide mIdPanel;

	// Use this for initialization
	void Start () {

		mIdPanel = GameObject.Find("VuMarkManager").GetComponent<PanelShowHide>();
		mIdPanel.ResetShowTrigger();
		ScriptHandler=GameObject.Find("VuMarkManager").GetComponent<VuMarkHandler>();

	}

	// Update is called once per frame
	void Update () {
		distance=ScriptHandler.distance;
		Debug.Log("Distance" + distance.ToString ());
		StartCoroutine(ShowPanelAfter(0.5f, distance.ToString(), ScriptHandler.vuMarkId, null));

	}
	private IEnumerator ShowPanelAfter(float seconds, string vuMarkTitle, string vuMarkId, Sprite vuMarkImage)
	{
		yield return new WaitForSeconds(seconds);

		mIdPanel.Show(vuMarkTitle, vuMarkId, vuMarkImage);
	}
}

