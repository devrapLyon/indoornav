﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class script_test : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.A)) {
			//Set VuMark to new position
			GameObject.Find ("VuMark").transform.position =
				this.transform.position;
			//Set Origin to new position
			GameObject.Find ("Origin").transform.position =
				this.transform.position;
			
			//Active this NavMeshAgent, LineRenderer, Find.cs Gameobject
			GameObject.Find("Origin").GetComponent<NavMeshAgent>().enabled=true;
			GameObject.Find ("Origin").GetComponent<LineRenderer> ().enabled = true;
			GameObject.Find ("Origin").GetComponent<Finder> ().enabled = true;
		}
		if (Input.GetKey (KeyCode.B)) {
			GameObject.Find("Origin").GetComponent<NavMeshAgent>().enabled=false;
			GameObject.Find ("Origin").GetComponent<LineRenderer> ().enabled = false;
			GameObject.Find ("Origin").GetComponent<Finder> ().enabled = false;
		}

	}
}
