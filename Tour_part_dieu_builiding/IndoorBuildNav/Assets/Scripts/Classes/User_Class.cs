﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class User_Class : MonoBehaviour {

	public class User{
		public string firstName{ get; set;}
		public string lastName{ get; set;}
		public string mobileNumber{ get; set;}
		public float  pos_x{ get; set;}
		public float  pos_y{ get; set;}
		public float  pos_z{ get; set;}
		public string Key { get; set;}
		public string user_level {get; set;}
		public User(){
			
		}
		public User (float pos_x ,float pos_y ,float pos_z,string user_level ,string key)
		{

     		this.pos_x=pos_x;
			this.pos_y=pos_y;
			this.pos_z=pos_z;
			this.user_level=user_level;
			this.Key=key;

		}

	}
	public User NewUser;

}
