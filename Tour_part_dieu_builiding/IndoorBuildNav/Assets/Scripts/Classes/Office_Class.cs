﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Office_Class : MonoBehaviour {

	public class Salle{
		
		public string label { get; set;}
		public float  pos_x { get; set;}
		public float  pos_y { get; set;}
		public float  pos_z { get; set;}
		public string office_level{ get; set;}
		public string Key   { get; set;}
		public Salle(){

		}
		public Salle (float pos_x,float pos_y,float pos_z,string office_level,string key)
		{

			this.pos_x=pos_x;
			this.pos_y=pos_y;
			this.pos_z=pos_z;
			this.office_level=office_level;
			this.Key=key;

		}

	}
	public Salle NewSalle;
}
