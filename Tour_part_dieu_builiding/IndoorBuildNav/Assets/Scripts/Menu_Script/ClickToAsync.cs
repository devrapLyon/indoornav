﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ClickToAsync : MonoBehaviour {

	//Variables declartion
	public Slider loadingbar;
	public GameObject loadimage;
	private AsyncOperation async;


	//Method declaration
	public void ClickAsync(int level){
		loadimage.SetActive (true);
		StartCoroutine (LoadLevelWithBar (level));
	}

	IEnumerator LoadLevelWithBar(int level){
		yield return new WaitForSeconds (3);
		async = SceneManager.LoadSceneAsync (level);
		while (!async.isDone){
			loadingbar.value = async.progress;
			yield return null;
		}
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
