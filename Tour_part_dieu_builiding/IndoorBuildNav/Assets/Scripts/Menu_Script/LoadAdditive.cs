﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadAdditive : MonoBehaviour {

	//Load Scene with destroy all gameobjects
	//SceneManager.loadScene(level)
	//Load new scene on the current scene

	public void ChangeScene(int level){
		SceneManager.LoadScene (level,LoadSceneMode.Additive);
	}

}
