﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Globalization;

public class ButtonListButton : MonoBehaviour {

	[SerializeField]
	private Text BtnTxt;
	[SerializeField]
	private GameObject BtnsList;
	[SerializeField]
	private GameObject Obj_dest;
	//Text
	public Text Txt_action;
	//Menu
	public GameObject Menu_Bdd;


	private string tmpStr;
	private ButtonListControl BtnListControl;
	private Change_View change_view_script;
	//public Sprite newSprite;
	//Start Function
	void Start(){
		//initialize the button list control 

		BtnListControl=BtnsList.GetComponent<ButtonListControl> ();
		//this.GetComponent<Image>().overrideSprite=newSprite;
		change_view_script = GameObject.Find ("Canvas").GetComponent<Change_View> ();
	}

	public void SetText(string strText){
		//this.GetComponent<Image> ().sprite = sprite;
		tmpStr = strText;
		BtnTxt.text = strText;
	}

	public void onClick(){
		//Get button name which include the key of users and office
		string strbtnkey = EventSystem.current.currentSelectedGameObject.name;
		//User button
		if (BtnListControl.IDiscUser.ContainsKey(strbtnkey)){
			change_view_script.destination_checked = true;
			Txt_action.text = "Scaner le marqueur";

			Debug.Log("key in the user dictionary is"+BtnListControl.IDiscUser[strbtnkey].pos_x +" y"+ BtnListControl.IDiscUser[strbtnkey].pos_y+" z"+BtnListControl.IDiscUser[strbtnkey].pos_z+" "+BtnListControl.IDiscUser[strbtnkey].user_level);
			Obj_dest.transform.position = new Vector3 (BtnListControl.IDiscUser[strbtnkey].pos_x,
				BtnListControl.IDiscUser[strbtnkey].pos_y,
				BtnListControl.IDiscUser[strbtnkey].pos_z);
			Obj_dest.GetComponent<Destination_script> ().level = BtnListControl.IDiscUser[strbtnkey].user_level;
			Menu_Bdd.SetActive (false);
		}
		//Office button
		if (BtnListControl.IDiscOffice.ContainsKey (strbtnkey)) {
			change_view_script.destination_checked = true;
			Txt_action.text = "Scaner le marqueur";

			Debug.Log("key in the office dictionary is"+BtnListControl.IDiscOffice[strbtnkey].pos_x+" y"+BtnListControl.IDiscOffice[strbtnkey].pos_y+" z"+BtnListControl.IDiscOffice[strbtnkey].pos_z+" " +BtnListControl.IDiscOffice[strbtnkey].office_level);
			Obj_dest.transform.position = new Vector3 (BtnListControl.IDiscOffice[strbtnkey].pos_x,
				BtnListControl.IDiscOffice[strbtnkey].pos_y,
				BtnListControl.IDiscOffice[strbtnkey].pos_z);
			Obj_dest.GetComponent<Destination_script> ().level = BtnListControl.IDiscOffice [strbtnkey].office_level;
			Menu_Bdd.SetActive (false);


		}
		Debug.Log(EventSystem.current.currentSelectedGameObject.name);
		//BtnListControl.ButtonChecked (tmpStr);

	}
}
