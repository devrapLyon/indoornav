﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Firebase.Unity.Editor;
using UnityEngine.EventSystems;
using System.Globalization;



/// <summary>
/// Functionality to get the data from database
/// </summary>

public class ButtonListControl : MonoBehaviour {
	//database variables 

	FirebaseApp app;

	[SerializeField]
	private GameObject BtnTemplate;
	[SerializeField]
	private List<GameObject> buttons;
	public Dictionary <string, User_Class.User> IDiscUser= new Dictionary<string, User_Class.User>();
	public Dictionary <string, Office_Class.Salle> IDiscOffice = new Dictionary<string, Office_Class.Salle> ();
	private User_Class UserScript;
	private Office_Class OfficeScript;
	Dictionary <string, User_Class.User>.ValueCollection valueUserColl;
	Dictionary <string, Office_Class.Salle>.ValueCollection valueOfficeColl;
	//public Sprite newSprite;
	//image sprite

	// Use this for initialization
	void Start () {
		//Get reference to user 
		//initialize 
		UserScript=this.GetComponent<User_Class>();
		OfficeScript = this.GetComponent<Office_Class>();

		DatabaseReference mdatabase;
		app=FirebaseApp.DefaultInstance;

		app.SetEditorDatabaseUrl ("https://indoornav-3168c.firebaseio.com/");
		//app.SetEditorDatabaseUrl("https://m500-c6b88.firebaseio.com/");
		mdatabase = FirebaseDatabase.DefaultInstance.GetReference("OfficeData");
		Get_office_datafirebase (mdatabase);
		mdatabase = FirebaseDatabase.DefaultInstance.GetReference("users");
		Get_users_datafirebase (mdatabase);

//		mdatabase.GetValueAsync().ContinueWith(task => {
//			if (task.IsFaulted) {
//				Debug.Log("task error");
//			}
//			else if (task.IsCompleted) {
//				DataSnapshot snapshot = task.Result;
//				foreach(DataSnapshot childSnapshot in snapshot.Children){
//					Debug.Log(snapshot.ChildrenCount.ToString());
				//GameObject button = Instantiate (BtnTemplate)as GameObject;
				//button.SetActive (true);
				//var jsonObject = JsonConvert.DeserializeObject<RootObject>(childSnapshot.GetRawJsonValue());
					//JObject jObject = JObject.Parse(); 
					//JToken lable = jObject[jsonObject.OfficeData.id_office.ToString()][jsonObject.OfficeData.id_office.lable.ToString()];
				//	button.GetComponent<ButtonListButton> ().SetText(""+jsonObject.OfficeData.id.lable.ToString());
					//Debug.Log(jsonObject.ToString());
				//button.transform.SetParent (BtnTemplate.transform.parent,false);
				//}

			//}
		//});
	}

	void HandleEventHandler (object sender, ValueChangedEventArgs e)
	{
		if (e.Snapshot != null && e.Snapshot.ChildrenCount > 0) {
			foreach (var childSnapshot in e.Snapshot.Children) {
				Debug.Log (""+childSnapshot.Child("firtName").Value);
			}
		}
	}
	
	public void ButtonChecked(string strTxt){
		Debug.Log (strTxt);
	}


	private void Get_office_datafirebase(DatabaseReference mdatabase){
		
	   mdatabase.GetValueAsync().ContinueWith(task => {
		if (task.IsFaulted) {
			Debug.Log("task error");
		}
		else if (task.IsCompleted) {
			DataSnapshot snapshot = task.Result;
			foreach(DataSnapshot childSnapshot in snapshot.Children){
					OfficeScript.NewSalle= new Office_Class.Salle(float.Parse(childSnapshot.Child("pos-x").Value.ToString(),CultureInfo.InvariantCulture.NumberFormat),
						float.Parse(childSnapshot.Child("pos-y").Value.ToString(),CultureInfo.InvariantCulture.NumberFormat),
						float.Parse(childSnapshot.Child("pos-z").Value.ToString(),CultureInfo.InvariantCulture.NumberFormat),
						childSnapshot.Child("level").Value.ToString(),
						childSnapshot.Key);
					IDiscOffice .Add(childSnapshot.Key,new Office_Class.Salle{pos_x=OfficeScript.NewSalle.pos_x,
						pos_y=OfficeScript.NewSalle.pos_y,
						pos_z=OfficeScript.NewSalle.pos_z,
						office_level=OfficeScript.NewSalle.office_level,
						Key=OfficeScript.NewSalle.Key});
					valueOfficeColl= IDiscOffice.Values;
					GameObject button = Instantiate (BtnTemplate)as GameObject;
					button.name=childSnapshot.Key;
					button.SetActive (true);
					button.GetComponent<ButtonListButton> ().SetText(""+childSnapshot.Child("label").Value);

					Debug.Log(childSnapshot.Child("label").Value);
					button.transform.SetParent (BtnTemplate.transform.parent,false);
					//add instantiate button to a list
					buttons.Add(button);
			}
	    }

	  });
	}
	private void Get_users_datafirebase(DatabaseReference mdatabase){

		mdatabase.GetValueAsync().ContinueWith(task => {
			if (task.IsFaulted) {
				Debug.Log("task error");
			}
			else if (task.IsCompleted) {
				DataSnapshot snapshot = task.Result;
				foreach(DataSnapshot childSnapshot in snapshot.Children){
					
					//Add value to Dictionary
					UserScript.NewUser= new User_Class.User(float.Parse(childSnapshot.Child("pos-x").Value.ToString(),CultureInfo.InvariantCulture.NumberFormat),
						float.Parse(childSnapshot.Child("pos-y").Value.ToString(),CultureInfo.InvariantCulture.NumberFormat),
						float.Parse(childSnapshot.Child("pos-z").Value.ToString(),CultureInfo.InvariantCulture.NumberFormat),
						childSnapshot.Child("level").Value.ToString(),
						childSnapshot.Key);
					IDiscUser.Add(childSnapshot.Key,new User_Class.User{pos_x=UserScript.NewUser.pos_x,
						pos_y=UserScript.NewUser.pos_y,
						pos_z=UserScript.NewUser.pos_z,
						user_level=UserScript.NewUser.user_level,
						Key=UserScript.NewUser.Key});
					valueUserColl= IDiscUser.Values;
					GameObject button = Instantiate (BtnTemplate)as GameObject;
					button.name=childSnapshot.Key;
					button.SetActive (true);
					button.GetComponent<ButtonListButton> ().SetText(""+childSnapshot.Child("firstName").Value);
					button.transform.SetParent (BtnTemplate.transform.parent,false);
					buttons.Add(button);
				}

//				if (IDiscUser.ContainsKey("100")){
//
//					Debug.Log("The key is in the dictionary"+IDiscUser["100"].mobileNumber);
//				}
				//	foreach (var s in valueUserColl) 
//				{
//					Debug.Log (s.mobileNumber + " "+s.Key);
//
//
//				}
			}

		});
	}
}
public class Key
{
	public string firstName { get; set; }
	public string lastName { get; set; }
	public string mobileNumber { get; set; }
}
public class Users
{
	public Key key { get; set; }
}

public class IdOffice
{
	public string lable { get; set; }
}

public class OfficeData
{
	public IdOffice id_office { get; set; }
}

public class RootObject
{
	public Users users { get; set; }
	public OfficeData OfficeData { get; set; }
}