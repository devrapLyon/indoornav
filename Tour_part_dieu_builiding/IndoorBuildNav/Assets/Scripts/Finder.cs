﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;
using System;
using Newtonsoft.Json;
using UnityEngine.UI;
using System.Threading.Tasks;
/// <summary>
/// Finder Class is responsible for get scaned marker (Id) and draw the path between your position and destination  
/// </summary>
public class Finder : MonoBehaviour {
	[SerializeField]
	GameObject _ArCamera;
	[SerializeField]
	GameObject _MainCamera_Scene;
	public GameObject _Vumark;
	public GameObject spawnObject;
	private GameObject arrowObj;
	[SerializeField]
	List <GameObject> lExit;
	[SerializeField]
	GameObject Obj_dest; 
	public List<Markers.Vumark_Class> markers;
	static int _Level;
	int VumarkNumber;
	//Element's class and Vumark's class
	private Markers vumark_class_script;
	VuMarkHandler vumarkhandler_Script;
	private Change_View change_view_script;
	[SerializeField]
	private Text Txt_action;
	public GameObject Menu_Bdd;

	void Start(){
		//initialize Exit object's list

		change_view_script = GameObject.Find ("Canvas").GetComponent<Change_View> ();
		//Access to Vumark object
		vumarkhandler_Script=GameObject.Find("VuMark").GetComponent<VuMarkHandler>();
		//initialize element and vumark script
		vumark_class_script = this.GetComponent<Markers> ();

		markers=new List<Markers.Vumark_Class>();

		//Find all the vumarks objects in the scene
		GameObject[] objs = GameObject.FindGameObjectsWithTag("vumark").ToArray();
		foreach (GameObject go in objs) {
			//Get vumark level number
			int marker_level_num = go.GetComponent<vumark_levid> ().vumark_level_num;
			//Get marker id
			string marker_id=go.GetComponent<vumark_levid> ().marker_id;
			//Instance of the vumark objects with different's level 
			vumark_class_script.vm = new Markers.Vumark_Class (go.transform.position.x, go.transform.position.y, go.transform.position.z,
				go.transform.eulerAngles.x,go.transform.eulerAngles.y,go.transform.eulerAngles.z,
				marker_level_num,marker_id);
			//Add Vumark object in the list of Vumark_Class
			markers.Add(vumark_class_script.vm);
		}//end of foreach
		foreach (var t in markers) {
			Debug.Log (t.LevelID_markers + " " + t.VuMarkID);
		}
		//disable all components on the Origin's Gameobject
		active_disable (false);
	}
	void Update(){
		

		//if level value of destination is not null or empty 

		if (Obj_dest.GetComponent<Destination_script>().level!=null ){
			#region vumark_detection
			if (vumarkhandler_Script.vuMarkId.Length!=0){
				Markers.Vumark_Class marker_temp=null;
				//Call when we detect a maker on the scene, then find the Vumark objects in the list of markers 
				//For example if i scan marker number 1, program will find the Vumark object with this number=>1 at the list.
				marker_temp=markers.Where(i => i.VuMarkID ==vumarkhandler_Script.vuMarkId).FirstOrDefault();
				//if the prefab are not defined in our program  
				if (marker_temp==null)
				{
					Debug.LogError("You don't define Vumark object with this id in the application");

				}
				//else set the position and localRotation of marker object to VuMark prefab in the Unity Scene.
				else 
				{

					GameObject.Find ("VuMark").transform.position = new Vector3(marker_temp.pos_vumark_x,marker_temp.pos_vumark_y,marker_temp.pos_vumark_z);
					GameObject.Find ("VuMark").transform.eulerAngles=new Vector3(marker_temp.Rot_vumark_x,marker_temp.Rot_vumark_y,marker_temp.Rot_vumark_z);

					//the origin prefab get marker postion and level
					this.transform.position = new Vector3(marker_temp.pos_vumark_x,marker_temp.pos_vumark_y,marker_temp.pos_vumark_z);
					this.GetComponent<Origin_scripts>().level=marker_temp.LevelID_markers;
					//Change view of MainCamera to current floor
					switch (this.GetComponent<Origin_scripts>().level){
					case 9:
						_MainCamera_Scene.transform.position= new Vector3 (0.6f,82.6f,-9.59f);
						//GameObject.Find("Tour_Part_Dieu_9.3_E").SetActive(true);
						//GameObject.Find("Tour_Part_Dieu_9.3_E (1)").SetActive(false);
					
						break;

					case 10:
						_MainCamera_Scene.transform.position= new Vector3 (64.7f, 84.5f ,-9.8f);

						//_MainCamera_Scene.transform.position= new Vector3 (51.1f,83.4f,-9.7f);
						//GameObject.Find("Tour_Part_Dieu_9.3_E").SetActive(false);
						//GameObject.Find("Tour_Part_Dieu_9.3_E (1)").SetActive(true);
							break;
					}

					//conditions
					//if Origin level is equal to elements (Destination) ==same floor
					if (this.GetComponent<Origin_scripts>().level ==int.Parse(Obj_dest.GetComponent<Destination_script>().level)) {
			        	active_disable(true);	
				        //Set element position to a vector 
						//GameObject.Find("Destination").transform.position = new Vector3 (elObj.pos_element_x,elObj.pos_element_y,elObj.pos_element_z);
						OnDrawGiz (GameObject.Find("Destination-pos_enter"));	
					//OnDrawGiz (GameObject.Find("Destination-pos_enter"));
						Debug.Log ("We are in the same level");
					}//end if
					//if Origin level is not equal to elements (Destination) ==another floor 
					if (this.GetComponent<Origin_scripts>().level != int.Parse(Obj_dest.GetComponent<Destination_script>().level)) {
						Debug.Log ("We are in the anther level");
						//Find the level and chose an exit 
					    GameObject Exit_Object=lExit.Where(i => i.gameObject.GetComponent<Exit_Level>().exit_level_var ==this.GetComponent<Origin_scripts>().level ).FirstOrDefault();
						active_disable(true);	
						OnDrawGiz (Exit_Object);
						//GameObject.Find("Exit_9")

					}//end if

				}//end else

			}//end second if

		}//end first if 
		else {
			Txt_action.text="Séectionner votre destionation";
			Menu_Bdd.SetActive(true);
		}
		#endregion
	}
	//Function to enable or disable Linerenderer or NavMeshAgent on the Origin component
	private void active_disable(bool state){
		this.GetComponent<NavMeshAgent>().enabled=state;
		this.GetComponent<LineRenderer> ().enabled = state;
	}
	/// <summary>
	/// Function to draw the line and arrows
	/// from Origing position  to destination position
	/// </summary>
	/// <param name="obj">Object.</param>
	void OnDrawGiz(GameObject obj)
	{
		//Destroy GameObjects 
       GameObject[] objs = GameObject.FindObjectsOfType(typeof(GameObject)).Select(g => g as GameObject).Where(g => g.name == "arrow_direction(Clone)").ToArray();
		foreach (GameObject go in objs) {
			Destroy (go);
		}
		Transform target = obj.GetComponent<Transform> ();
		NavMeshAgent nav;
		LineRenderer line;
		nav= this.GetComponent<NavMeshAgent>();
		if( nav == null || nav.path == null )
			return;
		line = this.GetComponent<LineRenderer>();

		line.material = new Material( Shader.Find( "Sprites/Default" ) ) { color = Color.yellow };
		line.startColor=Color.yellow;
		line.endColor = Color.green;


		nav.SetDestination (target.position);
		nav.isStopped=true;
		var path = nav.path;
		if (path.corners.Length < 2)
			return ;

		line.positionCount=path.corners.Length ;
	
		//line.alignment = LineAlignment.Local;
		//Draw the line
		for( int i = 0; i < path.corners.Length; i++ )
		{
			line.SetPosition( i, path.corners[i] );


		}
		//Draw the arrows on the line 
		for( int i = 0; i < path.corners.Length; i++ )
		{
			//if (i<path.corners.Length-1)
			if (i<path.corners.Length-1){
				
				Vector3 heading= path.corners[i+1]-path.corners[i]; 

				//Vector3 position_arrow = new Vector3 (path.corners [i].x,path.corners[i].y+0.1f,path.corners[i].z);
					
				var rotation_obj=Quaternion.LookRotation(heading); 
				for (int j = 0; j < 6; j++) {
					Vector3 posnew=Vector3.Lerp(path.corners[i],path.corners[i+1],j*0.15f);
					Vector3 position_arrow = new Vector3 (posnew.x,0.42f,posnew.z);
					GameObject obj_clone=Instantiate(spawnObject, 
						position_arrow, 
						rotation_obj) as GameObject;
					Vector3 tmp = obj_clone.transform.eulerAngles;
					tmp.x = GameObject.Find ("arrow_direction").transform.eulerAngles.x;
					obj_clone.transform.eulerAngles = tmp;
				}
//				    GameObject obj_clone=Instantiate(spawnObject, 
//					position_arrow, 
//					rotation_obj) as GameObject;

//			Vector3 tmp = obj_clone.transform.eulerAngles;
//			tmp.x = GameObject.Find ("arrow_direction").transform.eulerAngles.x;
//			obj_clone.transform.eulerAngles = tmp;

				//Debug.Log ("Direction is : "+direction);
			}

			//Quaternion.identity
			//path.corners[ i ]
			//line.SetPositions(pos);
			//Debug.DrawRay(path.corners[ i ], target.transform.position , Color.red, 20);
		}

	}



} 
/// <summary>
/// var object=resources.findobjectsoftypeall<Gameobject>().where(obj=>obj.name=="Name");
/// foreach (GameObject obj in object){
/// destroy (obj)
/// 
/// another solution 
/// List<GameObject> goList=new List<GameObject>();
/// string nametoadd="Cube";
/// 
/// void start()
/// {
///   foreach(GameObject go in Gameobject.FindObjectsOfType(typeof(GameObject)))
/// {
///   if (go.name==nameToAdd)
///  goList.add(go);
/// }
///  print(golist.Count);
/// }
/// }
/// </summary>
/// 
///   GameObject[] objs ;
//objs = GameObject.FindGameObjectsWithTag("LightUsers");
//foreach(gameObject lightuser in objs) {
//	lightuser.GetComponent<light>().enabled=false;
//}//https://answers.unity3d.com/questions/570961/gather-all-gameobjects-with-a-certain-name-in-a-sc.html
//arrow_direction(Clone)
//http://answers.unity3d.com/questions/1206000/what-scale-factor-when-using-meters-as-unit-in-ske.html