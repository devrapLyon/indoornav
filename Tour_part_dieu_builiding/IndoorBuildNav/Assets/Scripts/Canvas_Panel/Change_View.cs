﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class Change_View : MonoBehaviour {
	//Building elements
	private GameObject[] obj_bats;
	public GameObject Origin;
	//MainCamera and ARCamera;
	public GameObject MCamera, ArCamera;
	//Text
	public Text Txt_action;
	public Text txt_version;
	//Menu
	public GameObject Menu_Bdd;
	private bool isShowing;
	//Check if the destination is selected
	public  bool destination_checked;

	public Button BtnPlan,BtnNaviguer,BtnExit,Btn_Destination;

	// Use this for initialization
	void Start () {
		Txt_action.text = "IndoorNav";
		destination_checked = false;
		obj_bats = GameObject.FindGameObjectsWithTag("bat_elements");
		BtnPlan.onClick.AddListener(()=> Toggle_View_Plan());
		BtnNaviguer.onClick.AddListener (() => Toggle_View_Navigation ());
		BtnExit.onClick.AddListener (() => app_Exit ());
		Btn_Destination.onClick.AddListener(() => Toggle_Menu_Destination ());
	}
	/// <summary>
	/// Function to switch to map
	/// </summary>
	void Toggle_View_Plan(){
		//Show BtnNaviguer button and Hide BtnPlan
		BtnNaviguer.gameObject.SetActive(true);
		BtnPlan.gameObject.SetActive (false);
		Btn_Destination.gameObject.SetActive (false);
		//Change Scale arrow and size 
		   GameObject.Find("arrow_direction").transform.localScale = new Vector3(0.005f,0.005f, 0.002f);

		//Change text action
		Txt_action.text="IndoorNav";
		txt_version.gameObject.SetActive (true);
		//We cannot find the inactive object for that we use MeshRenderer to enable or disable a gameobject on the scene
		Origin.GetComponent<LineRenderer>().alignment=LineAlignment.View;
		Origin.GetComponent<LineRenderer> ().startWidth=1f;
		foreach(GameObject obj in obj_bats){
			obj.GetComponent<MeshRenderer> ().enabled = true;
		
			if (ArCamera.activeSelf){
				ArCamera.SetActive (false);
				MCamera.SetActive (true);
			}
			//disappear menu
			if (Menu_Bdd.activeSelf) {
				Menu_Bdd.SetActive (false);
			}
		}
	}
	/// <summary>
	/// Function to switch to ARCamera
	/// </summary>
	void Toggle_View_Navigation(){
		
		//Hide BtnNaviguer button and show BtnPlan
		BtnNaviguer.gameObject.SetActive(false);
		BtnPlan.gameObject.SetActive (true);
		Btn_Destination.gameObject.SetActive (true);
		txt_version.gameObject.SetActive (false);

		//if the destination is selected
		if (destination_checked==true)
		{
			
			Txt_action.text = "Scaner le marqueur";
			//Change arrow scale arrow

			GameObject.Find("arrow_direction").transform.localScale =  new Vector3(0.001f,0.001f, 0.001f);

			Origin.GetComponent<LineRenderer>().alignment=LineAlignment.Local;
			Origin.GetComponent<LineRenderer> ().startWidth=0f;
			foreach(GameObject obj in obj_bats){
				obj.GetComponent<MeshRenderer> ().enabled = false;
			}
			//MainCamera
			if (MCamera.activeSelf) {
				ArCamera.SetActive (true);
				MCamera.SetActive (false);
			}
			//Firebase Menu appear menu
			if (Menu_Bdd.activeSelf) {
				Menu_Bdd.SetActive (false);
			}
				
		}
		//if the destination is not checked
		if (destination_checked==false) 
		{
			Txt_action.text = "Sélectionner votre destination";
			//Change arrow scale arrow

			GameObject.Find("arrow_direction").transform.localScale =  new Vector3(0.001f,0.001f, 0.001f);

			Origin.GetComponent<LineRenderer>().alignment=LineAlignment.Local;
			Origin.GetComponent<LineRenderer> ().startWidth=0f;
			foreach(GameObject obj in obj_bats){
				obj.GetComponent<MeshRenderer> ().enabled = false;
			}
			//MainCamera
			if (MCamera.activeSelf) {
				ArCamera.SetActive (true);
				MCamera.SetActive (false);
			}
			//Firebase Menu appear menu
			if (!Menu_Bdd.activeSelf) {
				Menu_Bdd.SetActive (true);
			}
		}

	}
	/// <summary>
	/// Destination menu function
	/// </summary>
	/// 
	public void Toggle_Menu_Destination (){
		
		if (!Menu_Bdd.activeSelf) {
			Menu_Bdd.SetActive (true);
		} else {
			Menu_Bdd.SetActive (false);
		} 

	}
	void app_Exit(){
		Application.Quit ();
	}

}
////Change size arrow y
//GameObject.Find("arrow_direction").transform.position= new Vector3 (GameObject.Find("arrow_direction").transform.position.x,GameObject.Find("arrow_direction").transform.position.x+40.0f,GameObject.Find("arrow_direction").transform.position.z);
