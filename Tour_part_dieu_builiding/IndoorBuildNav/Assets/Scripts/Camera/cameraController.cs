﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraController : MonoBehaviour {

	float panSpeed=4f;
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey ("w")) {
			//new Vector3 (0f, 0f, panSpeed * Time.deltaTime);
			transform.Translate(Vector3.forward * panSpeed*Time.deltaTime);
	}
		if (Input.GetKey ("s")) {
			transform.Translate(Vector3.back * panSpeed*Time.deltaTime);

		}
	}
}
