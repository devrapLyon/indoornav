﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.AI;
//using System.Linq;
//using System;
//using Newtonsoft.Json;
//
//public class Find_Second_Floor : MonoBehaviour {
//
//	//Variables declaritions
//
//	static List<float> nodes = new List<float>();
//	public List<Markers.Vumark_Class> markers;
//	//List<int> marker_ltwo =new List<int> {6,7,8,9,10};
//	//Dictionarys 
//	//Dictionary<string, elements_class>IDicUser= new Dictionary<string, elements_class>();
//
//	static int _Level;
//	int VumarkNumber;
//	//Element's class and Vumark's class
//	private Elements element_script;
//	private Markers vumark_class_script;
//	VuMarkHandler vumarkhandler_Script;		
//
//	void Start(){
//		//initialize element and vumark script
//		element_script=GameObject.Find("VuMark").GetComponent<Elements>();
//		vumark_class_script = GameObject.Find ("VuMark").GetComponent<Markers> ();
//		markers=new List<Markers.Vumark_Class>();
//
//		//Find all the vumarks objects in the scene
//		GameObject[] objs = GameObject.FindGameObjectsWithTag("vumark").ToArray();
//		foreach (GameObject go in objs) {
//			//Get vumark level number
//			int marker_level_num = go.GetComponent<vumark_levid> ().vumark_level_num;
//			//Get marker id
//			string marker_id=go.GetComponent<vumark_levid> ().marker_id;
//			//Instance of the vumark objects with different's level 
//			vumark_class_script.vm = new Markers.Vumark_Class (go.transform.position.x, go.transform.position.y, go.transform.position.z,
//				                         go.transform.eulerAngles.x,go.transform.eulerAngles.y,go.transform.eulerAngles.z,
//				                         marker_level_num,marker_id);
//			//Add Vumark object in the list of Vumark_Class
//			markers.Add(vumark_class_script.vm);
//		}//end of foreach
//		foreach(var t in markers){
//			Debug.Log (t.LevelID_markers+" "+t.VuMarkID);
//		}
//		//Access to Vumark object
//		vumarkhandler_Script=GameObject.Find("VuMark").GetComponent<VuMarkHandler>();
//		//Create a destination GameObject 
//		//read all information that is about destination and set all of them to a gameobject
//		Vector3 pos_destination = GameObject.Find ("Destination").transform.position;
//		Vector3 Rot_destination = GameObject.Find ("Destination").transform.eulerAngles;
//		//Instance element's class
//		element_script.element_obj=new Elements.elements_class(pos_destination.x,pos_destination.y,pos_destination.z,Rot_destination.x,
//			Rot_destination.y,Rot_destination.z,2);
//		
//		//Instance VuMark's class
//		//Vumark_Class vumObj = new Vumark_Class (-48f,1f,-4f,1,"11");
//		//Origin get VuMark Position
//		//GameObject.Find ("Origin").transform.position = new Vector3 (vumObj.pos_vumark_x, vumObj.pos_vumark_y, vumObj.pos_vumark_z);
//
//		//Get Marker levelID and set to Origin_Level
//		//this.GetComponent<Origin_scripts>().level=vumObj.LevelID_markers;
//
//
//	}
//	void Update()
//	{	
//		#region vumark_detection
//		if (vumarkhandler_Script.vuMarkId.Length!=0){
//		Markers.Vumark_Class marker_temp=null;
//		//Call when we detect a maker on the scene, then find the Vumark objects in the list of markers 
//		//For example if i scan marker number 1, program will find the Vumark object with this number=>1 at the list.
//		marker_temp=markers.Where(i => i.VuMarkID ==vumarkhandler_Script.vuMarkId).FirstOrDefault();
//
//		//if the prefab are not defined in our program  
//		if (marker_temp==null)
//		{
//			Debug.LogError("You don't define Vumark object with this id in the application");
//
//		}
//		//else set the position and localRotation of marker object to VuMark prefab in the Unity Scene.
//		else 
//		{
//
//			GameObject.Find ("VuMark").transform.position = new Vector3(marker_temp.pos_vumark_x,marker_temp.pos_vumark_y,marker_temp.pos_vumark_z);
//			GameObject.Find ("VuMark").transform.eulerAngles=new Vector3(marker_temp.Rot_vumark_x,marker_temp.Rot_vumark_y,marker_temp.Rot_vumark_z);
//
//			//the origin prefab get marker postion and level
//			this.transform.position = new Vector3(marker_temp.pos_vumark_x,marker_temp.pos_vumark_y,marker_temp.pos_vumark_z);
//			this.GetComponent<Origin_scripts>().level=marker_temp.LevelID_markers;
//			//conditions
//			//if Origin level is equal to elements (Destination) ==same floor
//			if (this.GetComponent<Origin_scripts>().level == element_script.element_obj.LevelID_elements) {
//				//Set element position to a vector 
//				//GameObject.Find("Destination").transform.position = new Vector3 (elObj.pos_element_x,elObj.pos_element_y,elObj.pos_element_z);
//				OnDrawGiz (GameObject.Find("Destination"));
//				Debug.Log ("We are in the same level");
//			}//end if
//			//if Origin level is not equal to elements (Destination) ==another floor 
//			if (this.GetComponent<Origin_scripts>().level != element_script.element_obj.LevelID_elements) {
//				OnDrawGiz (GameObject.Find ("Exit_0"));
//				Debug.Log ("We are in the anther level");
//			}//end if
//
//			}//end else
//
//		}//end first if
//
//		#endregion
//
//	}//end update 
//
//	/// <summary>
//	/// Function to draw the line and arrows
//	/// from Origing position  to destination position
//	/// </summary>
//	/// <param name="obj">Object.</param>
//	void OnDrawGiz(GameObject obj)
//	{
//		//Destroy GameObjects 
//		Transform target = obj.GetComponent<Transform> ();
//		NavMeshAgent nav;
//		LineRenderer line;
//		nav= this.GetComponent<NavMeshAgent>();
//		if( nav == null || nav.path == null )
//			return;
//		line = this.GetComponent<LineRenderer>();
//		line.material = new Material( Shader.Find( "Sprites/Default" ) ) { color = Color.yellow };
//
//		line.startColor=Color.yellow;
//		line.endColor = Color.green;
//
//		nav.SetDestination (target.position);
//		nav.isStopped=true;
//		var path = nav.path;
//		if (path.corners.Length < 2)
//			return ;
//
//		line.positionCount=path.corners.Length ;
//		//line.alignment = LineAlignment.Local;
//		//Draw the line
//		for( int i = 0; i < path.corners.Length; i++ )
//		{
//			line.SetPosition( i, path.corners[i] );
//		}
//
//	}
//	#region methodes
//	//this function is for adding an element to lsit of elements (emplyes, office, hall)
//	//it wil be call when i click to GetMyPos button
//	private void add_elements (){
//		
//		//Create an object from element_class 
//		//Get Vumark level
//		//Calculate the distance between marker and my position
//		//Add position and level maybe orientation to element_Class object.
//		//Add this object to the database 
//	}
//	#endregion
//} 
//
//
