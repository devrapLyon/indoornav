﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Key
struct Key_t2293551073;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Users
struct  Users_t907518852  : public Il2CppObject
{
public:
	// Key Users::<key>k__BackingField
	Key_t2293551073 * ___U3CkeyU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CkeyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Users_t907518852, ___U3CkeyU3Ek__BackingField_0)); }
	inline Key_t2293551073 * get_U3CkeyU3Ek__BackingField_0() const { return ___U3CkeyU3Ek__BackingField_0; }
	inline Key_t2293551073 ** get_address_of_U3CkeyU3Ek__BackingField_0() { return &___U3CkeyU3Ek__BackingField_0; }
	inline void set_U3CkeyU3Ek__BackingField_0(Key_t2293551073 * value)
	{
		___U3CkeyU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CkeyU3Ek__BackingField_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
