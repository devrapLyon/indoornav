﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Office_Class/Salle
struct  Salle_t3888431915  : public Il2CppObject
{
public:
	// System.String Office_Class/Salle::<label>k__BackingField
	String_t* ___U3ClabelU3Ek__BackingField_0;
	// System.Single Office_Class/Salle::<pos_x>k__BackingField
	float ___U3Cpos_xU3Ek__BackingField_1;
	// System.Single Office_Class/Salle::<pos_y>k__BackingField
	float ___U3Cpos_yU3Ek__BackingField_2;
	// System.Single Office_Class/Salle::<pos_z>k__BackingField
	float ___U3Cpos_zU3Ek__BackingField_3;
	// System.String Office_Class/Salle::<office_level>k__BackingField
	String_t* ___U3Coffice_levelU3Ek__BackingField_4;
	// System.String Office_Class/Salle::<Key>k__BackingField
	String_t* ___U3CKeyU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3ClabelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Salle_t3888431915, ___U3ClabelU3Ek__BackingField_0)); }
	inline String_t* get_U3ClabelU3Ek__BackingField_0() const { return ___U3ClabelU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3ClabelU3Ek__BackingField_0() { return &___U3ClabelU3Ek__BackingField_0; }
	inline void set_U3ClabelU3Ek__BackingField_0(String_t* value)
	{
		___U3ClabelU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClabelU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3Cpos_xU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Salle_t3888431915, ___U3Cpos_xU3Ek__BackingField_1)); }
	inline float get_U3Cpos_xU3Ek__BackingField_1() const { return ___U3Cpos_xU3Ek__BackingField_1; }
	inline float* get_address_of_U3Cpos_xU3Ek__BackingField_1() { return &___U3Cpos_xU3Ek__BackingField_1; }
	inline void set_U3Cpos_xU3Ek__BackingField_1(float value)
	{
		___U3Cpos_xU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3Cpos_yU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Salle_t3888431915, ___U3Cpos_yU3Ek__BackingField_2)); }
	inline float get_U3Cpos_yU3Ek__BackingField_2() const { return ___U3Cpos_yU3Ek__BackingField_2; }
	inline float* get_address_of_U3Cpos_yU3Ek__BackingField_2() { return &___U3Cpos_yU3Ek__BackingField_2; }
	inline void set_U3Cpos_yU3Ek__BackingField_2(float value)
	{
		___U3Cpos_yU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3Cpos_zU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Salle_t3888431915, ___U3Cpos_zU3Ek__BackingField_3)); }
	inline float get_U3Cpos_zU3Ek__BackingField_3() const { return ___U3Cpos_zU3Ek__BackingField_3; }
	inline float* get_address_of_U3Cpos_zU3Ek__BackingField_3() { return &___U3Cpos_zU3Ek__BackingField_3; }
	inline void set_U3Cpos_zU3Ek__BackingField_3(float value)
	{
		___U3Cpos_zU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3Coffice_levelU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Salle_t3888431915, ___U3Coffice_levelU3Ek__BackingField_4)); }
	inline String_t* get_U3Coffice_levelU3Ek__BackingField_4() const { return ___U3Coffice_levelU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3Coffice_levelU3Ek__BackingField_4() { return &___U3Coffice_levelU3Ek__BackingField_4; }
	inline void set_U3Coffice_levelU3Ek__BackingField_4(String_t* value)
	{
		___U3Coffice_levelU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3Coffice_levelU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CKeyU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Salle_t3888431915, ___U3CKeyU3Ek__BackingField_5)); }
	inline String_t* get_U3CKeyU3Ek__BackingField_5() const { return ___U3CKeyU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CKeyU3Ek__BackingField_5() { return &___U3CKeyU3Ek__BackingField_5; }
	inline void set_U3CKeyU3Ek__BackingField_5(String_t* value)
	{
		___U3CKeyU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CKeyU3Ek__BackingField_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
