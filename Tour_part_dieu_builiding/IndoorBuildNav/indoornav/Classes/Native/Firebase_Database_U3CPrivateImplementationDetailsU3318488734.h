﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "Firebase_Database_U3CPrivateImplementationDetailsU1473450479.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{6eb14239-c70b-4b0e-9bbc-78fe4bfbc003}
struct  U3CPrivateImplementationDetailsU3EU7B6eb14239U2Dc70bU2D4b0eU2D9bbcU2D78fe4bfbc003U7D_t318488734  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3EU7B6eb14239U2Dc70bU2D4b0eU2D9bbcU2D78fe4bfbc003U7D_t318488734_StaticFields
{
public:
	// <PrivateImplementationDetails>{6eb14239-c70b-4b0e-9bbc-78fe4bfbc003}/$ArrayType=64 <PrivateImplementationDetails>{6eb14239-c70b-4b0e-9bbc-78fe4bfbc003}::$field-2
	U24ArrayTypeU3D64_t1473450479  ___U24fieldU2D2_0;
	// <PrivateImplementationDetails>{6eb14239-c70b-4b0e-9bbc-78fe4bfbc003}/$ArrayType=64 <PrivateImplementationDetails>{6eb14239-c70b-4b0e-9bbc-78fe4bfbc003}::$field-3
	U24ArrayTypeU3D64_t1473450479  ___U24fieldU2D3_1;

public:
	inline static int32_t get_offset_of_U24fieldU2D2_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B6eb14239U2Dc70bU2D4b0eU2D9bbcU2D78fe4bfbc003U7D_t318488734_StaticFields, ___U24fieldU2D2_0)); }
	inline U24ArrayTypeU3D64_t1473450479  get_U24fieldU2D2_0() const { return ___U24fieldU2D2_0; }
	inline U24ArrayTypeU3D64_t1473450479 * get_address_of_U24fieldU2D2_0() { return &___U24fieldU2D2_0; }
	inline void set_U24fieldU2D2_0(U24ArrayTypeU3D64_t1473450479  value)
	{
		___U24fieldU2D2_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D3_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B6eb14239U2Dc70bU2D4b0eU2D9bbcU2D78fe4bfbc003U7D_t318488734_StaticFields, ___U24fieldU2D3_1)); }
	inline U24ArrayTypeU3D64_t1473450479  get_U24fieldU2D3_1() const { return ___U24fieldU2D3_1; }
	inline U24ArrayTypeU3D64_t1473450479 * get_address_of_U24fieldU2D3_1() { return &___U24fieldU2D3_1; }
	inline void set_U24fieldU2D3_1(U24ArrayTypeU3D64_t1473450479  value)
	{
		___U24fieldU2D3_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
