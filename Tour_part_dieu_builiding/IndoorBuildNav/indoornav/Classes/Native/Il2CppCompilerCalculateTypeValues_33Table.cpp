﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Google_Sharpen_Google_Sharpen_ThreadFactory1392637388.h"
#include "Google_Sharpen_Google_Sharpen_Thread1322377586.h"
#include "Google_Sharpen_Google_Sharpen_ThreadGroup2181833315.h"
#include "Google_Sharpen_Google_Sharpen_ScheduledThreadPoolE2537379786.h"
#include "Google_Sharpen_Google_Sharpen_Scheduler2347715885.h"
#include "Google_Sharpen_Google_Sharpen_Scheduler_U3CHasTasks451012389.h"
#include "Google_Sharpen_Google_Sharpen_Runtime423218882.h"
#include "Google_Sharpen_Google_Sharpen_OutputStream3965982961.h"
#include "Google_Sharpen_Google_Sharpen_MessageDigest1820469897.h"
#include "Google_Sharpen_Google_Sharpen_InputStream39831546.h"
#include "Google_Sharpen_Google_Sharpen_Extensions996338116.h"
#include "Google_Sharpen_Google_Sharpen_Executors2516589264.h"
#include "Google_Sharpen_Google_Sharpen_CharacterCodingExcep3867687652.h"
#include "Google_Sharpen_Google_Sharpen_NoSuchElementExceptio335811679.h"
#include "Google_Sharpen_Google_Sharpen_UnsupportedEncodingEx551349759.h"
#include "Google_Sharpen_Google_Sharpen_URISyntaxException3236900932.h"
#include "Google_Sharpen_Google_Sharpen_Collections4125780067.h"
#include "Google_Sharpen_Google_Sharpen_AtomicLong1771203261.h"
#include "Google_Sharpen_Google_Sharpen_AtomicInteger4174655693.h"
#include "Google_Sharpen_Google_Sharpen_AtomicBoolean895648235.h"
#include "Google_Sharpen_Google_Sharpen_Arrays2916779344.h"
#include "Vuforia_UnityExtensions_U3CModuleU3E3783534214.h"
#include "Vuforia_UnityExtensions_Vuforia_ARController2638793709.h"
#include "Vuforia_UnityExtensions_Vuforia_ARController_U3CU32604000414.h"
#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearARCo1398758191.h"
#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearARCo2121820252.h"
#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearARCo3746630162.h"
#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearARCon342269456.h"
#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearARCo2750347603.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearDevice1202635122.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearDevice_EyeID642957731.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearDevice_Eyew1521251591.h"
#include "Vuforia_UnityExtensions_Vuforia_NullHoloLensApiAbs1386933393.h"
#include "Vuforia_UnityExtensions_Vuforia_DeviceTracker2183873360.h"
#include "Vuforia_UnityExtensions_Vuforia_DeviceTrackerARCon3939888793.h"
#include "Vuforia_UnityExtensions_Vuforia_DistortionRenderin3766399464.h"
#include "Vuforia_UnityExtensions_Vuforia_DistortionRenderin2945034146.h"
#include "Vuforia_UnityExtensions_Vuforia_DelegateHelper1202011487.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeEyewearUser117253723.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeEyewearCal3632467967.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeEyewearDev2977282393.h"
#include "Vuforia_UnityExtensions_Vuforia_DedicatedEyewearDevi22891981.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraConfiguratio3904398347.h"
#include "Vuforia_UnityExtensions_Vuforia_BaseCameraConfigurat38459502.h"
#include "Vuforia_UnityExtensions_Vuforia_BaseStereoViewerCa1102239676.h"
#include "Vuforia_UnityExtensions_Vuforia_StereoViewerCamera3365023487.h"
#include "Vuforia_UnityExtensions_Vuforia_HoloLensExtendedTr3502001541.h"
#include "Vuforia_UnityExtensions_Vuforia_HoloLensExtendedTr1161658011.h"
#include "Vuforia_UnityExtensions_Vuforia_HoloLensExtendedTr3432166560.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaExtendedTra2074328369.h"
#include "Vuforia_UnityExtensions_Vuforia_VuMarkManagerImpl1660847547.h"
#include "Vuforia_UnityExtensions_Vuforia_InstanceIdImpl3955455590.h"
#include "Vuforia_UnityExtensions_Vuforia_VuMarkTargetImpl2700679413.h"
#include "Vuforia_UnityExtensions_Vuforia_VuMarkTemplateImpl199901830.h"
#include "Vuforia_UnityExtensions_Vuforia_MixedRealityContro1276557833.h"
#include "Vuforia_UnityExtensions_Vuforia_MixedRealityControll38013191.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalDeviceTr3644694819.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalDeviceTra111727860.h"
#include "Vuforia_UnityExtensions_Vuforia_CustomViewerParamet779886969.h"
#include "Vuforia_UnityExtensions_Vuforia_DeviceTrackingMana2097550852.h"
#include "Vuforia_UnityExtensions_Vuforia_FactorySetter648583075.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearCalibration2025108506.h"
#include "Vuforia_UnityExtensions_Vuforia_BackgroundPlaneAbs3732945727.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearUserCalibra1518014586.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalPlayMode3894463544.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalDeviceTra832065887.h"
#include "Vuforia_UnityExtensions_Vuforia_IOSCamRecoveringHel979448318.h"
#include "Vuforia_UnityExtensions_Vuforia_MeshUtils2110180948.h"
#include "Vuforia_UnityExtensions_Vuforia_ExternalStereoCame4187656756.h"
#include "Vuforia_UnityExtensions_Vuforia_NullHideExcessArea2290611987.h"
#include "Vuforia_UnityExtensions_Vuforia_StencilHideExcessA3377289090.h"
#include "Vuforia_UnityExtensions_Vuforia_LegacyHideExcessAr3475621185.h"
#include "Vuforia_UnityExtensions_Vuforia_DedicatedEyewearCam816511398.h"
#include "Vuforia_UnityExtensions_Vuforia_NullCameraConfigura133234522.h"
#include "Vuforia_UnityExtensions_Vuforia_MonoCameraConfigur3796201132.h"
#include "Vuforia_UnityExtensions_Vuforia_UnityCameraExtensi2392150382.h"
#include "Vuforia_UnityExtensions_Vuforia_View3542740111.h"
#include "Vuforia_UnityExtensions_Vuforia_ViewerParameters1247673784.h"
#include "Vuforia_UnityExtensions_Vuforia_ViewerParametersLi3152440868.h"
#include "Vuforia_UnityExtensions_Vuforia_ObjectTargetAbstra2805337095.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice3827827595.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Camer2705300828.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Focus4087668361.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3300 = { sizeof (ThreadFactory_t1392637388), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3301 = { sizeof (Thread_t1322377586), -1, sizeof(Thread_t1322377586_StaticFields), sizeof(Thread_t1322377586_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable3301[6] = 
{
	Thread_t1322377586_StaticFields::get_offset_of_defaultGroup_0(),
	Thread_t1322377586::get_offset_of_interrupted_1(),
	Thread_t1322377586::get_offset_of_runnable_2(),
	Thread_t1322377586::get_offset_of_tgroup_3(),
	Thread_t1322377586::get_offset_of_thread_4(),
	THREAD_STATIC_FIELD_OFFSET,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3302 = { sizeof (ThreadGroup_t2181833315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3302[1] = 
{
	ThreadGroup_t2181833315::get_offset_of_threads_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3303 = { sizeof (ScheduledThreadPoolExecutor_t2537379786), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3303[1] = 
{
	ScheduledThreadPoolExecutor_t2537379786::get_offset_of_executeExistingDelayedTasksAfterShutdownPolicy_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3304 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3304[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3305 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3306 = { sizeof (Scheduler_t2347715885), -1, sizeof(Scheduler_t2347715885_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3306[4] = 
{
	Scheduler_t2347715885_StaticFields::get_offset_of_Instance_0(),
	Scheduler_t2347715885::get_offset_of_tasks_1(),
	Scheduler_t2347715885::get_offset_of_scheduler_2(),
	Scheduler_t2347715885::get_offset_of_newTask_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3307 = { sizeof (U3CHasTasksU3Ec__AnonStorey0_t451012389), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3307[1] = 
{
	U3CHasTasksU3Ec__AnonStorey0_t451012389::get_offset_of_owner_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3308 = { sizeof (Runtime_t423218882), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3309 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3310 = { sizeof (OutputStream_t3965982961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3310[1] = 
{
	OutputStream_t3965982961::get_offset_of_Wrapped_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3311 = { sizeof (MessageDigest_t1820469897), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3312 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3312[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3313 = { sizeof (InputStream_t39831546), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3313[2] = 
{
	InputStream_t39831546::get_offset_of_mark_0(),
	InputStream_t39831546::get_offset_of_Wrapped_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3314 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3315 = { sizeof (Extensions_t996338116), -1, sizeof(Extensions_t996338116_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3315[2] = 
{
	Extensions_t996338116_StaticFields::get_offset_of_EpochTicks_0(),
	Extensions_t996338116_StaticFields::get_offset_of_Utf8Encoder_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3316 = { sizeof (Executors_t2516589264), -1, sizeof(Executors_t2516589264_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3316[1] = 
{
	Executors_t2516589264_StaticFields::get_offset_of_defaultThreadFactory_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3317 = { sizeof (CharacterCodingException_t3867687652), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3318 = { sizeof (NoSuchElementException_t335811679), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3319 = { sizeof (UnsupportedEncodingException_t551349759), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3320 = { sizeof (URISyntaxException_t3236900932), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3321 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3321[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3322 = { sizeof (Collections_t4125780067), -1, sizeof(Collections_t4125780067_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3322[1] = 
{
	Collections_t4125780067_StaticFields::get_offset_of_empty_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3323 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3324 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3324[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3325 = { sizeof (AtomicLong_t1771203261), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3325[1] = 
{
	AtomicLong_t1771203261::get_offset_of_val_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3326 = { sizeof (AtomicInteger_t4174655693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3326[1] = 
{
	AtomicInteger_t4174655693::get_offset_of_val_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3327 = { sizeof (AtomicBoolean_t895648235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3327[1] = 
{
	AtomicBoolean_t895648235::get_offset_of__actualValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3328 = { sizeof (Arrays_t2916779344), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3329 = { sizeof (U3CModuleU3E_t3783534228), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3330 = { sizeof (ARController_t2638793709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3330[1] = 
{
	ARController_t2638793709::get_offset_of_mVuforiaBehaviour_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3331 = { sizeof (U3CU3Ec__DisplayClass11_0_t2604000414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3331[1] = 
{
	U3CU3Ec__DisplayClass11_0_t2604000414::get_offset_of_controller_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3332 = { sizeof (DigitalEyewearARController_t1398758191), -1, sizeof(DigitalEyewearARController_t1398758191_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3332[28] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	DigitalEyewearARController_t1398758191::get_offset_of_mCameraOffset_7(),
	DigitalEyewearARController_t1398758191::get_offset_of_mDistortionRenderingMode_8(),
	DigitalEyewearARController_t1398758191::get_offset_of_mDistortionRenderingLayer_9(),
	DigitalEyewearARController_t1398758191::get_offset_of_mEyewearType_10(),
	DigitalEyewearARController_t1398758191::get_offset_of_mStereoFramework_11(),
	DigitalEyewearARController_t1398758191::get_offset_of_mSeeThroughConfiguration_12(),
	DigitalEyewearARController_t1398758191::get_offset_of_mViewerName_13(),
	DigitalEyewearARController_t1398758191::get_offset_of_mViewerManufacturer_14(),
	DigitalEyewearARController_t1398758191::get_offset_of_mUseCustomViewer_15(),
	DigitalEyewearARController_t1398758191::get_offset_of_mCustomViewer_16(),
	DigitalEyewearARController_t1398758191::get_offset_of_mCentralAnchorPoint_17(),
	DigitalEyewearARController_t1398758191::get_offset_of_mParentAnchorPoint_18(),
	DigitalEyewearARController_t1398758191::get_offset_of_mPrimaryCamera_19(),
	DigitalEyewearARController_t1398758191::get_offset_of_mPrimaryCameraOriginalRect_20(),
	DigitalEyewearARController_t1398758191::get_offset_of_mSecondaryCamera_21(),
	DigitalEyewearARController_t1398758191::get_offset_of_mSecondaryCameraOriginalRect_22(),
	DigitalEyewearARController_t1398758191::get_offset_of_mSecondaryCameraDisabledLocally_23(),
	DigitalEyewearARController_t1398758191::get_offset_of_mVuforiaBehaviour_24(),
	DigitalEyewearARController_t1398758191::get_offset_of_mDistortionRenderingBhvr_25(),
	DigitalEyewearARController_t1398758191::get_offset_of_mSetFocusPlaneAutomatically_26(),
	DigitalEyewearARController_t1398758191_StaticFields::get_offset_of_mInstance_27(),
	DigitalEyewearARController_t1398758191_StaticFields::get_offset_of_mPadlock_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3333 = { sizeof (EyewearType_t2121820252)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3333[4] = 
{
	EyewearType_t2121820252::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3334 = { sizeof (StereoFramework_t3746630162)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3334[4] = 
{
	StereoFramework_t3746630162::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3335 = { sizeof (SeeThroughConfiguration_t342269456)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3335[3] = 
{
	SeeThroughConfiguration_t342269456::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3336 = { sizeof (SerializableViewerParameters_t2750347603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3336[11] = 
{
	SerializableViewerParameters_t2750347603::get_offset_of_Version_0(),
	SerializableViewerParameters_t2750347603::get_offset_of_Name_1(),
	SerializableViewerParameters_t2750347603::get_offset_of_Manufacturer_2(),
	SerializableViewerParameters_t2750347603::get_offset_of_ButtonType_3(),
	SerializableViewerParameters_t2750347603::get_offset_of_ScreenToLensDistance_4(),
	SerializableViewerParameters_t2750347603::get_offset_of_InterLensDistance_5(),
	SerializableViewerParameters_t2750347603::get_offset_of_TrayAlignment_6(),
	SerializableViewerParameters_t2750347603::get_offset_of_LensCenterToTrayDistance_7(),
	SerializableViewerParameters_t2750347603::get_offset_of_DistortionCoefficients_8(),
	SerializableViewerParameters_t2750347603::get_offset_of_FieldOfView_9(),
	SerializableViewerParameters_t2750347603::get_offset_of_ContainsMagnet_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3337 = { sizeof (EyewearDevice_t1202635122), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3338 = { sizeof (EyeID_t642957731)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3338[4] = 
{
	EyeID_t642957731::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3339 = { sizeof (EyewearCalibrationReading_t1521251591)+ sizeof (Il2CppObject), sizeof(EyewearCalibrationReading_t1521251591_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3339[5] = 
{
	EyewearCalibrationReading_t1521251591::get_offset_of_pose_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EyewearCalibrationReading_t1521251591::get_offset_of_scale_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EyewearCalibrationReading_t1521251591::get_offset_of_centerX_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EyewearCalibrationReading_t1521251591::get_offset_of_centerY_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EyewearCalibrationReading_t1521251591::get_offset_of_unused_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3340 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3341 = { sizeof (NullHoloLensApiAbstraction_t1386933393), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3342 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3343 = { sizeof (DeviceTracker_t2183873360), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3344 = { sizeof (DeviceTrackerARController_t3939888793), -1, sizeof(DeviceTrackerARController_t3939888793_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3344[13] = 
{
	DeviceTrackerARController_t3939888793_StaticFields::get_offset_of_DEFAULT_HEAD_PIVOT_1(),
	DeviceTrackerARController_t3939888793_StaticFields::get_offset_of_DEFAULT_HANDHELD_PIVOT_2(),
	DeviceTrackerARController_t3939888793::get_offset_of_mAutoInitTracker_3(),
	DeviceTrackerARController_t3939888793::get_offset_of_mAutoStartTracker_4(),
	DeviceTrackerARController_t3939888793::get_offset_of_mPosePrediction_5(),
	DeviceTrackerARController_t3939888793::get_offset_of_mModelCorrectionMode_6(),
	DeviceTrackerARController_t3939888793::get_offset_of_mModelTransformEnabled_7(),
	DeviceTrackerARController_t3939888793::get_offset_of_mModelTransform_8(),
	DeviceTrackerARController_t3939888793::get_offset_of_mTrackerStarted_9(),
	DeviceTrackerARController_t3939888793::get_offset_of_mTrackerWasActiveBeforePause_10(),
	DeviceTrackerARController_t3939888793::get_offset_of_mTrackerWasActiveBeforeDisabling_11(),
	DeviceTrackerARController_t3939888793_StaticFields::get_offset_of_mInstance_12(),
	DeviceTrackerARController_t3939888793_StaticFields::get_offset_of_mPadlock_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3345 = { sizeof (DistortionRenderingMode_t3766399464)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3345[4] = 
{
	DistortionRenderingMode_t3766399464::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3346 = { sizeof (DistortionRenderingBehaviour_t2945034146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3346[12] = 
{
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mSingleTexture_2(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mRenderLayer_3(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mOriginalCullingMasks_4(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mStereoCameras_5(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mMeshes_6(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mTextures_7(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mStarted_8(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mVideoBackgroundChanged_9(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mOriginalLeftViewport_10(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mOriginalRightViewport_11(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mDualTextureLeftViewport_12(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mDualTextureRightViewport_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3347 = { sizeof (DelegateHelper_t1202011487), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3348 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3349 = { sizeof (PlayModeEyewearUserCalibratorImpl_t117253723), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3350 = { sizeof (PlayModeEyewearCalibrationProfileManagerImpl_t3632467967), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3351 = { sizeof (PlayModeEyewearDevice_t2977282393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3351[3] = 
{
	PlayModeEyewearDevice_t2977282393::get_offset_of_mProfileManager_1(),
	PlayModeEyewearDevice_t2977282393::get_offset_of_mCalibrator_2(),
	PlayModeEyewearDevice_t2977282393::get_offset_of_mDummyPredictiveTracking_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3352 = { sizeof (DedicatedEyewearDevice_t22891981), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3352[2] = 
{
	DedicatedEyewearDevice_t22891981::get_offset_of_mProfileManager_1(),
	DedicatedEyewearDevice_t22891981::get_offset_of_mCalibrator_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3353 = { sizeof (CameraConfigurationUtility_t3904398347), -1, sizeof(CameraConfigurationUtility_t3904398347_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3353[6] = 
{
	CameraConfigurationUtility_t3904398347_StaticFields::get_offset_of_MIN_CENTER_0(),
	CameraConfigurationUtility_t3904398347_StaticFields::get_offset_of_MAX_CENTER_1(),
	CameraConfigurationUtility_t3904398347_StaticFields::get_offset_of_MAX_BOTTOM_2(),
	CameraConfigurationUtility_t3904398347_StaticFields::get_offset_of_MAX_TOP_3(),
	CameraConfigurationUtility_t3904398347_StaticFields::get_offset_of_MAX_LEFT_4(),
	CameraConfigurationUtility_t3904398347_StaticFields::get_offset_of_MAX_RIGHT_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3354 = { sizeof (BaseCameraConfiguration_t38459502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3354[10] = 
{
	BaseCameraConfiguration_t38459502::get_offset_of_mCameraDeviceMode_0(),
	BaseCameraConfiguration_t38459502::get_offset_of_mLastVideoBackGroundMirroredFromSDK_1(),
	BaseCameraConfiguration_t38459502::get_offset_of_mOnVideoBackgroundConfigChanged_2(),
	BaseCameraConfiguration_t38459502::get_offset_of_mVideoBackgroundBehaviours_3(),
	BaseCameraConfiguration_t38459502::get_offset_of_mVideoBackgroundViewportRect_4(),
	BaseCameraConfiguration_t38459502::get_offset_of_mRenderVideoBackground_5(),
	BaseCameraConfiguration_t38459502::get_offset_of_mProjectionOrientation_6(),
	BaseCameraConfiguration_t38459502::get_offset_of_mInitialReflection_7(),
	BaseCameraConfiguration_t38459502::get_offset_of_mBackgroundPlaneBehaviour_8(),
	BaseCameraConfiguration_t38459502::get_offset_of_mCameraParameterChanged_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3355 = { sizeof (BaseStereoViewerCameraConfiguration_t1102239676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3355[5] = 
{
	BaseStereoViewerCameraConfiguration_t1102239676::get_offset_of_mPrimaryCamera_10(),
	BaseStereoViewerCameraConfiguration_t1102239676::get_offset_of_mSecondaryCamera_11(),
	BaseStereoViewerCameraConfiguration_t1102239676::get_offset_of_mSkewFrustum_12(),
	BaseStereoViewerCameraConfiguration_t1102239676::get_offset_of_mScreenWidth_13(),
	BaseStereoViewerCameraConfiguration_t1102239676::get_offset_of_mScreenHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3356 = { sizeof (StereoViewerCameraConfiguration_t3365023487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3356[7] = 
{
	0,
	StereoViewerCameraConfiguration_t3365023487::get_offset_of_mLastAppliedLeftNearClipPlane_16(),
	StereoViewerCameraConfiguration_t3365023487::get_offset_of_mLastAppliedLeftFarClipPlane_17(),
	StereoViewerCameraConfiguration_t3365023487::get_offset_of_mLastAppliedRightNearClipPlane_18(),
	StereoViewerCameraConfiguration_t3365023487::get_offset_of_mLastAppliedRightFarClipPlane_19(),
	StereoViewerCameraConfiguration_t3365023487::get_offset_of_mCameraOffset_20(),
	StereoViewerCameraConfiguration_t3365023487::get_offset_of_mIsDistorted_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3357 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3358 = { sizeof (HoloLensExtendedTrackingManager_t3502001541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3358[15] = 
{
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mNumFramesStablePose_0(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mMaxPoseRelDistance_1(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mMaxPoseAngleDiff_2(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mMaxCamPoseAbsDistance_3(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mMaxCamPoseAngleDiff_4(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mMinNumFramesPoseOff_5(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mMinPoseUpdateRelDistance_6(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mMinPoseUpdateAngleDiff_7(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mTrackableSizeInViewThreshold_8(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mMaxDistanceFromViewCenterForPoseUpdate_9(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mSetWorldAnchors_10(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mTrackingList_11(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mTrackablesExtendedTrackingEnabled_12(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mTrackablesCurrentlyExtendedTracked_13(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mExtendedTrackablesState_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3359 = { sizeof (PoseInfo_t1161658011)+ sizeof (Il2CppObject), sizeof(PoseInfo_t1161658011 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3359[3] = 
{
	PoseInfo_t1161658011::get_offset_of_Position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PoseInfo_t1161658011::get_offset_of_Rotation_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PoseInfo_t1161658011::get_offset_of_NumFramesPoseWasOff_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3360 = { sizeof (PoseAgeEntry_t3432166560)+ sizeof (Il2CppObject), sizeof(PoseAgeEntry_t3432166560 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3360[3] = 
{
	PoseAgeEntry_t3432166560::get_offset_of_Pose_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PoseAgeEntry_t3432166560::get_offset_of_CameraPose_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PoseAgeEntry_t3432166560::get_offset_of_Age_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3361 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3362 = { sizeof (VuforiaExtendedTrackingManager_t2074328369), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3363 = { sizeof (VuMarkManagerImpl_t1660847547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3363[6] = 
{
	VuMarkManagerImpl_t1660847547::get_offset_of_mBehaviours_0(),
	VuMarkManagerImpl_t1660847547::get_offset_of_mActiveVuMarkTargets_1(),
	VuMarkManagerImpl_t1660847547::get_offset_of_mDestroyedBehaviours_2(),
	VuMarkManagerImpl_t1660847547::get_offset_of_mOnVuMarkDetected_3(),
	VuMarkManagerImpl_t1660847547::get_offset_of_mOnVuMarkLost_4(),
	VuMarkManagerImpl_t1660847547::get_offset_of_mOnVuMarkBehaviourDetected_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3364 = { sizeof (InstanceIdImpl_t3955455590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3364[5] = 
{
	InstanceIdImpl_t3955455590::get_offset_of_mDataType_0(),
	InstanceIdImpl_t3955455590::get_offset_of_mBuffer_1(),
	InstanceIdImpl_t3955455590::get_offset_of_mNumericValue_2(),
	InstanceIdImpl_t3955455590::get_offset_of_mDataLength_3(),
	InstanceIdImpl_t3955455590::get_offset_of_mCachedStringValue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3365 = { sizeof (VuMarkTargetImpl_t2700679413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3365[5] = 
{
	VuMarkTargetImpl_t2700679413::get_offset_of_mVuMarkTemplate_0(),
	VuMarkTargetImpl_t2700679413::get_offset_of_mInstanceId_1(),
	VuMarkTargetImpl_t2700679413::get_offset_of_mTargetId_2(),
	VuMarkTargetImpl_t2700679413::get_offset_of_mInstanceImage_3(),
	VuMarkTargetImpl_t2700679413::get_offset_of_mInstanceImageHeader_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3366 = { sizeof (VuMarkTemplateImpl_t199901830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3366[3] = 
{
	VuMarkTemplateImpl_t199901830::get_offset_of_mUserData_4(),
	VuMarkTemplateImpl_t199901830::get_offset_of_mOrigin_5(),
	VuMarkTemplateImpl_t199901830::get_offset_of_mTrackingFromRuntimeAppearance_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3367 = { sizeof (MixedRealityController_t1276557833), -1, sizeof(MixedRealityController_t1276557833_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3367[13] = 
{
	MixedRealityController_t1276557833_StaticFields::get_offset_of_mInstance_0(),
	MixedRealityController_t1276557833::get_offset_of_mVuforiaBehaviour_1(),
	MixedRealityController_t1276557833::get_offset_of_mDigitalEyewearBehaviour_2(),
	MixedRealityController_t1276557833::get_offset_of_mVideoBackgroundManager_3(),
	MixedRealityController_t1276557833::get_offset_of_mViewerHasBeenSetExternally_4(),
	MixedRealityController_t1276557833::get_offset_of_mViewerParameters_5(),
	MixedRealityController_t1276557833::get_offset_of_mFrameWorkHasBeenSetExternally_6(),
	MixedRealityController_t1276557833::get_offset_of_mStereoFramework_7(),
	MixedRealityController_t1276557833::get_offset_of_mCentralAnchorPoint_8(),
	MixedRealityController_t1276557833::get_offset_of_mLeftCameraOfExternalSDK_9(),
	MixedRealityController_t1276557833::get_offset_of_mRightCameraOfExternalSDK_10(),
	MixedRealityController_t1276557833::get_offset_of_mObjectTrackerStopped_11(),
	MixedRealityController_t1276557833::get_offset_of_mAutoStopCameraIfNotRequired_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3368 = { sizeof (Mode_t38013191)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3368[7] = 
{
	Mode_t38013191::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3369 = { sizeof (RotationalDeviceTracker_t3644694819), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3370 = { sizeof (MODEL_CORRECTION_MODE_t111727860)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3370[4] = 
{
	MODEL_CORRECTION_MODE_t111727860::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3371 = { sizeof (CustomViewerParameters_t779886969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3371[7] = 
{
	CustomViewerParameters_t779886969::get_offset_of_mVersion_1(),
	CustomViewerParameters_t779886969::get_offset_of_mName_2(),
	CustomViewerParameters_t779886969::get_offset_of_mManufacturer_3(),
	CustomViewerParameters_t779886969::get_offset_of_mButtonType_4(),
	CustomViewerParameters_t779886969::get_offset_of_mScreenToLensDistance_5(),
	CustomViewerParameters_t779886969::get_offset_of_mTrayAlignment_6(),
	CustomViewerParameters_t779886969::get_offset_of_mMagnet_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3372 = { sizeof (DeviceTrackingManager_t2097550852), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3372[4] = 
{
	DeviceTrackingManager_t2097550852::get_offset_of_mDeviceTrackerPositonOffset_0(),
	DeviceTrackingManager_t2097550852::get_offset_of_mDeviceTrackerRotationOffset_1(),
	DeviceTrackingManager_t2097550852::get_offset_of_mBeforeDevicePoseUpdated_2(),
	DeviceTrackingManager_t2097550852::get_offset_of_mAfterDevicePoseUpdated_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3373 = { sizeof (FactorySetter_t648583075), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3374 = { sizeof (EyewearCalibrationProfileManagerImpl_t2025108506), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3375 = { sizeof (BackgroundPlaneAbstractBehaviour_t3732945727), -1, sizeof(BackgroundPlaneAbstractBehaviour_t3732945727_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3375[13] = 
{
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mTextureInfo_2(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mViewWidth_3(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mViewHeight_4(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mNumFramesToUpdateVideoBg_5(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mCamera_6(),
	BackgroundPlaneAbstractBehaviour_t3732945727_StaticFields::get_offset_of_maxDisplacement_7(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_defaultNumDivisions_8(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mMesh_9(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mStereoDepth_10(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mBackgroundOffset_11(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mVuforiaBehaviour_12(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mBackgroundPlacedCallback_13(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mNumDivisions_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3376 = { sizeof (EyewearUserCalibratorImpl_t1518014586), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3377 = { sizeof (RotationalPlayModeDeviceTrackerImpl_t3894463544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3377[3] = 
{
	RotationalPlayModeDeviceTrackerImpl_t3894463544::get_offset_of_mRotation_1(),
	RotationalPlayModeDeviceTrackerImpl_t3894463544::get_offset_of_mModelCorrectionTransform_2(),
	RotationalPlayModeDeviceTrackerImpl_t3894463544::get_offset_of_mModelCorrection_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3378 = { sizeof (RotationalDeviceTrackerImpl_t832065887), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3379 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3380 = { sizeof (IOSCamRecoveringHelper_t979448318), -1, sizeof(IOSCamRecoveringHelper_t979448318_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3380[9] = 
{
	0,
	0,
	0,
	IOSCamRecoveringHelper_t979448318_StaticFields::get_offset_of_sHasJustResumed_3(),
	IOSCamRecoveringHelper_t979448318_StaticFields::get_offset_of_sCheckFailedFrameAfterResume_4(),
	IOSCamRecoveringHelper_t979448318_StaticFields::get_offset_of_sCheckedFailedFrameCounter_5(),
	IOSCamRecoveringHelper_t979448318_StaticFields::get_offset_of_sWaitToRecoverCameraRestart_6(),
	IOSCamRecoveringHelper_t979448318_StaticFields::get_offset_of_sWaitedFrameRecoverCounter_7(),
	IOSCamRecoveringHelper_t979448318_StaticFields::get_offset_of_sRecoveryAttemptCounter_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3381 = { sizeof (MeshUtils_t2110180948), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3382 = { sizeof (ExternalStereoCameraConfiguration_t4187656756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3382[21] = 
{
	0,
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedLeftNearClipPlane_16(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedLeftFarClipPlane_17(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedRightNearClipPlane_18(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedRightFarClipPlane_19(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedLeftVerticalVirtualFoV_20(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedLeftHorizontalVirtualFoV_21(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedRightVerticalVirtualFoV_22(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedRightHorizontalVirtualFoV_23(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedLeftProjection_24(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedRightProjection_25(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewLeftNearClipPlane_26(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewLeftFarClipPlane_27(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewRightNearClipPlane_28(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewRightFarClipPlane_29(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewLeftVerticalVirtualFoV_30(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewLeftHorizontalVirtualFoV_31(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewRightVerticalVirtualFoV_32(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewRightHorizontalVirtualFoV_33(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mExternallySetLeftMatrix_34(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mExternallySetRightMatrix_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3383 = { sizeof (NullHideExcessAreaClipping_t2290611987), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3384 = { sizeof (StencilHideExcessAreaClipping_t3377289090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3384[13] = 
{
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mGameObject_0(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mMatteShader_1(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mClippingPlane_2(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mCamera_3(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mCameraNearPlane_4(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mCameraFarPlane_5(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mCameraPixelRect_6(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mCameraFieldOfView_7(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mVuforiaBehaviour_8(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mPlanesActivated_9(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mBgPlane_10(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mBgPlaneLocalPos_11(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mBgPlaneLocalScale_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3385 = { sizeof (LegacyHideExcessAreaClipping_t3475621185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3385[21] = 
{
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mGameObject_0(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mMatteShader_1(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mBgPlane_2(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mLeftPlane_3(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mRightPlane_4(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mTopPlane_5(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mBottomPlane_6(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mCamera_7(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mBgPlaneLocalPos_8(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mBgPlaneLocalScale_9(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mCameraNearPlane_10(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mCameraPixelRect_11(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mCameraFieldOFView_12(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mVuforiaBehaviour_13(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mHideBehaviours_14(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mDeactivatedHideBehaviours_15(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mPlanesActivated_16(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mLeftPlaneCachedScale_17(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mRightPlaneCachedScale_18(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mBottomPlaneCachedScale_19(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mTopPlaneCachedScale_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3386 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3387 = { sizeof (DedicatedEyewearCameraConfiguration_t816511398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3387[13] = 
{
	0,
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mPrimaryCamera_11(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mSecondaryCamera_12(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mScreenWidth_13(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mScreenHeight_14(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mNeedToCheckStereo_15(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mLastAppliedNearClipPlane_16(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mLastAppliedFarClipPlane_17(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mLastAppliedVirtualFoV_18(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mNewNearClipPlane_19(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mNewFarClipPlane_20(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mNewVirtualFoV_21(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mEyewearDevice_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3388 = { sizeof (NullCameraConfiguration_t133234522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3388[1] = 
{
	NullCameraConfiguration_t133234522::get_offset_of_mProjectionOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3389 = { sizeof (MonoCameraConfiguration_t3796201132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3389[7] = 
{
	0,
	MonoCameraConfiguration_t3796201132::get_offset_of_mPrimaryCamera_11(),
	MonoCameraConfiguration_t3796201132::get_offset_of_mCameraViewPortWidth_12(),
	MonoCameraConfiguration_t3796201132::get_offset_of_mCameraViewPortHeight_13(),
	MonoCameraConfiguration_t3796201132::get_offset_of_mLastAppliedNearClipPlane_14(),
	MonoCameraConfiguration_t3796201132::get_offset_of_mLastAppliedFarClipPlane_15(),
	MonoCameraConfiguration_t3796201132::get_offset_of_mLastAppliedFoV_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3390 = { sizeof (UnityCameraExtensions_t2392150382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3390[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3391 = { sizeof (View_t3542740111)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3391[6] = 
{
	View_t3542740111::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3392 = { sizeof (ViewerParameters_t1247673784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3392[1] = 
{
	ViewerParameters_t1247673784::get_offset_of_mNativeVP_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3393 = { sizeof (ViewerParametersList_t3152440868), -1, sizeof(ViewerParametersList_t3152440868_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3393[2] = 
{
	ViewerParametersList_t3152440868::get_offset_of_mNativeVPL_0(),
	ViewerParametersList_t3152440868_StaticFields::get_offset_of_mListForAuthoringTools_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3394 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3395 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3396 = { sizeof (ObjectTargetAbstractBehaviour_t2805337095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3396[12] = 
{
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mObjectTarget_20(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mAspectRatioXY_21(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mAspectRatioXZ_22(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mShowBoundingBox_23(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mBBoxMin_24(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mBBoxMax_25(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mPreviewImage_26(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mLength_27(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mWidth_28(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mHeight_29(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mLastTransformScale_30(),
	ObjectTargetAbstractBehaviour_t2805337095::get_offset_of_mLastSize_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3397 = { sizeof (CameraDevice_t3827827595), -1, sizeof(CameraDevice_t3827827595_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3397[1] = 
{
	CameraDevice_t3827827595_StaticFields::get_offset_of_mInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3398 = { sizeof (CameraDeviceMode_t2705300828)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3398[4] = 
{
	CameraDeviceMode_t2705300828::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3399 = { sizeof (FocusMode_t4087668361)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3399[6] = 
{
	FocusMode_t4087668361::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
