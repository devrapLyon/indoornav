﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// User_Class/User
struct User_t3842584798;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// User_Class
struct  User_Class_t3086874568  : public MonoBehaviour_t1158329972
{
public:
	// User_Class/User User_Class::NewUser
	User_t3842584798 * ___NewUser_2;

public:
	inline static int32_t get_offset_of_NewUser_2() { return static_cast<int32_t>(offsetof(User_Class_t3086874568, ___NewUser_2)); }
	inline User_t3842584798 * get_NewUser_2() const { return ___NewUser_2; }
	inline User_t3842584798 ** get_address_of_NewUser_2() { return &___NewUser_2; }
	inline void set_NewUser_2(User_t3842584798 * value)
	{
		___NewUser_2 = value;
		Il2CppCodeGenWriteBarrier(&___NewUser_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
