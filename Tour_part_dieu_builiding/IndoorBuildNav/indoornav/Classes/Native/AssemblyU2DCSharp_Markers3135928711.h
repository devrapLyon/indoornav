﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Markers/Vumark_Class
struct Vumark_Class_t29598747;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Markers
struct  Markers_t3135928711  : public MonoBehaviour_t1158329972
{
public:
	// Markers/Vumark_Class Markers::vm
	Vumark_Class_t29598747 * ___vm_2;

public:
	inline static int32_t get_offset_of_vm_2() { return static_cast<int32_t>(offsetof(Markers_t3135928711, ___vm_2)); }
	inline Vumark_Class_t29598747 * get_vm_2() const { return ___vm_2; }
	inline Vumark_Class_t29598747 ** get_address_of_vm_2() { return &___vm_2; }
	inline void set_vm_2(Vumark_Class_t29598747 * value)
	{
		___vm_2 = value;
		Il2CppCodeGenWriteBarrier(&___vm_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
