﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Firebase.FirebaseApp
struct FirebaseApp_t210707726;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// System.Collections.Generic.Dictionary`2<System.String,User_Class/User>
struct Dictionary_2_t1462396764;
// System.Collections.Generic.Dictionary`2<System.String,Office_Class/Salle>
struct Dictionary_2_t1508243881;
// User_Class
struct User_Class_t3086874568;
// Office_Class
struct Office_Class_t2060087375;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,User_Class/User>
struct ValueCollection_t165456607;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Office_Class/Salle>
struct ValueCollection_t211303724;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonListControl
struct  ButtonListControl_t933417769  : public MonoBehaviour_t1158329972
{
public:
	// Firebase.FirebaseApp ButtonListControl::app
	FirebaseApp_t210707726 * ___app_2;
	// UnityEngine.GameObject ButtonListControl::BtnTemplate
	GameObject_t1756533147 * ___BtnTemplate_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ButtonListControl::buttons
	List_1_t1125654279 * ___buttons_4;
	// System.Collections.Generic.Dictionary`2<System.String,User_Class/User> ButtonListControl::IDiscUser
	Dictionary_2_t1462396764 * ___IDiscUser_5;
	// System.Collections.Generic.Dictionary`2<System.String,Office_Class/Salle> ButtonListControl::IDiscOffice
	Dictionary_2_t1508243881 * ___IDiscOffice_6;
	// User_Class ButtonListControl::UserScript
	User_Class_t3086874568 * ___UserScript_7;
	// Office_Class ButtonListControl::OfficeScript
	Office_Class_t2060087375 * ___OfficeScript_8;
	// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,User_Class/User> ButtonListControl::valueUserColl
	ValueCollection_t165456607 * ___valueUserColl_9;
	// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Office_Class/Salle> ButtonListControl::valueOfficeColl
	ValueCollection_t211303724 * ___valueOfficeColl_10;

public:
	inline static int32_t get_offset_of_app_2() { return static_cast<int32_t>(offsetof(ButtonListControl_t933417769, ___app_2)); }
	inline FirebaseApp_t210707726 * get_app_2() const { return ___app_2; }
	inline FirebaseApp_t210707726 ** get_address_of_app_2() { return &___app_2; }
	inline void set_app_2(FirebaseApp_t210707726 * value)
	{
		___app_2 = value;
		Il2CppCodeGenWriteBarrier(&___app_2, value);
	}

	inline static int32_t get_offset_of_BtnTemplate_3() { return static_cast<int32_t>(offsetof(ButtonListControl_t933417769, ___BtnTemplate_3)); }
	inline GameObject_t1756533147 * get_BtnTemplate_3() const { return ___BtnTemplate_3; }
	inline GameObject_t1756533147 ** get_address_of_BtnTemplate_3() { return &___BtnTemplate_3; }
	inline void set_BtnTemplate_3(GameObject_t1756533147 * value)
	{
		___BtnTemplate_3 = value;
		Il2CppCodeGenWriteBarrier(&___BtnTemplate_3, value);
	}

	inline static int32_t get_offset_of_buttons_4() { return static_cast<int32_t>(offsetof(ButtonListControl_t933417769, ___buttons_4)); }
	inline List_1_t1125654279 * get_buttons_4() const { return ___buttons_4; }
	inline List_1_t1125654279 ** get_address_of_buttons_4() { return &___buttons_4; }
	inline void set_buttons_4(List_1_t1125654279 * value)
	{
		___buttons_4 = value;
		Il2CppCodeGenWriteBarrier(&___buttons_4, value);
	}

	inline static int32_t get_offset_of_IDiscUser_5() { return static_cast<int32_t>(offsetof(ButtonListControl_t933417769, ___IDiscUser_5)); }
	inline Dictionary_2_t1462396764 * get_IDiscUser_5() const { return ___IDiscUser_5; }
	inline Dictionary_2_t1462396764 ** get_address_of_IDiscUser_5() { return &___IDiscUser_5; }
	inline void set_IDiscUser_5(Dictionary_2_t1462396764 * value)
	{
		___IDiscUser_5 = value;
		Il2CppCodeGenWriteBarrier(&___IDiscUser_5, value);
	}

	inline static int32_t get_offset_of_IDiscOffice_6() { return static_cast<int32_t>(offsetof(ButtonListControl_t933417769, ___IDiscOffice_6)); }
	inline Dictionary_2_t1508243881 * get_IDiscOffice_6() const { return ___IDiscOffice_6; }
	inline Dictionary_2_t1508243881 ** get_address_of_IDiscOffice_6() { return &___IDiscOffice_6; }
	inline void set_IDiscOffice_6(Dictionary_2_t1508243881 * value)
	{
		___IDiscOffice_6 = value;
		Il2CppCodeGenWriteBarrier(&___IDiscOffice_6, value);
	}

	inline static int32_t get_offset_of_UserScript_7() { return static_cast<int32_t>(offsetof(ButtonListControl_t933417769, ___UserScript_7)); }
	inline User_Class_t3086874568 * get_UserScript_7() const { return ___UserScript_7; }
	inline User_Class_t3086874568 ** get_address_of_UserScript_7() { return &___UserScript_7; }
	inline void set_UserScript_7(User_Class_t3086874568 * value)
	{
		___UserScript_7 = value;
		Il2CppCodeGenWriteBarrier(&___UserScript_7, value);
	}

	inline static int32_t get_offset_of_OfficeScript_8() { return static_cast<int32_t>(offsetof(ButtonListControl_t933417769, ___OfficeScript_8)); }
	inline Office_Class_t2060087375 * get_OfficeScript_8() const { return ___OfficeScript_8; }
	inline Office_Class_t2060087375 ** get_address_of_OfficeScript_8() { return &___OfficeScript_8; }
	inline void set_OfficeScript_8(Office_Class_t2060087375 * value)
	{
		___OfficeScript_8 = value;
		Il2CppCodeGenWriteBarrier(&___OfficeScript_8, value);
	}

	inline static int32_t get_offset_of_valueUserColl_9() { return static_cast<int32_t>(offsetof(ButtonListControl_t933417769, ___valueUserColl_9)); }
	inline ValueCollection_t165456607 * get_valueUserColl_9() const { return ___valueUserColl_9; }
	inline ValueCollection_t165456607 ** get_address_of_valueUserColl_9() { return &___valueUserColl_9; }
	inline void set_valueUserColl_9(ValueCollection_t165456607 * value)
	{
		___valueUserColl_9 = value;
		Il2CppCodeGenWriteBarrier(&___valueUserColl_9, value);
	}

	inline static int32_t get_offset_of_valueOfficeColl_10() { return static_cast<int32_t>(offsetof(ButtonListControl_t933417769, ___valueOfficeColl_10)); }
	inline ValueCollection_t211303724 * get_valueOfficeColl_10() const { return ___valueOfficeColl_10; }
	inline ValueCollection_t211303724 ** get_address_of_valueOfficeColl_10() { return &___valueOfficeColl_10; }
	inline void set_valueOfficeColl_10(ValueCollection_t211303724 * value)
	{
		___valueOfficeColl_10 = value;
		Il2CppCodeGenWriteBarrier(&___valueOfficeColl_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
