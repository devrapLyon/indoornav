﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2537511179.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_V377558711.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_V798282663.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_V335720575.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_V983378685.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3676452127.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3891512348.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3957486809.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_4238726192.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1740384518.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_4046762215.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_4117501338.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1410970950.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_4273066827.h"
#include "Firebase_Database_Firebase_Database_Internal_Loggi2060490269.h"
#include "Firebase_Database_Firebase_Database_Internal_Loggin438307305.h"
#include "Firebase_Database_Firebase_Database_Internal_Snapsh489078478.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps1197802383.h"
#include "Firebase_Database_Firebase_Database_Internal_Snapsh767528674.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps1490646017.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps2321959569.h"
#include "Firebase_Database_Firebase_Database_Internal_Snapsh932129784.h"
#include "Firebase_Database_Firebase_Database_Internal_Snapsh394951299.h"
#include "Firebase_Database_Firebase_Database_Internal_Snapsh794407394.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps3585126227.h"
#include "Firebase_Database_Firebase_Database_Internal_Snapsh202416347.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps1995558443.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps1476446587.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps3730276818.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps4188338151.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps3473034241.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps2425062456.h"
#include "Firebase_Database_Firebase_Database_Internal_Snapsh376444473.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps3823268051.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps3399591604.h"
#include "Firebase_Database_Firebase_Database_Internal_Snapsh373297280.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps1794667560.h"
#include "Firebase_Database_Firebase_Database_Internal_Snapsh787885785.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps2640059010.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps1041335204.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps1691052188.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps4219187984.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps3332049502.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps4202017214.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps2434126241.h"
#include "Firebase_Database_Firebase_Database_Internal_Snapsh813221359.h"
#include "Firebase_Database_Firebase_Database_Internal_TubeSo243344344.h"
#include "Firebase_Database_Firebase_Database_Internal_TubeSo394104996.h"
#include "Firebase_Database_Firebase_Database_Internal_TubeS2890779574.h"
#include "Firebase_Database_Firebase_Database_Internal_TubeS2930208447.h"
#include "Firebase_Database_Firebase_Database_Internal_TubeS1191325154.h"
#include "Firebase_Database_Firebase_Database_Internal_TubeSo453589537.h"
#include "Firebase_Database_Firebase_Database_Internal_TubeSo517291204.h"
#include "Firebase_Database_Firebase_Database_Internal_TubeSo780773228.h"
#include "Firebase_Database_Firebase_Database_Internal_TubeSoc11059436.h"
#include "Firebase_Database_Firebase_Database_Internal_TubeS2830312828.h"
#include "Firebase_Database_Firebase_Database_Internal_TubeSo719632682.h"
#include "Firebase_Database_Firebase_Database_Internal_TubeS3581815132.h"
#include "Firebase_Database_Firebase_Database_Internal_TubeS4002296479.h"
#include "Firebase_Database_Firebase_Database_Internal_Util_J120734841.h"
#include "Firebase_Database_Firebase_Database_Internal_Utili2696542967.h"
#include "Firebase_Database_Firebase_Database_Internal_Utili1056641960.h"
#include "Firebase_Database_Firebase_Database_Internal_Utilit699088793.h"
#include "Firebase_Database_Firebase_Database_Internal_Utili4243835735.h"
#include "Firebase_Database_Firebase_Database_Internal_Utili3954903604.h"
#include "Firebase_Database_Firebase_Database_Internal_Utili2619975485.h"
#include "Firebase_Database_Firebase_Database_Internal_Utili3991894683.h"
#include "Firebase_Database_Firebase_Database_Internal_Utili1255537880.h"
#include "Firebase_Database_Firebase_Database_Internal_Utili2243316744.h"
#include "Firebase_Database_Firebase_Database_Internal_Utili1722631061.h"
#include "Firebase_Database_Firebase_Database_Core_AuthToken3681374264.h"
#include "Firebase_Database_Firebase_Database_Core_DatabaseP1459671864.h"
#include "Firebase_Database_Firebase_Database_Logger225270238.h"
#include "Firebase_Database_Firebase_Database_Logger_Level2798387899.h"
#include "Firebase_Database_Firebase_Database_DotNet_DotNetP2951135975.h"
#include "Firebase_Database_Firebase_Database_DotNet_DotNetPl601611002.h"
#include "Firebase_Database_Firebase_Database_DotNet_DotNetP1624051538.h"
#include "Firebase_Database_Firebase_Database_DotNet_DotNetP4147568072.h"
#include "Firebase_Database_Firebase_Database_FirebaseDataba1318758358.h"
#include "Firebase_Database_Firebase_Database_Internal_Base64124663375.h"
#include "Firebase_Database_Firebase_Database_Internal_Base64255634182.h"
#include "Firebase_Database_Firebase_Database_Internal_Base63204051639.h"
#include "Firebase_Database_U3CPrivateImplementationDetailsU3318488734.h"
#include "Firebase_Database_U3CPrivateImplementationDetailsU1473450479.h"
#include "UnityEngine_UI_U3CModuleU3E3783534214.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventHandle942672932.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3800 = { sizeof (ViewFrom_t2537511179)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3800[3] = 
{
	ViewFrom_t2537511179::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3801 = { sizeof (QuerySpec_t377558711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3801[2] = 
{
	QuerySpec_t377558711::get_offset_of__params_0(),
	QuerySpec_t377558711::get_offset_of__path_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3802 = { sizeof (View_t798282663), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3802[5] = 
{
	View_t798282663::get_offset_of__eventGenerator_0(),
	View_t798282663::get_offset_of__eventRegistrations_1(),
	View_t798282663::get_offset_of__processor_2(),
	View_t798282663::get_offset_of__query_3(),
	View_t798282663::get_offset_of__viewCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3803 = { sizeof (OperationResult_t335720575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3803[2] = 
{
	OperationResult_t335720575::get_offset_of_Changes_0(),
	OperationResult_t335720575::get_offset_of_Events_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3804 = { sizeof (ViewCache_t983378685), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3804[2] = 
{
	ViewCache_t983378685::get_offset_of__eventSnap_0(),
	ViewCache_t983378685::get_offset_of__serverSnap_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3805 = { sizeof (ViewProcessor_t3676452127), -1, sizeof(ViewProcessor_t3676452127_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3805[2] = 
{
	ViewProcessor_t3676452127_StaticFields::get_offset_of_NoCompleteSource_0(),
	ViewProcessor_t3676452127::get_offset_of__filter_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3806 = { sizeof (ProcessorResult_t3891512348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3806[2] = 
{
	ProcessorResult_t3891512348::get_offset_of_Changes_0(),
	ProcessorResult_t3891512348::get_offset_of_ViewCache_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3807 = { sizeof (CompleteChildSource431_t3957486809), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3808 = { sizeof (WriteTreeCompleteChildSource_t4238726192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3808[3] = 
{
	WriteTreeCompleteChildSource_t4238726192::get_offset_of__optCompleteServerCache_0(),
	WriteTreeCompleteChildSource_t4238726192::get_offset_of__viewCache_1(),
	WriteTreeCompleteChildSource_t4238726192::get_offset_of__writes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3809 = { sizeof (ChildChangeAccumulator_t1740384518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3809[1] = 
{
	ChildChangeAccumulator_t1740384518::get_offset_of__changeMap_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3810 = { sizeof (IndexedFilter_t4046762215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3810[1] = 
{
	IndexedFilter_t4046762215::get_offset_of__index_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3811 = { sizeof (LimitedFilter_t4117501338), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3811[4] = 
{
	LimitedFilter_t4117501338::get_offset_of__index_0(),
	LimitedFilter_t4117501338::get_offset_of__limit_1(),
	LimitedFilter_t4117501338::get_offset_of__rangedFilter_2(),
	LimitedFilter_t4117501338::get_offset_of__reverse_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3812 = { sizeof (NodeFilter_t1410970950), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3813 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3814 = { sizeof (RangedFilter_t4273066827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3814[4] = 
{
	RangedFilter_t4273066827::get_offset_of__endPost_0(),
	RangedFilter_t4273066827::get_offset_of__index_1(),
	RangedFilter_t4273066827::get_offset_of__indexedFilter_2(),
	RangedFilter_t4273066827::get_offset_of__startPost_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3815 = { sizeof (DefaultLogger_t2060490269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3815[3] = 
{
	DefaultLogger_t2060490269::get_offset_of__enabledComponents_0(),
	DefaultLogger_t2060490269::get_offset_of__minLevel_1(),
	DefaultLogger_t2060490269::get_offset_of__app_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3816 = { sizeof (LogWrapper_t438307305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3816[3] = 
{
	LogWrapper_t438307305::get_offset_of__component_0(),
	LogWrapper_t438307305::get_offset_of__logger_1(),
	LogWrapper_t438307305::get_offset_of__prefix_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3817 = { sizeof (BooleanNode_t489078478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3817[1] = 
{
	BooleanNode_t489078478::get_offset_of__value_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3818 = { sizeof (ChildKey_t1197802383), -1, sizeof(ChildKey_t1197802383_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3818[5] = 
{
	ChildKey_t1197802383_StaticFields::get_offset_of_MinKey_0(),
	ChildKey_t1197802383_StaticFields::get_offset_of_MaxKey_1(),
	ChildKey_t1197802383_StaticFields::get_offset_of_PriorityChildKey_2(),
	ChildKey_t1197802383_StaticFields::get_offset_of_InfoChildKey_3(),
	ChildKey_t1197802383::get_offset_of__key_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3819 = { sizeof (IntegerChildKey_t767528674), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3819[1] = 
{
	IntegerChildKey_t767528674::get_offset_of__mintValue_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3820 = { sizeof (ChildrenNode_t1490646017), -1, sizeof(ChildrenNode_t1490646017_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3820[4] = 
{
	ChildrenNode_t1490646017_StaticFields::get_offset_of_NameOnlyComparator_1(),
	ChildrenNode_t1490646017::get_offset_of__children_2(),
	ChildrenNode_t1490646017::get_offset_of__priority_3(),
	ChildrenNode_t1490646017::get_offset_of__lazyHash_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3821 = { sizeof (Comparator17_t2321959569), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3822 = { sizeof (NamedNodeIterator_t932129784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3822[1] = 
{
	NamedNodeIterator_t932129784::get_offset_of__iterator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3823 = { sizeof (ChildVisitor_t394951299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3824 = { sizeof (NodeVisitor245_t794407394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3824[3] = 
{
	NodeVisitor245_t794407394::get_offset_of__enclosing_0(),
	NodeVisitor245_t794407394::get_offset_of__visitor_1(),
	NodeVisitor245_t794407394::get_offset_of__passedPriorityKey_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3825 = { sizeof (CompoundHash_t3585126227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3825[2] = 
{
	CompoundHash_t3585126227::get_offset_of__hashes_0(),
	CompoundHash_t3585126227::get_offset_of__posts_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3826 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3827 = { sizeof (SimpleSizeSplitStrategy_t202416347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3827[1] = 
{
	SimpleSizeSplitStrategy_t202416347::get_offset_of__splitThreshold_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3828 = { sizeof (CompoundHashBuilder_t1995558443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3828[8] = 
{
	CompoundHashBuilder_t1995558443::get_offset_of_CurrentHashes_0(),
	CompoundHashBuilder_t1995558443::get_offset_of_CurrentPaths_1(),
	CompoundHashBuilder_t1995558443::get_offset_of_SplitStrategy_2(),
	CompoundHashBuilder_t1995558443::get_offset_of_CurrentPathValue_3(),
	CompoundHashBuilder_t1995558443::get_offset_of_CurrentPathDepth_4(),
	CompoundHashBuilder_t1995558443::get_offset_of_LastLeafDepth_5(),
	CompoundHashBuilder_t1995558443::get_offset_of_NeedsComma_6(),
	CompoundHashBuilder_t1995558443::get_offset_of_OptHashValueBuilder_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3829 = { sizeof (ChildVisitor206_t1476446587), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3829[1] = 
{
	ChildVisitor206_t1476446587::get_offset_of__state_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3830 = { sizeof (DeferredValueNode_t3730276818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3830[1] = 
{
	DeferredValueNode_t3730276818::get_offset_of__value_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3831 = { sizeof (DoubleNode_t4188338151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3831[1] = 
{
	DoubleNode_t4188338151::get_offset_of__value_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3832 = { sizeof (EmptyNode_t3473034241), -1, sizeof(EmptyNode_t3473034241_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3832[1] = 
{
	EmptyNode_t3473034241_StaticFields::get_offset_of_EmptyNodeValue_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3833 = { sizeof (Index_t2425062456), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3834 = { sizeof (IndexedNode_t376444473), -1, sizeof(IndexedNode_t376444473_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3834[4] = 
{
	IndexedNode_t376444473_StaticFields::get_offset_of_FallbackIndex_0(),
	IndexedNode_t376444473::get_offset_of__index_1(),
	IndexedNode_t376444473::get_offset_of__node_2(),
	IndexedNode_t376444473::get_offset_of__indexed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3835 = { sizeof (KeyIndex_t3823268051), -1, sizeof(KeyIndex_t3823268051_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3835[1] = 
{
	KeyIndex_t3823268051_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3836 = { sizeof (LeafNode_t3399591604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3836[2] = 
{
	LeafNode_t3399591604::get_offset_of_Priority_1(),
	LeafNode_t3399591604::get_offset_of__lazyHash_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3837 = { sizeof (LeafType_t373297280)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3837[5] = 
{
	LeafType_t373297280::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3838 = { sizeof (LongNode_t1794667560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3838[1] = 
{
	LongNode_t1794667560::get_offset_of__value_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3839 = { sizeof (NamedNode_t787885785), -1, sizeof(NamedNode_t787885785_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3839[4] = 
{
	NamedNode_t787885785_StaticFields::get_offset_of_MinNode_0(),
	NamedNode_t787885785_StaticFields::get_offset_of_MaxNode_1(),
	NamedNode_t787885785::get_offset_of__name_2(),
	NamedNode_t787885785::get_offset_of__node_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3840 = { sizeof (Node_t2640059010), -1, sizeof(Node_t2640059010_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3840[1] = 
{
	Node_t2640059010_StaticFields::get_offset_of_MaxNode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3841 = { sizeof (HashVersion_t1041335204)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3841[3] = 
{
	HashVersion_t1041335204::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3842 = { sizeof (ChildrenNode56_t1691052188), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3843 = { sizeof (NodeUtilities_t4219187984), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3844 = { sizeof (PriorityIndex_t3332049502), -1, sizeof(PriorityIndex_t3332049502_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3844[1] = 
{
	PriorityIndex_t3332049502_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3845 = { sizeof (PriorityUtilities_t4202017214), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3846 = { sizeof (RangeMerge_t2434126241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3846[3] = 
{
	RangeMerge_t2434126241::get_offset_of__optExclusiveStart_0(),
	RangeMerge_t2434126241::get_offset_of__optInclusiveEnd_1(),
	RangeMerge_t2434126241::get_offset_of__snap_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3847 = { sizeof (StringNode_t813221359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3847[1] = 
{
	StringNode_t813221359::get_offset_of__value_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3848 = { sizeof (MessageBuilderFactory_t243344344), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3849 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3850 = { sizeof (BinaryBuilder_t394104996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3850[2] = 
{
	BinaryBuilder_t394104996::get_offset_of__pendingBytes_0(),
	BinaryBuilder_t394104996::get_offset_of__pendingByteCount_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3851 = { sizeof (TextBuilder_t2890779574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3851[1] = 
{
	TextBuilder_t2890779574::get_offset_of__builder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3852 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3853 = { sizeof (WebSocket_t2930208447), -1, sizeof(WebSocket_t2930208447_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3853[14] = 
{
	WebSocket_t2930208447_StaticFields::get_offset_of_ClientCount_0(),
	WebSocket_t2930208447_StaticFields::get_offset_of__threadFactory_1(),
	WebSocket_t2930208447_StaticFields::get_offset_of__intializer_2(),
	WebSocket_t2930208447::get_offset_of__clientId_3(),
	WebSocket_t2930208447::get_offset_of__handshake_4(),
	WebSocket_t2930208447::get_offset_of__innerThread_5(),
	WebSocket_t2930208447::get_offset_of__logger_6(),
	WebSocket_t2930208447::get_offset_of__receiver_7(),
	WebSocket_t2930208447::get_offset_of__sync_8(),
	WebSocket_t2930208447::get_offset_of__url_9(),
	WebSocket_t2930208447::get_offset_of__writer_10(),
	WebSocket_t2930208447::get_offset_of__eventHandler_11(),
	WebSocket_t2930208447::get_offset_of__socket_12(),
	WebSocket_t2930208447::get_offset_of__state_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3854 = { sizeof (State_t1191325154)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3854[6] = 
{
	State_t1191325154::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3855 = { sizeof (ThreadInitializer55_t453589537), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3856 = { sizeof (Runnable101_t517291204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3856[1] = 
{
	Runnable101_t517291204::get_offset_of__enclosing_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3857 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3858 = { sizeof (WebSocketException_t780773228), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3859 = { sizeof (WebSocketHandshake_t11059436), -1, sizeof(WebSocketHandshake_t11059436_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3859[5] = 
{
	WebSocketHandshake_t11059436_StaticFields::get_offset_of_Random_0(),
	WebSocketHandshake_t11059436::get_offset_of__extraHeaders_1(),
	WebSocketHandshake_t11059436::get_offset_of__nonce_2(),
	WebSocketHandshake_t11059436::get_offset_of__protocol_3(),
	WebSocketHandshake_t11059436::get_offset_of__url_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3860 = { sizeof (WebSocketMessage_t2830312828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3860[3] = 
{
	WebSocketMessage_t2830312828::get_offset_of__byteMessage_0(),
	WebSocketMessage_t2830312828::get_offset_of__opcode_1(),
	WebSocketMessage_t2830312828::get_offset_of__stringMessage_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3861 = { sizeof (WebSocketReceiver_t719632682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3861[6] = 
{
	WebSocketReceiver_t719632682::get_offset_of__inputHeader_0(),
	WebSocketReceiver_t719632682::get_offset_of__websocket_1(),
	WebSocketReceiver_t719632682::get_offset_of__eventHandler_2(),
	WebSocketReceiver_t719632682::get_offset_of__input_3(),
	WebSocketReceiver_t719632682::get_offset_of__pendingBuilder_4(),
	WebSocketReceiver_t719632682::get_offset_of__stop_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3862 = { sizeof (WebSocketWriter_t3581815132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3862[8] = 
{
	WebSocketWriter_t3581815132::get_offset_of__innerThread_0(),
	WebSocketWriter_t3581815132::get_offset_of__pendingBuffers_1(),
	WebSocketWriter_t3581815132::get_offset_of__random_2(),
	WebSocketWriter_t3581815132::get_offset_of__sync_3(),
	WebSocketWriter_t3581815132::get_offset_of__websocket_4(),
	WebSocketWriter_t3581815132::get_offset_of__channel_5(),
	WebSocketWriter_t3581815132::get_offset_of__closeSent_6(),
	WebSocketWriter_t3581815132::get_offset_of__stop_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3863 = { sizeof (Runnable33_t4002296479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3863[1] = 
{
	Runnable33_t4002296479::get_offset_of__enclosing_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3864 = { sizeof (JsonMapper_t120734841), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3865 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3866 = { sizeof (DefaultClock_t2696542967), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3867 = { sizeof (DefaultRunLoop_t1056641960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3867[1] = 
{
	DefaultRunLoop_t1056641960::get_offset_of__executor_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3868 = { sizeof (FirebaseThreadFactory_t699088793), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3868[1] = 
{
	FirebaseThreadFactory_t699088793::get_offset_of__enclosing_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3869 = { sizeof (ExceptionCatcher_t4243835735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3869[2] = 
{
	ExceptionCatcher_t4243835735::get_offset_of__enclosing_0(),
	ExceptionCatcher_t4243835735::get_offset_of__inner_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3870 = { sizeof (ScheduledThreadPoolExecutor42_t3954903604), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3871 = { sizeof (NodeSizeEstimator_t2619975485), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3872 = { sizeof (OffsetClock_t3991894683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3872[2] = 
{
	OffsetClock_t3991894683::get_offset_of__baseClock_0(),
	OffsetClock_t3991894683::get_offset_of__offset_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3873 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3873[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3874 = { sizeof (ParsedUrl_t1255537880), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3874[2] = 
{
	ParsedUrl_t1255537880::get_offset_of_Path_0(),
	ParsedUrl_t1255537880::get_offset_of_RepoInfo_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3875 = { sizeof (Utilities_t2243316744), -1, sizeof(Utilities_t2243316744_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3875[1] = 
{
	Utilities_t2243316744_StaticFields::get_offset_of_HexCharacters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3876 = { sizeof (Validation_t1722631061), -1, sizeof(Validation_t1722631061_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3876[2] = 
{
	Validation_t1722631061_StaticFields::get_offset_of_InvalidPathRegex_0(),
	Validation_t1722631061_StaticFields::get_offset_of_InvalidKeyRegex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3877 = { sizeof (AuthTokenProvider_t3681374264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3877[1] = 
{
	AuthTokenProvider_t3681374264::get_offset_of__app_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3878 = { sizeof (DatabasePlatform_t1459671864), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3879 = { sizeof (Logger_t225270238), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3880 = { sizeof (Level_t2798387899)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3880[6] = 
{
	Level_t2798387899::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3881 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3882 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3883 = { sizeof (DotNetPlatform_t2951135975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3883[1] = 
{
	DotNetPlatform_t2951135975::get_offset_of_U3CFirebaseAppU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3884 = { sizeof (SynchronizationContextTarget_t601611002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3884[2] = 
{
	SynchronizationContextTarget_t601611002::get_offset_of__context_0(),
	SynchronizationContextTarget_t601611002::get_offset_of__executor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3885 = { sizeof (U3CPostEventU3Ec__AnonStorey0_t1624051538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3885[1] = 
{
	U3CPostEventU3Ec__AnonStorey0_t1624051538::get_offset_of_r_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3886 = { sizeof (DefaultRunLoop40_t4147568072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3886[1] = 
{
	DefaultRunLoop40_t4147568072::get_offset_of__logger_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3887 = { sizeof (FirebaseDatabase_t1318758358), -1, sizeof(FirebaseDatabase_t1318758358_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3887[7] = 
{
	FirebaseDatabase_t1318758358_StaticFields::get_offset_of_DatabaseInstances_0(),
	FirebaseDatabase_t1318758358_StaticFields::get_offset_of_SSync_1(),
	FirebaseDatabase_t1318758358::get_offset_of__config_2(),
	FirebaseDatabase_t1318758358::get_offset_of__repoInfo_3(),
	FirebaseDatabase_t1318758358::get_offset_of__sync_4(),
	FirebaseDatabase_t1318758358::get_offset_of__repo_5(),
	FirebaseDatabase_t1318758358::get_offset_of_U3CAppU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3888 = { sizeof (Base64_t124663375), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3889 = { sizeof (Coder_t4255634182), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3889[2] = 
{
	Coder_t4255634182::get_offset_of_Op_0(),
	Coder_t4255634182::get_offset_of_Output_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3890 = { sizeof (Encoder_t3204051639), -1, sizeof(Encoder_t3204051639_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3890[9] = 
{
	Encoder_t3204051639_StaticFields::get_offset_of_Encode_2(),
	Encoder_t3204051639_StaticFields::get_offset_of_EncodeWebsafe_3(),
	Encoder_t3204051639::get_offset_of__alphabet_4(),
	Encoder_t3204051639::get_offset_of__tail_5(),
	Encoder_t3204051639::get_offset_of_DoCr_6(),
	Encoder_t3204051639::get_offset_of_DoNewline_7(),
	Encoder_t3204051639::get_offset_of_DoPadding_8(),
	Encoder_t3204051639::get_offset_of__count_9(),
	Encoder_t3204051639::get_offset_of_TailLen_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3891 = { sizeof (U3CPrivateImplementationDetailsU3EU7B6eb14239U2Dc70bU2D4b0eU2D9bbcU2D78fe4bfbc003U7D_t318488734), -1, sizeof(U3CPrivateImplementationDetailsU3EU7B6eb14239U2Dc70bU2D4b0eU2D9bbcU2D78fe4bfbc003U7D_t318488734_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3891[2] = 
{
	U3CPrivateImplementationDetailsU3EU7B6eb14239U2Dc70bU2D4b0eU2D9bbcU2D78fe4bfbc003U7D_t318488734_StaticFields::get_offset_of_U24fieldU2D2_0(),
	U3CPrivateImplementationDetailsU3EU7B6eb14239U2Dc70bU2D4b0eU2D9bbcU2D78fe4bfbc003U7D_t318488734_StaticFields::get_offset_of_U24fieldU2D3_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3892 = { sizeof (U24ArrayTypeU3D64_t1473450479)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D64_t1473450479 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3893 = { sizeof (U3CModuleU3E_t3783534230), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3894 = { sizeof (EventHandle_t942672932)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3894[3] = 
{
	EventHandle_t942672932::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3895 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3896 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3897 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3898 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3899 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
