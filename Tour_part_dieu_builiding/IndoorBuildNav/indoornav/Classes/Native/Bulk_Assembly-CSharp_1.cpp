﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour2515041812.h"
#include "mscorlib_System_Void1841601450.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstr2478279366.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviour359035403.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBeh3319870759.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterB3249343815.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponen1383853028.h"
#include "mscorlib_System_Object2689449295.h"
#include "Vuforia_UnityExtensions_Vuforia_MaskOutAbstractBeh3489038957.h"
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour2994129365.h"
#include "Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBeh4084926705.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour3058161409.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetAbstrac3327552701.h"
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour2654589389.h"
#include "Vuforia_UnityExtensions_Vuforia_MultiTargetAbstrac3616801211.h"
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour3504654311.h"
#include "Vuforia_UnityExtensions_Vuforia_CylinderTargetAbstr665872082.h"
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour2091399712.h"
#include "Vuforia_UnityExtensions_Vuforia_WordAbstractBehavi2878458725.h"
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour3366478421.h"
#include "Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBe2386081773.h"
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour3400239837.h"
#include "Vuforia_UnityExtensions_Vuforia_ObjectTargetAbstra2805337095.h"
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour3836044259.h"
#include "Vuforia_UnityExtensions_Vuforia_VuMarkAbstractBeha1830666997.h"
#include "AssemblyU2DCSharp_Vuforia_VuMarkBehaviour2060629989.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractCon1891710424.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaConfiguration3823746026.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaRuntimeInitializa1850075444.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRuntime2075282796.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractCon3866211740.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "Vuforia_UnityExtensions_Vuforia_NullUnityPlayer754446093.h"
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayer852788525.h"
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayer3656371703.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeUnityPlayer918240325.h"
#include "AssemblyU2DCSharp_Vuforia_WSAUnityPlayer425981959.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour2494532455.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UnityEngine_MeshFilter3026937449.h"
#include "UnityEngine_UnityEngine_Mesh1356156583.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManager2424874861.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Behaviour955675639.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventH1535150527.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour1779888572.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour4057911311.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitE2149396216.h"
#include "UnityEngine_UnityEngine_ScreenOrientation4019489636.h"
#include "UnityEngine_UnityEngine_DeviceOrientation895964084.h"
#include "AssemblyU2DCSharp_vumark_levid1745599613.h"
#include "AssemblyU2DCSharp_VuMarkEventHandler2972421956.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "AssemblyU2DCSharp_VuMarkHandler583794080.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_PanelShowHide775446595.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackerManager308318605.h"
#include "Vuforia_UnityExtensions_Vuforia_StateManager3369465942.h"
#include "Vuforia_UnityExtensions_Vuforia_VuMarkManager3604726399.h"
#include "mscorlib_System_Action_1_gen24784135.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_Type1303803226.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "System_Core_System_Func_2_gen2495701363.h"
#include "System_Core_System_Func_2_gen3404947624.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"
#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearARCo1398758191.h"
#include "UnityEngine_UnityEngine_AI_NavMeshAgent2761625415.h"
#include "UnityEngine_UnityEngine_LineRenderer849157671.h"
#include "AssemblyU2DCSharp_VuMarkHandler_U3CShowPanelAfterU1558983881.h"
#include "Vuforia_UnityExtensions_Vuforia_InstanceIdType467315012.h"
#include "mscorlib_System_UInt642909196914.h"
#include "UnityEngine_UnityEngine_TextureFormat1386130234.h"
#include "UnityEngine_UnityEngine_TextureWrapMode3683976566.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "Vuforia_UnityExtensions_Vuforia_Image1391689025.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "mscorlib_System_UInt322149682021.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharp_VuMarkTapHandler2561605313.h"
#include "AssemblyU2DCSharp_TapHandler3409799063.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste3466835263.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3685274804.h"

// Vuforia.VirtualButtonBehaviour
struct VirtualButtonBehaviour_t2515041812;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t2478279366;
// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t359035403;
// Vuforia.VuforiaAbstractBehaviour
struct VuforiaAbstractBehaviour_t3319870759;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Vuforia.ComponentFactoryStarterBehaviour
struct ComponentFactoryStarterBehaviour_t3249343815;
// System.Object
struct Il2CppObject;
// UnityEngine.Object
struct Object_t1021602117;
// Vuforia.VuforiaBehaviourComponentFactory
struct VuforiaBehaviourComponentFactory_t1383853028;
// Vuforia.MaskOutAbstractBehaviour
struct MaskOutAbstractBehaviour_t3489038957;
// Vuforia.MaskOutBehaviour
struct MaskOutBehaviour_t2994129365;
// Vuforia.TurnOffAbstractBehaviour
struct TurnOffAbstractBehaviour_t4084926705;
// Vuforia.TurnOffBehaviour
struct TurnOffBehaviour_t3058161409;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t3327552701;
// Vuforia.ImageTargetBehaviour
struct ImageTargetBehaviour_t2654589389;
// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t3616801211;
// Vuforia.MultiTargetBehaviour
struct MultiTargetBehaviour_t3504654311;
// Vuforia.CylinderTargetAbstractBehaviour
struct CylinderTargetAbstractBehaviour_t665872082;
// Vuforia.CylinderTargetBehaviour
struct CylinderTargetBehaviour_t2091399712;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t2878458725;
// Vuforia.WordBehaviour
struct WordBehaviour_t3366478421;
// Vuforia.TextRecoAbstractBehaviour
struct TextRecoAbstractBehaviour_t2386081773;
// Vuforia.TextRecoBehaviour
struct TextRecoBehaviour_t3400239837;
// Vuforia.ObjectTargetAbstractBehaviour
struct ObjectTargetAbstractBehaviour_t2805337095;
// Vuforia.ObjectTargetBehaviour
struct ObjectTargetBehaviour_t3836044259;
// Vuforia.VuMarkAbstractBehaviour
struct VuMarkAbstractBehaviour_t1830666997;
// Vuforia.VuMarkBehaviour
struct VuMarkBehaviour_t2060629989;
// Vuforia.VuforiaAbstractConfiguration
struct VuforiaAbstractConfiguration_t1891710424;
// Vuforia.VuforiaConfiguration
struct VuforiaConfiguration_t3823746026;
// Vuforia.VuforiaRuntime
struct VuforiaRuntime_t2075282796;
// Vuforia.IUnityPlayer
struct IUnityPlayer_t2720985375;
// Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration
struct GenericVuforiaConfiguration_t3866211740;
// Vuforia.NullUnityPlayer
struct NullUnityPlayer_t754446093;
// Vuforia.AndroidUnityPlayer
struct AndroidUnityPlayer_t852788525;
// Vuforia.IOSUnityPlayer
struct IOSUnityPlayer_t3656371703;
// Vuforia.PlayModeUnityPlayer
struct PlayModeUnityPlayer_t918240325;
// Vuforia.WSAUnityPlayer
struct WSAUnityPlayer_t425981959;
// Vuforia.WireframeBehaviour
struct WireframeBehaviour_t2494532455;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// UnityEngine.Material
struct Material_t193706927;
// Vuforia.VuforiaManager
struct VuforiaManager_t2424874861;
// UnityEngine.Camera[]
struct CameraU5BU5D_t3079764780;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.String
struct String_t;
// UnityEngine.Behaviour
struct Behaviour_t955675639;
// Vuforia.WireframeTrackableEventHandler
struct WireframeTrackableEventHandler_t1535150527;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t1779888572;
// Vuforia.ITrackableEventHandler
struct ITrackableEventHandler_t696806248;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t2810717544;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t462843629;
// Vuforia.WireframeBehaviour[]
struct WireframeBehaviourU5BU5D_t2935582494;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.Collider
struct Collider_t3497673348;
// vumark_levid
struct vumark_levid_t1745599613;
// VuMarkEventHandler
struct VuMarkEventHandler_t2972421956;
// UnityEngine.Animator
struct Animator_t69676727;
// VuMarkHandler
struct VuMarkHandler_t583794080;
// PanelShowHide
struct PanelShowHide_t775446595;
// Vuforia.TrackerManager
struct TrackerManager_t308318605;
// System.Action`1<Vuforia.VuMarkTarget>
struct Action_1_t24784135;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// Vuforia.VuMarkTarget
struct VuMarkTarget_t222984753;
// System.Type
struct Type_t;
// UnityEngine.Object[]
struct ObjectU5BU5D_t4217747464;
// System.Func`2<UnityEngine.Object,UnityEngine.GameObject>
struct Func_2_t2495701363;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2825504181;
// System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject>
struct IEnumerable_1_t2048660192;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Object>
struct IEnumerable_1_t1313729162;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// System.Func`2<UnityEngine.GameObject,System.Boolean>
struct Func_2_t3404947624;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t3961629604;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// Vuforia.DigitalEyewearARController
struct DigitalEyewearARController_t1398758191;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.AI.NavMeshAgent
struct NavMeshAgent_t2761625415;
// UnityEngine.LineRenderer
struct LineRenderer_t849157671;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// VuMarkHandler/<ShowPanelAfter>c__Iterator0
struct U3CShowPanelAfterU3Ec__Iterator0_t1558983881;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t3839502067;
// System.NotSupportedException
struct NotSupportedException_t1793819818;
// VuMarkTapHandler
struct VuMarkTapHandler_t2561605313;
// TapHandler
struct TapHandler_t3409799063;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t3685274804;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
extern const MethodInfo* GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107_MethodInfo_var;
extern const uint32_t VuforiaBehaviour_Awake_m3772880569_MetadataUsageId;
extern Il2CppClass* VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984_MethodInfo_var;
extern const uint32_t VuforiaBehaviour_get_Instance_m790681192_MetadataUsageId;
extern const MethodInfo* GameObject_AddComponent_TisMaskOutBehaviour_t2994129365_m1425322069_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m3602623915_MetadataUsageId;
extern const MethodInfo* GameObject_AddComponent_TisVirtualButtonBehaviour_t2515041812_m3118105184_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m273150123_MetadataUsageId;
extern const MethodInfo* GameObject_AddComponent_TisTurnOffBehaviour_t3058161409_m2052882313_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m3691970091_MetadataUsageId;
extern const MethodInfo* GameObject_AddComponent_TisImageTargetBehaviour_t2654589389_m3351511077_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m110195307_MetadataUsageId;
extern const MethodInfo* GameObject_AddComponent_TisMultiTargetBehaviour_t3504654311_m1135184387_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m1008182955_MetadataUsageId;
extern const MethodInfo* GameObject_AddComponent_TisCylinderTargetBehaviour_t2091399712_m912348918_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m2573942991_MetadataUsageId;
extern const MethodInfo* GameObject_AddComponent_TisWordBehaviour_t3366478421_m80916061_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddWordBehaviour_m3806508403_MetadataUsageId;
extern const MethodInfo* GameObject_AddComponent_TisTextRecoBehaviour_t3400239837_m3447592645_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m2915043667_MetadataUsageId;
extern const MethodInfo* GameObject_AddComponent_TisObjectTargetBehaviour_t3836044259_m2560584915_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m3475568395_MetadataUsageId;
extern const MethodInfo* GameObject_AddComponent_TisVuMarkBehaviour_t2060629989_m1435287781_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddVuMarkBehaviour_m2639490483_MetadataUsageId;
extern const MethodInfo* ScriptableObject_CreateInstance_TisVuforiaConfiguration_t3823746026_m559723354_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_CreateVuforiaConfiguration_m931474600_MetadataUsageId;
extern Il2CppClass* VuforiaAbstractConfiguration_t1891710424_il2cpp_TypeInfo_var;
extern const uint32_t VuforiaConfiguration__ctor_m2808042321_MetadataUsageId;
extern Il2CppClass* VuforiaUnity_t657456673_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaRuntime_t2075282796_il2cpp_TypeInfo_var;
extern const uint32_t VuforiaRuntimeInitialization_InitPlatform_m3197388960_MetadataUsageId;
extern const uint32_t VuforiaRuntimeInitialization_InitVuforia_m2546903059_MetadataUsageId;
extern Il2CppClass* NullUnityPlayer_t754446093_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidUnityPlayer_t852788525_il2cpp_TypeInfo_var;
extern Il2CppClass* IOSUnityPlayer_t3656371703_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayModeUnityPlayer_t918240325_il2cpp_TypeInfo_var;
extern Il2CppClass* WSAUnityPlayer_t425981959_il2cpp_TypeInfo_var;
extern const uint32_t VuforiaRuntimeInitialization_CreateUnityPlayer_m1650732109_MetadataUsageId;
extern Il2CppClass* Material_t193706927_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral599011397;
extern const uint32_t WireframeBehaviour_Start_m2184757344_MetadataUsageId;
extern Il2CppClass* VuforiaManager_t2424874861_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral895546098;
extern const uint32_t WireframeBehaviour_OnRenderObject_m2411339956_MetadataUsageId;
extern const uint32_t WireframeBehaviour_OnDrawGizmos_m4179942854_MetadataUsageId;
extern const MethodInfo* Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957_MethodInfo_var;
extern const uint32_t WireframeTrackableEventHandler_Start_m1475947192_MetadataUsageId;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1040926105;
extern Il2CppCodeGenString* _stringLiteral759218142;
extern const uint32_t WireframeTrackableEventHandler_OnTrackingFound_m563781220_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3033297088;
extern const uint32_t WireframeTrackableEventHandler_OnTrackingLost_m3180126272_MetadataUsageId;
extern Il2CppClass* SurfaceUtilities_t4096327849_il2cpp_TypeInfo_var;
extern const uint32_t WSAUnityPlayer_Update_m3453039605_MetadataUsageId;
extern const uint32_t WSAUnityPlayer_OnPause_m2815166635_MetadataUsageId;
extern const uint32_t WSAUnityPlayer_OnResume_m2572251502_MetadataUsageId;
extern const uint32_t WSAUnityPlayer_OnDestroy_m3845019931_MetadataUsageId;
extern const uint32_t WSAUnityPlayer_InitializeSurface_m2485130165_MetadataUsageId;
extern const uint32_t WSAUnityPlayer_SetUnityScreenOrientation_m3964512799_MetadataUsageId;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t WSAUnityPlayer_GetActualScreenOrientation_m2906081196_MetadataUsageId;
extern const uint32_t VuMarkEventHandler_Start_m1800205201_MetadataUsageId;
extern const MethodInfo* Component_GetComponentInChildren_TisAnimator_t69676727_m501749323_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1689944869;
extern const uint32_t VuMarkEventHandler_OnTrackingFound_m946784669_MetadataUsageId;
extern const uint32_t VuMarkEventHandler_OnTrackingLost_m2215196871_MetadataUsageId;
extern Il2CppClass* TrackerManager_t308318605_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t24784135_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisPanelShowHide_t775446595_m2828053312_MethodInfo_var;
extern const MethodInfo* VuMarkHandler_OnVuMarkDetected_m1182417521_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m91845076_MethodInfo_var;
extern const MethodInfo* VuMarkHandler_OnVuMarkLost_m2126394563_MethodInfo_var;
extern const uint32_t VuMarkHandler_Start_m2704101265_MetadataUsageId;
extern const uint32_t VuMarkHandler_OnDestroy_m3979816830_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1258021450;
extern const uint32_t VuMarkHandler_OnVuMarkDetected_m1182417521_MetadataUsageId;
extern const Il2CppType* GameObject_t1756533147_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* VuMarkHandler_t583794080_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t2495701363_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t3404947624_il2cpp_TypeInfo_var;
extern const MethodInfo* VuMarkHandler_U3COnVuMarkLostU3Em__0_m3059917024_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2347799250_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisObject_t1021602117_TisGameObject_t1756533147_m655585110_MethodInfo_var;
extern const MethodInfo* VuMarkHandler_U3COnVuMarkLostU3Em__1_m1693396336_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3594470121_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisGameObject_t1756533147_m1880019006_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisGameObject_t1756533147_m2652891882_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4104255622;
extern Il2CppCodeGenString* _stringLiteral768014653;
extern const uint32_t VuMarkHandler_OnVuMarkLost_m2126394563_MetadataUsageId;
extern Il2CppClass* DigitalEyewearARController_t1398758191_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t2122794042_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t3601158120_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisNavMeshAgent_t2761625415_m2898296133_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisLineRenderer_t849157671_m3748809323_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3560472239;
extern Il2CppCodeGenString* _stringLiteral3886465728;
extern const uint32_t VuMarkHandler_UpdateClosestTarget_m578840658_MetadataUsageId;
extern Il2CppClass* U3CShowPanelAfterU3Ec__Iterator0_t1558983881_il2cpp_TypeInfo_var;
extern const uint32_t VuMarkHandler_ShowPanelAfter_m2879824669_MetadataUsageId;
extern Il2CppClass* VuMarkTarget_t222984753_il2cpp_TypeInfo_var;
extern Il2CppClass* InstanceId_t1732537268_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1608738781;
extern Il2CppCodeGenString* _stringLiteral3311046853;
extern Il2CppCodeGenString* _stringLiteral2115023907;
extern const uint32_t VuMarkHandler_GetVuMarkDataType_m3559465785_MetadataUsageId;
extern const uint32_t VuMarkHandler_GetVuMarkString_m589384032_MetadataUsageId;
extern Il2CppClass* Texture2D_t3542995729_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4080548243;
extern const uint32_t VuMarkHandler_GetVuMarkImage_m2423536162_MetadataUsageId;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern const uint32_t VuMarkHandler_U3COnVuMarkLostU3Em__0_m3059917024_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1971614873;
extern const uint32_t VuMarkHandler_U3COnVuMarkLostU3Em__1_m1693396336_MetadataUsageId;
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CShowPanelAfterU3Ec__Iterator0_MoveNext_m1652436950_MetadataUsageId;
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CShowPanelAfterU3Ec__Iterator0_Reset_m1930662853_MetadataUsageId;
extern Il2CppClass* List_1_t3685274804_il2cpp_TypeInfo_var;
extern Il2CppClass* EventSystem_t3466835263_il2cpp_TypeInfo_var;
extern Il2CppClass* PointerEventData_t1599784723_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2989057823_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3435089276_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3279745867_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2728995068;
extern const uint32_t VuMarkTapHandler_CheckVuMarkCardHit_m945028729_MetadataUsageId;

// UnityEngine.Camera[]
struct CameraU5BU5D_t3079764780  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Camera_t189460977 * m_Items[1];

public:
	inline Camera_t189460977 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Camera_t189460977 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Camera_t189460977 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Camera_t189460977 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Camera_t189460977 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Camera_t189460977 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Vector3_t2243707580  m_Items[1];

public:
	inline Vector3_t2243707580  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t2243707580 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t2243707580  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t2243707580  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t2243707580 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t2243707580  value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t3030399641  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Renderer[]
struct RendererU5BU5D_t2810717544  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Renderer_t257310565 * m_Items[1];

public:
	inline Renderer_t257310565 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Renderer_t257310565 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Renderer_t257310565 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Renderer_t257310565 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Renderer_t257310565 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Renderer_t257310565 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Collider[]
struct ColliderU5BU5D_t462843629  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Collider_t3497673348 * m_Items[1];

public:
	inline Collider_t3497673348 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Collider_t3497673348 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Collider_t3497673348 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Collider_t3497673348 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Collider_t3497673348 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Collider_t3497673348 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.WireframeBehaviour[]
struct WireframeBehaviourU5BU5D_t2935582494  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) WireframeBehaviour_t2494532455 * m_Items[1];

public:
	inline WireframeBehaviour_t2494532455 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline WireframeBehaviour_t2494532455 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, WireframeBehaviour_t2494532455 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline WireframeBehaviour_t2494532455 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline WireframeBehaviour_t2494532455 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, WireframeBehaviour_t2494532455 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GameObject_t1756533147 * m_Items[1];

public:
	inline GameObject_t1756533147 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_t1756533147 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_t1756533147 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GameObject_t1756533147 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_t1756533147 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_t1756533147 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Object[]
struct ObjectU5BU5D_t4217747464  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Object_t1021602117 * m_Items[1];

public:
	inline Object_t1021602117 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Object_t1021602117 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Object_t1021602117 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Object_t1021602117 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Object_t1021602117 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Object_t1021602117 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3175043010_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C"  Il2CppObject * Object_FindObjectOfType_TisIl2CppObject_m758847274_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
// !!0 UnityEngine.ScriptableObject::CreateInstance<System.Object>()
extern "C"  Il2CppObject * ScriptableObject_CreateInstance_TisIl2CppObject_m926060499_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* GameObject_GetComponentsInChildren_TisIl2CppObject_m1163966231_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t3614634134* Component_GetComponentsInChildren_TisIl2CppObject_m4164830438_gshared (Component_t3819376471 * __this, bool p0, const MethodInfo* method);
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m2641795278_gshared (Component_t3819376471 * __this, const MethodInfo* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m584977596_gshared (Action_1_t2491248677 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.Void System.Func`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m1684831714_gshared (Func_2_t2825504181 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m3383050844_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t2825504181 * p1, const MethodInfo* method);
// System.Void System.Func`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m1354888807_gshared (Func_2_t3961629604 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Il2CppObject* Enumerable_Where_TisIl2CppObject_m1516493223_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t3961629604 * p1, const MethodInfo* method);
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  ObjectU5BU5D_t3614634134* Enumerable_ToArray_TisIl2CppObject_m3227540781_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m4211544902_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C"  void List_1__ctor_m2989057823_gshared (List_1_t3685274804 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Item(System.Int32)
extern "C"  RaycastResult_t21186376  List_1_get_Item_m3435089276_gshared (List_1_t3685274804 * __this, int32_t p0, const MethodInfo* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Count()
extern "C"  int32_t List_1_get_Count_m3279745867_gshared (List_1_t3685274804 * __this, const MethodInfo* method);

// System.Void Vuforia.VirtualButtonAbstractBehaviour::.ctor()
extern "C"  void VirtualButtonAbstractBehaviour__ctor_m855896756 (VirtualButtonAbstractBehaviour_t2478279366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaAbstractBehaviour::.ctor()
extern "C"  void VuforiaAbstractBehaviour__ctor_m3900338923 (VuforiaAbstractBehaviour_t3319870759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaBehaviour::AddOSSpecificExternalDatasetSearchDirs()
extern "C"  void VuforiaBehaviour_AddOSSpecificExternalDatasetSearchDirs_m4168849520 (VuforiaBehaviour_t359035403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ComponentFactoryStarterBehaviour>()
#define GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107(__this, method) ((  ComponentFactoryStarterBehaviour_t3249343815 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3175043010_gshared)(__this, method)
// System.Void Vuforia.VuforiaAbstractBehaviour::Awake()
extern "C"  void VuforiaAbstractBehaviour_Awake_m232471254 (VuforiaAbstractBehaviour_t3319870759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3764089466 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::FindObjectOfType<Vuforia.VuforiaBehaviour>()
#define Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984(__this /* static, unused */, method) ((  VuforiaBehaviour_t359035403 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m758847274_gshared)(__this /* static, unused */, method)
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.MaskOutBehaviour>()
#define GameObject_AddComponent_TisMaskOutBehaviour_t2994129365_m1425322069(__this, method) ((  MaskOutBehaviour_t2994129365 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3175043010_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.VirtualButtonBehaviour>()
#define GameObject_AddComponent_TisVirtualButtonBehaviour_t2515041812_m3118105184(__this, method) ((  VirtualButtonBehaviour_t2515041812 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3175043010_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.TurnOffBehaviour>()
#define GameObject_AddComponent_TisTurnOffBehaviour_t3058161409_m2052882313(__this, method) ((  TurnOffBehaviour_t3058161409 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3175043010_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ImageTargetBehaviour>()
#define GameObject_AddComponent_TisImageTargetBehaviour_t2654589389_m3351511077(__this, method) ((  ImageTargetBehaviour_t2654589389 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3175043010_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.MultiTargetBehaviour>()
#define GameObject_AddComponent_TisMultiTargetBehaviour_t3504654311_m1135184387(__this, method) ((  MultiTargetBehaviour_t3504654311 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3175043010_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.CylinderTargetBehaviour>()
#define GameObject_AddComponent_TisCylinderTargetBehaviour_t2091399712_m912348918(__this, method) ((  CylinderTargetBehaviour_t2091399712 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3175043010_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.WordBehaviour>()
#define GameObject_AddComponent_TisWordBehaviour_t3366478421_m80916061(__this, method) ((  WordBehaviour_t3366478421 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3175043010_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.TextRecoBehaviour>()
#define GameObject_AddComponent_TisTextRecoBehaviour_t3400239837_m3447592645(__this, method) ((  TextRecoBehaviour_t3400239837 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3175043010_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ObjectTargetBehaviour>()
#define GameObject_AddComponent_TisObjectTargetBehaviour_t3836044259_m2560584915(__this, method) ((  ObjectTargetBehaviour_t3836044259 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3175043010_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.VuMarkBehaviour>()
#define GameObject_AddComponent_TisVuMarkBehaviour_t2060629989_m1435287781(__this, method) ((  VuMarkBehaviour_t2060629989 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3175043010_gshared)(__this, method)
// !!0 UnityEngine.ScriptableObject::CreateInstance<Vuforia.VuforiaConfiguration>()
#define ScriptableObject_CreateInstance_TisVuforiaConfiguration_t3823746026_m559723354(__this /* static, unused */, method) ((  VuforiaConfiguration_t3823746026 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ScriptableObject_CreateInstance_TisIl2CppObject_m926060499_gshared)(__this /* static, unused */, method)
// System.Void Vuforia.VuforiaAbstractConfiguration::.ctor()
extern "C"  void VuforiaAbstractConfiguration__ctor_m1625969650 (VuforiaAbstractConfiguration_t1891710424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaUnity::SetAssetInitializationParameters()
extern "C"  void VuforiaUnity_SetAssetInitializationParameters_m3316561825 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaRuntime Vuforia.VuforiaRuntime::get_Instance()
extern "C"  VuforiaRuntime_t2075282796 * VuforiaRuntime_get_Instance_m4069915631 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.IUnityPlayer Vuforia.VuforiaRuntimeInitialization::CreateUnityPlayer()
extern "C"  Il2CppObject * VuforiaRuntimeInitialization_CreateUnityPlayer_m1650732109 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaRuntime::InitPlatform(Vuforia.IUnityPlayer)
extern "C"  void VuforiaRuntime_InitPlatform_m3306160978 (VuforiaRuntime_t2075282796 * __this, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaAbstractConfiguration Vuforia.VuforiaAbstractConfiguration::get_Instance()
extern "C"  VuforiaAbstractConfiguration_t1891710424 * VuforiaAbstractConfiguration_get_Instance_m3543652287 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration Vuforia.VuforiaAbstractConfiguration::get_Vuforia()
extern "C"  GenericVuforiaConfiguration_t3866211740 * VuforiaAbstractConfiguration_get_Vuforia_m3334047132 (VuforiaAbstractConfiguration_t1891710424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::get_DelayedInitialization()
extern "C"  bool GenericVuforiaConfiguration_get_DelayedInitialization_m282560601 (GenericVuforiaConfiguration_t3866211740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaRuntime::InitVuforia()
extern "C"  void VuforiaRuntime_InitVuforia_m2353933398 (VuforiaRuntime_t2075282796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::.ctor()
extern "C"  void NullUnityPlayer__ctor_m483624113 (NullUnityPlayer_t754446093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C"  int32_t Application_get_platform_m3989224144 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.AndroidUnityPlayer::.ctor()
extern "C"  void AndroidUnityPlayer__ctor_m2233000524 (AndroidUnityPlayer_t852788525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.IOSUnityPlayer::.ctor()
extern "C"  void IOSUnityPlayer__ctor_m676692974 (IOSUnityPlayer_t3656371703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VuforiaRuntimeUtilities::IsPlayMode()
extern "C"  bool VuforiaRuntimeUtilities_IsPlayMode_m2939358997 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::.ctor()
extern "C"  void PlayModeUnityPlayer__ctor_m2126346857 (PlayModeUnityPlayer_t918240325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VuforiaRuntimeUtilities::IsWSARuntime()
extern "C"  bool VuforiaRuntimeUtilities_IsWSARuntime_m3848252715 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WSAUnityPlayer::.ctor()
extern "C"  void WSAUnityPlayer__ctor_m1279021334 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuMarkAbstractBehaviour::.ctor()
extern "C"  void VuMarkAbstractBehaviour__ctor_m326197713 (VuMarkAbstractBehaviour_t1830666997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_green()
extern "C"  Color_t2020392075  Color_get_green_m2671273823 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m2402264703 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::.ctor(UnityEngine.Material)
extern "C"  void Material__ctor_m1440882780 (Material_t193706927 * __this, Material_t193706927 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C"  void Debug_LogWarning_m2503577968 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaManager Vuforia.VuforiaManager::get_Instance()
extern "C"  VuforiaManager_t2424874861 * VuforiaManager_get_Instance_m425433003 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Camera>()
#define GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(__this, method) ((  CameraU5BU5D_t3079764780* (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisIl2CppObject_m1163966231_gshared)(__this, method)
// UnityEngine.Camera UnityEngine.Camera::get_current()
extern "C"  Camera_t189460977 * Camera_get_current_m2639890517 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
#define Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, method) ((  MeshFilter_t3026937449 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m2856731593 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh UnityEngine.MeshFilter::get_sharedMesh()
extern "C"  Mesh_t1356156583 * MeshFilter_get_sharedMesh_m1310789932 (MeshFilter_t3026937449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine.Mesh::get_vertices()
extern "C"  Vector3U5BU5D_t1172311765* Mesh_get_vertices_m626989480 (Mesh_t1356156583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] UnityEngine.Mesh::get_triangles()
extern "C"  Int32U5BU5D_t3030399641* Mesh_get_triangles_m3988715512 (Mesh_t1356156583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::PushMatrix()
extern "C"  void GL_PushMatrix_m1979053131 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m2697483695 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_localToWorldMatrix()
extern "C"  Matrix4x4_t2933234003  Transform_get_localToWorldMatrix_m2868579006 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::MultMatrix(UnityEngine.Matrix4x4)
extern "C"  void GL_MultMatrix_m767401141 (Il2CppObject * __this /* static, unused */, Matrix4x4_t2933234003  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Material::SetPass(System.Int32)
extern "C"  bool Material_SetPass_m2448940266 (Material_t193706927 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetColor(System.String,UnityEngine.Color)
extern "C"  void Material_SetColor_m650857509 (Material_t193706927 * __this, String_t* p0, Color_t2020392075  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::Begin(System.Int32)
extern "C"  void GL_Begin_m3874173032 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::Vertex(UnityEngine.Vector3)
extern "C"  void GL_Vertex_m4110027235 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::End()
extern "C"  void GL_End_m2374230645 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::PopMatrix()
extern "C"  void GL_PopMatrix_m856033754 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C"  bool Behaviour_get_enabled_m4079055610 (Behaviour_t955675639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3275118058 * GameObject_get_transform_m909382139 (GameObject_t1756533147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2243707580  Transform_get_position_m1104419803 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t4030073918  Transform_get_rotation_m1033555130 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_lossyScale()
extern "C"  Vector3_t2243707580  Transform_get_lossyScale_m1638545862 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::TRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Matrix4x4_t2933234003  Matrix4x4_TRS_m1913765359 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Quaternion_t4030073918  p1, Vector3_t2243707580  p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::set_matrix(UnityEngine.Matrix4x4)
extern "C"  void Gizmos_set_matrix_m1590313986 (Il2CppObject * __this /* static, unused */, Matrix4x4_t2933234003  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::set_color(UnityEngine.Color)
extern "C"  void Gizmos_set_color_m494992840 (Il2CppObject * __this /* static, unused */, Color_t2020392075  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Gizmos_DrawLine_m1315654064 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<Vuforia.TrackableBehaviour>()
#define Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957(__this, method) ((  TrackableBehaviour_t1779888572 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Void Vuforia.TrackableBehaviour::RegisterTrackableEventHandler(Vuforia.ITrackableEventHandler)
extern "C"  void TrackableBehaviour_RegisterTrackableEventHandler_m1156666476 (TrackableBehaviour_t1779888572 * __this, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingFound()
extern "C"  void WireframeTrackableEventHandler_OnTrackingFound_m563781220 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingLost()
extern "C"  void WireframeTrackableEventHandler_OnTrackingLost_m3180126272 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Renderer>(System.Boolean)
#define Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, p0, method) ((  RendererU5BU5D_t2810717544* (*) (Component_t3819376471 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m4164830438_gshared)(__this, p0, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Collider>(System.Boolean)
#define Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, p0, method) ((  ColliderU5BU5D_t462843629* (*) (Component_t3819376471 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m4164830438_gshared)(__this, p0, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<Vuforia.WireframeBehaviour>(System.Boolean)
#define Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, p0, method) ((  WireframeBehaviourU5BU5D_t2935582494* (*) (Component_t3819376471 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m4164830438_gshared)(__this, p0, method)
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern "C"  void Renderer_set_enabled_m142717579 (Renderer_t257310565 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider::set_enabled(System.Boolean)
extern "C"  void Collider_set_enabled_m3489100454 (Collider_t3497673348 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C"  void Behaviour_set_enabled_m1796096907 (Behaviour_t955675639 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.TrackableBehaviour::get_TrackableName()
extern "C"  String_t* TrackableBehaviour_get_TrackableName_m3173853042 (TrackableBehaviour_t1779888572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m612901809 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m920475918 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::.ctor()
extern "C"  void WordAbstractBehaviour__ctor_m1415816009 (WordAbstractBehaviour_t2878458725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WSAUnityPlayer::setPlatFormNative()
extern "C"  void WSAUnityPlayer_setPlatFormNative_m413995854 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.WSAUnityPlayer::initVuforiaWSA(System.String)
extern "C"  int32_t WSAUnityPlayer_initVuforiaWSA_m2461582347 (Il2CppObject * __this /* static, unused */, String_t* ___licenseKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WSAUnityPlayer::InitializeSurface()
extern "C"  void WSAUnityPlayer_InitializeSurface_m2485130165 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SurfaceUtilities::HasSurfaceBeenRecreated()
extern "C"  bool SurfaceUtilities_HasSurfaceBeenRecreated_m2740261893 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScreenOrientation Vuforia.WSAUnityPlayer::GetActualScreenOrientation()
extern "C"  int32_t WSAUnityPlayer_GetActualScreenOrientation_m2906081196 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WSAUnityPlayer::SetUnityScreenOrientation()
extern "C"  void WSAUnityPlayer_SetUnityScreenOrientation_m3964512799 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaUnity::OnPause()
extern "C"  void VuforiaUnity_OnPause_m2422224752 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaUnity::OnResume()
extern "C"  void VuforiaUnity_OnResume_m2186520633 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaUnity::Deinit()
extern "C"  void VuforiaUnity_Deinit_m4072609744 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceUtilities::OnSurfaceCreated()
extern "C"  void SurfaceUtilities_OnSurfaceCreated_m3675640541 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceUtilities::SetSurfaceOrientation(UnityEngine.ScreenOrientation)
extern "C"  void SurfaceUtilities_SetSurfaceOrientation_m3106547277 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WSAUnityPlayer::setSurfaceOrientationWSA(System.Int32)
extern "C"  void WSAUnityPlayer_setSurfaceOrientationWSA_m2082052129 (Il2CppObject * __this /* static, unused */, int32_t ___screenOrientation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScreenOrientation UnityEngine.Screen::get_orientation()
extern "C"  int32_t Screen_get_orientation_m879255848 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.DeviceOrientation UnityEngine.Input::get_deviceOrientation()
extern "C"  int32_t Input_get_deviceOrientation_m2415424840 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VuMarkEventHandler::OnTrackingFound()
extern "C"  void VuMarkEventHandler_OnTrackingFound_m946784669 (VuMarkEventHandler_t2972421956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VuMarkEventHandler::OnTrackingLost()
extern "C"  void VuMarkEventHandler_OnTrackingLost_m2215196871 (VuMarkEventHandler_t2972421956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.Animator>()
#define Component_GetComponentInChildren_TisAnimator_t69676727_m501749323(__this, method) ((  Animator_t69676727 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m2641795278_gshared)(__this, method)
// System.Void UnityEngine.Animator::SetTrigger(System.String)
extern "C"  void Animator_SetTrigger_m3418492570 (Animator_t69676727 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<PanelShowHide>()
#define Component_GetComponent_TisPanelShowHide_t775446595_m2828053312(__this, method) ((  PanelShowHide_t775446595 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// Vuforia.TrackerManager Vuforia.TrackerManager::get_Instance()
extern "C"  TrackerManager_t308318605 * TrackerManager_get_Instance_m2067083115 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<Vuforia.VuMarkTarget>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m91845076(__this, p0, p1, method) ((  void (*) (Action_1_t24784135 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m584977596_gshared)(__this, p0, p1, method)
// System.Void VuMarkHandler::UpdateClosestTarget()
extern "C"  void VuMarkHandler_UpdateClosestTarget_m578840658 (VuMarkHandler_t583794080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VuMarkHandler::GetVuMarkString(Vuforia.VuMarkTarget)
extern "C"  String_t* VuMarkHandler_GetVuMarkString_m589384032 (VuMarkHandler_t583794080 * __this, Il2CppObject * ___vumark0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m2596409543 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m432505302 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t2330101084  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t4217747464* Object_FindObjectsOfType_m2121813744 (Il2CppObject * __this /* static, unused */, Type_t * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Func`2<UnityEngine.Object,UnityEngine.GameObject>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2347799250(__this, p0, p1, method) ((  void (*) (Func_2_t2495701363 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m1684831714_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<UnityEngine.Object,UnityEngine.GameObject>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisObject_t1021602117_TisGameObject_t1756533147_m655585110(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2495701363 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m3383050844_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void System.Func`2<UnityEngine.GameObject,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3594470121(__this, p0, p1, method) ((  void (*) (Func_2_t3404947624 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m1354888807_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<UnityEngine.GameObject>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisGameObject_t1756533147_m1880019006(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3404947624 *, const MethodInfo*))Enumerable_Where_TisIl2CppObject_m1516493223_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] System.Linq.Enumerable::ToArray<UnityEngine.GameObject>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisGameObject_t1756533147_m2652891882(__this /* static, unused */, p0, method) ((  GameObjectU5BU5D_t3057952154* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m3227540781_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m4145850038 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.DigitalEyewearARController Vuforia.DigitalEyewearARController::get_Instance()
extern "C"  DigitalEyewearARController_t1398758191 * DigitalEyewearARController_get_Instance_m277595763 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera Vuforia.DigitalEyewearARController::get_PrimaryCamera()
extern "C"  Camera_t189460977 * DigitalEyewearARController_get_PrimaryCamera_m334515774 (DigitalEyewearARController_t1398758191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t189460977 * Camera_get_main_m475173995 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Transform_InverseTransformPoint_m2648491174 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t2243707579  Vector2_get_zero_m3966848876 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t2243707580  Vector2_op_Implicit_m176791411 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Distance_m1859670022 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m56707527 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuMarkTarget Vuforia.VuMarkAbstractBehaviour::get_VuMarkTarget()
extern "C"  Il2CppObject * VuMarkAbstractBehaviour_get_VuMarkTarget_m4042803881 (VuMarkAbstractBehaviour_t1830666997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VuMarkHandler::GetVuMarkDataType(Vuforia.VuMarkTarget)
extern "C"  String_t* VuMarkHandler_GetVuMarkDataType_m3559465785 (VuMarkHandler_t583794080 * __this, Il2CppObject * ___vumark0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite VuMarkHandler::GetVuMarkImage(Vuforia.VuMarkTarget)
extern "C"  Sprite_t309593783 * VuMarkHandler_GetVuMarkImage_m2423536162 (VuMarkHandler_t583794080 * __this, Il2CppObject * ___vumark0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C"  GameObject_t1756533147 * GameObject_Find_m836511350 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.AI.NavMeshAgent>()
#define GameObject_GetComponent_TisNavMeshAgent_t2761625415_m2898296133(__this, method) ((  NavMeshAgent_t2761625415 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m4211544902_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.LineRenderer>()
#define GameObject_GetComponent_TisLineRenderer_t849157671_m3748809323(__this, method) ((  LineRenderer_t849157671 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m4211544902_gshared)(__this, method)
// System.Void VuMarkHandler/<ShowPanelAfter>c__Iterator0::.ctor()
extern "C"  void U3CShowPanelAfterU3Ec__Iterator0__ctor_m249051542 (U3CShowPanelAfterU3Ec__Iterator0_t1558983881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.UInt64::ToString()
extern "C"  String_t* UInt64_ToString_m446228920 (uint64_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern "C"  void Texture2D__ctor_m1873923924 (Texture2D_t3542995729 * __this, int32_t p0, int32_t p1, int32_t p2, bool p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)
extern "C"  void Texture_set_wrapMode_m333956747 (Texture_t2243626319 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::Apply()
extern "C"  void Texture2D_Apply_m3543341930 (Texture2D_t3542995729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m1220545469 (Rect_t3681755626 * __this, float p0, float p1, float p2, float p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3067419446 (Vector2_t2243707579 * __this, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2)
extern "C"  Sprite_t309593783 * Sprite_Create_m3262956430 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * p0, Rect_t3681755626  p1, Vector2_t2243707579  p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m2079638459 (Object_t1021602117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m1790663636 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C"  void WaitForSeconds__ctor_m1990515539 (WaitForSeconds_t3839502067 * __this, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelShowHide::Show(System.String,System.String,UnityEngine.Sprite)
extern "C"  void PanelShowHide_Show_m438154159 (PanelShowHide_t775446595 * __this, String_t* ___title0, String_t* ___id1, Sprite_t309593783 * ___image2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m3232764727 (NotSupportedException_t1793819818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TapHandler::.ctor()
extern "C"  void TapHandler__ctor_m3084098962 (TapHandler_t3409799063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TapHandler::OnSingleTap()
extern "C"  void TapHandler_OnSingleTap_m916998152 (TapHandler_t3409799063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VuMarkTapHandler::CheckVuMarkCardHit()
extern "C"  bool VuMarkTapHandler_CheckVuMarkCardHit_m945028729 (VuMarkTapHandler_t2561605313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
#define List_1__ctor_m2989057823(__this, method) ((  void (*) (List_1_t3685274804 *, const MethodInfo*))List_1__ctor_m2989057823_gshared)(__this, method)
// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.EventSystem::get_current()
extern "C"  EventSystem_t3466835263 * EventSystem_get_current_m319019811 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerEventData::.ctor(UnityEngine.EventSystems.EventSystem)
extern "C"  void PointerEventData__ctor_m3674067728 (PointerEventData_t1599784723 * __this, EventSystem_t3466835263 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C"  Vector3_t2243707580  Input_get_mousePosition_m146923508 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.PointerEventData::set_position(UnityEngine.Vector2)
extern "C"  void PointerEventData_set_position_m794507622 (PointerEventData_t1599784723 * __this, Vector2_t2243707579  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventSystem::RaycastAll(UnityEngine.EventSystems.PointerEventData,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern "C"  void EventSystem_RaycastAll_m4000413739 (EventSystem_t3466835263 * __this, PointerEventData_t1599784723 * p0, List_1_t3685274804 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Item(System.Int32)
#define List_1_get_Item_m3435089276(__this, p0, method) ((  RaycastResult_t21186376  (*) (List_1_t3685274804 *, int32_t, const MethodInfo*))List_1_get_Item_m3435089276_gshared)(__this, p0, method)
// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::get_gameObject()
extern "C"  GameObject_t1756533147 * RaycastResult_get_gameObject_m2999022658 (RaycastResult_t21186376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
#define GameObject_GetComponent_TisAnimator_t69676727_m2717502299(__this, method) ((  Animator_t69676727 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m4211544902_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Count()
#define List_1_get_Count_m3279745867(__this, method) ((  int32_t (*) (List_1_t3685274804 *, const MethodInfo*))List_1_get_Count_m3279745867_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Vuforia.VirtualButtonBehaviour::.ctor()
extern "C"  void VirtualButtonBehaviour__ctor_m3984899111 (VirtualButtonBehaviour_t2515041812 * __this, const MethodInfo* method)
{
	{
		VirtualButtonAbstractBehaviour__ctor_m855896756(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::.ctor()
extern "C"  void VuforiaBehaviour__ctor_m3143982076 (VuforiaBehaviour_t359035403 * __this, const MethodInfo* method)
{
	{
		VuforiaAbstractBehaviour__ctor_m3900338923(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::Awake()
extern "C"  void VuforiaBehaviour_Awake_m3772880569 (VuforiaBehaviour_t359035403 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_Awake_m3772880569_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VuforiaBehaviour_AddOSSpecificExternalDatasetSearchDirs_m4168849520(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107(L_0, /*hidden argument*/GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107_MethodInfo_var);
		VuforiaAbstractBehaviour_Awake_m232471254(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::get_Instance()
extern "C"  VuforiaBehaviour_t359035403 * VuforiaBehaviour_get_Instance_m790681192 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_get_Instance_m790681192_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_0 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_mVuforiaBehaviour_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_2 = Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->set_mVuforiaBehaviour_19(L_2);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_3 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_mVuforiaBehaviour_19();
		return L_3;
	}
}
// System.Void Vuforia.VuforiaBehaviour::AddOSSpecificExternalDatasetSearchDirs()
extern "C"  void VuforiaBehaviour_AddOSSpecificExternalDatasetSearchDirs_m4168849520 (VuforiaBehaviour_t359035403 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::.cctor()
extern "C"  void VuforiaBehaviour__cctor_m1552098219 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviourComponentFactory::.ctor()
extern "C"  void VuforiaBehaviourComponentFactory__ctor_m4023364043 (VuforiaBehaviourComponentFactory_t1383853028 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.MaskOutAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
extern "C"  MaskOutAbstractBehaviour_t3489038957 * VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m3602623915 (VuforiaBehaviourComponentFactory_t1383853028 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m3602623915_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___gameObject0;
		NullCheck(L_0);
		MaskOutBehaviour_t2994129365 * L_1 = GameObject_AddComponent_TisMaskOutBehaviour_t2994129365_m1425322069(L_0, /*hidden argument*/GameObject_AddComponent_TisMaskOutBehaviour_t2994129365_m1425322069_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
extern "C"  VirtualButtonAbstractBehaviour_t2478279366 * VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m273150123 (VuforiaBehaviourComponentFactory_t1383853028 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m273150123_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___gameObject0;
		NullCheck(L_0);
		VirtualButtonBehaviour_t2515041812 * L_1 = GameObject_AddComponent_TisVirtualButtonBehaviour_t2515041812_m3118105184(L_0, /*hidden argument*/GameObject_AddComponent_TisVirtualButtonBehaviour_t2515041812_m3118105184_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.TurnOffAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
extern "C"  TurnOffAbstractBehaviour_t4084926705 * VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m3691970091 (VuforiaBehaviourComponentFactory_t1383853028 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m3691970091_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___gameObject0;
		NullCheck(L_0);
		TurnOffBehaviour_t3058161409 * L_1 = GameObject_AddComponent_TisTurnOffBehaviour_t3058161409_m2052882313(L_0, /*hidden argument*/GameObject_AddComponent_TisTurnOffBehaviour_t3058161409_m2052882313_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.ImageTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
extern "C"  ImageTargetAbstractBehaviour_t3327552701 * VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m110195307 (VuforiaBehaviourComponentFactory_t1383853028 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m110195307_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___gameObject0;
		NullCheck(L_0);
		ImageTargetBehaviour_t2654589389 * L_1 = GameObject_AddComponent_TisImageTargetBehaviour_t2654589389_m3351511077(L_0, /*hidden argument*/GameObject_AddComponent_TisImageTargetBehaviour_t2654589389_m3351511077_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.MultiTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
extern "C"  MultiTargetAbstractBehaviour_t3616801211 * VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m1008182955 (VuforiaBehaviourComponentFactory_t1383853028 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m1008182955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___gameObject0;
		NullCheck(L_0);
		MultiTargetBehaviour_t3504654311 * L_1 = GameObject_AddComponent_TisMultiTargetBehaviour_t3504654311_m1135184387(L_0, /*hidden argument*/GameObject_AddComponent_TisMultiTargetBehaviour_t3504654311_m1135184387_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
extern "C"  CylinderTargetAbstractBehaviour_t665872082 * VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m2573942991 (VuforiaBehaviourComponentFactory_t1383853028 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m2573942991_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___gameObject0;
		NullCheck(L_0);
		CylinderTargetBehaviour_t2091399712 * L_1 = GameObject_AddComponent_TisCylinderTargetBehaviour_t2091399712_m912348918(L_0, /*hidden argument*/GameObject_AddComponent_TisCylinderTargetBehaviour_t2091399712_m912348918_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.WordAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
extern "C"  WordAbstractBehaviour_t2878458725 * VuforiaBehaviourComponentFactory_AddWordBehaviour_m3806508403 (VuforiaBehaviourComponentFactory_t1383853028 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddWordBehaviour_m3806508403_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___gameObject0;
		NullCheck(L_0);
		WordBehaviour_t3366478421 * L_1 = GameObject_AddComponent_TisWordBehaviour_t3366478421_m80916061(L_0, /*hidden argument*/GameObject_AddComponent_TisWordBehaviour_t3366478421_m80916061_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.TextRecoAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
extern "C"  TextRecoAbstractBehaviour_t2386081773 * VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m2915043667 (VuforiaBehaviourComponentFactory_t1383853028 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m2915043667_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___gameObject0;
		NullCheck(L_0);
		TextRecoBehaviour_t3400239837 * L_1 = GameObject_AddComponent_TisTextRecoBehaviour_t3400239837_m3447592645(L_0, /*hidden argument*/GameObject_AddComponent_TisTextRecoBehaviour_t3400239837_m3447592645_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
extern "C"  ObjectTargetAbstractBehaviour_t2805337095 * VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m3475568395 (VuforiaBehaviourComponentFactory_t1383853028 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m3475568395_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___gameObject0;
		NullCheck(L_0);
		ObjectTargetBehaviour_t3836044259 * L_1 = GameObject_AddComponent_TisObjectTargetBehaviour_t3836044259_m2560584915(L_0, /*hidden argument*/GameObject_AddComponent_TisObjectTargetBehaviour_t3836044259_m2560584915_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.VuMarkAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddVuMarkBehaviour(UnityEngine.GameObject)
extern "C"  VuMarkAbstractBehaviour_t1830666997 * VuforiaBehaviourComponentFactory_AddVuMarkBehaviour_m2639490483 (VuforiaBehaviourComponentFactory_t1383853028 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddVuMarkBehaviour_m2639490483_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___gameObject0;
		NullCheck(L_0);
		VuMarkBehaviour_t2060629989 * L_1 = GameObject_AddComponent_TisVuMarkBehaviour_t2060629989_m1435287781(L_0, /*hidden argument*/GameObject_AddComponent_TisVuMarkBehaviour_t2060629989_m1435287781_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.VuforiaAbstractConfiguration Vuforia.VuforiaBehaviourComponentFactory::CreateVuforiaConfiguration()
extern "C"  VuforiaAbstractConfiguration_t1891710424 * VuforiaBehaviourComponentFactory_CreateVuforiaConfiguration_m931474600 (VuforiaBehaviourComponentFactory_t1383853028 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_CreateVuforiaConfiguration_m931474600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VuforiaConfiguration_t3823746026 * L_0 = ScriptableObject_CreateInstance_TisVuforiaConfiguration_t3823746026_m559723354(NULL /*static, unused*/, /*hidden argument*/ScriptableObject_CreateInstance_TisVuforiaConfiguration_t3823746026_m559723354_MethodInfo_var);
		return L_0;
	}
}
// System.Void Vuforia.VuforiaConfiguration::.ctor()
extern "C"  void VuforiaConfiguration__ctor_m2808042321 (VuforiaConfiguration_t3823746026 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaConfiguration__ctor_m2808042321_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaAbstractConfiguration_t1891710424_il2cpp_TypeInfo_var);
		VuforiaAbstractConfiguration__ctor_m1625969650(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaRuntimeInitialization::InitPlatform()
extern "C"  void VuforiaRuntimeInitialization_InitPlatform_m3197388960 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaRuntimeInitialization_InitPlatform_m3197388960_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaUnity_t657456673_il2cpp_TypeInfo_var);
		VuforiaUnity_SetAssetInitializationParameters_m3316561825(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntime_t2075282796_il2cpp_TypeInfo_var);
		VuforiaRuntime_t2075282796 * L_0 = VuforiaRuntime_get_Instance_m4069915631(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_1 = VuforiaRuntimeInitialization_CreateUnityPlayer_m1650732109(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		VuforiaRuntime_InitPlatform_m3306160978(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaRuntimeInitialization::InitVuforia()
extern "C"  void VuforiaRuntimeInitialization_InitVuforia_m2546903059 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaRuntimeInitialization_InitVuforia_m2546903059_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaAbstractConfiguration_t1891710424_il2cpp_TypeInfo_var);
		VuforiaAbstractConfiguration_t1891710424 * L_0 = VuforiaAbstractConfiguration_get_Instance_m3543652287(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		GenericVuforiaConfiguration_t3866211740 * L_1 = VuforiaAbstractConfiguration_get_Vuforia_m3334047132(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = GenericVuforiaConfiguration_get_DelayedInitialization_m282560601(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntime_t2075282796_il2cpp_TypeInfo_var);
		VuforiaRuntime_t2075282796 * L_3 = VuforiaRuntime_get_Instance_m4069915631(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		VuforiaRuntime_InitVuforia_m2353933398(L_3, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// Vuforia.IUnityPlayer Vuforia.VuforiaRuntimeInitialization::CreateUnityPlayer()
extern "C"  Il2CppObject * VuforiaRuntimeInitialization_CreateUnityPlayer_m1650732109 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaRuntimeInitialization_CreateUnityPlayer_m1650732109_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		NullUnityPlayer_t754446093 * L_0 = (NullUnityPlayer_t754446093 *)il2cpp_codegen_object_new(NullUnityPlayer_t754446093_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m483624113(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_001d;
		}
	}
	{
		AndroidUnityPlayer_t852788525 * L_2 = (AndroidUnityPlayer_t852788525 *)il2cpp_codegen_object_new(AndroidUnityPlayer_t852788525_il2cpp_TypeInfo_var);
		AndroidUnityPlayer__ctor_m2233000524(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0058;
	}

IL_001d:
	{
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_0033;
		}
	}
	{
		IOSUnityPlayer_t3656371703 * L_4 = (IOSUnityPlayer_t3656371703 *)il2cpp_codegen_object_new(IOSUnityPlayer_t3656371703_il2cpp_TypeInfo_var);
		IOSUnityPlayer__ctor_m676692974(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0058;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_5 = VuforiaRuntimeUtilities_IsPlayMode_m2939358997(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		PlayModeUnityPlayer_t918240325 * L_6 = (PlayModeUnityPlayer_t918240325 *)il2cpp_codegen_object_new(PlayModeUnityPlayer_t918240325_il2cpp_TypeInfo_var);
		PlayModeUnityPlayer__ctor_m2126346857(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0058;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_7 = VuforiaRuntimeUtilities_IsWSARuntime_m3848252715(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0058;
		}
	}
	{
		WSAUnityPlayer_t425981959 * L_8 = (WSAUnityPlayer_t425981959 *)il2cpp_codegen_object_new(WSAUnityPlayer_t425981959_il2cpp_TypeInfo_var);
		WSAUnityPlayer__ctor_m1279021334(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0058:
	{
		Il2CppObject * L_9 = V_0;
		return L_9;
	}
}
// System.Void Vuforia.VuMarkBehaviour::.ctor()
extern "C"  void VuMarkBehaviour__ctor_m1415860126 (VuMarkBehaviour_t2060629989 * __this, const MethodInfo* method)
{
	{
		VuMarkAbstractBehaviour__ctor_m326197713(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::.ctor()
extern "C"  void WireframeBehaviour__ctor_m420914080 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	{
		__this->set_ShowLines_3((bool)1);
		Color_t2020392075  L_0 = Color_get_green_m2671273823(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_LineColor_4(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::Start()
extern "C"  void WireframeBehaviour_Start_m2184757344 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_Start_m2184757344_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_mLineMaterial_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral599011397, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OnRenderObject()
extern "C"  void WireframeBehaviour_OnRenderObject_m2411339956 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_OnRenderObject_m2411339956_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)0;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)1;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_mLineMaterial_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral599011397, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_mLineMaterial_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 0, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_mLineMaterial_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral895546098, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)1));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)2));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)3));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OnDrawGizmos()
extern "C"  void WireframeBehaviour_OnDrawGizmos_m4179942854 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_OnDrawGizmos_m4179942854_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)2));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)3));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::.ctor()
extern "C"  void WireframeTrackableEventHandler__ctor_m4253736968 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::Start()
extern "C"  void WireframeTrackableEventHandler_Start_m1475947192 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_Start_m1475947192_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t1779888572 * L_0 = Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957_MethodInfo_var);
		__this->set_mTrackableBehaviour_2(L_0);
		TrackableBehaviour_t1779888572 * L_1 = __this->get_mTrackableBehaviour_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_3 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m1156666476(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void WireframeTrackableEventHandler_OnTrackableStateChanged_m106630617 (WireframeTrackableEventHandler_t1535150527 * __this, int32_t ___previousStatus0, int32_t ___newStatus1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___newStatus1;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___newStatus1;
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_OnTrackingFound_m563781220(__this, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_OnTrackingLost_m3180126272(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingFound()
extern "C"  void WireframeTrackableEventHandler_OnTrackingFound_m563781220 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_OnTrackingFound_m563781220_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)1, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)1, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)1, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1040926105, L_31, _stringLiteral759218142, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingLost()
extern "C"  void WireframeTrackableEventHandler_OnTrackingLost_m3180126272 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_OnTrackingLost_m3180126272_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)0, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)0, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1040926105, L_31, _stringLiteral3033297088, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WordBehaviour::.ctor()
extern "C"  void WordBehaviour__ctor_m581909702 (WordBehaviour_t3366478421 * __this, const MethodInfo* method)
{
	{
		WordAbstractBehaviour__ctor_m1415816009(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::.ctor()
extern "C"  void WSAUnityPlayer__ctor_m1279021334 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::LoadNativeLibraries()
extern "C"  void WSAUnityPlayer_LoadNativeLibraries_m2310474608 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::InitializePlatform()
extern "C"  void WSAUnityPlayer_InitializePlatform_m2450540007 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method)
{
	{
		WSAUnityPlayer_setPlatFormNative_m413995854(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VuforiaUnity/InitError Vuforia.WSAUnityPlayer::InitializeVuforia(System.String)
extern "C"  int32_t WSAUnityPlayer_InitializeVuforia_m1467576933 (WSAUnityPlayer_t425981959 * __this, String_t* ___licenseKey0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___licenseKey0;
		int32_t L_1 = WSAUnityPlayer_initVuforiaWSA_m2461582347(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		WSAUnityPlayer_InitializeSurface_m2485130165(__this, /*hidden argument*/NULL);
	}

IL_0014:
	{
		int32_t L_3 = V_0;
		return (int32_t)(L_3);
	}
}
// System.Void Vuforia.WSAUnityPlayer::StartScene()
extern "C"  void WSAUnityPlayer_StartScene_m2884648656 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::Update()
extern "C"  void WSAUnityPlayer_Update_m3453039605 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WSAUnityPlayer_Update_m3453039605_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t4096327849_il2cpp_TypeInfo_var);
		bool L_0 = SurfaceUtilities_HasSurfaceBeenRecreated_m2740261893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		WSAUnityPlayer_InitializeSurface_m2485130165(__this, /*hidden argument*/NULL);
		goto IL_002e;
	}

IL_0015:
	{
		int32_t L_1 = WSAUnityPlayer_GetActualScreenOrientation_m2906081196(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		int32_t L_3 = __this->get_mScreenOrientation_0();
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_002e;
		}
	}
	{
		WSAUnityPlayer_SetUnityScreenOrientation_m3964512799(__this, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::Dispose()
extern "C"  void WSAUnityPlayer_Dispose_m459169223 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::OnPause()
extern "C"  void WSAUnityPlayer_OnPause_m2815166635 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WSAUnityPlayer_OnPause_m2815166635_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaUnity_t657456673_il2cpp_TypeInfo_var);
		VuforiaUnity_OnPause_m2422224752(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::OnResume()
extern "C"  void WSAUnityPlayer_OnResume_m2572251502 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WSAUnityPlayer_OnResume_m2572251502_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaUnity_t657456673_il2cpp_TypeInfo_var);
		VuforiaUnity_OnResume_m2186520633(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::OnDestroy()
extern "C"  void WSAUnityPlayer_OnDestroy_m3845019931 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WSAUnityPlayer_OnDestroy_m3845019931_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaUnity_t657456673_il2cpp_TypeInfo_var);
		VuforiaUnity_Deinit_m4072609744(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::InitializeSurface()
extern "C"  void WSAUnityPlayer_InitializeSurface_m2485130165 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WSAUnityPlayer_InitializeSurface_m2485130165_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t4096327849_il2cpp_TypeInfo_var);
		SurfaceUtilities_OnSurfaceCreated_m3675640541(NULL /*static, unused*/, /*hidden argument*/NULL);
		WSAUnityPlayer_SetUnityScreenOrientation_m3964512799(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::SetUnityScreenOrientation()
extern "C"  void WSAUnityPlayer_SetUnityScreenOrientation_m3964512799 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WSAUnityPlayer_SetUnityScreenOrientation_m3964512799_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = WSAUnityPlayer_GetActualScreenOrientation_m2906081196(__this, /*hidden argument*/NULL);
		__this->set_mScreenOrientation_0(L_0);
		int32_t L_1 = __this->get_mScreenOrientation_0();
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t4096327849_il2cpp_TypeInfo_var);
		SurfaceUtilities_SetSurfaceOrientation_m3106547277(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_mScreenOrientation_0();
		WSAUnityPlayer_setSurfaceOrientationWSA_m2082052129(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.ScreenOrientation Vuforia.WSAUnityPlayer::GetActualScreenOrientation()
extern "C"  int32_t WSAUnityPlayer_GetActualScreenOrientation_m2906081196 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WSAUnityPlayer_GetActualScreenOrientation_m2906081196_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = Screen_get_orientation_m879255848(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((!(((uint32_t)L_1) == ((uint32_t)5))))
		{
			goto IL_0053;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_2 = Input_get_deviceOrientation_m2415424840(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = V_1;
		switch (((int32_t)((int32_t)L_3-(int32_t)1)))
		{
			case 0:
			{
				goto IL_003e;
			}
			case 1:
			{
				goto IL_0045;
			}
			case 2:
			{
				goto IL_0030;
			}
			case 3:
			{
				goto IL_0037;
			}
		}
	}
	{
		goto IL_004c;
	}

IL_0030:
	{
		V_0 = 3;
		goto IL_0053;
	}

IL_0037:
	{
		V_0 = 4;
		goto IL_0053;
	}

IL_003e:
	{
		V_0 = 1;
		goto IL_0053;
	}

IL_0045:
	{
		V_0 = 2;
		goto IL_0053;
	}

IL_004c:
	{
		V_0 = 3;
		goto IL_0053;
	}

IL_0053:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Void Vuforia.WSAUnityPlayer::setPlatFormNative()
extern "C"  void WSAUnityPlayer_setPlatFormNative_m413995854 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("VuforiaWrapper"), "setPlatFormNative", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'setPlatFormNative'"));
		}
	}

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Int32 Vuforia.WSAUnityPlayer::initVuforiaWSA(System.String)
extern "C"  int32_t WSAUnityPlayer_initVuforiaWSA_m2461582347 (Il2CppObject * __this /* static, unused */, String_t* ___licenseKey0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(char*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("VuforiaWrapper"), "initVuforiaWSA", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'initVuforiaWSA'"));
		}
	}

	// Marshaling of parameter '___licenseKey0' to native representation
	char* ____licenseKey0_marshaled = NULL;
	____licenseKey0_marshaled = il2cpp_codegen_marshal_string(___licenseKey0);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(____licenseKey0_marshaled);

	// Marshaling cleanup of parameter '___licenseKey0' native representation
	il2cpp_codegen_marshal_free(____licenseKey0_marshaled);
	____licenseKey0_marshaled = NULL;

	return returnValue;
}
// System.Void Vuforia.WSAUnityPlayer::setSurfaceOrientationWSA(System.Int32)
extern "C"  void WSAUnityPlayer_setSurfaceOrientationWSA_m2082052129 (Il2CppObject * __this /* static, unused */, int32_t ___screenOrientation0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("VuforiaWrapper"), "setSurfaceOrientationWSA", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'setSurfaceOrientationWSA'"));
		}
	}

	// Native function invocation
	il2cppPInvokeFunc(___screenOrientation0);

}
// System.Void vumark_levid::.ctor()
extern "C"  void vumark_levid__ctor_m3613878124 (vumark_levid_t1745599613 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VuMarkEventHandler::.ctor()
extern "C"  void VuMarkEventHandler__ctor_m3963129809 (VuMarkEventHandler_t2972421956 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VuMarkEventHandler::Start()
extern "C"  void VuMarkEventHandler_Start_m1800205201 (VuMarkEventHandler_t2972421956 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuMarkEventHandler_Start_m1800205201_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t1779888572 * L_0 = Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957_MethodInfo_var);
		__this->set_mTrackableBehaviour_2(L_0);
		TrackableBehaviour_t1779888572 * L_1 = __this->get_mTrackableBehaviour_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_3 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m1156666476(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void VuMarkEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void VuMarkEventHandler_OnTrackableStateChanged_m587379858 (VuMarkEventHandler_t2972421956 * __this, int32_t ___previousStatus0, int32_t ___newStatus1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___newStatus1;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = ___newStatus1;
		if ((((int32_t)L_1) == ((int32_t)3)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_2 = ___newStatus1;
		if ((!(((uint32_t)L_2) == ((uint32_t)4))))
		{
			goto IL_0020;
		}
	}

IL_0015:
	{
		VuMarkEventHandler_OnTrackingFound_m946784669(__this, /*hidden argument*/NULL);
		goto IL_0026;
	}

IL_0020:
	{
		VuMarkEventHandler_OnTrackingLost_m2215196871(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void VuMarkEventHandler::OnTrackingFound()
extern "C"  void VuMarkEventHandler_OnTrackingFound_m946784669 (VuMarkEventHandler_t2972421956 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuMarkEventHandler_OnTrackingFound_m946784669_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	Animator_t69676727 * V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		Animator_t69676727 * L_2 = Component_GetComponentInChildren_TisAnimator_t69676727_m501749323(__this, /*hidden argument*/Component_GetComponentInChildren_TisAnimator_t69676727_m501749323_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0035;
	}

IL_0022:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)1, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0035:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0022;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0060;
	}

IL_004b:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)1, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0060:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004b;
		}
	}
	{
		Animator_t69676727 * L_21 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0082;
		}
	}
	{
		Animator_t69676727 * L_23 = V_2;
		NullCheck(L_23);
		Animator_SetTrigger_m3418492570(L_23, _stringLiteral1689944869, /*hidden argument*/NULL);
	}

IL_0082:
	{
		TrackableBehaviour_t1779888572 * L_24 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_24);
		String_t* L_25 = TrackableBehaviour_get_TrackableName_m3173853042(L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1040926105, L_25, _stringLiteral759218142, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VuMarkEventHandler::OnTrackingLost()
extern "C"  void VuMarkEventHandler_OnTrackingLost_m2215196871 (VuMarkEventHandler_t2972421956 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuMarkEventHandler_OnTrackingLost_m2215196871_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	Renderer_t257310565 * V_2 = NULL;
	RendererU5BU5D_t2810717544* V_3 = NULL;
	int32_t V_4 = 0;
	Collider_t3497673348 * V_5 = NULL;
	ColliderU5BU5D_t462843629* V_6 = NULL;
	int32_t V_7 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		RendererU5BU5D_t2810717544* L_2 = V_0;
		V_3 = L_2;
		V_4 = 0;
		goto IL_002c;
	}

IL_001a:
	{
		RendererU5BU5D_t2810717544* L_3 = V_3;
		int32_t L_4 = V_4;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Renderer_t257310565 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_2 = L_6;
		Renderer_t257310565 * L_7 = V_2;
		NullCheck(L_7);
		Renderer_set_enabled_m142717579(L_7, (bool)0, /*hidden argument*/NULL);
		int32_t L_8 = V_4;
		V_4 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002c:
	{
		int32_t L_9 = V_4;
		RendererU5BU5D_t2810717544* L_10 = V_3;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_11 = V_1;
		V_6 = L_11;
		V_7 = 0;
		goto IL_0056;
	}

IL_0041:
	{
		ColliderU5BU5D_t462843629* L_12 = V_6;
		int32_t L_13 = V_7;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		Collider_t3497673348 * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_5 = L_15;
		Collider_t3497673348 * L_16 = V_5;
		NullCheck(L_16);
		Collider_set_enabled_m3489100454(L_16, (bool)0, /*hidden argument*/NULL);
		int32_t L_17 = V_7;
		V_7 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0056:
	{
		int32_t L_18 = V_7;
		ColliderU5BU5D_t462843629* L_19 = V_6;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length)))))))
		{
			goto IL_0041;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_20 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_20);
		String_t* L_21 = TrackableBehaviour_get_TrackableName_m3173853042(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1040926105, L_21, _stringLiteral3033297088, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VuMarkHandler::.ctor()
extern "C"  void VuMarkHandler__ctor_m296874417 (VuMarkHandler_t583794080 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VuMarkHandler::Start()
extern "C"  void VuMarkHandler_Start_m2704101265 (VuMarkHandler_t583794080 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuMarkHandler_Start_m2704101265_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PanelShowHide_t775446595 * L_0 = Component_GetComponent_TisPanelShowHide_t775446595_m2828053312(__this, /*hidden argument*/Component_GetComponent_TisPanelShowHide_t775446595_m2828053312_MethodInfo_var);
		__this->set_mIdPanel_2(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t308318605_il2cpp_TypeInfo_var);
		TrackerManager_t308318605 * L_1 = TrackerManager_get_Instance_m2067083115(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		StateManager_t3369465942 * L_2 = VirtFuncInvoker0< StateManager_t3369465942 * >::Invoke(7 /* Vuforia.StateManager Vuforia.TrackerManager::GetStateManager() */, L_1);
		NullCheck(L_2);
		VuMarkManager_t3604726399 * L_3 = VirtFuncInvoker0< VuMarkManager_t3604726399 * >::Invoke(9 /* Vuforia.VuMarkManager Vuforia.StateManager::GetVuMarkManager() */, L_2);
		__this->set_mVuMarkManager_3(L_3);
		VuMarkManager_t3604726399 * L_4 = __this->get_mVuMarkManager_3();
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)VuMarkHandler_OnVuMarkDetected_m1182417521_MethodInfo_var);
		Action_1_t24784135 * L_6 = (Action_1_t24784135 *)il2cpp_codegen_object_new(Action_1_t24784135_il2cpp_TypeInfo_var);
		Action_1__ctor_m91845076(L_6, __this, L_5, /*hidden argument*/Action_1__ctor_m91845076_MethodInfo_var);
		NullCheck(L_4);
		VirtActionInvoker1< Action_1_t24784135 * >::Invoke(8 /* System.Void Vuforia.VuMarkManager::RegisterVuMarkDetectedCallback(System.Action`1<Vuforia.VuMarkTarget>) */, L_4, L_6);
		VuMarkManager_t3604726399 * L_7 = __this->get_mVuMarkManager_3();
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)VuMarkHandler_OnVuMarkLost_m2126394563_MethodInfo_var);
		Action_1_t24784135 * L_9 = (Action_1_t24784135 *)il2cpp_codegen_object_new(Action_1_t24784135_il2cpp_TypeInfo_var);
		Action_1__ctor_m91845076(L_9, __this, L_8, /*hidden argument*/Action_1__ctor_m91845076_MethodInfo_var);
		NullCheck(L_7);
		VirtActionInvoker1< Action_1_t24784135 * >::Invoke(10 /* System.Void Vuforia.VuMarkManager::RegisterVuMarkLostCallback(System.Action`1<Vuforia.VuMarkTarget>) */, L_7, L_9);
		return;
	}
}
// System.Void VuMarkHandler::Update()
extern "C"  void VuMarkHandler_Update_m498336142 (VuMarkHandler_t583794080 * __this, const MethodInfo* method)
{
	{
		VuMarkHandler_UpdateClosestTarget_m578840658(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VuMarkHandler::OnDestroy()
extern "C"  void VuMarkHandler_OnDestroy_m3979816830 (VuMarkHandler_t583794080 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuMarkHandler_OnDestroy_m3979816830_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VuMarkManager_t3604726399 * L_0 = __this->get_mVuMarkManager_3();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)VuMarkHandler_OnVuMarkDetected_m1182417521_MethodInfo_var);
		Action_1_t24784135 * L_2 = (Action_1_t24784135 *)il2cpp_codegen_object_new(Action_1_t24784135_il2cpp_TypeInfo_var);
		Action_1__ctor_m91845076(L_2, __this, L_1, /*hidden argument*/Action_1__ctor_m91845076_MethodInfo_var);
		NullCheck(L_0);
		VirtActionInvoker1< Action_1_t24784135 * >::Invoke(9 /* System.Void Vuforia.VuMarkManager::UnregisterVuMarkDetectedCallback(System.Action`1<Vuforia.VuMarkTarget>) */, L_0, L_2);
		VuMarkManager_t3604726399 * L_3 = __this->get_mVuMarkManager_3();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)VuMarkHandler_OnVuMarkLost_m2126394563_MethodInfo_var);
		Action_1_t24784135 * L_5 = (Action_1_t24784135 *)il2cpp_codegen_object_new(Action_1_t24784135_il2cpp_TypeInfo_var);
		Action_1__ctor_m91845076(L_5, __this, L_4, /*hidden argument*/Action_1__ctor_m91845076_MethodInfo_var);
		NullCheck(L_3);
		VirtActionInvoker1< Action_1_t24784135 * >::Invoke(11 /* System.Void Vuforia.VuMarkManager::UnregisterVuMarkLostCallback(System.Action`1<Vuforia.VuMarkTarget>) */, L_3, L_5);
		return;
	}
}
// System.Void VuMarkHandler::OnVuMarkDetected(Vuforia.VuMarkTarget)
extern "C"  void VuMarkHandler_OnVuMarkDetected_m1182417521 (VuMarkHandler_t583794080 * __this, Il2CppObject * ___target0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuMarkHandler_OnVuMarkDetected_m1182417521_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		String_t* L_1 = VuMarkHandler_GetVuMarkString_m589384032(__this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1258021450, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VuMarkHandler::OnVuMarkLost(Vuforia.VuMarkTarget)
extern "C"  void VuMarkHandler_OnVuMarkLost_m2126394563 (VuMarkHandler_t583794080 * __this, Il2CppObject * ___target0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuMarkHandler_OnVuMarkLost_m2126394563_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObjectU5BU5D_t3057952154* V_0 = NULL;
	GameObject_t1756533147 * V_1 = NULL;
	GameObjectU5BU5D_t3057952154* V_2 = NULL;
	int32_t V_3 = 0;
	ObjectU5BU5D_t4217747464* G_B2_0 = NULL;
	ObjectU5BU5D_t4217747464* G_B1_0 = NULL;
	Il2CppObject* G_B4_0 = NULL;
	Il2CppObject* G_B3_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		String_t* L_1 = VuMarkHandler_GetVuMarkString_m589384032(__this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral4104255622, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Text_t356221433 * L_3 = __this->get_Txt_action_8();
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, _stringLiteral768014653);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(GameObject_t1756533147_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t4217747464* L_5 = Object_FindObjectsOfType_m2121813744(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Func_2_t2495701363 * L_6 = ((VuMarkHandler_t583794080_StaticFields*)VuMarkHandler_t583794080_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_9();
		G_B1_0 = L_5;
		if (L_6)
		{
			G_B2_0 = L_5;
			goto IL_004d;
		}
	}
	{
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)VuMarkHandler_U3COnVuMarkLostU3Em__0_m3059917024_MethodInfo_var);
		Func_2_t2495701363 * L_8 = (Func_2_t2495701363 *)il2cpp_codegen_object_new(Func_2_t2495701363_il2cpp_TypeInfo_var);
		Func_2__ctor_m2347799250(L_8, NULL, L_7, /*hidden argument*/Func_2__ctor_m2347799250_MethodInfo_var);
		((VuMarkHandler_t583794080_StaticFields*)VuMarkHandler_t583794080_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_9(L_8);
		G_B2_0 = G_B1_0;
	}

IL_004d:
	{
		Func_2_t2495701363 * L_9 = ((VuMarkHandler_t583794080_StaticFields*)VuMarkHandler_t583794080_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_9();
		Il2CppObject* L_10 = Enumerable_Select_TisObject_t1021602117_TisGameObject_t1756533147_m655585110(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)G_B2_0, L_9, /*hidden argument*/Enumerable_Select_TisObject_t1021602117_TisGameObject_t1756533147_m655585110_MethodInfo_var);
		Func_2_t3404947624 * L_11 = ((VuMarkHandler_t583794080_StaticFields*)VuMarkHandler_t583794080_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_10();
		G_B3_0 = L_10;
		if (L_11)
		{
			G_B4_0 = L_10;
			goto IL_006f;
		}
	}
	{
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)VuMarkHandler_U3COnVuMarkLostU3Em__1_m1693396336_MethodInfo_var);
		Func_2_t3404947624 * L_13 = (Func_2_t3404947624 *)il2cpp_codegen_object_new(Func_2_t3404947624_il2cpp_TypeInfo_var);
		Func_2__ctor_m3594470121(L_13, NULL, L_12, /*hidden argument*/Func_2__ctor_m3594470121_MethodInfo_var);
		((VuMarkHandler_t583794080_StaticFields*)VuMarkHandler_t583794080_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_10(L_13);
		G_B4_0 = G_B3_0;
	}

IL_006f:
	{
		Func_2_t3404947624 * L_14 = ((VuMarkHandler_t583794080_StaticFields*)VuMarkHandler_t583794080_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_10();
		Il2CppObject* L_15 = Enumerable_Where_TisGameObject_t1756533147_m1880019006(NULL /*static, unused*/, G_B4_0, L_14, /*hidden argument*/Enumerable_Where_TisGameObject_t1756533147_m1880019006_MethodInfo_var);
		GameObjectU5BU5D_t3057952154* L_16 = Enumerable_ToArray_TisGameObject_t1756533147_m2652891882(NULL /*static, unused*/, L_15, /*hidden argument*/Enumerable_ToArray_TisGameObject_t1756533147_m2652891882_MethodInfo_var);
		V_0 = L_16;
		GameObjectU5BU5D_t3057952154* L_17 = V_0;
		V_2 = L_17;
		V_3 = 0;
		goto IL_0096;
	}

IL_0088:
	{
		GameObjectU5BU5D_t3057952154* L_18 = V_2;
		int32_t L_19 = V_3;
		NullCheck(L_18);
		int32_t L_20 = L_19;
		GameObject_t1756533147 * L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		V_1 = L_21;
		GameObject_t1756533147 * L_22 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		int32_t L_23 = V_3;
		V_3 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_0096:
	{
		int32_t L_24 = V_3;
		GameObjectU5BU5D_t3057952154* L_25 = V_2;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_25)->max_length)))))))
		{
			goto IL_0088;
		}
	}
	{
		return;
	}
}
// System.Void VuMarkHandler::UpdateClosestTarget()
extern "C"  void VuMarkHandler_UpdateClosestTarget_m578840658 (VuMarkHandler_t583794080 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuMarkHandler_UpdateClosestTarget_m578840658_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Camera_t189460977 * V_0 = NULL;
	float V_1 = 0.0f;
	VuMarkAbstractBehaviour_t1830666997 * V_2 = NULL;
	Il2CppObject* V_3 = NULL;
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	String_t* V_6 = NULL;
	Sprite_t309593783 * V_7 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Camera_t189460977 * G_B2_0 = NULL;
	Camera_t189460977 * G_B1_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(DigitalEyewearARController_t1398758191_il2cpp_TypeInfo_var);
		DigitalEyewearARController_t1398758191 * L_0 = DigitalEyewearARController_get_Instance_m277595763(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Camera_t189460977 * L_1 = DigitalEyewearARController_get_PrimaryCamera_m334515774(L_0, /*hidden argument*/NULL);
		Camera_t189460977 * L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_0016;
		}
	}
	{
		Camera_t189460977 * L_3 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B2_0 = L_3;
	}

IL_0016:
	{
		V_0 = G_B2_0;
		V_1 = (std::numeric_limits<float>::infinity());
		VuMarkManager_t3604726399 * L_4 = __this->get_mVuMarkManager_3();
		NullCheck(L_4);
		Il2CppObject* L_5 = VirtFuncInvoker0< Il2CppObject* >::Invoke(6 /* System.Collections.Generic.IEnumerable`1<Vuforia.VuMarkAbstractBehaviour> Vuforia.VuMarkManager::GetActiveBehaviours() */, L_4);
		NullCheck(L_5);
		Il2CppObject* L_6 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Vuforia.VuMarkAbstractBehaviour>::GetEnumerator() */, IEnumerable_1_t2122794042_il2cpp_TypeInfo_var, L_5);
		V_3 = L_6;
	}

IL_002e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00a6;
		}

IL_0033:
		{
			Il2CppObject* L_7 = V_3;
			NullCheck(L_7);
			VuMarkAbstractBehaviour_t1830666997 * L_8 = InterfaceFuncInvoker0< VuMarkAbstractBehaviour_t1830666997 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Vuforia.VuMarkAbstractBehaviour>::get_Current() */, IEnumerator_1_t3601158120_il2cpp_TypeInfo_var, L_7);
			V_2 = L_8;
			VuMarkAbstractBehaviour_t1830666997 * L_9 = V_2;
			NullCheck(L_9);
			Transform_t3275118058 * L_10 = Component_get_transform_m2697483695(L_9, /*hidden argument*/NULL);
			NullCheck(L_10);
			Vector3_t2243707580  L_11 = Transform_get_position_m1104419803(L_10, /*hidden argument*/NULL);
			V_4 = L_11;
			Camera_t189460977 * L_12 = V_0;
			NullCheck(L_12);
			Transform_t3275118058 * L_13 = Component_get_transform_m2697483695(L_12, /*hidden argument*/NULL);
			Vector3_t2243707580  L_14 = V_4;
			NullCheck(L_13);
			Vector3_t2243707580  L_15 = Transform_InverseTransformPoint_m2648491174(L_13, L_14, /*hidden argument*/NULL);
			V_5 = L_15;
			Vector2_t2243707579  L_16 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
			Vector3_t2243707580  L_17 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
			Vector3_t2243707580  L_18 = V_5;
			float L_19 = Vector3_Distance_m1859670022(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
			__this->set_distance_6(L_19);
			float L_20 = __this->get_distance_6();
			float L_21 = L_20;
			Il2CppObject * L_22 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_21);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_23 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3560472239, L_22, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_Log_m920475918(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
			float L_24 = __this->get_distance_6();
			float L_25 = V_1;
			if ((!(((float)L_24) < ((float)L_25))))
			{
				goto IL_00a6;
			}
		}

IL_0093:
		{
			float L_26 = __this->get_distance_6();
			V_1 = L_26;
			VuMarkAbstractBehaviour_t1830666997 * L_27 = V_2;
			NullCheck(L_27);
			Il2CppObject * L_28 = VuMarkAbstractBehaviour_get_VuMarkTarget_m4042803881(L_27, /*hidden argument*/NULL);
			__this->set_mClosestVuMark_4(L_28);
		}

IL_00a6:
		{
			Il2CppObject* L_29 = V_3;
			NullCheck(L_29);
			bool L_30 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_29);
			if (L_30)
			{
				goto IL_0033;
			}
		}

IL_00b1:
		{
			IL2CPP_LEAVE(0xC3, FINALLY_00b6);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00b6;
	}

FINALLY_00b6:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_31 = V_3;
			if (!L_31)
			{
				goto IL_00c2;
			}
		}

IL_00bc:
		{
			Il2CppObject* L_32 = V_3;
			NullCheck(L_32);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_32);
		}

IL_00c2:
		{
			IL2CPP_END_FINALLY(182)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(182)
	{
		IL2CPP_JUMP_TBL(0xC3, IL_00c3)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00c3:
	{
		Il2CppObject * L_33 = __this->get_mClosestVuMark_4();
		if (!L_33)
		{
			goto IL_0143;
		}
	}
	{
		Il2CppObject * L_34 = __this->get_mCurrentVuMark_5();
		Il2CppObject * L_35 = __this->get_mClosestVuMark_4();
		if ((((Il2CppObject*)(Il2CppObject *)L_34) == ((Il2CppObject*)(Il2CppObject *)L_35)))
		{
			goto IL_0143;
		}
	}
	{
		Il2CppObject * L_36 = __this->get_mClosestVuMark_4();
		String_t* L_37 = VuMarkHandler_GetVuMarkString_m589384032(__this, L_36, /*hidden argument*/NULL);
		__this->set_vuMarkId_7(L_37);
		Il2CppObject * L_38 = __this->get_mClosestVuMark_4();
		String_t* L_39 = VuMarkHandler_GetVuMarkDataType_m3559465785(__this, L_38, /*hidden argument*/NULL);
		V_6 = L_39;
		Il2CppObject * L_40 = __this->get_mClosestVuMark_4();
		Sprite_t309593783 * L_41 = VuMarkHandler_GetVuMarkImage_m2423536162(__this, L_40, /*hidden argument*/NULL);
		V_7 = L_41;
		Il2CppObject * L_42 = __this->get_mClosestVuMark_4();
		__this->set_mCurrentVuMark_5(L_42);
		GameObject_t1756533147 * L_43 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3886465728, /*hidden argument*/NULL);
		NullCheck(L_43);
		NavMeshAgent_t2761625415 * L_44 = GameObject_GetComponent_TisNavMeshAgent_t2761625415_m2898296133(L_43, /*hidden argument*/GameObject_GetComponent_TisNavMeshAgent_t2761625415_m2898296133_MethodInfo_var);
		NullCheck(L_44);
		Behaviour_set_enabled_m1796096907(L_44, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_45 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3886465728, /*hidden argument*/NULL);
		NullCheck(L_45);
		LineRenderer_t849157671 * L_46 = GameObject_GetComponent_TisLineRenderer_t849157671_m3748809323(L_45, /*hidden argument*/GameObject_GetComponent_TisLineRenderer_t849157671_m3748809323_MethodInfo_var);
		NullCheck(L_46);
		Renderer_set_enabled_m142717579(L_46, (bool)0, /*hidden argument*/NULL);
	}

IL_0143:
	{
		return;
	}
}
// System.Collections.IEnumerator VuMarkHandler::ShowPanelAfter(System.Single,System.String,System.String,UnityEngine.Sprite)
extern "C"  Il2CppObject * VuMarkHandler_ShowPanelAfter_m2879824669 (VuMarkHandler_t583794080 * __this, float ___seconds0, String_t* ___vuMarkTitle1, String_t* ___vuMarkId2, Sprite_t309593783 * ___vuMarkImage3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuMarkHandler_ShowPanelAfter_m2879824669_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CShowPanelAfterU3Ec__Iterator0_t1558983881 * V_0 = NULL;
	{
		U3CShowPanelAfterU3Ec__Iterator0_t1558983881 * L_0 = (U3CShowPanelAfterU3Ec__Iterator0_t1558983881 *)il2cpp_codegen_object_new(U3CShowPanelAfterU3Ec__Iterator0_t1558983881_il2cpp_TypeInfo_var);
		U3CShowPanelAfterU3Ec__Iterator0__ctor_m249051542(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CShowPanelAfterU3Ec__Iterator0_t1558983881 * L_1 = V_0;
		float L_2 = ___seconds0;
		NullCheck(L_1);
		L_1->set_seconds_0(L_2);
		U3CShowPanelAfterU3Ec__Iterator0_t1558983881 * L_3 = V_0;
		String_t* L_4 = ___vuMarkTitle1;
		NullCheck(L_3);
		L_3->set_vuMarkTitle_1(L_4);
		U3CShowPanelAfterU3Ec__Iterator0_t1558983881 * L_5 = V_0;
		String_t* L_6 = ___vuMarkId2;
		NullCheck(L_5);
		L_5->set_vuMarkId_2(L_6);
		U3CShowPanelAfterU3Ec__Iterator0_t1558983881 * L_7 = V_0;
		Sprite_t309593783 * L_8 = ___vuMarkImage3;
		NullCheck(L_7);
		L_7->set_vuMarkImage_3(L_8);
		U3CShowPanelAfterU3Ec__Iterator0_t1558983881 * L_9 = V_0;
		NullCheck(L_9);
		L_9->set_U24this_4(__this);
		U3CShowPanelAfterU3Ec__Iterator0_t1558983881 * L_10 = V_0;
		return L_10;
	}
}
// System.String VuMarkHandler::GetVuMarkDataType(Vuforia.VuMarkTarget)
extern "C"  String_t* VuMarkHandler_GetVuMarkDataType_m3559465785 (VuMarkHandler_t583794080 * __this, Il2CppObject * ___vumark0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuMarkHandler_GetVuMarkDataType_m3559465785_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Il2CppObject * L_0 = ___vumark0;
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* Vuforia.InstanceId Vuforia.VuMarkTarget::get_InstanceId() */, VuMarkTarget_t222984753_il2cpp_TypeInfo_var, L_0);
		NullCheck(L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* Vuforia.InstanceIdType Vuforia.InstanceId::get_DataType() */, InstanceId_t1732537268_il2cpp_TypeInfo_var, L_1);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)1)))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)2)))
		{
			goto IL_0031;
		}
	}
	{
		goto IL_0037;
	}

IL_0025:
	{
		return _stringLiteral1608738781;
	}

IL_002b:
	{
		return _stringLiteral3311046853;
	}

IL_0031:
	{
		return _stringLiteral2115023907;
	}

IL_0037:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_6;
	}
}
// System.String VuMarkHandler::GetVuMarkString(Vuforia.VuMarkTarget)
extern "C"  String_t* VuMarkHandler_GetVuMarkString_m589384032 (VuMarkHandler_t583794080 * __this, Il2CppObject * ___vumark0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuMarkHandler_GetVuMarkString_m589384032_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	uint64_t V_1 = 0;
	{
		Il2CppObject * L_0 = ___vumark0;
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* Vuforia.InstanceId Vuforia.VuMarkTarget::get_InstanceId() */, VuMarkTarget_t222984753_il2cpp_TypeInfo_var, L_0);
		NullCheck(L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* Vuforia.InstanceIdType Vuforia.InstanceId::get_DataType() */, InstanceId_t1732537268_il2cpp_TypeInfo_var, L_1);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)1)))
		{
			goto IL_0031;
		}
	}
	{
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)2)))
		{
			goto IL_003d;
		}
	}
	{
		goto IL_0057;
	}

IL_0025:
	{
		Il2CppObject * L_6 = ___vumark0;
		NullCheck(L_6);
		Il2CppObject * L_7 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* Vuforia.InstanceId Vuforia.VuMarkTarget::get_InstanceId() */, VuMarkTarget_t222984753_il2cpp_TypeInfo_var, L_6);
		NullCheck(L_7);
		String_t* L_8 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String Vuforia.InstanceId::get_HexStringValue() */, InstanceId_t1732537268_il2cpp_TypeInfo_var, L_7);
		return L_8;
	}

IL_0031:
	{
		Il2CppObject * L_9 = ___vumark0;
		NullCheck(L_9);
		Il2CppObject * L_10 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* Vuforia.InstanceId Vuforia.VuMarkTarget::get_InstanceId() */, VuMarkTarget_t222984753_il2cpp_TypeInfo_var, L_9);
		NullCheck(L_10);
		String_t* L_11 = InterfaceFuncInvoker0< String_t* >::Invoke(3 /* System.String Vuforia.InstanceId::get_StringValue() */, InstanceId_t1732537268_il2cpp_TypeInfo_var, L_10);
		return L_11;
	}

IL_003d:
	{
		Il2CppObject * L_12 = ___vumark0;
		NullCheck(L_12);
		Il2CppObject * L_13 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* Vuforia.InstanceId Vuforia.VuMarkTarget::get_InstanceId() */, VuMarkTarget_t222984753_il2cpp_TypeInfo_var, L_12);
		NullCheck(L_13);
		uint64_t L_14 = InterfaceFuncInvoker0< uint64_t >::Invoke(4 /* System.UInt64 Vuforia.InstanceId::get_NumericValue() */, InstanceId_t1732537268_il2cpp_TypeInfo_var, L_13);
		V_1 = L_14;
		String_t* L_15 = UInt64_ToString_m446228920((&V_1), /*hidden argument*/NULL);
		return L_15;
	}

IL_0057:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_16;
	}
}
// UnityEngine.Sprite VuMarkHandler::GetVuMarkImage(Vuforia.VuMarkTarget)
extern "C"  Sprite_t309593783 * VuMarkHandler_GetVuMarkImage_m2423536162 (VuMarkHandler_t583794080 * __this, Il2CppObject * ___vumark0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuMarkHandler_GetVuMarkImage_m2423536162_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Image_t1391689025 * V_0 = NULL;
	Texture2D_t3542995729 * V_1 = NULL;
	Rect_t3681755626  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Il2CppObject * L_0 = ___vumark0;
		NullCheck(L_0);
		Image_t1391689025 * L_1 = InterfaceFuncInvoker0< Image_t1391689025 * >::Invoke(2 /* Vuforia.Image Vuforia.VuMarkTarget::get_InstanceImage() */, VuMarkTarget_t222984753_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		Image_t1391689025 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral4080548243, /*hidden argument*/NULL);
		return (Sprite_t309593783 *)NULL;
	}

IL_0019:
	{
		Image_t1391689025 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 Vuforia.Image::get_Width() */, L_3);
		Image_t1391689025 * L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Vuforia.Image::get_Height() */, L_5);
		Texture2D_t3542995729 * L_7 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1873923924(L_7, L_4, L_6, 4, (bool)0, /*hidden argument*/NULL);
		V_1 = L_7;
		Texture2D_t3542995729 * L_8 = V_1;
		NullCheck(L_8);
		Texture_set_wrapMode_m333956747(L_8, 1, /*hidden argument*/NULL);
		Image_t1391689025 * L_9 = V_0;
		Texture2D_t3542995729 * L_10 = V_1;
		NullCheck(L_9);
		VirtActionInvoker1< Texture2D_t3542995729 * >::Invoke(19 /* System.Void Vuforia.Image::CopyToTexture(UnityEngine.Texture2D) */, L_9, L_10);
		Texture2D_t3542995729 * L_11 = V_1;
		NullCheck(L_11);
		Texture2D_Apply_m3543341930(L_11, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_12);
		Texture2D_t3542995729 * L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_14);
		Rect__ctor_m1220545469((&V_2), (0.0f), (0.0f), (((float)((float)L_13))), (((float)((float)L_15))), /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_16 = V_1;
		Rect_t3681755626  L_17 = V_2;
		Vector2_t2243707579  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector2__ctor_m3067419446(&L_18, (0.5f), (0.5f), /*hidden argument*/NULL);
		Sprite_t309593783 * L_19 = Sprite_Create_m3262956430(NULL /*static, unused*/, L_16, L_17, L_18, /*hidden argument*/NULL);
		return L_19;
	}
}
// UnityEngine.GameObject VuMarkHandler::<OnVuMarkLost>m__0(UnityEngine.Object)
extern "C"  GameObject_t1756533147 * VuMarkHandler_U3COnVuMarkLostU3Em__0_m3059917024 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___g0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuMarkHandler_U3COnVuMarkLostU3Em__0_m3059917024_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object_t1021602117 * L_0 = ___g0;
		return ((GameObject_t1756533147 *)IsInstSealed(L_0, GameObject_t1756533147_il2cpp_TypeInfo_var));
	}
}
// System.Boolean VuMarkHandler::<OnVuMarkLost>m__1(UnityEngine.GameObject)
extern "C"  bool VuMarkHandler_U3COnVuMarkLostU3Em__1_m1693396336 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___g0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuMarkHandler_U3COnVuMarkLostU3Em__1_m1693396336_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___g0;
		NullCheck(L_0);
		String_t* L_1 = Object_get_name_m2079638459(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, _stringLiteral1971614873, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void VuMarkHandler/<ShowPanelAfter>c__Iterator0::.ctor()
extern "C"  void U3CShowPanelAfterU3Ec__Iterator0__ctor_m249051542 (U3CShowPanelAfterU3Ec__Iterator0_t1558983881 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VuMarkHandler/<ShowPanelAfter>c__Iterator0::MoveNext()
extern "C"  bool U3CShowPanelAfterU3Ec__Iterator0_MoveNext_m1652436950 (U3CShowPanelAfterU3Ec__Iterator0_t1558983881 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CShowPanelAfterU3Ec__Iterator0_MoveNext_m1652436950_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0046;
			}
		}
	}
	{
		goto IL_006f;
	}

IL_0021:
	{
		float L_2 = __this->get_seconds_0();
		WaitForSeconds_t3839502067 * L_3 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U24current_5(L_3);
		bool L_4 = __this->get_U24disposing_6();
		if (L_4)
		{
			goto IL_0041;
		}
	}
	{
		__this->set_U24PC_7(1);
	}

IL_0041:
	{
		goto IL_0071;
	}

IL_0046:
	{
		VuMarkHandler_t583794080 * L_5 = __this->get_U24this_4();
		NullCheck(L_5);
		PanelShowHide_t775446595 * L_6 = L_5->get_mIdPanel_2();
		String_t* L_7 = __this->get_vuMarkTitle_1();
		String_t* L_8 = __this->get_vuMarkId_2();
		Sprite_t309593783 * L_9 = __this->get_vuMarkImage_3();
		NullCheck(L_6);
		PanelShowHide_Show_m438154159(L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		__this->set_U24PC_7((-1));
	}

IL_006f:
	{
		return (bool)0;
	}

IL_0071:
	{
		return (bool)1;
	}
}
// System.Object VuMarkHandler/<ShowPanelAfter>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShowPanelAfterU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3385936434 (U3CShowPanelAfterU3Ec__Iterator0_t1558983881 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object VuMarkHandler/<ShowPanelAfter>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShowPanelAfterU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2310484906 (U3CShowPanelAfterU3Ec__Iterator0_t1558983881 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Void VuMarkHandler/<ShowPanelAfter>c__Iterator0::Dispose()
extern "C"  void U3CShowPanelAfterU3Ec__Iterator0_Dispose_m2793519947 (U3CShowPanelAfterU3Ec__Iterator0_t1558983881 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_6((bool)1);
		__this->set_U24PC_7((-1));
		return;
	}
}
// System.Void VuMarkHandler/<ShowPanelAfter>c__Iterator0::Reset()
extern "C"  void U3CShowPanelAfterU3Ec__Iterator0_Reset_m1930662853 (U3CShowPanelAfterU3Ec__Iterator0_t1558983881 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CShowPanelAfterU3Ec__Iterator0_Reset_m1930662853_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void VuMarkTapHandler::.ctor()
extern "C"  void VuMarkTapHandler__ctor_m808563102 (VuMarkTapHandler_t2561605313 * __this, const MethodInfo* method)
{
	{
		TapHandler__ctor_m3084098962(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VuMarkTapHandler::OnSingleTap()
extern "C"  void VuMarkTapHandler_OnSingleTap_m491386116 (VuMarkTapHandler_t2561605313 * __this, const MethodInfo* method)
{
	{
		TapHandler_OnSingleTap_m916998152(__this, /*hidden argument*/NULL);
		bool L_0 = VuMarkTapHandler_CheckVuMarkCardHit_m945028729(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		((TapHandler_t3409799063 *)__this)->set_mTapCount_5(0);
	}

IL_0018:
	{
		return;
	}
}
// System.Boolean VuMarkTapHandler::CheckVuMarkCardHit()
extern "C"  bool VuMarkTapHandler_CheckVuMarkCardHit_m945028729 (VuMarkTapHandler_t2561605313 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuMarkTapHandler_CheckVuMarkCardHit_m945028729_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3685274804 * V_0 = NULL;
	PointerEventData_t1599784723 * V_1 = NULL;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t V_4 = 0;
	GameObject_t1756533147 * V_5 = NULL;
	RaycastResult_t21186376  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Animator_t69676727 * V_7 = NULL;
	{
		List_1_t3685274804 * L_0 = (List_1_t3685274804 *)il2cpp_codegen_object_new(List_1_t3685274804_il2cpp_TypeInfo_var);
		List_1__ctor_m2989057823(L_0, /*hidden argument*/List_1__ctor_m2989057823_MethodInfo_var);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(EventSystem_t3466835263_il2cpp_TypeInfo_var);
		EventSystem_t3466835263 * L_1 = EventSystem_get_current_m319019811(NULL /*static, unused*/, /*hidden argument*/NULL);
		PointerEventData_t1599784723 * L_2 = (PointerEventData_t1599784723 *)il2cpp_codegen_object_new(PointerEventData_t1599784723_il2cpp_TypeInfo_var);
		PointerEventData__ctor_m3674067728(L_2, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		PointerEventData_t1599784723 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_4 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = (&V_2)->get_x_1();
		Vector3_t2243707580  L_6 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_6;
		float L_7 = (&V_3)->get_y_2();
		Vector2_t2243707579  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector2__ctor_m3067419446(&L_8, L_5, L_7, /*hidden argument*/NULL);
		NullCheck(L_3);
		PointerEventData_set_position_m794507622(L_3, L_8, /*hidden argument*/NULL);
		EventSystem_t3466835263 * L_9 = EventSystem_get_current_m319019811(NULL /*static, unused*/, /*hidden argument*/NULL);
		PointerEventData_t1599784723 * L_10 = V_1;
		List_1_t3685274804 * L_11 = V_0;
		NullCheck(L_9);
		EventSystem_RaycastAll_m4000413739(L_9, L_10, L_11, /*hidden argument*/NULL);
		V_4 = 0;
		goto IL_00ac;
	}

IL_004a:
	{
		List_1_t3685274804 * L_12 = V_0;
		int32_t L_13 = V_4;
		NullCheck(L_12);
		RaycastResult_t21186376  L_14 = List_1_get_Item_m3435089276(L_12, L_13, /*hidden argument*/List_1_get_Item_m3435089276_MethodInfo_var);
		V_6 = L_14;
		GameObject_t1756533147 * L_15 = RaycastResult_get_gameObject_m2999022658((&V_6), /*hidden argument*/NULL);
		V_5 = L_15;
		GameObject_t1756533147 * L_16 = __this->get_VuMarkCard_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00a6;
		}
	}
	{
		GameObject_t1756533147 * L_18 = V_5;
		GameObject_t1756533147 * L_19 = __this->get_VuMarkCard_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00a6;
		}
	}
	{
		GameObject_t1756533147 * L_21 = __this->get_VuMarkCard_6();
		NullCheck(L_21);
		Animator_t69676727 * L_22 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_21, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		V_7 = L_22;
		Animator_t69676727 * L_23 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_24 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00a4;
		}
	}
	{
		Animator_t69676727 * L_25 = V_7;
		NullCheck(L_25);
		Animator_SetTrigger_m3418492570(L_25, _stringLiteral2728995068, /*hidden argument*/NULL);
	}

IL_00a4:
	{
		return (bool)1;
	}

IL_00a6:
	{
		int32_t L_26 = V_4;
		V_4 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00ac:
	{
		int32_t L_27 = V_4;
		List_1_t3685274804 * L_28 = V_0;
		NullCheck(L_28);
		int32_t L_29 = List_1_get_Count_m3279745867(L_28, /*hidden argument*/List_1_get_Count_m3279745867_MethodInfo_var);
		if ((((int32_t)L_27) < ((int32_t)L_29)))
		{
			goto IL_004a;
		}
	}
	{
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
