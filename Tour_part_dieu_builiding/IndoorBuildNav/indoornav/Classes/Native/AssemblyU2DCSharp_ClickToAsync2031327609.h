﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Slider
struct Slider_t297367283;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3814632279;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClickToAsync
struct  ClickToAsync_t2031327609  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Slider ClickToAsync::loadingbar
	Slider_t297367283 * ___loadingbar_2;
	// UnityEngine.GameObject ClickToAsync::loadimage
	GameObject_t1756533147 * ___loadimage_3;
	// UnityEngine.AsyncOperation ClickToAsync::async
	AsyncOperation_t3814632279 * ___async_4;

public:
	inline static int32_t get_offset_of_loadingbar_2() { return static_cast<int32_t>(offsetof(ClickToAsync_t2031327609, ___loadingbar_2)); }
	inline Slider_t297367283 * get_loadingbar_2() const { return ___loadingbar_2; }
	inline Slider_t297367283 ** get_address_of_loadingbar_2() { return &___loadingbar_2; }
	inline void set_loadingbar_2(Slider_t297367283 * value)
	{
		___loadingbar_2 = value;
		Il2CppCodeGenWriteBarrier(&___loadingbar_2, value);
	}

	inline static int32_t get_offset_of_loadimage_3() { return static_cast<int32_t>(offsetof(ClickToAsync_t2031327609, ___loadimage_3)); }
	inline GameObject_t1756533147 * get_loadimage_3() const { return ___loadimage_3; }
	inline GameObject_t1756533147 ** get_address_of_loadimage_3() { return &___loadimage_3; }
	inline void set_loadimage_3(GameObject_t1756533147 * value)
	{
		___loadimage_3 = value;
		Il2CppCodeGenWriteBarrier(&___loadimage_3, value);
	}

	inline static int32_t get_offset_of_async_4() { return static_cast<int32_t>(offsetof(ClickToAsync_t2031327609, ___async_4)); }
	inline AsyncOperation_t3814632279 * get_async_4() const { return ___async_4; }
	inline AsyncOperation_t3814632279 ** get_address_of_async_4() { return &___async_4; }
	inline void set_async_4(AsyncOperation_t3814632279 * value)
	{
		___async_4 = value;
		Il2CppCodeGenWriteBarrier(&___async_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
