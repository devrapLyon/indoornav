﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveCamera
struct  MoveCamera_t3193441886  : public MonoBehaviour_t1158329972
{
public:
	// System.Single MoveCamera::turnSpeed
	float ___turnSpeed_2;
	// System.Single MoveCamera::panSpeed
	float ___panSpeed_3;
	// System.Single MoveCamera::zoomSpeed
	float ___zoomSpeed_4;
	// UnityEngine.Vector3 MoveCamera::mouseOrigin
	Vector3_t2243707580  ___mouseOrigin_5;
	// System.Boolean MoveCamera::isPanning
	bool ___isPanning_6;
	// System.Boolean MoveCamera::isRotating
	bool ___isRotating_7;
	// System.Boolean MoveCamera::isZooming
	bool ___isZooming_8;

public:
	inline static int32_t get_offset_of_turnSpeed_2() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___turnSpeed_2)); }
	inline float get_turnSpeed_2() const { return ___turnSpeed_2; }
	inline float* get_address_of_turnSpeed_2() { return &___turnSpeed_2; }
	inline void set_turnSpeed_2(float value)
	{
		___turnSpeed_2 = value;
	}

	inline static int32_t get_offset_of_panSpeed_3() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___panSpeed_3)); }
	inline float get_panSpeed_3() const { return ___panSpeed_3; }
	inline float* get_address_of_panSpeed_3() { return &___panSpeed_3; }
	inline void set_panSpeed_3(float value)
	{
		___panSpeed_3 = value;
	}

	inline static int32_t get_offset_of_zoomSpeed_4() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___zoomSpeed_4)); }
	inline float get_zoomSpeed_4() const { return ___zoomSpeed_4; }
	inline float* get_address_of_zoomSpeed_4() { return &___zoomSpeed_4; }
	inline void set_zoomSpeed_4(float value)
	{
		___zoomSpeed_4 = value;
	}

	inline static int32_t get_offset_of_mouseOrigin_5() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___mouseOrigin_5)); }
	inline Vector3_t2243707580  get_mouseOrigin_5() const { return ___mouseOrigin_5; }
	inline Vector3_t2243707580 * get_address_of_mouseOrigin_5() { return &___mouseOrigin_5; }
	inline void set_mouseOrigin_5(Vector3_t2243707580  value)
	{
		___mouseOrigin_5 = value;
	}

	inline static int32_t get_offset_of_isPanning_6() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___isPanning_6)); }
	inline bool get_isPanning_6() const { return ___isPanning_6; }
	inline bool* get_address_of_isPanning_6() { return &___isPanning_6; }
	inline void set_isPanning_6(bool value)
	{
		___isPanning_6 = value;
	}

	inline static int32_t get_offset_of_isRotating_7() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___isRotating_7)); }
	inline bool get_isRotating_7() const { return ___isRotating_7; }
	inline bool* get_address_of_isRotating_7() { return &___isRotating_7; }
	inline void set_isRotating_7(bool value)
	{
		___isRotating_7 = value;
	}

	inline static int32_t get_offset_of_isZooming_8() { return static_cast<int32_t>(offsetof(MoveCamera_t3193441886, ___isZooming_8)); }
	inline bool get_isZooming_8() const { return ___isZooming_8; }
	inline bool* get_address_of_isZooming_8() { return &___isZooming_8; }
	inline void set_isZooming_8(bool value)
	{
		___isZooming_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
