﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Office_Class/Salle
struct Salle_t3888431915;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Office_Class
struct  Office_Class_t2060087375  : public MonoBehaviour_t1158329972
{
public:
	// Office_Class/Salle Office_Class::NewSalle
	Salle_t3888431915 * ___NewSalle_2;

public:
	inline static int32_t get_offset_of_NewSalle_2() { return static_cast<int32_t>(offsetof(Office_Class_t2060087375, ___NewSalle_2)); }
	inline Salle_t3888431915 * get_NewSalle_2() const { return ___NewSalle_2; }
	inline Salle_t3888431915 ** get_address_of_NewSalle_2() { return &___NewSalle_2; }
	inline void set_NewSalle_2(Salle_t3888431915 * value)
	{
		___NewSalle_2 = value;
		Il2CppCodeGenWriteBarrier(&___NewSalle_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
