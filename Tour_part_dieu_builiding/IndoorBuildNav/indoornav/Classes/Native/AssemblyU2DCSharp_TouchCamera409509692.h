﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Single[]
struct SingleU5BU5D_t577127397;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchCamera
struct  TouchCamera_t409509692  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera TouchCamera::cam
	Camera_t189460977 * ___cam_8;
	// UnityEngine.Vector3 TouchCamera::lastPanPosition
	Vector3_t2243707580  ___lastPanPosition_9;
	// System.Int32 TouchCamera::panFingerId
	int32_t ___panFingerId_10;
	// System.Boolean TouchCamera::wasZoomingLastFrame
	bool ___wasZoomingLastFrame_11;
	// UnityEngine.Vector2[] TouchCamera::lastZoomPositions
	Vector2U5BU5D_t686124026* ___lastZoomPositions_12;

public:
	inline static int32_t get_offset_of_cam_8() { return static_cast<int32_t>(offsetof(TouchCamera_t409509692, ___cam_8)); }
	inline Camera_t189460977 * get_cam_8() const { return ___cam_8; }
	inline Camera_t189460977 ** get_address_of_cam_8() { return &___cam_8; }
	inline void set_cam_8(Camera_t189460977 * value)
	{
		___cam_8 = value;
		Il2CppCodeGenWriteBarrier(&___cam_8, value);
	}

	inline static int32_t get_offset_of_lastPanPosition_9() { return static_cast<int32_t>(offsetof(TouchCamera_t409509692, ___lastPanPosition_9)); }
	inline Vector3_t2243707580  get_lastPanPosition_9() const { return ___lastPanPosition_9; }
	inline Vector3_t2243707580 * get_address_of_lastPanPosition_9() { return &___lastPanPosition_9; }
	inline void set_lastPanPosition_9(Vector3_t2243707580  value)
	{
		___lastPanPosition_9 = value;
	}

	inline static int32_t get_offset_of_panFingerId_10() { return static_cast<int32_t>(offsetof(TouchCamera_t409509692, ___panFingerId_10)); }
	inline int32_t get_panFingerId_10() const { return ___panFingerId_10; }
	inline int32_t* get_address_of_panFingerId_10() { return &___panFingerId_10; }
	inline void set_panFingerId_10(int32_t value)
	{
		___panFingerId_10 = value;
	}

	inline static int32_t get_offset_of_wasZoomingLastFrame_11() { return static_cast<int32_t>(offsetof(TouchCamera_t409509692, ___wasZoomingLastFrame_11)); }
	inline bool get_wasZoomingLastFrame_11() const { return ___wasZoomingLastFrame_11; }
	inline bool* get_address_of_wasZoomingLastFrame_11() { return &___wasZoomingLastFrame_11; }
	inline void set_wasZoomingLastFrame_11(bool value)
	{
		___wasZoomingLastFrame_11 = value;
	}

	inline static int32_t get_offset_of_lastZoomPositions_12() { return static_cast<int32_t>(offsetof(TouchCamera_t409509692, ___lastZoomPositions_12)); }
	inline Vector2U5BU5D_t686124026* get_lastZoomPositions_12() const { return ___lastZoomPositions_12; }
	inline Vector2U5BU5D_t686124026** get_address_of_lastZoomPositions_12() { return &___lastZoomPositions_12; }
	inline void set_lastZoomPositions_12(Vector2U5BU5D_t686124026* value)
	{
		___lastZoomPositions_12 = value;
		Il2CppCodeGenWriteBarrier(&___lastZoomPositions_12, value);
	}
};

struct TouchCamera_t409509692_StaticFields
{
public:
	// System.Single TouchCamera::PanSpeed
	float ___PanSpeed_2;
	// System.Single TouchCamera::ZoomSpeedTouch
	float ___ZoomSpeedTouch_3;
	// System.Single TouchCamera::ZoomSpeedMouse
	float ___ZoomSpeedMouse_4;
	// System.Single[] TouchCamera::BoundsX
	SingleU5BU5D_t577127397* ___BoundsX_5;
	// System.Single[] TouchCamera::BoundsZ
	SingleU5BU5D_t577127397* ___BoundsZ_6;
	// System.Single[] TouchCamera::ZoomBounds
	SingleU5BU5D_t577127397* ___ZoomBounds_7;

public:
	inline static int32_t get_offset_of_PanSpeed_2() { return static_cast<int32_t>(offsetof(TouchCamera_t409509692_StaticFields, ___PanSpeed_2)); }
	inline float get_PanSpeed_2() const { return ___PanSpeed_2; }
	inline float* get_address_of_PanSpeed_2() { return &___PanSpeed_2; }
	inline void set_PanSpeed_2(float value)
	{
		___PanSpeed_2 = value;
	}

	inline static int32_t get_offset_of_ZoomSpeedTouch_3() { return static_cast<int32_t>(offsetof(TouchCamera_t409509692_StaticFields, ___ZoomSpeedTouch_3)); }
	inline float get_ZoomSpeedTouch_3() const { return ___ZoomSpeedTouch_3; }
	inline float* get_address_of_ZoomSpeedTouch_3() { return &___ZoomSpeedTouch_3; }
	inline void set_ZoomSpeedTouch_3(float value)
	{
		___ZoomSpeedTouch_3 = value;
	}

	inline static int32_t get_offset_of_ZoomSpeedMouse_4() { return static_cast<int32_t>(offsetof(TouchCamera_t409509692_StaticFields, ___ZoomSpeedMouse_4)); }
	inline float get_ZoomSpeedMouse_4() const { return ___ZoomSpeedMouse_4; }
	inline float* get_address_of_ZoomSpeedMouse_4() { return &___ZoomSpeedMouse_4; }
	inline void set_ZoomSpeedMouse_4(float value)
	{
		___ZoomSpeedMouse_4 = value;
	}

	inline static int32_t get_offset_of_BoundsX_5() { return static_cast<int32_t>(offsetof(TouchCamera_t409509692_StaticFields, ___BoundsX_5)); }
	inline SingleU5BU5D_t577127397* get_BoundsX_5() const { return ___BoundsX_5; }
	inline SingleU5BU5D_t577127397** get_address_of_BoundsX_5() { return &___BoundsX_5; }
	inline void set_BoundsX_5(SingleU5BU5D_t577127397* value)
	{
		___BoundsX_5 = value;
		Il2CppCodeGenWriteBarrier(&___BoundsX_5, value);
	}

	inline static int32_t get_offset_of_BoundsZ_6() { return static_cast<int32_t>(offsetof(TouchCamera_t409509692_StaticFields, ___BoundsZ_6)); }
	inline SingleU5BU5D_t577127397* get_BoundsZ_6() const { return ___BoundsZ_6; }
	inline SingleU5BU5D_t577127397** get_address_of_BoundsZ_6() { return &___BoundsZ_6; }
	inline void set_BoundsZ_6(SingleU5BU5D_t577127397* value)
	{
		___BoundsZ_6 = value;
		Il2CppCodeGenWriteBarrier(&___BoundsZ_6, value);
	}

	inline static int32_t get_offset_of_ZoomBounds_7() { return static_cast<int32_t>(offsetof(TouchCamera_t409509692_StaticFields, ___ZoomBounds_7)); }
	inline SingleU5BU5D_t577127397* get_ZoomBounds_7() const { return ___ZoomBounds_7; }
	inline SingleU5BU5D_t577127397** get_address_of_ZoomBounds_7() { return &___ZoomBounds_7; }
	inline void set_ZoomBounds_7(SingleU5BU5D_t577127397* value)
	{
		___ZoomBounds_7 = value;
		Il2CppCodeGenWriteBarrier(&___ZoomBounds_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
