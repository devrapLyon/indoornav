﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// IdOffice
struct IdOffice_t1887523751;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OfficeData
struct  OfficeData_t3828749716  : public Il2CppObject
{
public:
	// IdOffice OfficeData::<id_office>k__BackingField
	IdOffice_t1887523751 * ___U3Cid_officeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3Cid_officeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(OfficeData_t3828749716, ___U3Cid_officeU3Ek__BackingField_0)); }
	inline IdOffice_t1887523751 * get_U3Cid_officeU3Ek__BackingField_0() const { return ___U3Cid_officeU3Ek__BackingField_0; }
	inline IdOffice_t1887523751 ** get_address_of_U3Cid_officeU3Ek__BackingField_0() { return &___U3Cid_officeU3Ek__BackingField_0; }
	inline void set_U3Cid_officeU3Ek__BackingField_0(IdOffice_t1887523751 * value)
	{
		___U3Cid_officeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cid_officeU3Ek__BackingField_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
