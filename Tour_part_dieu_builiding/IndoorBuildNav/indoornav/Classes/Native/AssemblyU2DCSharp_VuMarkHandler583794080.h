﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// PanelShowHide
struct PanelShowHide_t775446595;
// Vuforia.VuMarkManager
struct VuMarkManager_t3604726399;
// Vuforia.VuMarkTarget
struct VuMarkTarget_t222984753;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.Func`2<UnityEngine.Object,UnityEngine.GameObject>
struct Func_2_t2495701363;
// System.Func`2<UnityEngine.GameObject,System.Boolean>
struct Func_2_t3404947624;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VuMarkHandler
struct  VuMarkHandler_t583794080  : public MonoBehaviour_t1158329972
{
public:
	// PanelShowHide VuMarkHandler::mIdPanel
	PanelShowHide_t775446595 * ___mIdPanel_2;
	// Vuforia.VuMarkManager VuMarkHandler::mVuMarkManager
	VuMarkManager_t3604726399 * ___mVuMarkManager_3;
	// Vuforia.VuMarkTarget VuMarkHandler::mClosestVuMark
	Il2CppObject * ___mClosestVuMark_4;
	// Vuforia.VuMarkTarget VuMarkHandler::mCurrentVuMark
	Il2CppObject * ___mCurrentVuMark_5;
	// System.Single VuMarkHandler::distance
	float ___distance_6;
	// System.String VuMarkHandler::vuMarkId
	String_t* ___vuMarkId_7;
	// UnityEngine.UI.Text VuMarkHandler::Txt_action
	Text_t356221433 * ___Txt_action_8;

public:
	inline static int32_t get_offset_of_mIdPanel_2() { return static_cast<int32_t>(offsetof(VuMarkHandler_t583794080, ___mIdPanel_2)); }
	inline PanelShowHide_t775446595 * get_mIdPanel_2() const { return ___mIdPanel_2; }
	inline PanelShowHide_t775446595 ** get_address_of_mIdPanel_2() { return &___mIdPanel_2; }
	inline void set_mIdPanel_2(PanelShowHide_t775446595 * value)
	{
		___mIdPanel_2 = value;
		Il2CppCodeGenWriteBarrier(&___mIdPanel_2, value);
	}

	inline static int32_t get_offset_of_mVuMarkManager_3() { return static_cast<int32_t>(offsetof(VuMarkHandler_t583794080, ___mVuMarkManager_3)); }
	inline VuMarkManager_t3604726399 * get_mVuMarkManager_3() const { return ___mVuMarkManager_3; }
	inline VuMarkManager_t3604726399 ** get_address_of_mVuMarkManager_3() { return &___mVuMarkManager_3; }
	inline void set_mVuMarkManager_3(VuMarkManager_t3604726399 * value)
	{
		___mVuMarkManager_3 = value;
		Il2CppCodeGenWriteBarrier(&___mVuMarkManager_3, value);
	}

	inline static int32_t get_offset_of_mClosestVuMark_4() { return static_cast<int32_t>(offsetof(VuMarkHandler_t583794080, ___mClosestVuMark_4)); }
	inline Il2CppObject * get_mClosestVuMark_4() const { return ___mClosestVuMark_4; }
	inline Il2CppObject ** get_address_of_mClosestVuMark_4() { return &___mClosestVuMark_4; }
	inline void set_mClosestVuMark_4(Il2CppObject * value)
	{
		___mClosestVuMark_4 = value;
		Il2CppCodeGenWriteBarrier(&___mClosestVuMark_4, value);
	}

	inline static int32_t get_offset_of_mCurrentVuMark_5() { return static_cast<int32_t>(offsetof(VuMarkHandler_t583794080, ___mCurrentVuMark_5)); }
	inline Il2CppObject * get_mCurrentVuMark_5() const { return ___mCurrentVuMark_5; }
	inline Il2CppObject ** get_address_of_mCurrentVuMark_5() { return &___mCurrentVuMark_5; }
	inline void set_mCurrentVuMark_5(Il2CppObject * value)
	{
		___mCurrentVuMark_5 = value;
		Il2CppCodeGenWriteBarrier(&___mCurrentVuMark_5, value);
	}

	inline static int32_t get_offset_of_distance_6() { return static_cast<int32_t>(offsetof(VuMarkHandler_t583794080, ___distance_6)); }
	inline float get_distance_6() const { return ___distance_6; }
	inline float* get_address_of_distance_6() { return &___distance_6; }
	inline void set_distance_6(float value)
	{
		___distance_6 = value;
	}

	inline static int32_t get_offset_of_vuMarkId_7() { return static_cast<int32_t>(offsetof(VuMarkHandler_t583794080, ___vuMarkId_7)); }
	inline String_t* get_vuMarkId_7() const { return ___vuMarkId_7; }
	inline String_t** get_address_of_vuMarkId_7() { return &___vuMarkId_7; }
	inline void set_vuMarkId_7(String_t* value)
	{
		___vuMarkId_7 = value;
		Il2CppCodeGenWriteBarrier(&___vuMarkId_7, value);
	}

	inline static int32_t get_offset_of_Txt_action_8() { return static_cast<int32_t>(offsetof(VuMarkHandler_t583794080, ___Txt_action_8)); }
	inline Text_t356221433 * get_Txt_action_8() const { return ___Txt_action_8; }
	inline Text_t356221433 ** get_address_of_Txt_action_8() { return &___Txt_action_8; }
	inline void set_Txt_action_8(Text_t356221433 * value)
	{
		___Txt_action_8 = value;
		Il2CppCodeGenWriteBarrier(&___Txt_action_8, value);
	}
};

struct VuMarkHandler_t583794080_StaticFields
{
public:
	// System.Func`2<UnityEngine.Object,UnityEngine.GameObject> VuMarkHandler::<>f__am$cache0
	Func_2_t2495701363 * ___U3CU3Ef__amU24cache0_9;
	// System.Func`2<UnityEngine.GameObject,System.Boolean> VuMarkHandler::<>f__am$cache1
	Func_2_t3404947624 * ___U3CU3Ef__amU24cache1_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(VuMarkHandler_t583794080_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Func_2_t2495701363 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Func_2_t2495701363 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Func_2_t2495701363 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_10() { return static_cast<int32_t>(offsetof(VuMarkHandler_t583794080_StaticFields, ___U3CU3Ef__amU24cache1_10)); }
	inline Func_2_t3404947624 * get_U3CU3Ef__amU24cache1_10() const { return ___U3CU3Ef__amU24cache1_10; }
	inline Func_2_t3404947624 ** get_address_of_U3CU3Ef__amU24cache1_10() { return &___U3CU3Ef__amU24cache1_10; }
	inline void set_U3CU3Ef__amU24cache1_10(Func_2_t3404947624 * value)
	{
		___U3CU3Ef__amU24cache1_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
