﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// ButtonListControl
struct ButtonListControl_t933417769;
// Change_View
struct Change_View_t1623420308;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonListButton
struct  ButtonListButton_t2298477596  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text ButtonListButton::BtnTxt
	Text_t356221433 * ___BtnTxt_2;
	// UnityEngine.GameObject ButtonListButton::BtnsList
	GameObject_t1756533147 * ___BtnsList_3;
	// UnityEngine.GameObject ButtonListButton::Obj_dest
	GameObject_t1756533147 * ___Obj_dest_4;
	// UnityEngine.UI.Text ButtonListButton::Txt_action
	Text_t356221433 * ___Txt_action_5;
	// UnityEngine.GameObject ButtonListButton::Menu_Bdd
	GameObject_t1756533147 * ___Menu_Bdd_6;
	// System.String ButtonListButton::tmpStr
	String_t* ___tmpStr_7;
	// ButtonListControl ButtonListButton::BtnListControl
	ButtonListControl_t933417769 * ___BtnListControl_8;
	// Change_View ButtonListButton::change_view_script
	Change_View_t1623420308 * ___change_view_script_9;

public:
	inline static int32_t get_offset_of_BtnTxt_2() { return static_cast<int32_t>(offsetof(ButtonListButton_t2298477596, ___BtnTxt_2)); }
	inline Text_t356221433 * get_BtnTxt_2() const { return ___BtnTxt_2; }
	inline Text_t356221433 ** get_address_of_BtnTxt_2() { return &___BtnTxt_2; }
	inline void set_BtnTxt_2(Text_t356221433 * value)
	{
		___BtnTxt_2 = value;
		Il2CppCodeGenWriteBarrier(&___BtnTxt_2, value);
	}

	inline static int32_t get_offset_of_BtnsList_3() { return static_cast<int32_t>(offsetof(ButtonListButton_t2298477596, ___BtnsList_3)); }
	inline GameObject_t1756533147 * get_BtnsList_3() const { return ___BtnsList_3; }
	inline GameObject_t1756533147 ** get_address_of_BtnsList_3() { return &___BtnsList_3; }
	inline void set_BtnsList_3(GameObject_t1756533147 * value)
	{
		___BtnsList_3 = value;
		Il2CppCodeGenWriteBarrier(&___BtnsList_3, value);
	}

	inline static int32_t get_offset_of_Obj_dest_4() { return static_cast<int32_t>(offsetof(ButtonListButton_t2298477596, ___Obj_dest_4)); }
	inline GameObject_t1756533147 * get_Obj_dest_4() const { return ___Obj_dest_4; }
	inline GameObject_t1756533147 ** get_address_of_Obj_dest_4() { return &___Obj_dest_4; }
	inline void set_Obj_dest_4(GameObject_t1756533147 * value)
	{
		___Obj_dest_4 = value;
		Il2CppCodeGenWriteBarrier(&___Obj_dest_4, value);
	}

	inline static int32_t get_offset_of_Txt_action_5() { return static_cast<int32_t>(offsetof(ButtonListButton_t2298477596, ___Txt_action_5)); }
	inline Text_t356221433 * get_Txt_action_5() const { return ___Txt_action_5; }
	inline Text_t356221433 ** get_address_of_Txt_action_5() { return &___Txt_action_5; }
	inline void set_Txt_action_5(Text_t356221433 * value)
	{
		___Txt_action_5 = value;
		Il2CppCodeGenWriteBarrier(&___Txt_action_5, value);
	}

	inline static int32_t get_offset_of_Menu_Bdd_6() { return static_cast<int32_t>(offsetof(ButtonListButton_t2298477596, ___Menu_Bdd_6)); }
	inline GameObject_t1756533147 * get_Menu_Bdd_6() const { return ___Menu_Bdd_6; }
	inline GameObject_t1756533147 ** get_address_of_Menu_Bdd_6() { return &___Menu_Bdd_6; }
	inline void set_Menu_Bdd_6(GameObject_t1756533147 * value)
	{
		___Menu_Bdd_6 = value;
		Il2CppCodeGenWriteBarrier(&___Menu_Bdd_6, value);
	}

	inline static int32_t get_offset_of_tmpStr_7() { return static_cast<int32_t>(offsetof(ButtonListButton_t2298477596, ___tmpStr_7)); }
	inline String_t* get_tmpStr_7() const { return ___tmpStr_7; }
	inline String_t** get_address_of_tmpStr_7() { return &___tmpStr_7; }
	inline void set_tmpStr_7(String_t* value)
	{
		___tmpStr_7 = value;
		Il2CppCodeGenWriteBarrier(&___tmpStr_7, value);
	}

	inline static int32_t get_offset_of_BtnListControl_8() { return static_cast<int32_t>(offsetof(ButtonListButton_t2298477596, ___BtnListControl_8)); }
	inline ButtonListControl_t933417769 * get_BtnListControl_8() const { return ___BtnListControl_8; }
	inline ButtonListControl_t933417769 ** get_address_of_BtnListControl_8() { return &___BtnListControl_8; }
	inline void set_BtnListControl_8(ButtonListControl_t933417769 * value)
	{
		___BtnListControl_8 = value;
		Il2CppCodeGenWriteBarrier(&___BtnListControl_8, value);
	}

	inline static int32_t get_offset_of_change_view_script_9() { return static_cast<int32_t>(offsetof(ButtonListButton_t2298477596, ___change_view_script_9)); }
	inline Change_View_t1623420308 * get_change_view_script_9() const { return ___change_view_script_9; }
	inline Change_View_t1623420308 ** get_address_of_change_view_script_9() { return &___change_view_script_9; }
	inline void set_change_view_script_9(Change_View_t1623420308 * value)
	{
		___change_view_script_9 = value;
		Il2CppCodeGenWriteBarrier(&___change_view_script_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
