﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// System.Collections.Generic.List`1<Markers/Vumark_Class>
struct List_1_t3693687175;
// Markers
struct Markers_t3135928711;
// VuMarkHandler
struct VuMarkHandler_t583794080;
// Change_View
struct Change_View_t1623420308;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.Func`2<UnityEngine.Object,UnityEngine.GameObject>
struct Func_2_t2495701363;
// System.Func`2<UnityEngine.GameObject,System.Boolean>
struct Func_2_t3404947624;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Finder
struct  Finder_t2554545268  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Finder::_ArCamera
	GameObject_t1756533147 * ____ArCamera_2;
	// UnityEngine.GameObject Finder::_MainCamera_Scene
	GameObject_t1756533147 * ____MainCamera_Scene_3;
	// UnityEngine.GameObject Finder::_Vumark
	GameObject_t1756533147 * ____Vumark_4;
	// UnityEngine.GameObject Finder::spawnObject
	GameObject_t1756533147 * ___spawnObject_5;
	// UnityEngine.GameObject Finder::arrowObj
	GameObject_t1756533147 * ___arrowObj_6;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Finder::lExit
	List_1_t1125654279 * ___lExit_7;
	// UnityEngine.GameObject Finder::Obj_dest
	GameObject_t1756533147 * ___Obj_dest_8;
	// System.Collections.Generic.List`1<Markers/Vumark_Class> Finder::markers
	List_1_t3693687175 * ___markers_9;
	// System.Int32 Finder::VumarkNumber
	int32_t ___VumarkNumber_11;
	// Markers Finder::vumark_class_script
	Markers_t3135928711 * ___vumark_class_script_12;
	// VuMarkHandler Finder::vumarkhandler_Script
	VuMarkHandler_t583794080 * ___vumarkhandler_Script_13;
	// Change_View Finder::change_view_script
	Change_View_t1623420308 * ___change_view_script_14;
	// UnityEngine.UI.Text Finder::Txt_action
	Text_t356221433 * ___Txt_action_15;
	// UnityEngine.GameObject Finder::Menu_Bdd
	GameObject_t1756533147 * ___Menu_Bdd_16;

public:
	inline static int32_t get_offset_of__ArCamera_2() { return static_cast<int32_t>(offsetof(Finder_t2554545268, ____ArCamera_2)); }
	inline GameObject_t1756533147 * get__ArCamera_2() const { return ____ArCamera_2; }
	inline GameObject_t1756533147 ** get_address_of__ArCamera_2() { return &____ArCamera_2; }
	inline void set__ArCamera_2(GameObject_t1756533147 * value)
	{
		____ArCamera_2 = value;
		Il2CppCodeGenWriteBarrier(&____ArCamera_2, value);
	}

	inline static int32_t get_offset_of__MainCamera_Scene_3() { return static_cast<int32_t>(offsetof(Finder_t2554545268, ____MainCamera_Scene_3)); }
	inline GameObject_t1756533147 * get__MainCamera_Scene_3() const { return ____MainCamera_Scene_3; }
	inline GameObject_t1756533147 ** get_address_of__MainCamera_Scene_3() { return &____MainCamera_Scene_3; }
	inline void set__MainCamera_Scene_3(GameObject_t1756533147 * value)
	{
		____MainCamera_Scene_3 = value;
		Il2CppCodeGenWriteBarrier(&____MainCamera_Scene_3, value);
	}

	inline static int32_t get_offset_of__Vumark_4() { return static_cast<int32_t>(offsetof(Finder_t2554545268, ____Vumark_4)); }
	inline GameObject_t1756533147 * get__Vumark_4() const { return ____Vumark_4; }
	inline GameObject_t1756533147 ** get_address_of__Vumark_4() { return &____Vumark_4; }
	inline void set__Vumark_4(GameObject_t1756533147 * value)
	{
		____Vumark_4 = value;
		Il2CppCodeGenWriteBarrier(&____Vumark_4, value);
	}

	inline static int32_t get_offset_of_spawnObject_5() { return static_cast<int32_t>(offsetof(Finder_t2554545268, ___spawnObject_5)); }
	inline GameObject_t1756533147 * get_spawnObject_5() const { return ___spawnObject_5; }
	inline GameObject_t1756533147 ** get_address_of_spawnObject_5() { return &___spawnObject_5; }
	inline void set_spawnObject_5(GameObject_t1756533147 * value)
	{
		___spawnObject_5 = value;
		Il2CppCodeGenWriteBarrier(&___spawnObject_5, value);
	}

	inline static int32_t get_offset_of_arrowObj_6() { return static_cast<int32_t>(offsetof(Finder_t2554545268, ___arrowObj_6)); }
	inline GameObject_t1756533147 * get_arrowObj_6() const { return ___arrowObj_6; }
	inline GameObject_t1756533147 ** get_address_of_arrowObj_6() { return &___arrowObj_6; }
	inline void set_arrowObj_6(GameObject_t1756533147 * value)
	{
		___arrowObj_6 = value;
		Il2CppCodeGenWriteBarrier(&___arrowObj_6, value);
	}

	inline static int32_t get_offset_of_lExit_7() { return static_cast<int32_t>(offsetof(Finder_t2554545268, ___lExit_7)); }
	inline List_1_t1125654279 * get_lExit_7() const { return ___lExit_7; }
	inline List_1_t1125654279 ** get_address_of_lExit_7() { return &___lExit_7; }
	inline void set_lExit_7(List_1_t1125654279 * value)
	{
		___lExit_7 = value;
		Il2CppCodeGenWriteBarrier(&___lExit_7, value);
	}

	inline static int32_t get_offset_of_Obj_dest_8() { return static_cast<int32_t>(offsetof(Finder_t2554545268, ___Obj_dest_8)); }
	inline GameObject_t1756533147 * get_Obj_dest_8() const { return ___Obj_dest_8; }
	inline GameObject_t1756533147 ** get_address_of_Obj_dest_8() { return &___Obj_dest_8; }
	inline void set_Obj_dest_8(GameObject_t1756533147 * value)
	{
		___Obj_dest_8 = value;
		Il2CppCodeGenWriteBarrier(&___Obj_dest_8, value);
	}

	inline static int32_t get_offset_of_markers_9() { return static_cast<int32_t>(offsetof(Finder_t2554545268, ___markers_9)); }
	inline List_1_t3693687175 * get_markers_9() const { return ___markers_9; }
	inline List_1_t3693687175 ** get_address_of_markers_9() { return &___markers_9; }
	inline void set_markers_9(List_1_t3693687175 * value)
	{
		___markers_9 = value;
		Il2CppCodeGenWriteBarrier(&___markers_9, value);
	}

	inline static int32_t get_offset_of_VumarkNumber_11() { return static_cast<int32_t>(offsetof(Finder_t2554545268, ___VumarkNumber_11)); }
	inline int32_t get_VumarkNumber_11() const { return ___VumarkNumber_11; }
	inline int32_t* get_address_of_VumarkNumber_11() { return &___VumarkNumber_11; }
	inline void set_VumarkNumber_11(int32_t value)
	{
		___VumarkNumber_11 = value;
	}

	inline static int32_t get_offset_of_vumark_class_script_12() { return static_cast<int32_t>(offsetof(Finder_t2554545268, ___vumark_class_script_12)); }
	inline Markers_t3135928711 * get_vumark_class_script_12() const { return ___vumark_class_script_12; }
	inline Markers_t3135928711 ** get_address_of_vumark_class_script_12() { return &___vumark_class_script_12; }
	inline void set_vumark_class_script_12(Markers_t3135928711 * value)
	{
		___vumark_class_script_12 = value;
		Il2CppCodeGenWriteBarrier(&___vumark_class_script_12, value);
	}

	inline static int32_t get_offset_of_vumarkhandler_Script_13() { return static_cast<int32_t>(offsetof(Finder_t2554545268, ___vumarkhandler_Script_13)); }
	inline VuMarkHandler_t583794080 * get_vumarkhandler_Script_13() const { return ___vumarkhandler_Script_13; }
	inline VuMarkHandler_t583794080 ** get_address_of_vumarkhandler_Script_13() { return &___vumarkhandler_Script_13; }
	inline void set_vumarkhandler_Script_13(VuMarkHandler_t583794080 * value)
	{
		___vumarkhandler_Script_13 = value;
		Il2CppCodeGenWriteBarrier(&___vumarkhandler_Script_13, value);
	}

	inline static int32_t get_offset_of_change_view_script_14() { return static_cast<int32_t>(offsetof(Finder_t2554545268, ___change_view_script_14)); }
	inline Change_View_t1623420308 * get_change_view_script_14() const { return ___change_view_script_14; }
	inline Change_View_t1623420308 ** get_address_of_change_view_script_14() { return &___change_view_script_14; }
	inline void set_change_view_script_14(Change_View_t1623420308 * value)
	{
		___change_view_script_14 = value;
		Il2CppCodeGenWriteBarrier(&___change_view_script_14, value);
	}

	inline static int32_t get_offset_of_Txt_action_15() { return static_cast<int32_t>(offsetof(Finder_t2554545268, ___Txt_action_15)); }
	inline Text_t356221433 * get_Txt_action_15() const { return ___Txt_action_15; }
	inline Text_t356221433 ** get_address_of_Txt_action_15() { return &___Txt_action_15; }
	inline void set_Txt_action_15(Text_t356221433 * value)
	{
		___Txt_action_15 = value;
		Il2CppCodeGenWriteBarrier(&___Txt_action_15, value);
	}

	inline static int32_t get_offset_of_Menu_Bdd_16() { return static_cast<int32_t>(offsetof(Finder_t2554545268, ___Menu_Bdd_16)); }
	inline GameObject_t1756533147 * get_Menu_Bdd_16() const { return ___Menu_Bdd_16; }
	inline GameObject_t1756533147 ** get_address_of_Menu_Bdd_16() { return &___Menu_Bdd_16; }
	inline void set_Menu_Bdd_16(GameObject_t1756533147 * value)
	{
		___Menu_Bdd_16 = value;
		Il2CppCodeGenWriteBarrier(&___Menu_Bdd_16, value);
	}
};

struct Finder_t2554545268_StaticFields
{
public:
	// System.Int32 Finder::_Level
	int32_t ____Level_10;
	// System.Func`2<UnityEngine.Object,UnityEngine.GameObject> Finder::<>f__am$cache0
	Func_2_t2495701363 * ___U3CU3Ef__amU24cache0_17;
	// System.Func`2<UnityEngine.GameObject,System.Boolean> Finder::<>f__am$cache1
	Func_2_t3404947624 * ___U3CU3Ef__amU24cache1_18;

public:
	inline static int32_t get_offset_of__Level_10() { return static_cast<int32_t>(offsetof(Finder_t2554545268_StaticFields, ____Level_10)); }
	inline int32_t get__Level_10() const { return ____Level_10; }
	inline int32_t* get_address_of__Level_10() { return &____Level_10; }
	inline void set__Level_10(int32_t value)
	{
		____Level_10 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_17() { return static_cast<int32_t>(offsetof(Finder_t2554545268_StaticFields, ___U3CU3Ef__amU24cache0_17)); }
	inline Func_2_t2495701363 * get_U3CU3Ef__amU24cache0_17() const { return ___U3CU3Ef__amU24cache0_17; }
	inline Func_2_t2495701363 ** get_address_of_U3CU3Ef__amU24cache0_17() { return &___U3CU3Ef__amU24cache0_17; }
	inline void set_U3CU3Ef__amU24cache0_17(Func_2_t2495701363 * value)
	{
		___U3CU3Ef__amU24cache0_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_17, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_18() { return static_cast<int32_t>(offsetof(Finder_t2554545268_StaticFields, ___U3CU3Ef__amU24cache1_18)); }
	inline Func_2_t3404947624 * get_U3CU3Ef__amU24cache1_18() const { return ___U3CU3Ef__amU24cache1_18; }
	inline Func_2_t3404947624 ** get_address_of_U3CU3Ef__amU24cache1_18() { return &___U3CU3Ef__amU24cache1_18; }
	inline void set_U3CU3Ef__amU24cache1_18(Func_2_t3404947624 * value)
	{
		___U3CU3Ef__amU24cache1_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
