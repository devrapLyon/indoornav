﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Touch_zoom
struct  Touch_zoom_t1547134311  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Touch_zoom::perspectiveZoomSensitivity
	float ___perspectiveZoomSensitivity_2;

public:
	inline static int32_t get_offset_of_perspectiveZoomSensitivity_2() { return static_cast<int32_t>(offsetof(Touch_zoom_t1547134311, ___perspectiveZoomSensitivity_2)); }
	inline float get_perspectiveZoomSensitivity_2() const { return ___perspectiveZoomSensitivity_2; }
	inline float* get_address_of_perspectiveZoomSensitivity_2() { return &___perspectiveZoomSensitivity_2; }
	inline void set_perspectiveZoomSensitivity_2(float value)
	{
		___perspectiveZoomSensitivity_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
