﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{6eb14239-c70b-4b0e-9bbc-78fe4bfbc003}/$ArrayType=64
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D64_t1473450479 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D64_t1473450479__padding[64];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
