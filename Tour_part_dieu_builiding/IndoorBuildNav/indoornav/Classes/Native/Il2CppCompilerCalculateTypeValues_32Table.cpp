﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Firebase_App_MonoPInvokeCallbackAttribute1970456718.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE3515465473.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGException1412130252.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGException2876249339.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGException2443153790.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGPendingEx4193433529.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGStringHelp549488518.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGStringHelpe18001273.h"
#include "Firebase_App_Firebase_AppUtil2859790353.h"
#include "Firebase_App_Firebase_Variant4275788079.h"
#include "Firebase_App_Firebase_Variant_Type1054148541.h"
#include "Firebase_App_Firebase_VariantList2408673687.h"
#include "Firebase_App_Firebase_VariantList_VariantListEnume3934991014.h"
#include "Firebase_App_Firebase_VariantVariantMap4097173110.h"
#include "Firebase_App_Firebase_VariantVariantMap_VariantVari647394310.h"
#include "Firebase_App_Firebase_GooglePlayServicesAvailabilit722840220.h"
#include "Firebase_App_Firebase_LogLevel543421840.h"
#include "Firebase_App_Firebase_FirebaseApp210707726.h"
#include "Firebase_App_Firebase_FirebaseApp_FirebaseHandler2907300047.h"
#include "Firebase_App_Firebase_FirebaseApp_FirebaseHandler_1988210674.h"
#include "Firebase_App_Firebase_FirebaseApp_FirebaseHandler_1037795359.h"
#include "Firebase_App_Firebase_FirebaseApp_FirebaseHandler_M323097478.h"
#include "Firebase_App_Firebase_FirebaseApp_DestroyDelegate3635929227.h"
#include "Firebase_App_Firebase_FirebaseApp_CreateDelegate413676709.h"
#include "Firebase_App_Firebase_FirebaseApp_U3CCreateU3Ec__A1890575271.h"
#include "Firebase_App_Firebase_FirebaseApp_U3CCreateU3Ec__A1890575270.h"
#include "Firebase_App_Firebase_FirebaseApp_U3CSetLogLevelU3E567363225.h"
#include "Firebase_App_Firebase_FirebaseApp_U3CU3Ec__AnonSto1110811684.h"
#include "Firebase_App_Firebase_FirebaseApp_U3CU3Ec__AnonSto1110811683.h"
#include "Firebase_App_Firebase_FirebaseApp_U3CCheckDependen4291257269.h"
#include "Firebase_App_Firebase_FirebaseApp_U3CFixDependencie849757749.h"
#include "Firebase_App_Firebase_AppOptions1641189195.h"
#include "Firebase_App_Firebase_InitResult105293995.h"
#include "Firebase_App_Firebase_FutureVoid3180437.h"
#include "Firebase_App_Firebase_FutureVoid_Action2847228577.h"
#include "Firebase_App_Firebase_FutureVoid_MonoPInvokeCallbac296574561.h"
#include "Firebase_App_Firebase_FutureVoid_SWIG_CompletionDe2903358321.h"
#include "Firebase_App_Firebase_FutureVoid_U3CGetTaskU3Ec__A1008459717.h"
#include "Firebase_App_Firebase_FutureString4225986006.h"
#include "Firebase_App_Firebase_FutureString_Action3062064994.h"
#include "Firebase_App_Firebase_FutureString_MonoPInvokeCallb507919426.h"
#include "Firebase_App_Firebase_FutureString_SWIG_Completion1819656562.h"
#include "Firebase_App_Firebase_FutureString_U3CGetTaskU3Ec_4099292678.h"
#include "Firebase_App_Firebase_CharVector100493339.h"
#include "Firebase_App_Firebase_CharVector_CharVectorEnumera2036478274.h"
#include "Firebase_App_Firebase_StringList2332200693.h"
#include "Firebase_App_Firebase_StringList_StringListEnumera2590584790.h"
#include "Firebase_App_Firebase_StringStringMap3841415930.h"
#include "Firebase_App_Firebase_StringStringMap_StringString2508848358.h"
#include "Firebase_App_Firebase_FutureBase2698306134.h"
#include "Firebase_App_Firebase_FutureStatus4011176069.h"
#include "Firebase_App_Firebase_Unity_UnitySynchronizationCo2114871256.h"
#include "Firebase_App_Firebase_Unity_UnitySynchronizationCont99138606.h"
#include "Firebase_App_Firebase_Unity_UnitySynchronizationCo1234987676.h"
#include "Firebase_App_Firebase_Unity_UnitySynchronizationCo3455787875.h"
#include "Firebase_App_Firebase_Unity_UnitySynchronizationCon982275783.h"
#include "Firebase_App_Firebase_Unity_UnitySynchronizationCo3711159138.h"
#include "Firebase_App_Firebase_Unity_UnitySynchronizationCo2967043091.h"
#include "Firebase_App_Firebase_Unity_UnitySynchronizationCo2967043096.h"
#include "Firebase_App_Firebase_Unity_UnityPlatformServices848686304.h"
#include "Firebase_App_Firebase_Unity_UnityLoggingService1590581383.h"
#include "Firebase_App_Firebase_Unity_InstallRootCerts2159695568.h"
#include "Firebase_App_Firebase_Unity_WWWHttpRequest1831362850.h"
#include "Firebase_App_Firebase_Unity_WWWHttpRequest_U3CSendU937125532.h"
#include "Firebase_App_Firebase_Unity_UnityHttpFactoryService22354146.h"
#include "Firebase_App_Firebase_Platform_Default_UnityConfig1763038139.h"
#include "Firebase_App_Firebase_Unity_Editor_FirebaseEditorExt83728876.h"
#include "Firebase_App_Firebase_Platform_Services3629302694.h"
#include "Firebase_App_Firebase_Platform_DebugLogger542752859.h"
#include "Firebase_App_Firebase_Platform_NoopCertificateServ3432069064.h"
#include "Firebase_App_Firebase_Platform_FirebaseHttpRequest934394464.h"
#include "Firebase_App_Firebase_Platform_Security_ServiceCre2687018680.h"
#include "Firebase_App_Firebase_Platform_Security_ServiceCre3855616227.h"
#include "Firebase_App_Firebase_Platform_Security_ServiceAcco895213149.h"
#include "Firebase_App_Firebase_Platform_Security_ServiceAcc1020070256.h"
#include "Firebase_App_Firebase_Platform_Security_ServiceAcc2857161564.h"
#include "Firebase_App_Firebase_Platform_Security_GAuthToken3916815478.h"
#include "Firebase_App_Firebase_Platform_Default_SystemClock3210207471.h"
#include "Firebase_App_Firebase_Platform_Default_BaseAuthSer1401274330.h"
#include "Firebase_App_Firebase_Platform_Default_BaseAuthServ287602938.h"
#include "Firebase_App_Firebase_Platform_Default_AppConfigEx2317624261.h"
#include "Firebase_App_Firebase_VariantExtension1191592924.h"
#include "Firebase_App_Firebase_InitializationException1438959983.h"
#include "Firebase_App_Firebase_FirebaseException2567272216.h"
#include "Firebase_App_Firebase_DependencyStatus2752419415.h"
#include "Google_Sharpen_U3CModuleU3E3783534214.h"
#include "Google_Sharpen_Google_Sharpen_WrappedSystemStream831474186.h"
#include "Google_Sharpen_Google_Sharpen_URLEncoder1142290277.h"
#include "Google_Sharpen_Google_Sharpen_TimeUnit4006728025.h"
#include "Google_Sharpen_Google_Sharpen_TimeUnitExtensions1386558595.h"
#include "Google_Sharpen_Google_Sharpen_ThreadPoolExecutor376308723.h"
#include "Google_Sharpen_Google_Sharpen_RunnableAction2631350173.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3200 = { sizeof (MonoPInvokeCallbackAttribute_t1970456718), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3201 = { sizeof (AppUtilPINVOKE_t3515465473), -1, sizeof(AppUtilPINVOKE_t3515465473_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3201[2] = 
{
	AppUtilPINVOKE_t3515465473_StaticFields::get_offset_of_swigExceptionHelper_0(),
	AppUtilPINVOKE_t3515465473_StaticFields::get_offset_of_swigStringHelper_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3202 = { sizeof (SWIGExceptionHelper_t1412130252), -1, sizeof(SWIGExceptionHelper_t1412130252_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3202[14] = 
{
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_applicationDelegate_0(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_arithmeticDelegate_1(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_divideByZeroDelegate_2(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_indexOutOfRangeDelegate_3(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_invalidCastDelegate_4(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_invalidOperationDelegate_5(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_ioDelegate_6(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_nullReferenceDelegate_7(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_outOfMemoryDelegate_8(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_overflowDelegate_9(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_systemDelegate_10(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_argumentDelegate_11(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_argumentNullDelegate_12(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_argumentOutOfRangeDelegate_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3203 = { sizeof (ExceptionDelegate_t2876249339), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3204 = { sizeof (ExceptionArgumentDelegate_t2443153790), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3205 = { sizeof (SWIGPendingException_t4193433529), -1, sizeof(SWIGPendingException_t4193433529_StaticFields), sizeof(SWIGPendingException_t4193433529_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable3205[2] = 
{
	THREAD_STATIC_FIELD_OFFSET,
	SWIGPendingException_t4193433529_StaticFields::get_offset_of_numExceptionsPending_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3206 = { sizeof (SWIGStringHelper_t549488518), -1, sizeof(SWIGStringHelper_t549488518_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3206[1] = 
{
	SWIGStringHelper_t549488518_StaticFields::get_offset_of_stringDelegate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3207 = { sizeof (SWIGStringDelegate_t18001273), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3208 = { sizeof (AppUtil_t2859790353), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3209 = { sizeof (Variant_t4275788079), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3209[2] = 
{
	Variant_t4275788079::get_offset_of_swigCPtr_0(),
	Variant_t4275788079::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3210 = { sizeof (Type_t1054148541)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3210[11] = 
{
	Type_t1054148541::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3211 = { sizeof (VariantList_t2408673687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3211[2] = 
{
	VariantList_t2408673687::get_offset_of_swigCPtr_0(),
	VariantList_t2408673687::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3212 = { sizeof (VariantListEnumerator_t3934991014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3212[4] = 
{
	VariantListEnumerator_t3934991014::get_offset_of_collectionRef_0(),
	VariantListEnumerator_t3934991014::get_offset_of_currentIndex_1(),
	VariantListEnumerator_t3934991014::get_offset_of_currentObject_2(),
	VariantListEnumerator_t3934991014::get_offset_of_currentSize_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3213 = { sizeof (VariantVariantMap_t4097173110), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3213[2] = 
{
	VariantVariantMap_t4097173110::get_offset_of_swigCPtr_0(),
	VariantVariantMap_t4097173110::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3214 = { sizeof (VariantVariantMapEnumerator_t647394310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3214[5] = 
{
	VariantVariantMapEnumerator_t647394310::get_offset_of_collectionRef_0(),
	VariantVariantMapEnumerator_t647394310::get_offset_of_keyCollection_1(),
	VariantVariantMapEnumerator_t647394310::get_offset_of_currentIndex_2(),
	VariantVariantMapEnumerator_t647394310::get_offset_of_currentObject_3(),
	VariantVariantMapEnumerator_t647394310::get_offset_of_currentSize_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3215 = { sizeof (GooglePlayServicesAvailability_t722840220)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3215[9] = 
{
	GooglePlayServicesAvailability_t722840220::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3216 = { sizeof (LogLevel_t543421840)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3216[7] = 
{
	LogLevel_t543421840::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3217 = { sizeof (FirebaseApp_t210707726), -1, sizeof(FirebaseApp_t210707726_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3217[14] = 
{
	FirebaseApp_t210707726::get_offset_of_swigCPtr_0(),
	FirebaseApp_t210707726::get_offset_of_swigCMemOwn_1(),
	FirebaseApp_t210707726_StaticFields::get_offset_of_DEPENDENCY_NOT_FOUND_ERROR_ANDROID_2(),
	FirebaseApp_t210707726_StaticFields::get_offset_of_DEPENDENCY_NOT_FOUND_ERROR_IOS_3(),
	FirebaseApp_t210707726_StaticFields::get_offset_of_DEPENDENCY_NOT_FOUND_ERROR_GENERIC_4(),
	FirebaseApp_t210707726_StaticFields::get_offset_of_DLL_NOT_FOUND_ERROR_ANDROID_5(),
	FirebaseApp_t210707726_StaticFields::get_offset_of_DLL_NOT_FOUND_ERROR_IOS_6(),
	FirebaseApp_t210707726_StaticFields::get_offset_of_DLL_NOT_FOUND_ERROR_GENERIC_7(),
	FirebaseApp_t210707726::get_offset_of_Disposed_8(),
	FirebaseApp_t210707726_StaticFields::get_offset_of_nameToProxy_9(),
	FirebaseApp_t210707726_StaticFields::get_offset_of_cPtrToProxy_10(),
	FirebaseApp_t210707726_StaticFields::get_offset_of_cPtrRefCount_11(),
	FirebaseApp_t210707726::get_offset_of_destroy_12(),
	FirebaseApp_t210707726_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3218 = { sizeof (FirebaseHandler_t2907300047), -1, sizeof(FirebaseHandler_t2907300047_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3218[5] = 
{
	FirebaseHandler_t2907300047_StaticFields::get_offset_of_firebaseHandler_2(),
	FirebaseHandler_t2907300047::get_offset_of_Updated_3(),
	FirebaseHandler_t2907300047::get_offset_of_ApplicationFocusChanged_4(),
	FirebaseHandler_t2907300047_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_5(),
	FirebaseHandler_t2907300047_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3219 = { sizeof (LogMessageDelegate_t1988210674), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3220 = { sizeof (ApplicationFocusChangedEventArgs_t1037795359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3220[1] = 
{
	ApplicationFocusChangedEventArgs_t1037795359::get_offset_of_U3CHasFocusU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3221 = { sizeof (MonoPInvokeCallbackAttribute_t323097478), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3222 = { sizeof (DestroyDelegate_t3635929227), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3223 = { sizeof (CreateDelegate_t413676709), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3224 = { sizeof (U3CCreateU3Ec__AnonStorey0_t1890575271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3224[1] = 
{
	U3CCreateU3Ec__AnonStorey0_t1890575271::get_offset_of_options_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3225 = { sizeof (U3CCreateU3Ec__AnonStorey1_t1890575270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3225[2] = 
{
	U3CCreateU3Ec__AnonStorey1_t1890575270::get_offset_of_options_0(),
	U3CCreateU3Ec__AnonStorey1_t1890575270::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3226 = { sizeof (U3CSetLogLevelU3Ec__AnonStorey2_t567363225), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3226[1] = 
{
	U3CSetLogLevelU3Ec__AnonStorey2_t567363225::get_offset_of_level_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3227 = { sizeof (U3CU3Ec__AnonStorey3_t1110811684), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3227[1] = 
{
	U3CU3Ec__AnonStorey3_t1110811684::get_offset_of_level_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3228 = { sizeof (U3CU3Ec__AnonStorey4_t1110811683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3228[1] = 
{
	U3CU3Ec__AnonStorey4_t1110811683::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3229 = { sizeof (U3CCheckDependenciesU3Ec__AnonStorey5_t4291257269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3229[1] = 
{
	U3CCheckDependenciesU3Ec__AnonStorey5_t4291257269::get_offset_of_status_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3230 = { sizeof (U3CFixDependenciesAsyncU3Ec__AnonStorey6_t849757749), -1, sizeof(U3CFixDependenciesAsyncU3Ec__AnonStorey6_t849757749_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3230[2] = 
{
	U3CFixDependenciesAsyncU3Ec__AnonStorey6_t849757749::get_offset_of_task_0(),
	U3CFixDependenciesAsyncU3Ec__AnonStorey6_t849757749_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3231 = { sizeof (AppOptions_t1641189195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3231[2] = 
{
	AppOptions_t1641189195::get_offset_of_swigCPtr_0(),
	AppOptions_t1641189195::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3232 = { sizeof (InitResult_t105293995)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3232[3] = 
{
	InitResult_t105293995::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3233 = { sizeof (FutureVoid_t3180437), -1, sizeof(FutureVoid_t3180437_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3233[6] = 
{
	FutureVoid_t3180437::get_offset_of_swigCPtr_2(),
	FutureVoid_t3180437_StaticFields::get_offset_of_Callbacks_3(),
	FutureVoid_t3180437_StaticFields::get_offset_of_CallbackIndex_4(),
	FutureVoid_t3180437_StaticFields::get_offset_of_CallbackLock_5(),
	FutureVoid_t3180437::get_offset_of_callbackData_6(),
	FutureVoid_t3180437::get_offset_of_SWIG_CompletionCB_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3234 = { sizeof (Action_t2847228577), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3235 = { sizeof (MonoPInvokeCallbackAttribute_t296574561), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3236 = { sizeof (SWIG_CompletionDelegate_t2903358321), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3237 = { sizeof (U3CGetTaskU3Ec__AnonStorey0_t1008459717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3237[2] = 
{
	U3CGetTaskU3Ec__AnonStorey0_t1008459717::get_offset_of_fu_0(),
	U3CGetTaskU3Ec__AnonStorey0_t1008459717::get_offset_of_tcs_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3238 = { sizeof (FutureString_t4225986006), -1, sizeof(FutureString_t4225986006_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3238[6] = 
{
	FutureString_t4225986006::get_offset_of_swigCPtr_2(),
	FutureString_t4225986006_StaticFields::get_offset_of_Callbacks_3(),
	FutureString_t4225986006_StaticFields::get_offset_of_CallbackIndex_4(),
	FutureString_t4225986006_StaticFields::get_offset_of_CallbackLock_5(),
	FutureString_t4225986006::get_offset_of_callbackData_6(),
	FutureString_t4225986006::get_offset_of_SWIG_CompletionCB_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3239 = { sizeof (Action_t3062064994), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3240 = { sizeof (MonoPInvokeCallbackAttribute_t507919426), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3241 = { sizeof (SWIG_CompletionDelegate_t1819656562), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3242 = { sizeof (U3CGetTaskU3Ec__AnonStorey0_t4099292678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3242[2] = 
{
	U3CGetTaskU3Ec__AnonStorey0_t4099292678::get_offset_of_fu_0(),
	U3CGetTaskU3Ec__AnonStorey0_t4099292678::get_offset_of_tcs_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3243 = { sizeof (CharVector_t100493339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3243[2] = 
{
	CharVector_t100493339::get_offset_of_swigCPtr_0(),
	CharVector_t100493339::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3244 = { sizeof (CharVectorEnumerator_t2036478274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3244[4] = 
{
	CharVectorEnumerator_t2036478274::get_offset_of_collectionRef_0(),
	CharVectorEnumerator_t2036478274::get_offset_of_currentIndex_1(),
	CharVectorEnumerator_t2036478274::get_offset_of_currentObject_2(),
	CharVectorEnumerator_t2036478274::get_offset_of_currentSize_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3245 = { sizeof (StringList_t2332200693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3245[2] = 
{
	StringList_t2332200693::get_offset_of_swigCPtr_0(),
	StringList_t2332200693::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3246 = { sizeof (StringListEnumerator_t2590584790), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3246[4] = 
{
	StringListEnumerator_t2590584790::get_offset_of_collectionRef_0(),
	StringListEnumerator_t2590584790::get_offset_of_currentIndex_1(),
	StringListEnumerator_t2590584790::get_offset_of_currentObject_2(),
	StringListEnumerator_t2590584790::get_offset_of_currentSize_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3247 = { sizeof (StringStringMap_t3841415930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3247[2] = 
{
	StringStringMap_t3841415930::get_offset_of_swigCPtr_0(),
	StringStringMap_t3841415930::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3248 = { sizeof (StringStringMapEnumerator_t2508848358), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3248[5] = 
{
	StringStringMapEnumerator_t2508848358::get_offset_of_collectionRef_0(),
	StringStringMapEnumerator_t2508848358::get_offset_of_keyCollection_1(),
	StringStringMapEnumerator_t2508848358::get_offset_of_currentIndex_2(),
	StringStringMapEnumerator_t2508848358::get_offset_of_currentObject_3(),
	StringStringMapEnumerator_t2508848358::get_offset_of_currentSize_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3249 = { sizeof (FutureBase_t2698306134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3249[2] = 
{
	FutureBase_t2698306134::get_offset_of_swigCPtr_0(),
	FutureBase_t2698306134::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3250 = { sizeof (FutureStatus_t4011176069)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3250[4] = 
{
	FutureStatus_t4011176069::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3251 = { sizeof (UnitySynchronizationContext_t2114871256), -1, sizeof(UnitySynchronizationContext_t2114871256_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3251[5] = 
{
	UnitySynchronizationContext_t2114871256_StaticFields::get_offset_of__instance_1(),
	UnitySynchronizationContext_t2114871256::get_offset_of_queue_2(),
	UnitySynchronizationContext_t2114871256::get_offset_of_behavior_3(),
	UnitySynchronizationContext_t2114871256::get_offset_of_mainThreadId_4(),
	UnitySynchronizationContext_t2114871256_StaticFields::get_offset_of_signalDictionary_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3252 = { sizeof (SynchronizationContextBehavoir_t99138606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3252[1] = 
{
	SynchronizationContextBehavoir_t99138606::get_offset_of_callbackQueue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3253 = { sizeof (U3CStartU3Ec__Iterator0_t1234987676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3253[6] = 
{
	U3CStartU3Ec__Iterator0_t1234987676::get_offset_of_U3CentryU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1234987676::get_offset_of_U24locvar0_1(),
	U3CStartU3Ec__Iterator0_t1234987676::get_offset_of_U24this_2(),
	U3CStartU3Ec__Iterator0_t1234987676::get_offset_of_U24current_3(),
	U3CStartU3Ec__Iterator0_t1234987676::get_offset_of_U24disposing_4(),
	U3CStartU3Ec__Iterator0_t1234987676::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3254 = { sizeof (U3CSignaledCoroutineU3Ec__Iterator0_t3455787875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3254[5] = 
{
	U3CSignaledCoroutineU3Ec__Iterator0_t3455787875::get_offset_of_coroutine_0(),
	U3CSignaledCoroutineU3Ec__Iterator0_t3455787875::get_offset_of_newSignal_1(),
	U3CSignaledCoroutineU3Ec__Iterator0_t3455787875::get_offset_of_U24current_2(),
	U3CSignaledCoroutineU3Ec__Iterator0_t3455787875::get_offset_of_U24disposing_3(),
	U3CSignaledCoroutineU3Ec__Iterator0_t3455787875::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3255 = { sizeof (U3CSendCoroutineU3Ec__AnonStorey1_t982275783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3255[2] = 
{
	U3CSendCoroutineU3Ec__AnonStorey1_t982275783::get_offset_of_coroutine_0(),
	U3CSendCoroutineU3Ec__AnonStorey1_t982275783::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3256 = { sizeof (U3CSendCoroutineU3Ec__AnonStorey2_t3711159138), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3256[2] = 
{
	U3CSendCoroutineU3Ec__AnonStorey2_t3711159138::get_offset_of_newSignal_0(),
	U3CSendCoroutineU3Ec__AnonStorey2_t3711159138::get_offset_of_U3CU3Ef__refU241_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3257 = { sizeof (U3CSendU3Ec__AnonStorey3_t2967043091), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3258 = { sizeof (U3CSendU3Ec__AnonStorey4_t2967043096), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3259 = { sizeof (UnityPlatformServices_t848686304), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3260 = { sizeof (UnityLoggingService_t1590581383), -1, sizeof(UnityLoggingService_t1590581383_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3260[1] = 
{
	UnityLoggingService_t1590581383_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3261 = { sizeof (InstallRootCerts_t2159695568), -1, sizeof(InstallRootCerts_t2159695568_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3261[6] = 
{
	InstallRootCerts_t2159695568_StaticFields::get_offset_of_Sync_0(),
	InstallRootCerts_t2159695568_StaticFields::get_offset_of__installedRoots_1(),
	InstallRootCerts_t2159695568_StaticFields::get_offset_of__attemptedWebDownload_2(),
	InstallRootCerts_t2159695568_StaticFields::get_offset_of__instance_3(),
	InstallRootCerts_t2159695568_StaticFields::get_offset_of_TrustedRoot_4(),
	InstallRootCerts_t2159695568_StaticFields::get_offset_of_IntermediateCA_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3262 = { sizeof (WWWHttpRequest_t1831362850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3262[9] = 
{
	WWWHttpRequest_t1831362850::get_offset_of__headers_10(),
	WWWHttpRequest_t1831362850::get_offset_of__responseheaders_11(),
	WWWHttpRequest_t1831362850::get_offset_of__requestBody_12(),
	WWWHttpRequest_t1831362850::get_offset_of__error_13(),
	WWWHttpRequest_t1831362850::get_offset_of__executed_14(),
	WWWHttpRequest_t1831362850::get_offset_of__requestBodyBytes_15(),
	WWWHttpRequest_t1831362850::get_offset_of__responseBodyBytes_16(),
	WWWHttpRequest_t1831362850::get_offset_of__responseCode_17(),
	WWWHttpRequest_t1831362850::get_offset_of__responseLength_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3263 = { sizeof (U3CSendUnityRequestU3Ec__Iterator0_t937125532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3263[6] = 
{
	U3CSendUnityRequestU3Ec__Iterator0_t937125532::get_offset_of_U24locvar1_0(),
	U3CSendUnityRequestU3Ec__Iterator0_t937125532::get_offset_of_U3CwwwU3E__0_1(),
	U3CSendUnityRequestU3Ec__Iterator0_t937125532::get_offset_of_U24this_2(),
	U3CSendUnityRequestU3Ec__Iterator0_t937125532::get_offset_of_U24current_3(),
	U3CSendUnityRequestU3Ec__Iterator0_t937125532::get_offset_of_U24disposing_4(),
	U3CSendUnityRequestU3Ec__Iterator0_t937125532::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3264 = { sizeof (UnityHttpFactoryService_t22354146), -1, sizeof(UnityHttpFactoryService_t22354146_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3264[1] = 
{
	UnityHttpFactoryService_t22354146_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3265 = { sizeof (UnityConfigExtensions_t1763038139), -1, sizeof(UnityConfigExtensions_t1763038139_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3265[1] = 
{
	UnityConfigExtensions_t1763038139_StaticFields::get_offset_of__instance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3266 = { sizeof (FirebaseEditorExtensions_t83728876), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3267 = { sizeof (Services_t3629302694), -1, sizeof(Services_t3629302694_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3267[6] = 
{
	Services_t3629302694_StaticFields::get_offset_of_U3CAppConfigU3Ek__BackingField_0(),
	Services_t3629302694_StaticFields::get_offset_of_U3CAuthU3Ek__BackingField_1(),
	Services_t3629302694_StaticFields::get_offset_of_U3CRootCertsU3Ek__BackingField_2(),
	Services_t3629302694_StaticFields::get_offset_of_U3CClockU3Ek__BackingField_3(),
	Services_t3629302694_StaticFields::get_offset_of_U3CHttpFactoryU3Ek__BackingField_4(),
	Services_t3629302694_StaticFields::get_offset_of_U3CLoggingU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3268 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3269 = { sizeof (DebugLogger_t542752859), -1, sizeof(DebugLogger_t542752859_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3269[1] = 
{
	DebugLogger_t542752859_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3270 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3271 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3272 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3273 = { sizeof (NoopCertificateService_t3432069064), -1, sizeof(NoopCertificateService_t3432069064_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3273[1] = 
{
	NoopCertificateService_t3432069064_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3274 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3275 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3276 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3277 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3278 = { sizeof (FirebaseHttpRequest_t934394464), -1, sizeof(FirebaseHttpRequest_t934394464_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3278[10] = 
{
	FirebaseHttpRequest_t934394464::get_offset_of__url_0(),
	FirebaseHttpRequest_t934394464::get_offset_of__action_1(),
	FirebaseHttpRequest_t934394464_StaticFields::get_offset_of_HeaderContentLength_2(),
	FirebaseHttpRequest_t934394464_StaticFields::get_offset_of_HeaderContentType_3(),
	FirebaseHttpRequest_t934394464_StaticFields::get_offset_of_HeaderRange_4(),
	FirebaseHttpRequest_t934394464_StaticFields::get_offset_of_HeaderUserAgent_5(),
	FirebaseHttpRequest_t934394464_StaticFields::get_offset_of_HeaderStatus_6(),
	FirebaseHttpRequest_t934394464_StaticFields::get_offset_of_Timeout_7(),
	FirebaseHttpRequest_t934394464_StaticFields::get_offset_of_StatusNetworkUnavailable_8(),
	FirebaseHttpRequest_t934394464_StaticFields::get_offset_of_StatusOk_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3279 = { sizeof (ServiceCredential_t2687018680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3279[2] = 
{
	ServiceCredential_t2687018680::get_offset_of_U3CTokenServerUrlU3Ek__BackingField_0(),
	ServiceCredential_t2687018680::get_offset_of_U3CClockU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3280 = { sizeof (Initializer_t3855616227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3280[2] = 
{
	Initializer_t3855616227::get_offset_of_U3CTokenServerUrlU3Ek__BackingField_0(),
	Initializer_t3855616227::get_offset_of_U3CClockU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3281 = { sizeof (ServiceAccountCredential_t895213149), -1, sizeof(ServiceAccountCredential_t895213149_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3281[5] = 
{
	ServiceAccountCredential_t895213149::get_offset_of_U3CIdU3Ek__BackingField_2(),
	ServiceAccountCredential_t895213149::get_offset_of_U3CUserU3Ek__BackingField_3(),
	ServiceAccountCredential_t895213149::get_offset_of_U3CScopesU3Ek__BackingField_4(),
	ServiceAccountCredential_t895213149::get_offset_of_U3CKeyU3Ek__BackingField_5(),
	ServiceAccountCredential_t895213149_StaticFields::get_offset_of_UnixEpoch_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3282 = { sizeof (OAuthRequest_t1020070256), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3282[2] = 
{
	OAuthRequest_t1020070256::get_offset_of_Assertion_0(),
	OAuthRequest_t1020070256::get_offset_of_ResponseBody_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3283 = { sizeof (Initializer_t2857161564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3283[4] = 
{
	Initializer_t2857161564::get_offset_of_U3CIdU3Ek__BackingField_2(),
	Initializer_t2857161564::get_offset_of_U3CUserU3Ek__BackingField_3(),
	Initializer_t2857161564::get_offset_of_U3CScopesU3Ek__BackingField_4(),
	Initializer_t2857161564::get_offset_of_U3CKeyU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3284 = { sizeof (GAuthToken_t3916815478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3284[2] = 
{
	GAuthToken_t3916815478::get_offset_of__auth_0(),
	GAuthToken_t3916815478::get_offset_of__token_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3285 = { sizeof (SystemClock_t3210207471), -1, sizeof(SystemClock_t3210207471_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3285[1] = 
{
	SystemClock_t3210207471_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3286 = { sizeof (BaseAuthService_t1401274330), -1, sizeof(BaseAuthService_t1401274330_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3286[1] = 
{
	BaseAuthService_t1401274330_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3287 = { sizeof (U3CGetTokenAsyncU3Ec__AnonStorey0_t287602938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3287[1] = 
{
	U3CGetTokenAsyncU3Ec__AnonStorey0_t287602938::get_offset_of_listener_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3288 = { sizeof (AppConfigExtensions_t2317624261), -1, sizeof(AppConfigExtensions_t2317624261_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3288[5] = 
{
	AppConfigExtensions_t2317624261_StaticFields::get_offset_of_DefaultUpdateUrl_0(),
	AppConfigExtensions_t2317624261_StaticFields::get_offset_of_Default_1(),
	AppConfigExtensions_t2317624261_StaticFields::get_offset_of_Sync_2(),
	AppConfigExtensions_t2317624261_StaticFields::get_offset_of__instance_3(),
	AppConfigExtensions_t2317624261_StaticFields::get_offset_of_SStringState_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3289 = { sizeof (VariantExtension_t1191592924), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3290 = { sizeof (InitializationException_t1438959983), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3290[1] = 
{
	InitializationException_t1438959983::get_offset_of_U3CInitResultU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3291 = { sizeof (FirebaseException_t2567272216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3291[1] = 
{
	FirebaseException_t2567272216::get_offset_of_U3CErrorCodeU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3292 = { sizeof (DependencyStatus_t2752419415)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3292[9] = 
{
	DependencyStatus_t2752419415::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3293 = { sizeof (U3CModuleU3E_t3783534227), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3294 = { sizeof (WrappedSystemStream_t831474186), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3294[4] = 
{
	WrappedSystemStream_t831474186::get_offset_of_ist_2(),
	WrappedSystemStream_t831474186::get_offset_of_ost_3(),
	WrappedSystemStream_t831474186::get_offset_of_position_4(),
	WrappedSystemStream_t831474186::get_offset_of_markedPosition_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3295 = { sizeof (URLEncoder_t1142290277), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3296 = { sizeof (TimeUnit_t4006728025)+ sizeof (Il2CppObject), sizeof(int64_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3296[3] = 
{
	TimeUnit_t4006728025::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3297 = { sizeof (TimeUnitExtensions_t1386558595), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3298 = { sizeof (ThreadPoolExecutor_t376308723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3298[8] = 
{
	ThreadPoolExecutor_t376308723::get_offset_of__corePoolSize_0(),
	ThreadPoolExecutor_t376308723::get_offset_of__pendingTasks_1(),
	ThreadPoolExecutor_t376308723::get_offset_of__pool_2(),
	ThreadPoolExecutor_t376308723::get_offset_of__factory_3(),
	ThreadPoolExecutor_t376308723::get_offset_of__freeThreads_4(),
	ThreadPoolExecutor_t376308723::get_offset_of__maxPoolSize_5(),
	ThreadPoolExecutor_t376308723::get_offset_of__runningThreads_6(),
	ThreadPoolExecutor_t376308723::get_offset_of__shutdown_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3299 = { sizeof (RunnableAction_t2631350173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3299[1] = 
{
	RunnableAction_t2631350173::get_offset_of__action_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
