﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Markers/Vumark_Class
struct  Vumark_Class_t29598747  : public Il2CppObject
{
public:
	// System.Single Markers/Vumark_Class::<pos_vumark_x>k__BackingField
	float ___U3Cpos_vumark_xU3Ek__BackingField_0;
	// System.Single Markers/Vumark_Class::<pos_vumark_y>k__BackingField
	float ___U3Cpos_vumark_yU3Ek__BackingField_1;
	// System.Single Markers/Vumark_Class::<pos_vumark_z>k__BackingField
	float ___U3Cpos_vumark_zU3Ek__BackingField_2;
	// System.Single Markers/Vumark_Class::<Rot_vumark_x>k__BackingField
	float ___U3CRot_vumark_xU3Ek__BackingField_3;
	// System.Single Markers/Vumark_Class::<Rot_vumark_y>k__BackingField
	float ___U3CRot_vumark_yU3Ek__BackingField_4;
	// System.Single Markers/Vumark_Class::<Rot_vumark_z>k__BackingField
	float ___U3CRot_vumark_zU3Ek__BackingField_5;
	// System.Int32 Markers/Vumark_Class::<LevelID_markers>k__BackingField
	int32_t ___U3CLevelID_markersU3Ek__BackingField_6;
	// System.String Markers/Vumark_Class::<VuMarkID>k__BackingField
	String_t* ___U3CVuMarkIDU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3Cpos_vumark_xU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Vumark_Class_t29598747, ___U3Cpos_vumark_xU3Ek__BackingField_0)); }
	inline float get_U3Cpos_vumark_xU3Ek__BackingField_0() const { return ___U3Cpos_vumark_xU3Ek__BackingField_0; }
	inline float* get_address_of_U3Cpos_vumark_xU3Ek__BackingField_0() { return &___U3Cpos_vumark_xU3Ek__BackingField_0; }
	inline void set_U3Cpos_vumark_xU3Ek__BackingField_0(float value)
	{
		___U3Cpos_vumark_xU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Cpos_vumark_yU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Vumark_Class_t29598747, ___U3Cpos_vumark_yU3Ek__BackingField_1)); }
	inline float get_U3Cpos_vumark_yU3Ek__BackingField_1() const { return ___U3Cpos_vumark_yU3Ek__BackingField_1; }
	inline float* get_address_of_U3Cpos_vumark_yU3Ek__BackingField_1() { return &___U3Cpos_vumark_yU3Ek__BackingField_1; }
	inline void set_U3Cpos_vumark_yU3Ek__BackingField_1(float value)
	{
		___U3Cpos_vumark_yU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3Cpos_vumark_zU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Vumark_Class_t29598747, ___U3Cpos_vumark_zU3Ek__BackingField_2)); }
	inline float get_U3Cpos_vumark_zU3Ek__BackingField_2() const { return ___U3Cpos_vumark_zU3Ek__BackingField_2; }
	inline float* get_address_of_U3Cpos_vumark_zU3Ek__BackingField_2() { return &___U3Cpos_vumark_zU3Ek__BackingField_2; }
	inline void set_U3Cpos_vumark_zU3Ek__BackingField_2(float value)
	{
		___U3Cpos_vumark_zU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CRot_vumark_xU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Vumark_Class_t29598747, ___U3CRot_vumark_xU3Ek__BackingField_3)); }
	inline float get_U3CRot_vumark_xU3Ek__BackingField_3() const { return ___U3CRot_vumark_xU3Ek__BackingField_3; }
	inline float* get_address_of_U3CRot_vumark_xU3Ek__BackingField_3() { return &___U3CRot_vumark_xU3Ek__BackingField_3; }
	inline void set_U3CRot_vumark_xU3Ek__BackingField_3(float value)
	{
		___U3CRot_vumark_xU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CRot_vumark_yU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Vumark_Class_t29598747, ___U3CRot_vumark_yU3Ek__BackingField_4)); }
	inline float get_U3CRot_vumark_yU3Ek__BackingField_4() const { return ___U3CRot_vumark_yU3Ek__BackingField_4; }
	inline float* get_address_of_U3CRot_vumark_yU3Ek__BackingField_4() { return &___U3CRot_vumark_yU3Ek__BackingField_4; }
	inline void set_U3CRot_vumark_yU3Ek__BackingField_4(float value)
	{
		___U3CRot_vumark_yU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CRot_vumark_zU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Vumark_Class_t29598747, ___U3CRot_vumark_zU3Ek__BackingField_5)); }
	inline float get_U3CRot_vumark_zU3Ek__BackingField_5() const { return ___U3CRot_vumark_zU3Ek__BackingField_5; }
	inline float* get_address_of_U3CRot_vumark_zU3Ek__BackingField_5() { return &___U3CRot_vumark_zU3Ek__BackingField_5; }
	inline void set_U3CRot_vumark_zU3Ek__BackingField_5(float value)
	{
		___U3CRot_vumark_zU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CLevelID_markersU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Vumark_Class_t29598747, ___U3CLevelID_markersU3Ek__BackingField_6)); }
	inline int32_t get_U3CLevelID_markersU3Ek__BackingField_6() const { return ___U3CLevelID_markersU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CLevelID_markersU3Ek__BackingField_6() { return &___U3CLevelID_markersU3Ek__BackingField_6; }
	inline void set_U3CLevelID_markersU3Ek__BackingField_6(int32_t value)
	{
		___U3CLevelID_markersU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CVuMarkIDU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Vumark_Class_t29598747, ___U3CVuMarkIDU3Ek__BackingField_7)); }
	inline String_t* get_U3CVuMarkIDU3Ek__BackingField_7() const { return ___U3CVuMarkIDU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CVuMarkIDU3Ek__BackingField_7() { return &___U3CVuMarkIDU3Ek__BackingField_7; }
	inline void set_U3CVuMarkIDU3Ek__BackingField_7(String_t* value)
	{
		___U3CVuMarkIDU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CVuMarkIDU3Ek__BackingField_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
