﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Button
struct Button_t2872111280;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Change_View
struct  Change_View_t1623420308  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] Change_View::obj_bats
	GameObjectU5BU5D_t3057952154* ___obj_bats_2;
	// UnityEngine.GameObject Change_View::Origin
	GameObject_t1756533147 * ___Origin_3;
	// UnityEngine.GameObject Change_View::MCamera
	GameObject_t1756533147 * ___MCamera_4;
	// UnityEngine.GameObject Change_View::ArCamera
	GameObject_t1756533147 * ___ArCamera_5;
	// UnityEngine.UI.Text Change_View::Txt_action
	Text_t356221433 * ___Txt_action_6;
	// UnityEngine.UI.Text Change_View::txt_version
	Text_t356221433 * ___txt_version_7;
	// UnityEngine.GameObject Change_View::Menu_Bdd
	GameObject_t1756533147 * ___Menu_Bdd_8;
	// System.Boolean Change_View::isShowing
	bool ___isShowing_9;
	// System.Boolean Change_View::destination_checked
	bool ___destination_checked_10;
	// UnityEngine.UI.Button Change_View::BtnPlan
	Button_t2872111280 * ___BtnPlan_11;
	// UnityEngine.UI.Button Change_View::BtnNaviguer
	Button_t2872111280 * ___BtnNaviguer_12;
	// UnityEngine.UI.Button Change_View::BtnExit
	Button_t2872111280 * ___BtnExit_13;
	// UnityEngine.UI.Button Change_View::Btn_Destination
	Button_t2872111280 * ___Btn_Destination_14;

public:
	inline static int32_t get_offset_of_obj_bats_2() { return static_cast<int32_t>(offsetof(Change_View_t1623420308, ___obj_bats_2)); }
	inline GameObjectU5BU5D_t3057952154* get_obj_bats_2() const { return ___obj_bats_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_obj_bats_2() { return &___obj_bats_2; }
	inline void set_obj_bats_2(GameObjectU5BU5D_t3057952154* value)
	{
		___obj_bats_2 = value;
		Il2CppCodeGenWriteBarrier(&___obj_bats_2, value);
	}

	inline static int32_t get_offset_of_Origin_3() { return static_cast<int32_t>(offsetof(Change_View_t1623420308, ___Origin_3)); }
	inline GameObject_t1756533147 * get_Origin_3() const { return ___Origin_3; }
	inline GameObject_t1756533147 ** get_address_of_Origin_3() { return &___Origin_3; }
	inline void set_Origin_3(GameObject_t1756533147 * value)
	{
		___Origin_3 = value;
		Il2CppCodeGenWriteBarrier(&___Origin_3, value);
	}

	inline static int32_t get_offset_of_MCamera_4() { return static_cast<int32_t>(offsetof(Change_View_t1623420308, ___MCamera_4)); }
	inline GameObject_t1756533147 * get_MCamera_4() const { return ___MCamera_4; }
	inline GameObject_t1756533147 ** get_address_of_MCamera_4() { return &___MCamera_4; }
	inline void set_MCamera_4(GameObject_t1756533147 * value)
	{
		___MCamera_4 = value;
		Il2CppCodeGenWriteBarrier(&___MCamera_4, value);
	}

	inline static int32_t get_offset_of_ArCamera_5() { return static_cast<int32_t>(offsetof(Change_View_t1623420308, ___ArCamera_5)); }
	inline GameObject_t1756533147 * get_ArCamera_5() const { return ___ArCamera_5; }
	inline GameObject_t1756533147 ** get_address_of_ArCamera_5() { return &___ArCamera_5; }
	inline void set_ArCamera_5(GameObject_t1756533147 * value)
	{
		___ArCamera_5 = value;
		Il2CppCodeGenWriteBarrier(&___ArCamera_5, value);
	}

	inline static int32_t get_offset_of_Txt_action_6() { return static_cast<int32_t>(offsetof(Change_View_t1623420308, ___Txt_action_6)); }
	inline Text_t356221433 * get_Txt_action_6() const { return ___Txt_action_6; }
	inline Text_t356221433 ** get_address_of_Txt_action_6() { return &___Txt_action_6; }
	inline void set_Txt_action_6(Text_t356221433 * value)
	{
		___Txt_action_6 = value;
		Il2CppCodeGenWriteBarrier(&___Txt_action_6, value);
	}

	inline static int32_t get_offset_of_txt_version_7() { return static_cast<int32_t>(offsetof(Change_View_t1623420308, ___txt_version_7)); }
	inline Text_t356221433 * get_txt_version_7() const { return ___txt_version_7; }
	inline Text_t356221433 ** get_address_of_txt_version_7() { return &___txt_version_7; }
	inline void set_txt_version_7(Text_t356221433 * value)
	{
		___txt_version_7 = value;
		Il2CppCodeGenWriteBarrier(&___txt_version_7, value);
	}

	inline static int32_t get_offset_of_Menu_Bdd_8() { return static_cast<int32_t>(offsetof(Change_View_t1623420308, ___Menu_Bdd_8)); }
	inline GameObject_t1756533147 * get_Menu_Bdd_8() const { return ___Menu_Bdd_8; }
	inline GameObject_t1756533147 ** get_address_of_Menu_Bdd_8() { return &___Menu_Bdd_8; }
	inline void set_Menu_Bdd_8(GameObject_t1756533147 * value)
	{
		___Menu_Bdd_8 = value;
		Il2CppCodeGenWriteBarrier(&___Menu_Bdd_8, value);
	}

	inline static int32_t get_offset_of_isShowing_9() { return static_cast<int32_t>(offsetof(Change_View_t1623420308, ___isShowing_9)); }
	inline bool get_isShowing_9() const { return ___isShowing_9; }
	inline bool* get_address_of_isShowing_9() { return &___isShowing_9; }
	inline void set_isShowing_9(bool value)
	{
		___isShowing_9 = value;
	}

	inline static int32_t get_offset_of_destination_checked_10() { return static_cast<int32_t>(offsetof(Change_View_t1623420308, ___destination_checked_10)); }
	inline bool get_destination_checked_10() const { return ___destination_checked_10; }
	inline bool* get_address_of_destination_checked_10() { return &___destination_checked_10; }
	inline void set_destination_checked_10(bool value)
	{
		___destination_checked_10 = value;
	}

	inline static int32_t get_offset_of_BtnPlan_11() { return static_cast<int32_t>(offsetof(Change_View_t1623420308, ___BtnPlan_11)); }
	inline Button_t2872111280 * get_BtnPlan_11() const { return ___BtnPlan_11; }
	inline Button_t2872111280 ** get_address_of_BtnPlan_11() { return &___BtnPlan_11; }
	inline void set_BtnPlan_11(Button_t2872111280 * value)
	{
		___BtnPlan_11 = value;
		Il2CppCodeGenWriteBarrier(&___BtnPlan_11, value);
	}

	inline static int32_t get_offset_of_BtnNaviguer_12() { return static_cast<int32_t>(offsetof(Change_View_t1623420308, ___BtnNaviguer_12)); }
	inline Button_t2872111280 * get_BtnNaviguer_12() const { return ___BtnNaviguer_12; }
	inline Button_t2872111280 ** get_address_of_BtnNaviguer_12() { return &___BtnNaviguer_12; }
	inline void set_BtnNaviguer_12(Button_t2872111280 * value)
	{
		___BtnNaviguer_12 = value;
		Il2CppCodeGenWriteBarrier(&___BtnNaviguer_12, value);
	}

	inline static int32_t get_offset_of_BtnExit_13() { return static_cast<int32_t>(offsetof(Change_View_t1623420308, ___BtnExit_13)); }
	inline Button_t2872111280 * get_BtnExit_13() const { return ___BtnExit_13; }
	inline Button_t2872111280 ** get_address_of_BtnExit_13() { return &___BtnExit_13; }
	inline void set_BtnExit_13(Button_t2872111280 * value)
	{
		___BtnExit_13 = value;
		Il2CppCodeGenWriteBarrier(&___BtnExit_13, value);
	}

	inline static int32_t get_offset_of_Btn_Destination_14() { return static_cast<int32_t>(offsetof(Change_View_t1623420308, ___Btn_Destination_14)); }
	inline Button_t2872111280 * get_Btn_Destination_14() const { return ___Btn_Destination_14; }
	inline Button_t2872111280 ** get_address_of_Btn_Destination_14() { return &___Btn_Destination_14; }
	inline void set_Btn_Destination_14(Button_t2872111280 * value)
	{
		___Btn_Destination_14 = value;
		Il2CppCodeGenWriteBarrier(&___Btn_Destination_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
