﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Exit_Level
struct  Exit_Level_t1387234555  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 Exit_Level::exit_level_var
	int32_t ___exit_level_var_2;

public:
	inline static int32_t get_offset_of_exit_level_var_2() { return static_cast<int32_t>(offsetof(Exit_Level_t1387234555, ___exit_level_var_2)); }
	inline int32_t get_exit_level_var_2() const { return ___exit_level_var_2; }
	inline int32_t* get_address_of_exit_level_var_2() { return &___exit_level_var_2; }
	inline void set_exit_level_var_2(int32_t value)
	{
		___exit_level_var_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
