﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.Material[]
struct MaterialU5BU5D_t3123989686;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// outlineShader
struct  outlineShader_t244045327  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] outlineShader::gameonj_current
	GameObjectU5BU5D_t3057952154* ___gameonj_current_2;
	// UnityEngine.Material[] outlineShader::_newmat
	MaterialU5BU5D_t3123989686* ____newmat_3;
	// UnityEngine.Color outlineShader::newcolor
	Color_t2020392075  ___newcolor_4;
	// UnityEngine.KeyCode outlineShader::changecolor
	int32_t ___changecolor_5;

public:
	inline static int32_t get_offset_of_gameonj_current_2() { return static_cast<int32_t>(offsetof(outlineShader_t244045327, ___gameonj_current_2)); }
	inline GameObjectU5BU5D_t3057952154* get_gameonj_current_2() const { return ___gameonj_current_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_gameonj_current_2() { return &___gameonj_current_2; }
	inline void set_gameonj_current_2(GameObjectU5BU5D_t3057952154* value)
	{
		___gameonj_current_2 = value;
		Il2CppCodeGenWriteBarrier(&___gameonj_current_2, value);
	}

	inline static int32_t get_offset_of__newmat_3() { return static_cast<int32_t>(offsetof(outlineShader_t244045327, ____newmat_3)); }
	inline MaterialU5BU5D_t3123989686* get__newmat_3() const { return ____newmat_3; }
	inline MaterialU5BU5D_t3123989686** get_address_of__newmat_3() { return &____newmat_3; }
	inline void set__newmat_3(MaterialU5BU5D_t3123989686* value)
	{
		____newmat_3 = value;
		Il2CppCodeGenWriteBarrier(&____newmat_3, value);
	}

	inline static int32_t get_offset_of_newcolor_4() { return static_cast<int32_t>(offsetof(outlineShader_t244045327, ___newcolor_4)); }
	inline Color_t2020392075  get_newcolor_4() const { return ___newcolor_4; }
	inline Color_t2020392075 * get_address_of_newcolor_4() { return &___newcolor_4; }
	inline void set_newcolor_4(Color_t2020392075  value)
	{
		___newcolor_4 = value;
	}

	inline static int32_t get_offset_of_changecolor_5() { return static_cast<int32_t>(offsetof(outlineShader_t244045327, ___changecolor_5)); }
	inline int32_t get_changecolor_5() const { return ___changecolor_5; }
	inline int32_t* get_address_of_changecolor_5() { return &___changecolor_5; }
	inline void set_changecolor_5(int32_t value)
	{
		___changecolor_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
