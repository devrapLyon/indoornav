﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Users
struct Users_t907518852;
// OfficeData
struct OfficeData_t3828749716;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RootObject
struct  RootObject_t1671561673  : public Il2CppObject
{
public:
	// Users RootObject::<users>k__BackingField
	Users_t907518852 * ___U3CusersU3Ek__BackingField_0;
	// OfficeData RootObject::<OfficeData>k__BackingField
	OfficeData_t3828749716 * ___U3COfficeDataU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CusersU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RootObject_t1671561673, ___U3CusersU3Ek__BackingField_0)); }
	inline Users_t907518852 * get_U3CusersU3Ek__BackingField_0() const { return ___U3CusersU3Ek__BackingField_0; }
	inline Users_t907518852 ** get_address_of_U3CusersU3Ek__BackingField_0() { return &___U3CusersU3Ek__BackingField_0; }
	inline void set_U3CusersU3Ek__BackingField_0(Users_t907518852 * value)
	{
		___U3CusersU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CusersU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3COfficeDataU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RootObject_t1671561673, ___U3COfficeDataU3Ek__BackingField_1)); }
	inline OfficeData_t3828749716 * get_U3COfficeDataU3Ek__BackingField_1() const { return ___U3COfficeDataU3Ek__BackingField_1; }
	inline OfficeData_t3828749716 ** get_address_of_U3COfficeDataU3Ek__BackingField_1() { return &___U3COfficeDataU3Ek__BackingField_1; }
	inline void set_U3COfficeDataU3Ek__BackingField_1(OfficeData_t3828749716 * value)
	{
		___U3COfficeDataU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3COfficeDataU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
