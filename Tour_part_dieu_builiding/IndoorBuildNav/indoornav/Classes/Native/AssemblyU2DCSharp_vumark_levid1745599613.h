﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// vumark_levid
struct  vumark_levid_t1745599613  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 vumark_levid::vumark_level_num
	int32_t ___vumark_level_num_2;
	// System.String vumark_levid::marker_id
	String_t* ___marker_id_3;

public:
	inline static int32_t get_offset_of_vumark_level_num_2() { return static_cast<int32_t>(offsetof(vumark_levid_t1745599613, ___vumark_level_num_2)); }
	inline int32_t get_vumark_level_num_2() const { return ___vumark_level_num_2; }
	inline int32_t* get_address_of_vumark_level_num_2() { return &___vumark_level_num_2; }
	inline void set_vumark_level_num_2(int32_t value)
	{
		___vumark_level_num_2 = value;
	}

	inline static int32_t get_offset_of_marker_id_3() { return static_cast<int32_t>(offsetof(vumark_levid_t1745599613, ___marker_id_3)); }
	inline String_t* get_marker_id_3() const { return ___marker_id_3; }
	inline String_t** get_address_of_marker_id_3() { return &___marker_id_3; }
	inline void set_marker_id_3(String_t* value)
	{
		___marker_id_3 = value;
		Il2CppCodeGenWriteBarrier(&___marker_id_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
