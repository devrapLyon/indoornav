﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2737799006.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tran4064153.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr704776203.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3322033499.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3939605346.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr880920208.h"
#include "mscorlib_System_Reflection_Emit_Label4243202660.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3135385623.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21008373517.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4198889554.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2075247470.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3837169113.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour4057911311.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1611375883.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_823082168.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2860171163.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr551237730.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2883023109.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1223885651.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2118171515.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22284023804.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1906025159.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1058033362.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1772107233.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3836940203.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_818741072.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr795109283.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3927277168.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2356341726.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr743779247.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21436312919.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr216544365.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1996915623.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2129470893.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3646810779.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_271247988.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1152472943.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1584656104.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2713705386.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr553649823.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_888819835.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2168890597.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2354279283.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2990554165.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2213854845.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr339959515.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21174980068.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1854428742.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1676220171.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2343594867.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr699222221.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1316794068.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr427295924.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3652704231.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21979316409.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2008331585.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tra67869821.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2260454664.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3545401207.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1901028561.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1173087252.h"
#include "mscorlib_System_UInt16986882611.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr755467111.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22631255257.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr813661149.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3406061761.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4113099591.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr657337123.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23888080226.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3753673488.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3307931773.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2507364635.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile_Prof1724666488.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2827528371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23369039134.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2147938532.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1183155725.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3129600301.h"
#include "Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT3010530044.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2221772955.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22141048052.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2770174198.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3091254947.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2910497288.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManager_Tra1329355276.h"
#include "Vuforia_UnityExtensions_Vuforia_HoloLensExtendedTr3432166560.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2285374327.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22423752437.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3293788450.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1190977166.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2927905217.h"
#include "Vuforia_UnityExtensions_Vuforia_HoloLensExtendedTr1161658011.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tra32273707.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_153243888.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1040687830.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1208385095.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2908250109.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2908871899.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23049497188.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3917286022.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1188729987.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2766235847.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1079703083.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2399727785.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23132015601.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3383807694.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1697274930.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va642593763.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3251028295.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En276085701.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va457302414.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3065736946.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enu90794352.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1918244050.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge231711286.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1551735988.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va452961318.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3061395850.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enu86453256.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1070533165.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3678967697.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En704025103.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4200435530.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2513902766.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3833927468.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va523040081.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3131474613.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En156532019.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va809200314.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3417634846.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En442692252.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3350470340.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1663937576.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2983962278.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1613536655.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4221971187.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1247028593.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3968042187.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2281509423.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2265475503.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge578942739.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1898967441.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3522300472.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1835767708.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3155792410.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3003259380.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1316726616.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2636751318.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1775268298.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen88735534.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1408760236.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2057972683.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge371439919.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1691464621.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4082431430.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2395898666.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3715923368.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2683717434.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge997184670.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2317209372.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4077730222.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va400334773.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1954088138.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1768796789.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>
struct Transform_1_t2737799006;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t4064153;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct Transform_1_t704776203;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>
struct Transform_1_t3322033499;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>
struct Transform_1_t3939605346;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.DictionaryEntry>
struct Transform_1_t880920208;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>
struct Transform_1_t3135385623;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Int32>
struct Transform_1_t4198889554;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Reflection.Emit.Label>
struct Transform_1_t2075247470;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,System.Collections.DictionaryEntry>
struct Transform_1_t3837169113;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>>
struct Transform_1_t1611375883;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,System.Int32>
struct Transform_1_t2860171163;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status>
struct Transform_1_t551237730;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.DictionaryEntry>
struct Transform_1_t2883023109;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>
struct Transform_1_t2118171515;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Int32>
struct Transform_1_t1906025159;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct Transform_1_t1058033362;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t1772107233;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>>
struct Transform_1_t3836940203;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Int32>
struct Transform_1_t795109283;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Int64>
struct Transform_1_t3927277168;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t2356341726;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>
struct Transform_1_t743779247;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Int64>
struct Transform_1_t216544365;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Object>
struct Transform_1_t1996915623;
// System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t2129470893;
// System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Int32>>
struct Transform_1_t3646810779;
// System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Int32>
struct Transform_1_t1152472943;
// System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.IntPtr>
struct Transform_1_t1584656104;
// System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t2713705386;
// System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>
struct Transform_1_t553649823;
// System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.IntPtr>
struct Transform_1_t2168890597;
// System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Object>
struct Transform_1_t2354279283;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Boolean>
struct Transform_1_t2990554165;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.DictionaryEntry>
struct Transform_1_t2213854845;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct Transform_1_t339959515;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Object>
struct Transform_1_t1854428742;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t1676220171;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct Transform_1_t2343594867;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Int32>
struct Transform_1_t699222221;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Object>
struct Transform_1_t1316794068;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Collections.DictionaryEntry>
struct Transform_1_t427295924;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Int32>>>
struct Transform_1_t3652704231;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>>
struct Transform_1_t2008331585;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Object>
struct Transform_1_t67869821;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t2260454664;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Transform_1_t3545401207;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>
struct Transform_1_t1901028561;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.DictionaryEntry>
struct Transform_1_t1173087252;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>
struct Transform_1_t755467111;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Object>
struct Transform_1_t813661149;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.UInt16>
struct Transform_1_t3406061761;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,System.Collections.DictionaryEntry>
struct Transform_1_t4113099591;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Vector3>>
struct Transform_1_t657337123;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,System.Object>
struct Transform_1_t3753673488;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,UnityEngine.Vector3>
struct Transform_1_t3307931773;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>
struct Transform_1_t2507364635;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>
struct Transform_1_t2827528371;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Object>
struct Transform_1_t2147938532;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,Vuforia.WebCamProfile/ProfileData>
struct Transform_1_t1183155725;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t3129600301;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>
struct Transform_1_t2221772955;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,System.Object>
struct Transform_1_t2770174198;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,Vuforia.Image/PIXEL_FORMAT>
struct Transform_1_t3091254947;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,System.Collections.DictionaryEntry>
struct Transform_1_t2910497288;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>>
struct Transform_1_t2285374327;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>
struct Transform_1_t3293788450;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,Vuforia.VuforiaManager/TrackableIdPair>
struct Transform_1_t1190977166;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,System.Collections.DictionaryEntry>
struct Transform_1_t2927905217;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>>
struct Transform_1_t32273707;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>
struct Transform_1_t1040687830;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,Vuforia.VuforiaManager/TrackableIdPair>
struct Transform_1_t1208385095;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,System.Collections.DictionaryEntry>
struct Transform_1_t2908250109;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>>
struct Transform_1_t2908871899;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status>
struct Transform_1_t3917286022;
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,Vuforia.VuforiaManager/TrackableIdPair>
struct Transform_1_t1188729987;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1079703083;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1697274930;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Reflection.Emit.Label>
struct Dictionary_2_t3251028295;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour/Status>
struct Dictionary_2_t3065736946;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct Dictionary_2_t231711286;
// System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>
struct Dictionary_2_t3061395850;
// System.Collections.Generic.Dictionary`2<System.Int64,System.Object>
struct Dictionary_2_t3678967697;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>
struct Dictionary_2_t2513902766;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>
struct Dictionary_2_t3131474613;
// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t3417634846;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t1663937576;
// System.Collections.Generic.Dictionary`2<System.Object,System.Nullable`1<System.Int32>>
struct Dictionary_2_t4221971187;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2281509423;
// System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>
struct Dictionary_2_t578942739;
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.Vector3>
struct Dictionary_2_t1835767708;
// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t1316726616;
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct Dictionary_2_t88735534;
// System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>
struct Dictionary_2_t371439919;
// System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>
struct Dictionary_2_t2395898666;
// System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>
struct Dictionary_2_t997184670;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>
struct ValueCollection_t4077730222;
// System.ArgumentNullException
struct ArgumentNullException_t628810857;
// System.String
struct String_t;
// System.NotSupportedException
struct NotSupportedException_t1793819818;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3842368571;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>
struct ValueCollection_t400334773;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Reflection.Emit.Label>
struct ValueCollection_t1954088138;
// System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.Label>
struct IEnumerator_1_t1718726487;
// System.Reflection.Emit.Label[]
struct LabelU5BU5D_t196522893;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour/Status>
struct ValueCollection_t1768796789;
// System.Collections.Generic.IEnumerator`1<Vuforia.TrackableBehaviour/Status>
struct IEnumerator_1_t1533435138;
// Vuforia.TrackableBehaviour/Status[]
struct StatusU5BU5D_t3152771798;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m929665079_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m2179239469_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m1786442111_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m802712344_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m1919808363_MetadataUsageId;
extern Il2CppClass* Label_t4243202660_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1074189372_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m1679317215_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m3403453437_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m2566047391_MetadataUsageId;
extern Il2CppClass* Status_t4057911311_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m852013770_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m1836076395_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m1826169059_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m1026459083_MetadataUsageId;
extern Il2CppClass* VirtualButtonData_t1223885651_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1409521950_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m4146785439_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m3375480339_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m109519847_MetadataUsageId;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m652403149_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m1482372703_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m1955297614_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m1101679575_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m3448776694_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m655666259_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m3566819466_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m4016145208_MetadataUsageId;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1858167433_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m1958180191_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m2708840102_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m1506524387_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m4270324442_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m345445247_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m2914101698_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m3901963208_MetadataUsageId;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3826027984_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m1167293475_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m2640141359_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m2082152109_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m786657825_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m2336029567_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m3169382212_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m1856627011_MetadataUsageId;
extern Il2CppClass* Nullable_1_t334943763_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1988794314_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m3575693983_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m3499650370_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m172013252_MetadataUsageId;
extern Il2CppClass* UInt16_t986882611_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2990576644_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m3964891327_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m2109839950_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m1380725890_MetadataUsageId;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m629477401_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m1553007199_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m1402666263_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m4121449538_MetadataUsageId;
extern Il2CppClass* ProfileData_t1724666488_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3448866058_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m3175591935_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m3164846456_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m500565698_MetadataUsageId;
extern Il2CppClass* PIXEL_FORMAT_t3010530044_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1330432734_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m2760658979_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m3238894512_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m3515991146_MetadataUsageId;
extern Il2CppClass* TrackableIdPair_t1329355276_il2cpp_TypeInfo_var;
extern Il2CppClass* PoseAgeEntry_t3432166560_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3407289780_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m717873343_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m1508964964_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m270362696_MetadataUsageId;
extern Il2CppClass* PoseInfo_t1161658011_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1380819331_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m2103714143_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m3070020174_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m56665023_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m3927196739_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m170097695_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m3676348468_MetadataUsageId;
extern const uint32_t Transform_1_BeginInvoke_m170449759_MetadataUsageId;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t ValueCollection__ctor_m1016212932_MetadataUsageId;
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1909364658_MetadataUsageId;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4184581491_MetadataUsageId;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1476685027_MetadataUsageId;
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t ValueCollection_System_Collections_ICollection_get_SyncRoot_m3444889833_MetadataUsageId;
extern const uint32_t ValueCollection__ctor_m882866357_MetadataUsageId;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1903672223_MetadataUsageId;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3271993638_MetadataUsageId;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m98888100_MetadataUsageId;
extern const uint32_t ValueCollection_System_Collections_ICollection_get_SyncRoot_m3927965720_MetadataUsageId;
extern const uint32_t ValueCollection__ctor_m3461572578_MetadataUsageId;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1237476428_MetadataUsageId;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1332473421_MetadataUsageId;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1826798401_MetadataUsageId;
extern const uint32_t ValueCollection_System_Collections_ICollection_get_SyncRoot_m22966243_MetadataUsageId;
extern const uint32_t ValueCollection__ctor_m3664366612_MetadataUsageId;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2358931206_MetadataUsageId;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m526884159_MetadataUsageId;
extern const uint32_t ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m825862607_MetadataUsageId;
extern const uint32_t ValueCollection_System_Collections_ICollection_get_SyncRoot_m3948248397_MetadataUsageId;

// System.Int32[]
struct Int32U5BU5D_t3030399641  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t3614634134  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.Emit.Label[]
struct LabelU5BU5D_t196522893  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Label_t4243202660  m_Items[1];

public:
	inline Label_t4243202660  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Label_t4243202660 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Label_t4243202660  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Label_t4243202660  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Label_t4243202660 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Label_t4243202660  value)
	{
		m_Items[index] = value;
	}
};
// Vuforia.TrackableBehaviour/Status[]
struct StatusU5BU5D_t3152771798  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};


// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m488493188_gshared (Transform_1_t2737799006 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m4020530914_gshared (Transform_1_t4064153 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3749587448  Transform_1_Invoke_m1436021910_gshared (Transform_1_t704776203 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m3763570295_gshared (Transform_1_t3322033499 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m2347662626_gshared (Transform_1_t3939605346 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m2130809719_gshared (Transform_1_t880920208 * __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1008373517  Transform_1_Invoke_m387254326_gshared (Transform_1_t3135385623 * __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m1740004046_gshared (Transform_1_t4198889554 * __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Reflection.Emit.Label>::Invoke(TKey,TValue)
extern "C"  Label_t4243202660  Transform_1_Invoke_m2635029648_gshared (Transform_1_t2075247470 * __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m1424521241_gshared (Transform_1_t3837169113 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t823082168  Transform_1_Invoke_m3913597300_gshared (Transform_1_t1611375883 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m2457631416_gshared (Transform_1_t2860171163 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m3070581954_gshared (Transform_1_t551237730 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m1302756149_gshared (Transform_1_t2883023109 * __this, int32_t ___key0, VirtualButtonData_t1223885651  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t2284023804  Transform_1_Invoke_m557327862_gshared (Transform_1_t2118171515 * __this, int32_t ___key0, VirtualButtonData_t1223885651  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m2110329656_gshared (Transform_1_t1906025159 * __this, int32_t ___key0, VirtualButtonData_t1223885651  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Invoke(TKey,TValue)
extern "C"  VirtualButtonData_t1223885651  Transform_1_Invoke_m574052764_gshared (Transform_1_t1058033362 * __this, int32_t ___key0, VirtualButtonData_t1223885651  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m1135920892_gshared (Transform_1_t1772107233 * __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t818741072  Transform_1_Invoke_m2933042102_gshared (Transform_1_t3836940203 * __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m1013242963_gshared (Transform_1_t795109283 * __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Int64>::Invoke(TKey,TValue)
extern "C"  int64_t Transform_1_Invoke_m3525936996_gshared (Transform_1_t3927277168 * __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m1299483797_gshared (Transform_1_t2356341726 * __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1436312919  Transform_1_Invoke_m1183831012_gshared (Transform_1_t743779247 * __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Int64>::Invoke(TKey,TValue)
extern "C"  int64_t Transform_1_Invoke_m3042893651_gshared (Transform_1_t216544365 * __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m333092703_gshared (Transform_1_t1996915623 * __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m810218400_gshared (Transform_1_t2129470893 * __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Int32>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t271247988  Transform_1_Invoke_m3442926198_gshared (Transform_1_t3646810779 * __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m1529026043_gshared (Transform_1_t1152472943 * __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.IntPtr>::Invoke(TKey,TValue)
extern "C"  IntPtr_t Transform_1_Invoke_m2250487894_gshared (Transform_1_t1584656104 * __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m2675095773_gshared (Transform_1_t2713705386 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t888819835  Transform_1_Invoke_m1023901590_gshared (Transform_1_t553649823 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.IntPtr>::Invoke(TKey,TValue)
extern "C"  IntPtr_t Transform_1_Invoke_m524403747_gshared (Transform_1_t2168890597 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m1111426439_gshared (Transform_1_t2354279283 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Boolean>::Invoke(TKey,TValue)
extern "C"  bool Transform_1_Invoke_m2906736839_gshared (Transform_1_t2990554165 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m2509306846_gshared (Transform_1_t2213854845 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1174980068  Transform_1_Invoke_m4136847354_gshared (Transform_1_t339959515 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m3830249854_gshared (Transform_1_t1854428742 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m719893226_gshared (Transform_1_t1676220171 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3716250094  Transform_1_Invoke_m1172879766_gshared (Transform_1_t2343594867 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m2099058127_gshared (Transform_1_t699222221 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m2777900342_gshared (Transform_1_t1316794068 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m2455579403_gshared (Transform_1_t427295924 * __this, Il2CppObject * ___key0, Nullable_1_t334943763  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Int32>>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1979316409  Transform_1_Invoke_m514070902_gshared (Transform_1_t3652704231 * __this, Il2CppObject * ___key0, Nullable_1_t334943763  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>>::Invoke(TKey,TValue)
extern "C"  Nullable_1_t334943763  Transform_1_Invoke_m3781372707_gshared (Transform_1_t2008331585 * __this, Il2CppObject * ___key0, Nullable_1_t334943763  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m419001697_gshared (Transform_1_t67869821 * __this, Il2CppObject * ___key0, Nullable_1_t334943763  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m2770612589_gshared (Transform_1_t2260454664 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t38854645  Transform_1_Invoke_m1976033878_gshared (Transform_1_t3545401207 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m1224512163_gshared (Transform_1_t1901028561 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m3973223697_gshared (Transform_1_t1173087252 * __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t2631255257  Transform_1_Invoke_m2051902166_gshared (Transform_1_t755467111 * __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m3150503015_gshared (Transform_1_t813661149 * __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.UInt16>::Invoke(TKey,TValue)
extern "C"  uint16_t Transform_1_Invoke_m2472178915_gshared (Transform_1_t3406061761 * __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m544618940_gshared (Transform_1_t4113099591 * __this, Il2CppObject * ___key0, Vector3_t2243707580  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Vector3>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3888080226  Transform_1_Invoke_m1893645494_gshared (Transform_1_t657337123 * __this, Il2CppObject * ___key0, Vector3_t2243707580  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m588307748_gshared (Transform_1_t3753673488 * __this, Il2CppObject * ___key0, Vector3_t2243707580  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,UnityEngine.Vector3>::Invoke(TKey,TValue)
extern "C"  Vector3_t2243707580  Transform_1_Invoke_m469561379_gshared (Transform_1_t3307931773 * __this, Il2CppObject * ___key0, Vector3_t2243707580  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m3013029325_gshared (Transform_1_t2507364635 * __this, Il2CppObject * ___key0, ProfileData_t1724666488  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3369039134  Transform_1_Invoke_m909098262_gshared (Transform_1_t2827528371 * __this, Il2CppObject * ___key0, ProfileData_t1724666488  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m519637335_gshared (Transform_1_t2147938532 * __this, Il2CppObject * ___key0, ProfileData_t1724666488  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,Vuforia.WebCamProfile/ProfileData>::Invoke(TKey,TValue)
extern "C"  ProfileData_t1724666488  Transform_1_Invoke_m3366889123_gshared (Transform_1_t1183155725 * __this, Il2CppObject * ___key0, ProfileData_t1724666488  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m1263589125_gshared (Transform_1_t3129600301 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t2141048052  Transform_1_Invoke_m738258324_gshared (Transform_1_t2221772955 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m3399302511_gshared (Transform_1_t2770174198 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,Vuforia.Image/PIXEL_FORMAT>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m366087667_gshared (Transform_1_t3091254947 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m134111705_gshared (Transform_1_t2910497288 * __this, TrackableIdPair_t1329355276  ___key0, PoseAgeEntry_t3432166560  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t2423752437  Transform_1_Invoke_m1713490774_gshared (Transform_1_t2285374327 * __this, TrackableIdPair_t1329355276  ___key0, PoseAgeEntry_t3432166560  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::Invoke(TKey,TValue)
extern "C"  PoseAgeEntry_t3432166560  Transform_1_Invoke_m1107381605_gshared (Transform_1_t3293788450 * __this, TrackableIdPair_t1329355276  ___key0, PoseAgeEntry_t3432166560  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,Vuforia.VuforiaManager/TrackableIdPair>::Invoke(TKey,TValue)
extern "C"  TrackableIdPair_t1329355276  Transform_1_Invoke_m939805297_gshared (Transform_1_t1190977166 * __this, TrackableIdPair_t1329355276  ___key0, PoseAgeEntry_t3432166560  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m1256587466_gshared (Transform_1_t2927905217 * __this, TrackableIdPair_t1329355276  ___key0, PoseInfo_t1161658011  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t153243888  Transform_1_Invoke_m2222759478_gshared (Transform_1_t32273707 * __this, TrackableIdPair_t1329355276  ___key0, PoseInfo_t1161658011  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::Invoke(TKey,TValue)
extern "C"  PoseInfo_t1161658011  Transform_1_Invoke_m1374546105_gshared (Transform_1_t1040687830 * __this, TrackableIdPair_t1329355276  ___key0, PoseInfo_t1161658011  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,Vuforia.VuforiaManager/TrackableIdPair>::Invoke(TKey,TValue)
extern "C"  TrackableIdPair_t1329355276  Transform_1_Invoke_m1684249954_gshared (Transform_1_t1208385095 * __this, TrackableIdPair_t1329355276  ___key0, PoseInfo_t1161658011  ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m3734845782_gshared (Transform_1_t2908250109 * __this, TrackableIdPair_t1329355276  ___key0, int32_t ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3049497188  Transform_1_Invoke_m589798410_gshared (Transform_1_t2908871899 * __this, TrackableIdPair_t1329355276  ___key0, int32_t ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m2009610709_gshared (Transform_1_t3917286022 * __this, TrackableIdPair_t1329355276  ___key0, int32_t ___value1, const MethodInfo* method);
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,Vuforia.VuforiaManager/TrackableIdPair>::Invoke(TKey,TValue)
extern "C"  TrackableIdPair_t1329355276  Transform_1_Invoke_m1721734334_gshared (Transform_1_t1188729987 * __this, TrackableIdPair_t1329355276  ___key0, int32_t ___value1, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m370613445_gshared (Enumerator_t2766235847 * __this, Dictionary_2_t1079703083 * ___host0, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m2708916123_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4017233928_gshared (Enumerator_t2766235847 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::Reset()
extern "C"  void Enumerator_Reset_m2787013825_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m9625560_gshared (Enumerator_t2766235847 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m1631869499_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m2229103293_gshared (Enumerator_t2766235847 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3809319098_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3657937156_gshared (Enumerator_t2766235847 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m1136571287_gshared (KeyValuePair_2_t3132015601 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m25368454_gshared (Enumerator_t2766235847 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2988407410_gshared (Enumerator_t3383807694 * __this, Dictionary_2_t1697274930 * ___host0, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3562053380_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1648049763_gshared (Enumerator_t3383807694 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m761796566_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m655633499_gshared (Enumerator_t3383807694 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2243145188_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2369319718_gshared (Enumerator_t3383807694 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2770956757_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1091131935_gshared (Enumerator_t3383807694 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2897691047_gshared (KeyValuePair_2_t3749587448 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3006348140_gshared (Enumerator_t3383807694 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Reflection.Emit.Label>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2456172699_gshared (Enumerator_t642593763 * __this, Dictionary_2_t3251028295 * ___host0, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::get_CurrentValue()
extern "C"  Label_t4243202660  Enumerator_get_CurrentValue_m3952904993_gshared (Enumerator_t276085701 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Reflection.Emit.Label>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2983671754_gshared (Enumerator_t642593763 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::Reset()
extern "C"  void Enumerator_Reset_m2015547391_gshared (Enumerator_t276085701 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Reflection.Emit.Label>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m750766582_gshared (Enumerator_t642593763 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::Dispose()
extern "C"  void Enumerator_Dispose_m2214493861_gshared (Enumerator_t276085701 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Reflection.Emit.Label>::Dispose()
extern "C"  void Enumerator_Dispose_m484450631_gshared (Enumerator_t642593763 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3018149748_gshared (Enumerator_t276085701 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Reflection.Emit.Label>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1410782430_gshared (Enumerator_t642593763 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>::get_Value()
extern "C"  Label_t4243202660  KeyValuePair_2_get_Value_m590593229_gshared (KeyValuePair_2_t1008373517 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Reflection.Emit.Label>::get_Current()
extern "C"  Label_t4243202660  Enumerator_get_Current_m1765296160_gshared (Enumerator_t642593763 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1603410773_gshared (Enumerator_t457302414 * __this, Dictionary_2_t3065736946 * ___host0, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m1949022123_gshared (Enumerator_t90794352 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3236647928_gshared (Enumerator_t457302414 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::Reset()
extern "C"  void Enumerator_Reset_m3676619405_gshared (Enumerator_t90794352 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3086482046_gshared (Enumerator_t457302414 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::Dispose()
extern "C"  void Enumerator_Dispose_m3765489531_gshared (Enumerator_t90794352 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::Dispose()
extern "C"  void Enumerator_Dispose_m3575335869_gshared (Enumerator_t457302414 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3012296512_gshared (Enumerator_t90794352 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1629414154_gshared (Enumerator_t457302414 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m2492290363_gshared (KeyValuePair_2_t823082168 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3681121540_gshared (Enumerator_t457302414 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2172712953_gshared (Enumerator_t1918244050 * __this, Dictionary_2_t231711286 * ___host0, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_CurrentValue()
extern "C"  VirtualButtonData_t1223885651  Enumerator_get_CurrentValue_m2040133495_gshared (Enumerator_t1551735988 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m248398268_gshared (Enumerator_t1918244050 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Reset()
extern "C"  void Enumerator_Reset_m497205_gshared (Enumerator_t1551735988 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2984435500_gshared (Enumerator_t1918244050 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Dispose()
extern "C"  void Enumerator_Dispose_m2687880623_gshared (Enumerator_t1551735988 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Dispose()
extern "C"  void Enumerator_Dispose_m2702780945_gshared (Enumerator_t1918244050 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2819456918_gshared (Enumerator_t1551735988 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1213553088_gshared (Enumerator_t1918244050 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Value()
extern "C"  VirtualButtonData_t1223885651  KeyValuePair_2_get_Value_m2492481859_gshared (KeyValuePair_2_t2284023804 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Current()
extern "C"  VirtualButtonData_t1223885651  Enumerator_get_Current_m2919251530_gshared (Enumerator_t1918244050 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2270221822_gshared (Enumerator_t452961318 * __this, Dictionary_2_t3061395850 * ___host0, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Int32>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m1182545736_gshared (Enumerator_t86453256 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3271520495_gshared (Enumerator_t452961318 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Int32>::Reset()
extern "C"  void Enumerator_Reset_m199703458_gshared (Enumerator_t86453256 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3259539291_gshared (Enumerator_t452961318 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m3531528168_gshared (Enumerator_t86453256 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m808899306_gshared (Enumerator_t452961318 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3282539621_gshared (Enumerator_t86453256 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1846263727_gshared (Enumerator_t452961318 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3901703242_gshared (KeyValuePair_2_t818741072 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3488365209_gshared (Enumerator_t452961318 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m419014865_gshared (Enumerator_t1070533165 * __this, Dictionary_2_t3678967697 * ___host0, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m659195263_gshared (Enumerator_t704025103 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2712263668_gshared (Enumerator_t1070533165 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m598725905_gshared (Enumerator_t704025103 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m149043970_gshared (Enumerator_t1070533165 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3446908287_gshared (Enumerator_t704025103 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m942415041_gshared (Enumerator_t1070533165 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3139459564_gshared (Enumerator_t704025103 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2045321910_gshared (Enumerator_t1070533165 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1203523270_gshared (KeyValuePair_2_t1436312919 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1952080304_gshared (Enumerator_t1070533165 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4094254752_gshared (Enumerator_t4200435530 * __this, Dictionary_2_t2513902766 * ___host0, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Int32>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m2248732114_gshared (Enumerator_t3833927468 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1738297215_gshared (Enumerator_t4200435530 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Int32>::Reset()
extern "C"  void Enumerator_Reset_m1791350284_gshared (Enumerator_t3833927468 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1672746039_gshared (Enumerator_t4200435530 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m3738173754_gshared (Enumerator_t3833927468 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m2630138556_gshared (Enumerator_t4200435530 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3274679113_gshared (Enumerator_t3833927468 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1519402707_gshared (Enumerator_t4200435530 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m2072891806_gshared (KeyValuePair_2_t271247988 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m577317777_gshared (Enumerator_t4200435530 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1320388337_gshared (Enumerator_t523040081 * __this, Dictionary_2_t3131474613 * ___host0, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3147984131_gshared (Enumerator_t156532019 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2160496972_gshared (Enumerator_t523040081 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m1147555085_gshared (Enumerator_t156532019 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1146954164_gshared (Enumerator_t523040081 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2117437651_gshared (Enumerator_t156532019 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1062622161_gshared (Enumerator_t523040081 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m266619810_gshared (Enumerator_t156532019 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1404007384_gshared (Enumerator_t523040081 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m544293807_gshared (KeyValuePair_2_t888819835 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m419581838_gshared (Enumerator_t523040081 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m908409898_gshared (Enumerator_t809200314 * __this, Dictionary_2_t3417634846 * ___host0, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentValue()
extern "C"  bool Enumerator_get_CurrentValue_m4143929484_gshared (Enumerator_t442692252 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2625473469_gshared (Enumerator_t809200314 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Reset()
extern "C"  void Enumerator_Reset_m3115320746_gshared (Enumerator_t442692252 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2909592833_gshared (Enumerator_t809200314 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C"  void Enumerator_Dispose_m2711120408_gshared (Enumerator_t442692252 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C"  void Enumerator_Dispose_m1323464986_gshared (Enumerator_t809200314 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1856697671_gshared (Enumerator_t442692252 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1212551889_gshared (Enumerator_t809200314 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
extern "C"  bool KeyValuePair_2_get_Value_m1916631176_gshared (KeyValuePair_2_t1174980068 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C"  bool Enumerator_get_Current_m2986380627_gshared (Enumerator_t809200314 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3539306986_gshared (Enumerator_t3350470340 * __this, Dictionary_2_t1663937576 * ___host0, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m2645962456_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1805365227_gshared (Enumerator_t3350470340 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Reset()
extern "C"  void Enumerator_Reset_m1132695838_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3294415347_gshared (Enumerator_t3350470340 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m401572848_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m2532362830_gshared (Enumerator_t3350470340 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m435964161_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2534596951_gshared (Enumerator_t3350470340 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3699669100_gshared (KeyValuePair_2_t3716250094 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2838387513_gshared (Enumerator_t3350470340 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1378921111_gshared (Enumerator_t1613536655 * __this, Dictionary_2_t4221971187 * ___host0, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::get_CurrentValue()
extern "C"  Nullable_1_t334943763  Enumerator_get_CurrentValue_m1012672069_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1317997224_gshared (Enumerator_t1613536655 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::Reset()
extern "C"  void Enumerator_Reset_m2206907019_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3216819908_gshared (Enumerator_t1613536655 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::Dispose()
extern "C"  void Enumerator_Dispose_m2832749025_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::Dispose()
extern "C"  void Enumerator_Dispose_m1453665971_gshared (Enumerator_t1613536655 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m688389906_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3793414252_gshared (Enumerator_t1613536655 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Int32>>::get_Value()
extern "C"  Nullable_1_t334943763  KeyValuePair_2_get_Value_m2361283873_gshared (KeyValuePair_2_t1979316409 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::get_Current()
extern "C"  Nullable_1_t334943763  Enumerator_get_Current_m4077586382_gshared (Enumerator_t1613536655 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3819430617_gshared (Enumerator_t3968042187 * __this, Dictionary_2_t2281509423 * ___host0, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m402763047_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3933483934_gshared (Enumerator_t3968042187 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m3129803197_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2482663638_gshared (Enumerator_t3968042187 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1905011127_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m4238653081_gshared (Enumerator_t3968042187 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3349738440_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m335649778_gshared (Enumerator_t3968042187 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1251901674_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1976109889_gshared (Enumerator_t3968042187 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1132884941_gshared (Enumerator_t2265475503 * __this, Dictionary_2_t578942739 * ___host0, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::get_CurrentValue()
extern "C"  uint16_t Enumerator_get_CurrentValue_m3339719667_gshared (Enumerator_t1898967441 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m581780706_gshared (Enumerator_t2265475503 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::Reset()
extern "C"  void Enumerator_Reset_m1141574313_gshared (Enumerator_t1898967441 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt16>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m101480946_gshared (Enumerator_t2265475503 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::Dispose()
extern "C"  void Enumerator_Dispose_m2601453635_gshared (Enumerator_t1898967441 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt16>::Dispose()
extern "C"  void Enumerator_Dispose_m2244457285_gshared (Enumerator_t2265475503 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4229610700_gshared (Enumerator_t1898967441 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt16>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m696545430_gshared (Enumerator_t2265475503 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::get_Value()
extern "C"  uint16_t KeyValuePair_2_get_Value_m3658972159_gshared (KeyValuePair_2_t2631255257 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt16>::get_Current()
extern "C"  uint16_t Enumerator_get_Current_m1144777792_gshared (Enumerator_t2265475503 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.Vector3>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3161733182_gshared (Enumerator_t3522300472 * __this, Dictionary_2_t1835767708 * ___host0, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.Vector3>::get_CurrentValue()
extern "C"  Vector3_t2243707580  Enumerator_get_CurrentValue_m3593693876_gshared (Enumerator_t3155792410 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1396268095_gshared (Enumerator_t3522300472 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.Vector3>::Reset()
extern "C"  void Enumerator_Reset_m3146199818_gshared (Enumerator_t3155792410 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.Vector3>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2451736067_gshared (Enumerator_t3522300472 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.Vector3>::Dispose()
extern "C"  void Enumerator_Dispose_m498224468_gshared (Enumerator_t3155792410 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.Vector3>::Dispose()
extern "C"  void Enumerator_Dispose_m2813853538_gshared (Enumerator_t3522300472 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.Vector3>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2125092321_gshared (Enumerator_t3155792410 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.Vector3>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2172016871_gshared (Enumerator_t3522300472 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Vector3>::get_Value()
extern "C"  Vector3_t2243707580  KeyValuePair_2_get_Value_m3480897294_gshared (KeyValuePair_2_t3888080226 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.Vector3>::get_Current()
extern "C"  Vector3_t2243707580  Enumerator_get_Current_m3456750973_gshared (Enumerator_t3522300472 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2002289345_gshared (Enumerator_t3003259380 * __this, Dictionary_2_t1316726616 * ___host0, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_CurrentValue()
extern "C"  ProfileData_t1724666488  Enumerator_get_CurrentValue_m2118029427_gshared (Enumerator_t2636751318 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1152917532_gshared (Enumerator_t3003259380 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::Reset()
extern "C"  void Enumerator_Reset_m4069406173_gshared (Enumerator_t2636751318 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m155444740_gshared (Enumerator_t3003259380 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::Dispose()
extern "C"  void Enumerator_Dispose_m346153571_gshared (Enumerator_t2636751318 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::Dispose()
extern "C"  void Enumerator_Dispose_m4230661281_gshared (Enumerator_t3003259380 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2696697426_gshared (Enumerator_t2636751318 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m536014408_gshared (Enumerator_t3003259380 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Value()
extern "C"  ProfileData_t1724666488  KeyValuePair_2_get_Value_m699551839_gshared (KeyValuePair_2_t3369039134 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Current()
extern "C"  ProfileData_t1724666488  Enumerator_get_Current_m2996020126_gshared (Enumerator_t3003259380 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m668593521_gshared (Enumerator_t1775268298 * __this, Dictionary_2_t88735534 * ___host0, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m1347854879_gshared (Enumerator_t1408760236 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2127871028_gshared (Enumerator_t1775268298 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m3214492505_gshared (Enumerator_t1408760236 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m8906282_gshared (Enumerator_t1775268298 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3604910775_gshared (Enumerator_t1408760236 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2294050025_gshared (Enumerator_t1775268298 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1380568772_gshared (Enumerator_t1408760236 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m139596990_gshared (Enumerator_t1775268298 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m4144029831_gshared (KeyValuePair_2_t2141048052 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3067148225_gshared (Enumerator_t1775268298 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1541783957_gshared (Enumerator_t2057972683 * __this, Dictionary_2_t371439919 * ___host0, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::get_CurrentValue()
extern "C"  PoseAgeEntry_t3432166560  Enumerator_get_CurrentValue_m491091915_gshared (Enumerator_t1691464621 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2421552874_gshared (Enumerator_t2057972683 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::Reset()
extern "C"  void Enumerator_Reset_m1703409689_gshared (Enumerator_t1691464621 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2268110914_gshared (Enumerator_t2057972683 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::Dispose()
extern "C"  void Enumerator_Dispose_m591512691_gshared (Enumerator_t1691464621 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::Dispose()
extern "C"  void Enumerator_Dispose_m3235936213_gshared (Enumerator_t2057972683 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3813949020_gshared (Enumerator_t1691464621 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2047441542_gshared (Enumerator_t2057972683 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::get_Value()
extern "C"  PoseAgeEntry_t3432166560  KeyValuePair_2_get_Value_m252031535_gshared (KeyValuePair_2_t2423752437 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::get_Current()
extern "C"  PoseAgeEntry_t3432166560  Enumerator_get_Current_m3189532360_gshared (Enumerator_t2057972683 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3682789228_gshared (Enumerator_t4082431430 * __this, Dictionary_2_t2395898666 * ___host0, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::get_CurrentValue()
extern "C"  PoseInfo_t1161658011  Enumerator_get_CurrentValue_m95183650_gshared (Enumerator_t3715923368 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m624745849_gshared (Enumerator_t4082431430 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::Reset()
extern "C"  void Enumerator_Reset_m531859192_gshared (Enumerator_t3715923368 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1681422929_gshared (Enumerator_t4082431430 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m2977499502_gshared (Enumerator_t3715923368 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m1595156764_gshared (Enumerator_t4082431430 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4104902027_gshared (Enumerator_t3715923368 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m66940849_gshared (Enumerator_t4082431430 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::get_Value()
extern "C"  PoseInfo_t1161658011  KeyValuePair_2_get_Value_m1229996980_gshared (KeyValuePair_2_t153243888 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::get_Current()
extern "C"  PoseInfo_t1161658011  Enumerator_get_Current_m2272525103_gshared (Enumerator_t4082431430 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1971000786_gshared (Enumerator_t2683717434 * __this, Dictionary_2_t997184670 * ___host0, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m3861338020_gshared (Enumerator_t2317209372 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m514775589_gshared (Enumerator_t2683717434 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::Reset()
extern "C"  void Enumerator_Reset_m2100056858_gshared (Enumerator_t2317209372 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3523229681_gshared (Enumerator_t2683717434 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::Dispose()
extern "C"  void Enumerator_Dispose_m1953131992_gshared (Enumerator_t2317209372 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::Dispose()
extern "C"  void Enumerator_Dispose_m2249060810_gshared (Enumerator_t2683717434 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1170226423_gshared (Enumerator_t2317209372 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1436514993_gshared (Enumerator_t2683717434 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m1579783160_gshared (KeyValuePair_2_t3049497188 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3856955819_gshared (Enumerator_t2683717434 * __this, const MethodInfo* method);

// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m488493188(__this, ___key0, ___value1, method) ((  int32_t (*) (Transform_1_t2737799006 *, int32_t, int32_t, const MethodInfo*))Transform_1_Invoke_m488493188_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m4020530914(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t4064153 *, int32_t, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m4020530914_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1436021910(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t3749587448  (*) (Transform_1_t704776203 *, int32_t, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m1436021910_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m3763570295(__this, ___key0, ___value1, method) ((  int32_t (*) (Transform_1_t3322033499 *, int32_t, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m3763570295_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2347662626(__this, ___key0, ___value1, method) ((  Il2CppObject * (*) (Transform_1_t3939605346 *, int32_t, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m2347662626_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2130809719(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t880920208 *, int32_t, Label_t4243202660 , const MethodInfo*))Transform_1_Invoke_m2130809719_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m387254326(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t1008373517  (*) (Transform_1_t3135385623 *, int32_t, Label_t4243202660 , const MethodInfo*))Transform_1_Invoke_m387254326_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Int32>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1740004046(__this, ___key0, ___value1, method) ((  int32_t (*) (Transform_1_t4198889554 *, int32_t, Label_t4243202660 , const MethodInfo*))Transform_1_Invoke_m1740004046_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Reflection.Emit.Label>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2635029648(__this, ___key0, ___value1, method) ((  Label_t4243202660  (*) (Transform_1_t2075247470 *, int32_t, Label_t4243202660 , const MethodInfo*))Transform_1_Invoke_m2635029648_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1424521241(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t3837169113 *, int32_t, int32_t, const MethodInfo*))Transform_1_Invoke_m1424521241_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m3913597300(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t823082168  (*) (Transform_1_t1611375883 *, int32_t, int32_t, const MethodInfo*))Transform_1_Invoke_m3913597300_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,System.Int32>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2457631416(__this, ___key0, ___value1, method) ((  int32_t (*) (Transform_1_t2860171163 *, int32_t, int32_t, const MethodInfo*))Transform_1_Invoke_m2457631416_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m3070581954(__this, ___key0, ___value1, method) ((  int32_t (*) (Transform_1_t551237730 *, int32_t, int32_t, const MethodInfo*))Transform_1_Invoke_m3070581954_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1302756149(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t2883023109 *, int32_t, VirtualButtonData_t1223885651 , const MethodInfo*))Transform_1_Invoke_m1302756149_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m557327862(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t2284023804  (*) (Transform_1_t2118171515 *, int32_t, VirtualButtonData_t1223885651 , const MethodInfo*))Transform_1_Invoke_m557327862_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Int32>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2110329656(__this, ___key0, ___value1, method) ((  int32_t (*) (Transform_1_t1906025159 *, int32_t, VirtualButtonData_t1223885651 , const MethodInfo*))Transform_1_Invoke_m2110329656_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m574052764(__this, ___key0, ___value1, method) ((  VirtualButtonData_t1223885651  (*) (Transform_1_t1058033362 *, int32_t, VirtualButtonData_t1223885651 , const MethodInfo*))Transform_1_Invoke_m574052764_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1135920892(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t1772107233 *, int64_t, int32_t, const MethodInfo*))Transform_1_Invoke_m1135920892_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2933042102(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t818741072  (*) (Transform_1_t3836940203 *, int64_t, int32_t, const MethodInfo*))Transform_1_Invoke_m2933042102_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Int32>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1013242963(__this, ___key0, ___value1, method) ((  int32_t (*) (Transform_1_t795109283 *, int64_t, int32_t, const MethodInfo*))Transform_1_Invoke_m1013242963_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Int64>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m3525936996(__this, ___key0, ___value1, method) ((  int64_t (*) (Transform_1_t3927277168 *, int64_t, int32_t, const MethodInfo*))Transform_1_Invoke_m3525936996_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1299483797(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t2356341726 *, int64_t, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m1299483797_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1183831012(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t1436312919  (*) (Transform_1_t743779247 *, int64_t, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m1183831012_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Int64>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m3042893651(__this, ___key0, ___value1, method) ((  int64_t (*) (Transform_1_t216544365 *, int64_t, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m3042893651_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Object>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m333092703(__this, ___key0, ___value1, method) ((  Il2CppObject * (*) (Transform_1_t1996915623 *, int64_t, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m333092703_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m810218400(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t2129470893 *, IntPtr_t, int32_t, const MethodInfo*))Transform_1_Invoke_m810218400_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Int32>>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m3442926198(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t271247988  (*) (Transform_1_t3646810779 *, IntPtr_t, int32_t, const MethodInfo*))Transform_1_Invoke_m3442926198_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Int32>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1529026043(__this, ___key0, ___value1, method) ((  int32_t (*) (Transform_1_t1152472943 *, IntPtr_t, int32_t, const MethodInfo*))Transform_1_Invoke_m1529026043_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.IntPtr>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2250487894(__this, ___key0, ___value1, method) ((  IntPtr_t (*) (Transform_1_t1584656104 *, IntPtr_t, int32_t, const MethodInfo*))Transform_1_Invoke_m2250487894_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2675095773(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t2713705386 *, IntPtr_t, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m2675095773_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1023901590(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t888819835  (*) (Transform_1_t553649823 *, IntPtr_t, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m1023901590_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.IntPtr>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m524403747(__this, ___key0, ___value1, method) ((  IntPtr_t (*) (Transform_1_t2168890597 *, IntPtr_t, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m524403747_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Object>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1111426439(__this, ___key0, ___value1, method) ((  Il2CppObject * (*) (Transform_1_t2354279283 *, IntPtr_t, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m1111426439_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Boolean>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2906736839(__this, ___key0, ___value1, method) ((  bool (*) (Transform_1_t2990554165 *, Il2CppObject *, bool, const MethodInfo*))Transform_1_Invoke_m2906736839_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2509306846(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t2213854845 *, Il2CppObject *, bool, const MethodInfo*))Transform_1_Invoke_m2509306846_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m4136847354(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t1174980068  (*) (Transform_1_t339959515 *, Il2CppObject *, bool, const MethodInfo*))Transform_1_Invoke_m4136847354_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Object>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m3830249854(__this, ___key0, ___value1, method) ((  Il2CppObject * (*) (Transform_1_t1854428742 *, Il2CppObject *, bool, const MethodInfo*))Transform_1_Invoke_m3830249854_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m719893226(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t1676220171 *, Il2CppObject *, int32_t, const MethodInfo*))Transform_1_Invoke_m719893226_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1172879766(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t3716250094  (*) (Transform_1_t2343594867 *, Il2CppObject *, int32_t, const MethodInfo*))Transform_1_Invoke_m1172879766_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Int32>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2099058127(__this, ___key0, ___value1, method) ((  int32_t (*) (Transform_1_t699222221 *, Il2CppObject *, int32_t, const MethodInfo*))Transform_1_Invoke_m2099058127_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Object>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2777900342(__this, ___key0, ___value1, method) ((  Il2CppObject * (*) (Transform_1_t1316794068 *, Il2CppObject *, int32_t, const MethodInfo*))Transform_1_Invoke_m2777900342_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2455579403(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t427295924 *, Il2CppObject *, Nullable_1_t334943763 , const MethodInfo*))Transform_1_Invoke_m2455579403_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Int32>>>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m514070902(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t1979316409  (*) (Transform_1_t3652704231 *, Il2CppObject *, Nullable_1_t334943763 , const MethodInfo*))Transform_1_Invoke_m514070902_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m3781372707(__this, ___key0, ___value1, method) ((  Nullable_1_t334943763  (*) (Transform_1_t2008331585 *, Il2CppObject *, Nullable_1_t334943763 , const MethodInfo*))Transform_1_Invoke_m3781372707_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Object>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m419001697(__this, ___key0, ___value1, method) ((  Il2CppObject * (*) (Transform_1_t67869821 *, Il2CppObject *, Nullable_1_t334943763 , const MethodInfo*))Transform_1_Invoke_m419001697_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2770612589(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t2260454664 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m2770612589_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1976033878(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t38854645  (*) (Transform_1_t3545401207 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m1976033878_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1224512163(__this, ___key0, ___value1, method) ((  Il2CppObject * (*) (Transform_1_t1901028561 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m1224512163_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m3973223697(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t1173087252 *, Il2CppObject *, uint16_t, const MethodInfo*))Transform_1_Invoke_m3973223697_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2051902166(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t2631255257  (*) (Transform_1_t755467111 *, Il2CppObject *, uint16_t, const MethodInfo*))Transform_1_Invoke_m2051902166_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Object>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m3150503015(__this, ___key0, ___value1, method) ((  Il2CppObject * (*) (Transform_1_t813661149 *, Il2CppObject *, uint16_t, const MethodInfo*))Transform_1_Invoke_m3150503015_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.UInt16>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2472178915(__this, ___key0, ___value1, method) ((  uint16_t (*) (Transform_1_t3406061761 *, Il2CppObject *, uint16_t, const MethodInfo*))Transform_1_Invoke_m2472178915_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m544618940(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t4113099591 *, Il2CppObject *, Vector3_t2243707580 , const MethodInfo*))Transform_1_Invoke_m544618940_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Vector3>>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1893645494(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t3888080226  (*) (Transform_1_t657337123 *, Il2CppObject *, Vector3_t2243707580 , const MethodInfo*))Transform_1_Invoke_m1893645494_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,System.Object>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m588307748(__this, ___key0, ___value1, method) ((  Il2CppObject * (*) (Transform_1_t3753673488 *, Il2CppObject *, Vector3_t2243707580 , const MethodInfo*))Transform_1_Invoke_m588307748_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,UnityEngine.Vector3>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m469561379(__this, ___key0, ___value1, method) ((  Vector3_t2243707580  (*) (Transform_1_t3307931773 *, Il2CppObject *, Vector3_t2243707580 , const MethodInfo*))Transform_1_Invoke_m469561379_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m3013029325(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t2507364635 *, Il2CppObject *, ProfileData_t1724666488 , const MethodInfo*))Transform_1_Invoke_m3013029325_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m909098262(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t3369039134  (*) (Transform_1_t2827528371 *, Il2CppObject *, ProfileData_t1724666488 , const MethodInfo*))Transform_1_Invoke_m909098262_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Object>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m519637335(__this, ___key0, ___value1, method) ((  Il2CppObject * (*) (Transform_1_t2147938532 *, Il2CppObject *, ProfileData_t1724666488 , const MethodInfo*))Transform_1_Invoke_m519637335_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,Vuforia.WebCamProfile/ProfileData>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m3366889123(__this, ___key0, ___value1, method) ((  ProfileData_t1724666488  (*) (Transform_1_t1183155725 *, Il2CppObject *, ProfileData_t1724666488 , const MethodInfo*))Transform_1_Invoke_m3366889123_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1263589125(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t3129600301 *, int32_t, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m1263589125_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m738258324(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t2141048052  (*) (Transform_1_t2221772955 *, int32_t, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m738258324_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,System.Object>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m3399302511(__this, ___key0, ___value1, method) ((  Il2CppObject * (*) (Transform_1_t2770174198 *, int32_t, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m3399302511_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,Vuforia.Image/PIXEL_FORMAT>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m366087667(__this, ___key0, ___value1, method) ((  int32_t (*) (Transform_1_t3091254947 *, int32_t, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m366087667_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m134111705(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t2910497288 *, TrackableIdPair_t1329355276 , PoseAgeEntry_t3432166560 , const MethodInfo*))Transform_1_Invoke_m134111705_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1713490774(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t2423752437  (*) (Transform_1_t2285374327 *, TrackableIdPair_t1329355276 , PoseAgeEntry_t3432166560 , const MethodInfo*))Transform_1_Invoke_m1713490774_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1107381605(__this, ___key0, ___value1, method) ((  PoseAgeEntry_t3432166560  (*) (Transform_1_t3293788450 *, TrackableIdPair_t1329355276 , PoseAgeEntry_t3432166560 , const MethodInfo*))Transform_1_Invoke_m1107381605_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,Vuforia.VuforiaManager/TrackableIdPair>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m939805297(__this, ___key0, ___value1, method) ((  TrackableIdPair_t1329355276  (*) (Transform_1_t1190977166 *, TrackableIdPair_t1329355276 , PoseAgeEntry_t3432166560 , const MethodInfo*))Transform_1_Invoke_m939805297_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1256587466(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t2927905217 *, TrackableIdPair_t1329355276 , PoseInfo_t1161658011 , const MethodInfo*))Transform_1_Invoke_m1256587466_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2222759478(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t153243888  (*) (Transform_1_t32273707 *, TrackableIdPair_t1329355276 , PoseInfo_t1161658011 , const MethodInfo*))Transform_1_Invoke_m2222759478_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1374546105(__this, ___key0, ___value1, method) ((  PoseInfo_t1161658011  (*) (Transform_1_t1040687830 *, TrackableIdPair_t1329355276 , PoseInfo_t1161658011 , const MethodInfo*))Transform_1_Invoke_m1374546105_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,Vuforia.VuforiaManager/TrackableIdPair>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1684249954(__this, ___key0, ___value1, method) ((  TrackableIdPair_t1329355276  (*) (Transform_1_t1208385095 *, TrackableIdPair_t1329355276 , PoseInfo_t1161658011 , const MethodInfo*))Transform_1_Invoke_m1684249954_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m3734845782(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t2908250109 *, TrackableIdPair_t1329355276 , int32_t, const MethodInfo*))Transform_1_Invoke_m3734845782_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m589798410(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t3049497188  (*) (Transform_1_t2908871899 *, TrackableIdPair_t1329355276 , int32_t, const MethodInfo*))Transform_1_Invoke_m589798410_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2009610709(__this, ___key0, ___value1, method) ((  int32_t (*) (Transform_1_t3917286022 *, TrackableIdPair_t1329355276 , int32_t, const MethodInfo*))Transform_1_Invoke_m2009610709_gshared)(__this, ___key0, ___value1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,Vuforia.VuforiaManager/TrackableIdPair>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1721734334(__this, ___key0, ___value1, method) ((  TrackableIdPair_t1329355276  (*) (Transform_1_t1188729987 *, TrackableIdPair_t1329355276 , int32_t, const MethodInfo*))Transform_1_Invoke_m1721734334_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m370613445(__this, ___host0, method) ((  void (*) (Enumerator_t2766235847 *, Dictionary_2_t1079703083 *, const MethodInfo*))Enumerator__ctor_m370613445_gshared)(__this, ___host0, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2708916123(__this, method) ((  int32_t (*) (Enumerator_t2399727785 *, const MethodInfo*))Enumerator_get_CurrentValue_m2708916123_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4017233928(__this, method) ((  Il2CppObject * (*) (Enumerator_t2766235847 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m4017233928_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::Reset()
#define Enumerator_Reset_m2787013825(__this, method) ((  void (*) (Enumerator_t2399727785 *, const MethodInfo*))Enumerator_Reset_m2787013825_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m9625560(__this, method) ((  void (*) (Enumerator_t2766235847 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m9625560_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::Dispose()
#define Enumerator_Dispose_m1631869499(__this, method) ((  void (*) (Enumerator_t2399727785 *, const MethodInfo*))Enumerator_Dispose_m1631869499_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::Dispose()
#define Enumerator_Dispose_m2229103293(__this, method) ((  void (*) (Enumerator_t2766235847 *, const MethodInfo*))Enumerator_Dispose_m2229103293_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m3809319098(__this, method) ((  bool (*) (Enumerator_t2399727785 *, const MethodInfo*))Enumerator_MoveNext_m3809319098_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m3657937156(__this, method) ((  bool (*) (Enumerator_t2766235847 *, const MethodInfo*))Enumerator_MoveNext_m3657937156_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m1136571287(__this, method) ((  int32_t (*) (KeyValuePair_2_t3132015601 *, const MethodInfo*))KeyValuePair_2_get_Value_m1136571287_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::get_Current()
#define Enumerator_get_Current_m25368454(__this, method) ((  int32_t (*) (Enumerator_t2766235847 *, const MethodInfo*))Enumerator_get_Current_m25368454_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2988407410(__this, ___host0, method) ((  void (*) (Enumerator_t3383807694 *, Dictionary_2_t1697274930 *, const MethodInfo*))Enumerator__ctor_m2988407410_gshared)(__this, ___host0, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3562053380(__this, method) ((  Il2CppObject * (*) (Enumerator_t3017299632 *, const MethodInfo*))Enumerator_get_CurrentValue_m3562053380_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1648049763(__this, method) ((  Il2CppObject * (*) (Enumerator_t3383807694 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1648049763_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Reset()
#define Enumerator_Reset_m761796566(__this, method) ((  void (*) (Enumerator_t3017299632 *, const MethodInfo*))Enumerator_Reset_m761796566_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m655633499(__this, method) ((  void (*) (Enumerator_t3383807694 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m655633499_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Dispose()
#define Enumerator_Dispose_m2243145188(__this, method) ((  void (*) (Enumerator_t3017299632 *, const MethodInfo*))Enumerator_Dispose_m2243145188_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::Dispose()
#define Enumerator_Dispose_m2369319718(__this, method) ((  void (*) (Enumerator_t3383807694 *, const MethodInfo*))Enumerator_Dispose_m2369319718_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::MoveNext()
#define Enumerator_MoveNext_m2770956757(__this, method) ((  bool (*) (Enumerator_t3017299632 *, const MethodInfo*))Enumerator_MoveNext_m2770956757_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::MoveNext()
#define Enumerator_MoveNext_m1091131935(__this, method) ((  bool (*) (Enumerator_t3383807694 *, const MethodInfo*))Enumerator_MoveNext_m1091131935_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m2897691047(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t3749587448 *, const MethodInfo*))KeyValuePair_2_get_Value_m2897691047_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::get_Current()
#define Enumerator_get_Current_m3006348140(__this, method) ((  Il2CppObject * (*) (Enumerator_t3383807694 *, const MethodInfo*))Enumerator_get_Current_m3006348140_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Reflection.Emit.Label>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2456172699(__this, ___host0, method) ((  void (*) (Enumerator_t642593763 *, Dictionary_2_t3251028295 *, const MethodInfo*))Enumerator__ctor_m2456172699_gshared)(__this, ___host0, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3952904993(__this, method) ((  Label_t4243202660  (*) (Enumerator_t276085701 *, const MethodInfo*))Enumerator_get_CurrentValue_m3952904993_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Reflection.Emit.Label>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2983671754(__this, method) ((  Il2CppObject * (*) (Enumerator_t642593763 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2983671754_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::Reset()
#define Enumerator_Reset_m2015547391(__this, method) ((  void (*) (Enumerator_t276085701 *, const MethodInfo*))Enumerator_Reset_m2015547391_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Reflection.Emit.Label>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m750766582(__this, method) ((  void (*) (Enumerator_t642593763 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m750766582_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::Dispose()
#define Enumerator_Dispose_m2214493861(__this, method) ((  void (*) (Enumerator_t276085701 *, const MethodInfo*))Enumerator_Dispose_m2214493861_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Reflection.Emit.Label>::Dispose()
#define Enumerator_Dispose_m484450631(__this, method) ((  void (*) (Enumerator_t642593763 *, const MethodInfo*))Enumerator_Dispose_m484450631_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::MoveNext()
#define Enumerator_MoveNext_m3018149748(__this, method) ((  bool (*) (Enumerator_t276085701 *, const MethodInfo*))Enumerator_MoveNext_m3018149748_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Reflection.Emit.Label>::MoveNext()
#define Enumerator_MoveNext_m1410782430(__this, method) ((  bool (*) (Enumerator_t642593763 *, const MethodInfo*))Enumerator_MoveNext_m1410782430_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>::get_Value()
#define KeyValuePair_2_get_Value_m590593229(__this, method) ((  Label_t4243202660  (*) (KeyValuePair_2_t1008373517 *, const MethodInfo*))KeyValuePair_2_get_Value_m590593229_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Reflection.Emit.Label>::get_Current()
#define Enumerator_get_Current_m1765296160(__this, method) ((  Label_t4243202660  (*) (Enumerator_t642593763 *, const MethodInfo*))Enumerator_get_Current_m1765296160_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1603410773(__this, ___host0, method) ((  void (*) (Enumerator_t457302414 *, Dictionary_2_t3065736946 *, const MethodInfo*))Enumerator__ctor_m1603410773_gshared)(__this, ___host0, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1949022123(__this, method) ((  int32_t (*) (Enumerator_t90794352 *, const MethodInfo*))Enumerator_get_CurrentValue_m1949022123_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3236647928(__this, method) ((  Il2CppObject * (*) (Enumerator_t457302414 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3236647928_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::Reset()
#define Enumerator_Reset_m3676619405(__this, method) ((  void (*) (Enumerator_t90794352 *, const MethodInfo*))Enumerator_Reset_m3676619405_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3086482046(__this, method) ((  void (*) (Enumerator_t457302414 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3086482046_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::Dispose()
#define Enumerator_Dispose_m3765489531(__this, method) ((  void (*) (Enumerator_t90794352 *, const MethodInfo*))Enumerator_Dispose_m3765489531_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::Dispose()
#define Enumerator_Dispose_m3575335869(__this, method) ((  void (*) (Enumerator_t457302414 *, const MethodInfo*))Enumerator_Dispose_m3575335869_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::MoveNext()
#define Enumerator_MoveNext_m3012296512(__this, method) ((  bool (*) (Enumerator_t90794352 *, const MethodInfo*))Enumerator_MoveNext_m3012296512_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::MoveNext()
#define Enumerator_MoveNext_m1629414154(__this, method) ((  bool (*) (Enumerator_t457302414 *, const MethodInfo*))Enumerator_MoveNext_m1629414154_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::get_Value()
#define KeyValuePair_2_get_Value_m2492290363(__this, method) ((  int32_t (*) (KeyValuePair_2_t823082168 *, const MethodInfo*))KeyValuePair_2_get_Value_m2492290363_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::get_Current()
#define Enumerator_get_Current_m3681121540(__this, method) ((  int32_t (*) (Enumerator_t457302414 *, const MethodInfo*))Enumerator_get_Current_m3681121540_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2172712953(__this, ___host0, method) ((  void (*) (Enumerator_t1918244050 *, Dictionary_2_t231711286 *, const MethodInfo*))Enumerator__ctor_m2172712953_gshared)(__this, ___host0, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2040133495(__this, method) ((  VirtualButtonData_t1223885651  (*) (Enumerator_t1551735988 *, const MethodInfo*))Enumerator_get_CurrentValue_m2040133495_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m248398268(__this, method) ((  Il2CppObject * (*) (Enumerator_t1918244050 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m248398268_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Reset()
#define Enumerator_Reset_m497205(__this, method) ((  void (*) (Enumerator_t1551735988 *, const MethodInfo*))Enumerator_Reset_m497205_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2984435500(__this, method) ((  void (*) (Enumerator_t1918244050 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2984435500_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Dispose()
#define Enumerator_Dispose_m2687880623(__this, method) ((  void (*) (Enumerator_t1551735988 *, const MethodInfo*))Enumerator_Dispose_m2687880623_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Dispose()
#define Enumerator_Dispose_m2702780945(__this, method) ((  void (*) (Enumerator_t1918244050 *, const MethodInfo*))Enumerator_Dispose_m2702780945_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::MoveNext()
#define Enumerator_MoveNext_m2819456918(__this, method) ((  bool (*) (Enumerator_t1551735988 *, const MethodInfo*))Enumerator_MoveNext_m2819456918_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::MoveNext()
#define Enumerator_MoveNext_m1213553088(__this, method) ((  bool (*) (Enumerator_t1918244050 *, const MethodInfo*))Enumerator_MoveNext_m1213553088_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Value()
#define KeyValuePair_2_get_Value_m2492481859(__this, method) ((  VirtualButtonData_t1223885651  (*) (KeyValuePair_2_t2284023804 *, const MethodInfo*))KeyValuePair_2_get_Value_m2492481859_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Current()
#define Enumerator_get_Current_m2919251530(__this, method) ((  VirtualButtonData_t1223885651  (*) (Enumerator_t1918244050 *, const MethodInfo*))Enumerator_get_Current_m2919251530_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2270221822(__this, ___host0, method) ((  void (*) (Enumerator_t452961318 *, Dictionary_2_t3061395850 *, const MethodInfo*))Enumerator__ctor_m2270221822_gshared)(__this, ___host0, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Int32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1182545736(__this, method) ((  int32_t (*) (Enumerator_t86453256 *, const MethodInfo*))Enumerator_get_CurrentValue_m1182545736_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3271520495(__this, method) ((  Il2CppObject * (*) (Enumerator_t452961318 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3271520495_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Int32>::Reset()
#define Enumerator_Reset_m199703458(__this, method) ((  void (*) (Enumerator_t86453256 *, const MethodInfo*))Enumerator_Reset_m199703458_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Int32>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3259539291(__this, method) ((  void (*) (Enumerator_t452961318 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3259539291_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Int32>::Dispose()
#define Enumerator_Dispose_m3531528168(__this, method) ((  void (*) (Enumerator_t86453256 *, const MethodInfo*))Enumerator_Dispose_m3531528168_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Int32>::Dispose()
#define Enumerator_Dispose_m808899306(__this, method) ((  void (*) (Enumerator_t452961318 *, const MethodInfo*))Enumerator_Dispose_m808899306_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m3282539621(__this, method) ((  bool (*) (Enumerator_t86453256 *, const MethodInfo*))Enumerator_MoveNext_m3282539621_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m1846263727(__this, method) ((  bool (*) (Enumerator_t452961318 *, const MethodInfo*))Enumerator_MoveNext_m1846263727_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m3901703242(__this, method) ((  int32_t (*) (KeyValuePair_2_t818741072 *, const MethodInfo*))KeyValuePair_2_get_Value_m3901703242_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Int32>::get_Current()
#define Enumerator_get_Current_m3488365209(__this, method) ((  int32_t (*) (Enumerator_t452961318 *, const MethodInfo*))Enumerator_get_Current_m3488365209_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m419014865(__this, ___host0, method) ((  void (*) (Enumerator_t1070533165 *, Dictionary_2_t3678967697 *, const MethodInfo*))Enumerator__ctor_m419014865_gshared)(__this, ___host0, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m659195263(__this, method) ((  Il2CppObject * (*) (Enumerator_t704025103 *, const MethodInfo*))Enumerator_get_CurrentValue_m659195263_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2712263668(__this, method) ((  Il2CppObject * (*) (Enumerator_t1070533165 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2712263668_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::Reset()
#define Enumerator_Reset_m598725905(__this, method) ((  void (*) (Enumerator_t704025103 *, const MethodInfo*))Enumerator_Reset_m598725905_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m149043970(__this, method) ((  void (*) (Enumerator_t1070533165 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m149043970_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::Dispose()
#define Enumerator_Dispose_m3446908287(__this, method) ((  void (*) (Enumerator_t704025103 *, const MethodInfo*))Enumerator_Dispose_m3446908287_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::Dispose()
#define Enumerator_Dispose_m942415041(__this, method) ((  void (*) (Enumerator_t1070533165 *, const MethodInfo*))Enumerator_Dispose_m942415041_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::MoveNext()
#define Enumerator_MoveNext_m3139459564(__this, method) ((  bool (*) (Enumerator_t704025103 *, const MethodInfo*))Enumerator_MoveNext_m3139459564_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::MoveNext()
#define Enumerator_MoveNext_m2045321910(__this, method) ((  bool (*) (Enumerator_t1070533165 *, const MethodInfo*))Enumerator_MoveNext_m2045321910_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m1203523270(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t1436312919 *, const MethodInfo*))KeyValuePair_2_get_Value_m1203523270_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::get_Current()
#define Enumerator_get_Current_m1952080304(__this, method) ((  Il2CppObject * (*) (Enumerator_t1070533165 *, const MethodInfo*))Enumerator_get_Current_m1952080304_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m4094254752(__this, ___host0, method) ((  void (*) (Enumerator_t4200435530 *, Dictionary_2_t2513902766 *, const MethodInfo*))Enumerator__ctor_m4094254752_gshared)(__this, ___host0, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Int32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2248732114(__this, method) ((  int32_t (*) (Enumerator_t3833927468 *, const MethodInfo*))Enumerator_get_CurrentValue_m2248732114_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1738297215(__this, method) ((  Il2CppObject * (*) (Enumerator_t4200435530 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1738297215_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Int32>::Reset()
#define Enumerator_Reset_m1791350284(__this, method) ((  void (*) (Enumerator_t3833927468 *, const MethodInfo*))Enumerator_Reset_m1791350284_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Int32>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1672746039(__this, method) ((  void (*) (Enumerator_t4200435530 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1672746039_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Int32>::Dispose()
#define Enumerator_Dispose_m3738173754(__this, method) ((  void (*) (Enumerator_t3833927468 *, const MethodInfo*))Enumerator_Dispose_m3738173754_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Int32>::Dispose()
#define Enumerator_Dispose_m2630138556(__this, method) ((  void (*) (Enumerator_t4200435530 *, const MethodInfo*))Enumerator_Dispose_m2630138556_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m3274679113(__this, method) ((  bool (*) (Enumerator_t3833927468 *, const MethodInfo*))Enumerator_MoveNext_m3274679113_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m1519402707(__this, method) ((  bool (*) (Enumerator_t4200435530 *, const MethodInfo*))Enumerator_MoveNext_m1519402707_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m2072891806(__this, method) ((  int32_t (*) (KeyValuePair_2_t271247988 *, const MethodInfo*))KeyValuePair_2_get_Value_m2072891806_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Int32>::get_Current()
#define Enumerator_get_Current_m577317777(__this, method) ((  int32_t (*) (Enumerator_t4200435530 *, const MethodInfo*))Enumerator_get_Current_m577317777_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1320388337(__this, ___host0, method) ((  void (*) (Enumerator_t523040081 *, Dictionary_2_t3131474613 *, const MethodInfo*))Enumerator__ctor_m1320388337_gshared)(__this, ___host0, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3147984131(__this, method) ((  Il2CppObject * (*) (Enumerator_t156532019 *, const MethodInfo*))Enumerator_get_CurrentValue_m3147984131_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Object>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2160496972(__this, method) ((  Il2CppObject * (*) (Enumerator_t523040081 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2160496972_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::Reset()
#define Enumerator_Reset_m1147555085(__this, method) ((  void (*) (Enumerator_t156532019 *, const MethodInfo*))Enumerator_Reset_m1147555085_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Object>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1146954164(__this, method) ((  void (*) (Enumerator_t523040081 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1146954164_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::Dispose()
#define Enumerator_Dispose_m2117437651(__this, method) ((  void (*) (Enumerator_t156532019 *, const MethodInfo*))Enumerator_Dispose_m2117437651_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Object>::Dispose()
#define Enumerator_Dispose_m1062622161(__this, method) ((  void (*) (Enumerator_t523040081 *, const MethodInfo*))Enumerator_Dispose_m1062622161_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::MoveNext()
#define Enumerator_MoveNext_m266619810(__this, method) ((  bool (*) (Enumerator_t156532019 *, const MethodInfo*))Enumerator_MoveNext_m266619810_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Object>::MoveNext()
#define Enumerator_MoveNext_m1404007384(__this, method) ((  bool (*) (Enumerator_t523040081 *, const MethodInfo*))Enumerator_MoveNext_m1404007384_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m544293807(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t888819835 *, const MethodInfo*))KeyValuePair_2_get_Value_m544293807_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Object>::get_Current()
#define Enumerator_get_Current_m419581838(__this, method) ((  Il2CppObject * (*) (Enumerator_t523040081 *, const MethodInfo*))Enumerator_get_Current_m419581838_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m908409898(__this, ___host0, method) ((  void (*) (Enumerator_t809200314 *, Dictionary_2_t3417634846 *, const MethodInfo*))Enumerator__ctor_m908409898_gshared)(__this, ___host0, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m4143929484(__this, method) ((  bool (*) (Enumerator_t442692252 *, const MethodInfo*))Enumerator_get_CurrentValue_m4143929484_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2625473469(__this, method) ((  Il2CppObject * (*) (Enumerator_t809200314 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2625473469_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Reset()
#define Enumerator_Reset_m3115320746(__this, method) ((  void (*) (Enumerator_t442692252 *, const MethodInfo*))Enumerator_Reset_m3115320746_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2909592833(__this, method) ((  void (*) (Enumerator_t809200314 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2909592833_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Dispose()
#define Enumerator_Dispose_m2711120408(__this, method) ((  void (*) (Enumerator_t442692252 *, const MethodInfo*))Enumerator_Dispose_m2711120408_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::Dispose()
#define Enumerator_Dispose_m1323464986(__this, method) ((  void (*) (Enumerator_t809200314 *, const MethodInfo*))Enumerator_Dispose_m1323464986_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::MoveNext()
#define Enumerator_MoveNext_m1856697671(__this, method) ((  bool (*) (Enumerator_t442692252 *, const MethodInfo*))Enumerator_MoveNext_m1856697671_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::MoveNext()
#define Enumerator_MoveNext_m1212551889(__this, method) ((  bool (*) (Enumerator_t809200314 *, const MethodInfo*))Enumerator_MoveNext_m1212551889_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
#define KeyValuePair_2_get_Value_m1916631176(__this, method) ((  bool (*) (KeyValuePair_2_t1174980068 *, const MethodInfo*))KeyValuePair_2_get_Value_m1916631176_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::get_Current()
#define Enumerator_get_Current_m2986380627(__this, method) ((  bool (*) (Enumerator_t809200314 *, const MethodInfo*))Enumerator_get_Current_m2986380627_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3539306986(__this, ___host0, method) ((  void (*) (Enumerator_t3350470340 *, Dictionary_2_t1663937576 *, const MethodInfo*))Enumerator__ctor_m3539306986_gshared)(__this, ___host0, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2645962456(__this, method) ((  int32_t (*) (Enumerator_t2983962278 *, const MethodInfo*))Enumerator_get_CurrentValue_m2645962456_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1805365227(__this, method) ((  Il2CppObject * (*) (Enumerator_t3350470340 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1805365227_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Reset()
#define Enumerator_Reset_m1132695838(__this, method) ((  void (*) (Enumerator_t2983962278 *, const MethodInfo*))Enumerator_Reset_m1132695838_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3294415347(__this, method) ((  void (*) (Enumerator_t3350470340 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3294415347_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Dispose()
#define Enumerator_Dispose_m401572848(__this, method) ((  void (*) (Enumerator_t2983962278 *, const MethodInfo*))Enumerator_Dispose_m401572848_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::Dispose()
#define Enumerator_Dispose_m2532362830(__this, method) ((  void (*) (Enumerator_t3350470340 *, const MethodInfo*))Enumerator_Dispose_m2532362830_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m435964161(__this, method) ((  bool (*) (Enumerator_t2983962278 *, const MethodInfo*))Enumerator_MoveNext_m435964161_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m2534596951(__this, method) ((  bool (*) (Enumerator_t3350470340 *, const MethodInfo*))Enumerator_MoveNext_m2534596951_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m3699669100(__this, method) ((  int32_t (*) (KeyValuePair_2_t3716250094 *, const MethodInfo*))KeyValuePair_2_get_Value_m3699669100_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::get_Current()
#define Enumerator_get_Current_m2838387513(__this, method) ((  int32_t (*) (Enumerator_t3350470340 *, const MethodInfo*))Enumerator_get_Current_m2838387513_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1378921111(__this, ___host0, method) ((  void (*) (Enumerator_t1613536655 *, Dictionary_2_t4221971187 *, const MethodInfo*))Enumerator__ctor_m1378921111_gshared)(__this, ___host0, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1012672069(__this, method) ((  Nullable_1_t334943763  (*) (Enumerator_t1247028593 *, const MethodInfo*))Enumerator_get_CurrentValue_m1012672069_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1317997224(__this, method) ((  Il2CppObject * (*) (Enumerator_t1613536655 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1317997224_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::Reset()
#define Enumerator_Reset_m2206907019(__this, method) ((  void (*) (Enumerator_t1247028593 *, const MethodInfo*))Enumerator_Reset_m2206907019_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3216819908(__this, method) ((  void (*) (Enumerator_t1613536655 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3216819908_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::Dispose()
#define Enumerator_Dispose_m2832749025(__this, method) ((  void (*) (Enumerator_t1247028593 *, const MethodInfo*))Enumerator_Dispose_m2832749025_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::Dispose()
#define Enumerator_Dispose_m1453665971(__this, method) ((  void (*) (Enumerator_t1613536655 *, const MethodInfo*))Enumerator_Dispose_m1453665971_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::MoveNext()
#define Enumerator_MoveNext_m688389906(__this, method) ((  bool (*) (Enumerator_t1247028593 *, const MethodInfo*))Enumerator_MoveNext_m688389906_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::MoveNext()
#define Enumerator_MoveNext_m3793414252(__this, method) ((  bool (*) (Enumerator_t1613536655 *, const MethodInfo*))Enumerator_MoveNext_m3793414252_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Int32>>::get_Value()
#define KeyValuePair_2_get_Value_m2361283873(__this, method) ((  Nullable_1_t334943763  (*) (KeyValuePair_2_t1979316409 *, const MethodInfo*))KeyValuePair_2_get_Value_m2361283873_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::get_Current()
#define Enumerator_get_Current_m4077586382(__this, method) ((  Nullable_1_t334943763  (*) (Enumerator_t1613536655 *, const MethodInfo*))Enumerator_get_Current_m4077586382_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3819430617(__this, ___host0, method) ((  void (*) (Enumerator_t3968042187 *, Dictionary_2_t2281509423 *, const MethodInfo*))Enumerator__ctor_m3819430617_gshared)(__this, ___host0, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m402763047(__this, method) ((  Il2CppObject * (*) (Enumerator_t3601534125 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3933483934(__this, method) ((  Il2CppObject * (*) (Enumerator_t3968042187 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3933483934_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Reset()
#define Enumerator_Reset_m3129803197(__this, method) ((  void (*) (Enumerator_t3601534125 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2482663638(__this, method) ((  void (*) (Enumerator_t3968042187 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2482663638_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
#define Enumerator_Dispose_m1905011127(__this, method) ((  void (*) (Enumerator_t3601534125 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::Dispose()
#define Enumerator_Dispose_m4238653081(__this, method) ((  void (*) (Enumerator_t3968042187 *, const MethodInfo*))Enumerator_Dispose_m4238653081_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
#define Enumerator_MoveNext_m3349738440(__this, method) ((  bool (*) (Enumerator_t3601534125 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::MoveNext()
#define Enumerator_MoveNext_m335649778(__this, method) ((  bool (*) (Enumerator_t3968042187 *, const MethodInfo*))Enumerator_MoveNext_m335649778_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m1251901674(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t38854645 *, const MethodInfo*))KeyValuePair_2_get_Value_m1251901674_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::get_Current()
#define Enumerator_get_Current_m1976109889(__this, method) ((  Il2CppObject * (*) (Enumerator_t3968042187 *, const MethodInfo*))Enumerator_get_Current_m1976109889_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1132884941(__this, ___host0, method) ((  void (*) (Enumerator_t2265475503 *, Dictionary_2_t578942739 *, const MethodInfo*))Enumerator__ctor_m1132884941_gshared)(__this, ___host0, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3339719667(__this, method) ((  uint16_t (*) (Enumerator_t1898967441 *, const MethodInfo*))Enumerator_get_CurrentValue_m3339719667_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt16>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m581780706(__this, method) ((  Il2CppObject * (*) (Enumerator_t2265475503 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m581780706_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::Reset()
#define Enumerator_Reset_m1141574313(__this, method) ((  void (*) (Enumerator_t1898967441 *, const MethodInfo*))Enumerator_Reset_m1141574313_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt16>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m101480946(__this, method) ((  void (*) (Enumerator_t2265475503 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m101480946_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::Dispose()
#define Enumerator_Dispose_m2601453635(__this, method) ((  void (*) (Enumerator_t1898967441 *, const MethodInfo*))Enumerator_Dispose_m2601453635_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt16>::Dispose()
#define Enumerator_Dispose_m2244457285(__this, method) ((  void (*) (Enumerator_t2265475503 *, const MethodInfo*))Enumerator_Dispose_m2244457285_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::MoveNext()
#define Enumerator_MoveNext_m4229610700(__this, method) ((  bool (*) (Enumerator_t1898967441 *, const MethodInfo*))Enumerator_MoveNext_m4229610700_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt16>::MoveNext()
#define Enumerator_MoveNext_m696545430(__this, method) ((  bool (*) (Enumerator_t2265475503 *, const MethodInfo*))Enumerator_MoveNext_m696545430_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::get_Value()
#define KeyValuePair_2_get_Value_m3658972159(__this, method) ((  uint16_t (*) (KeyValuePair_2_t2631255257 *, const MethodInfo*))KeyValuePair_2_get_Value_m3658972159_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt16>::get_Current()
#define Enumerator_get_Current_m1144777792(__this, method) ((  uint16_t (*) (Enumerator_t2265475503 *, const MethodInfo*))Enumerator_get_Current_m1144777792_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.Vector3>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3161733182(__this, ___host0, method) ((  void (*) (Enumerator_t3522300472 *, Dictionary_2_t1835767708 *, const MethodInfo*))Enumerator__ctor_m3161733182_gshared)(__this, ___host0, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.Vector3>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3593693876(__this, method) ((  Vector3_t2243707580  (*) (Enumerator_t3155792410 *, const MethodInfo*))Enumerator_get_CurrentValue_m3593693876_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1396268095(__this, method) ((  Il2CppObject * (*) (Enumerator_t3522300472 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1396268095_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.Vector3>::Reset()
#define Enumerator_Reset_m3146199818(__this, method) ((  void (*) (Enumerator_t3155792410 *, const MethodInfo*))Enumerator_Reset_m3146199818_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.Vector3>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2451736067(__this, method) ((  void (*) (Enumerator_t3522300472 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2451736067_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.Vector3>::Dispose()
#define Enumerator_Dispose_m498224468(__this, method) ((  void (*) (Enumerator_t3155792410 *, const MethodInfo*))Enumerator_Dispose_m498224468_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.Vector3>::Dispose()
#define Enumerator_Dispose_m2813853538(__this, method) ((  void (*) (Enumerator_t3522300472 *, const MethodInfo*))Enumerator_Dispose_m2813853538_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.Vector3>::MoveNext()
#define Enumerator_MoveNext_m2125092321(__this, method) ((  bool (*) (Enumerator_t3155792410 *, const MethodInfo*))Enumerator_MoveNext_m2125092321_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.Vector3>::MoveNext()
#define Enumerator_MoveNext_m2172016871(__this, method) ((  bool (*) (Enumerator_t3522300472 *, const MethodInfo*))Enumerator_MoveNext_m2172016871_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Vector3>::get_Value()
#define KeyValuePair_2_get_Value_m3480897294(__this, method) ((  Vector3_t2243707580  (*) (KeyValuePair_2_t3888080226 *, const MethodInfo*))KeyValuePair_2_get_Value_m3480897294_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.Vector3>::get_Current()
#define Enumerator_get_Current_m3456750973(__this, method) ((  Vector3_t2243707580  (*) (Enumerator_t3522300472 *, const MethodInfo*))Enumerator_get_Current_m3456750973_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2002289345(__this, ___host0, method) ((  void (*) (Enumerator_t3003259380 *, Dictionary_2_t1316726616 *, const MethodInfo*))Enumerator__ctor_m2002289345_gshared)(__this, ___host0, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2118029427(__this, method) ((  ProfileData_t1724666488  (*) (Enumerator_t2636751318 *, const MethodInfo*))Enumerator_get_CurrentValue_m2118029427_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1152917532(__this, method) ((  Il2CppObject * (*) (Enumerator_t3003259380 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1152917532_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::Reset()
#define Enumerator_Reset_m4069406173(__this, method) ((  void (*) (Enumerator_t2636751318 *, const MethodInfo*))Enumerator_Reset_m4069406173_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m155444740(__this, method) ((  void (*) (Enumerator_t3003259380 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m155444740_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::Dispose()
#define Enumerator_Dispose_m346153571(__this, method) ((  void (*) (Enumerator_t2636751318 *, const MethodInfo*))Enumerator_Dispose_m346153571_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::Dispose()
#define Enumerator_Dispose_m4230661281(__this, method) ((  void (*) (Enumerator_t3003259380 *, const MethodInfo*))Enumerator_Dispose_m4230661281_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::MoveNext()
#define Enumerator_MoveNext_m2696697426(__this, method) ((  bool (*) (Enumerator_t2636751318 *, const MethodInfo*))Enumerator_MoveNext_m2696697426_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::MoveNext()
#define Enumerator_MoveNext_m536014408(__this, method) ((  bool (*) (Enumerator_t3003259380 *, const MethodInfo*))Enumerator_MoveNext_m536014408_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Value()
#define KeyValuePair_2_get_Value_m699551839(__this, method) ((  ProfileData_t1724666488  (*) (KeyValuePair_2_t3369039134 *, const MethodInfo*))KeyValuePair_2_get_Value_m699551839_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Current()
#define Enumerator_get_Current_m2996020126(__this, method) ((  ProfileData_t1724666488  (*) (Enumerator_t3003259380 *, const MethodInfo*))Enumerator_get_Current_m2996020126_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m668593521(__this, ___host0, method) ((  void (*) (Enumerator_t1775268298 *, Dictionary_2_t88735534 *, const MethodInfo*))Enumerator__ctor_m668593521_gshared)(__this, ___host0, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1347854879(__this, method) ((  Il2CppObject * (*) (Enumerator_t1408760236 *, const MethodInfo*))Enumerator_get_CurrentValue_m1347854879_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2127871028(__this, method) ((  Il2CppObject * (*) (Enumerator_t1775268298 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2127871028_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::Reset()
#define Enumerator_Reset_m3214492505(__this, method) ((  void (*) (Enumerator_t1408760236 *, const MethodInfo*))Enumerator_Reset_m3214492505_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m8906282(__this, method) ((  void (*) (Enumerator_t1775268298 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m8906282_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::Dispose()
#define Enumerator_Dispose_m3604910775(__this, method) ((  void (*) (Enumerator_t1408760236 *, const MethodInfo*))Enumerator_Dispose_m3604910775_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::Dispose()
#define Enumerator_Dispose_m2294050025(__this, method) ((  void (*) (Enumerator_t1775268298 *, const MethodInfo*))Enumerator_Dispose_m2294050025_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::MoveNext()
#define Enumerator_MoveNext_m1380568772(__this, method) ((  bool (*) (Enumerator_t1408760236 *, const MethodInfo*))Enumerator_MoveNext_m1380568772_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::MoveNext()
#define Enumerator_MoveNext_m139596990(__this, method) ((  bool (*) (Enumerator_t1775268298 *, const MethodInfo*))Enumerator_MoveNext_m139596990_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m4144029831(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t2141048052 *, const MethodInfo*))KeyValuePair_2_get_Value_m4144029831_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Current()
#define Enumerator_get_Current_m3067148225(__this, method) ((  Il2CppObject * (*) (Enumerator_t1775268298 *, const MethodInfo*))Enumerator_get_Current_m3067148225_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1541783957(__this, ___host0, method) ((  void (*) (Enumerator_t2057972683 *, Dictionary_2_t371439919 *, const MethodInfo*))Enumerator__ctor_m1541783957_gshared)(__this, ___host0, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m491091915(__this, method) ((  PoseAgeEntry_t3432166560  (*) (Enumerator_t1691464621 *, const MethodInfo*))Enumerator_get_CurrentValue_m491091915_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2421552874(__this, method) ((  Il2CppObject * (*) (Enumerator_t2057972683 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2421552874_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::Reset()
#define Enumerator_Reset_m1703409689(__this, method) ((  void (*) (Enumerator_t1691464621 *, const MethodInfo*))Enumerator_Reset_m1703409689_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2268110914(__this, method) ((  void (*) (Enumerator_t2057972683 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2268110914_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::Dispose()
#define Enumerator_Dispose_m591512691(__this, method) ((  void (*) (Enumerator_t1691464621 *, const MethodInfo*))Enumerator_Dispose_m591512691_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::Dispose()
#define Enumerator_Dispose_m3235936213(__this, method) ((  void (*) (Enumerator_t2057972683 *, const MethodInfo*))Enumerator_Dispose_m3235936213_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::MoveNext()
#define Enumerator_MoveNext_m3813949020(__this, method) ((  bool (*) (Enumerator_t1691464621 *, const MethodInfo*))Enumerator_MoveNext_m3813949020_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::MoveNext()
#define Enumerator_MoveNext_m2047441542(__this, method) ((  bool (*) (Enumerator_t2057972683 *, const MethodInfo*))Enumerator_MoveNext_m2047441542_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::get_Value()
#define KeyValuePair_2_get_Value_m252031535(__this, method) ((  PoseAgeEntry_t3432166560  (*) (KeyValuePair_2_t2423752437 *, const MethodInfo*))KeyValuePair_2_get_Value_m252031535_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::get_Current()
#define Enumerator_get_Current_m3189532360(__this, method) ((  PoseAgeEntry_t3432166560  (*) (Enumerator_t2057972683 *, const MethodInfo*))Enumerator_get_Current_m3189532360_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3682789228(__this, ___host0, method) ((  void (*) (Enumerator_t4082431430 *, Dictionary_2_t2395898666 *, const MethodInfo*))Enumerator__ctor_m3682789228_gshared)(__this, ___host0, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m95183650(__this, method) ((  PoseInfo_t1161658011  (*) (Enumerator_t3715923368 *, const MethodInfo*))Enumerator_get_CurrentValue_m95183650_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m624745849(__this, method) ((  Il2CppObject * (*) (Enumerator_t4082431430 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m624745849_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::Reset()
#define Enumerator_Reset_m531859192(__this, method) ((  void (*) (Enumerator_t3715923368 *, const MethodInfo*))Enumerator_Reset_m531859192_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1681422929(__this, method) ((  void (*) (Enumerator_t4082431430 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1681422929_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::Dispose()
#define Enumerator_Dispose_m2977499502(__this, method) ((  void (*) (Enumerator_t3715923368 *, const MethodInfo*))Enumerator_Dispose_m2977499502_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::Dispose()
#define Enumerator_Dispose_m1595156764(__this, method) ((  void (*) (Enumerator_t4082431430 *, const MethodInfo*))Enumerator_Dispose_m1595156764_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::MoveNext()
#define Enumerator_MoveNext_m4104902027(__this, method) ((  bool (*) (Enumerator_t3715923368 *, const MethodInfo*))Enumerator_MoveNext_m4104902027_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::MoveNext()
#define Enumerator_MoveNext_m66940849(__this, method) ((  bool (*) (Enumerator_t4082431430 *, const MethodInfo*))Enumerator_MoveNext_m66940849_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::get_Value()
#define KeyValuePair_2_get_Value_m1229996980(__this, method) ((  PoseInfo_t1161658011  (*) (KeyValuePair_2_t153243888 *, const MethodInfo*))KeyValuePair_2_get_Value_m1229996980_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::get_Current()
#define Enumerator_get_Current_m2272525103(__this, method) ((  PoseInfo_t1161658011  (*) (Enumerator_t4082431430 *, const MethodInfo*))Enumerator_get_Current_m2272525103_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1971000786(__this, ___host0, method) ((  void (*) (Enumerator_t2683717434 *, Dictionary_2_t997184670 *, const MethodInfo*))Enumerator__ctor_m1971000786_gshared)(__this, ___host0, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3861338020(__this, method) ((  int32_t (*) (Enumerator_t2317209372 *, const MethodInfo*))Enumerator_get_CurrentValue_m3861338020_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m514775589(__this, method) ((  Il2CppObject * (*) (Enumerator_t2683717434 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m514775589_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::Reset()
#define Enumerator_Reset_m2100056858(__this, method) ((  void (*) (Enumerator_t2317209372 *, const MethodInfo*))Enumerator_Reset_m2100056858_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3523229681(__this, method) ((  void (*) (Enumerator_t2683717434 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3523229681_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::Dispose()
#define Enumerator_Dispose_m1953131992(__this, method) ((  void (*) (Enumerator_t2317209372 *, const MethodInfo*))Enumerator_Dispose_m1953131992_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::Dispose()
#define Enumerator_Dispose_m2249060810(__this, method) ((  void (*) (Enumerator_t2683717434 *, const MethodInfo*))Enumerator_Dispose_m2249060810_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::MoveNext()
#define Enumerator_MoveNext_m1170226423(__this, method) ((  bool (*) (Enumerator_t2317209372 *, const MethodInfo*))Enumerator_MoveNext_m1170226423_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::MoveNext()
#define Enumerator_MoveNext_m1436514993(__this, method) ((  bool (*) (Enumerator_t2683717434 *, const MethodInfo*))Enumerator_MoveNext_m1436514993_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::get_Value()
#define KeyValuePair_2_get_Value_m1579783160(__this, method) ((  int32_t (*) (KeyValuePair_2_t3049497188 *, const MethodInfo*))KeyValuePair_2_get_Value_m1579783160_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::get_Current()
#define Enumerator_get_Current_m3856955819(__this, method) ((  int32_t (*) (Enumerator_t2683717434 *, const MethodInfo*))Enumerator_get_Current_m3856955819_gshared)(__this, method)
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m3380712306 (ArgumentNullException_t628810857 * __this, String_t* ___paramName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor(System.String)
extern "C"  void NotSupportedException__ctor_m836173213 (NotSupportedException_t1793819818 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3512061196_gshared (Transform_1_t2737799006 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m488493188_gshared (Transform_1_t2737799006 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m488493188((Transform_1_t2737799006 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m929665079_gshared (Transform_1_t2737799006 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m929665079_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m805300450_gshared (Transform_1_t2737799006 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2152205186_gshared (Transform_1_t4064153 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m4020530914_gshared (Transform_1_t4064153 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m4020530914((Transform_1_t4064153 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2179239469_gshared (Transform_1_t4064153 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2179239469_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m620026520_gshared (Transform_1_t4064153 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m713310742_gshared (Transform_1_t704776203 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3749587448  Transform_1_Invoke_m1436021910_gshared (Transform_1_t704776203 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1436021910((Transform_1_t704776203 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3749587448  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t3749587448  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1786442111_gshared (Transform_1_t704776203 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1786442111_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3749587448  Transform_1_EndInvoke_m590952364_gshared (Transform_1_t704776203 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t3749587448 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3844763875_gshared (Transform_1_t3322033499 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m3763570295_gshared (Transform_1_t3322033499 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3763570295((Transform_1_t3322033499 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m802712344_gshared (Transform_1_t3322033499 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m802712344_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m336301945_gshared (Transform_1_t3322033499 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2914458810_gshared (Transform_1_t3939605346 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m2347662626_gshared (Transform_1_t3939605346 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2347662626((Transform_1_t3939605346 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1919808363_gshared (Transform_1_t3939605346 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1919808363_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1010744720_gshared (Transform_1_t3939605346 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4097589907_gshared (Transform_1_t880920208 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m2130809719_gshared (Transform_1_t880920208 * __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2130809719((Transform_1_t880920208 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1074189372_gshared (Transform_1_t880920208 * __this, int32_t ___key0, Label_t4243202660  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1074189372_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Label_t4243202660_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m3923273717_gshared (Transform_1_t880920208 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m219296054_gshared (Transform_1_t3135385623 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1008373517  Transform_1_Invoke_m387254326_gshared (Transform_1_t3135385623 * __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m387254326((Transform_1_t3135385623 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1008373517  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t1008373517  (*FunctionPointerType) (void* __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1679317215_gshared (Transform_1_t3135385623 * __this, int32_t ___key0, Label_t4243202660  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1679317215_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Label_t4243202660_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1008373517  Transform_1_EndInvoke_m3592138636_gshared (Transform_1_t3135385623 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t1008373517 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4251647142_gshared (Transform_1_t4198889554 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m1740004046_gshared (Transform_1_t4198889554 * __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1740004046((Transform_1_t4198889554 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3403453437_gshared (Transform_1_t4198889554 * __this, int32_t ___key0, Label_t4243202660  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3403453437_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Label_t4243202660_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m1497660408_gshared (Transform_1_t4198889554 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Reflection.Emit.Label>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1165979048_gshared (Transform_1_t2075247470 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Reflection.Emit.Label>::Invoke(TKey,TValue)
extern "C"  Label_t4243202660  Transform_1_Invoke_m2635029648_gshared (Transform_1_t2075247470 * __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2635029648((Transform_1_t2075247470 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Label_t4243202660  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Label_t4243202660  (*FunctionPointerType) (void* __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Reflection.Emit.Label>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2566047391_gshared (Transform_1_t2075247470 * __this, int32_t ___key0, Label_t4243202660  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2566047391_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Label_t4243202660_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Reflection.Emit.Label>::EndInvoke(System.IAsyncResult)
extern "C"  Label_t4243202660  Transform_1_EndInvoke_m2836568466_gshared (Transform_1_t2075247470 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Label_t4243202660 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m422834957_gshared (Transform_1_t3837169113 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m1424521241_gshared (Transform_1_t3837169113 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1424521241((Transform_1_t3837169113 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m852013770_gshared (Transform_1_t3837169113 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m852013770_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Status_t4057911311_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m2450384407_gshared (Transform_1_t3837169113 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1978900308_gshared (Transform_1_t1611375883 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t823082168  Transform_1_Invoke_m3913597300_gshared (Transform_1_t1611375883 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3913597300((Transform_1_t1611375883 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t823082168  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t823082168  (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1836076395_gshared (Transform_1_t1611375883 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1836076395_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Status_t4057911311_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t823082168  Transform_1_EndInvoke_m3402850782_gshared (Transform_1_t1611375883 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t823082168 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2756235072_gshared (Transform_1_t2860171163 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m2457631416_gshared (Transform_1_t2860171163 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2457631416((Transform_1_t2860171163 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1826169059_gshared (Transform_1_t2860171163 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1826169059_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Status_t4057911311_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m3593578882_gshared (Transform_1_t2860171163 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1169108698_gshared (Transform_1_t551237730 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m3070581954_gshared (Transform_1_t551237730 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3070581954((Transform_1_t551237730 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1026459083_gshared (Transform_1_t551237730 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1026459083_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Status_t4057911311_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m1474024048_gshared (Transform_1_t551237730 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3792954185_gshared (Transform_1_t2883023109 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m1302756149_gshared (Transform_1_t2883023109 * __this, int32_t ___key0, VirtualButtonData_t1223885651  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1302756149((Transform_1_t2883023109 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, VirtualButtonData_t1223885651  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int32_t ___key0, VirtualButtonData_t1223885651  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1409521950_gshared (Transform_1_t2883023109 * __this, int32_t ___key0, VirtualButtonData_t1223885651  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1409521950_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(VirtualButtonData_t1223885651_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m4195138263_gshared (Transform_1_t2883023109 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2246978550_gshared (Transform_1_t2118171515 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t2284023804  Transform_1_Invoke_m557327862_gshared (Transform_1_t2118171515 * __this, int32_t ___key0, VirtualButtonData_t1223885651  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m557327862((Transform_1_t2118171515 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2284023804  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, VirtualButtonData_t1223885651  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t2284023804  (*FunctionPointerType) (void* __this, int32_t ___key0, VirtualButtonData_t1223885651  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m4146785439_gshared (Transform_1_t2118171515 * __this, int32_t ___key0, VirtualButtonData_t1223885651  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m4146785439_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(VirtualButtonData_t1223885651_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t2284023804  Transform_1_EndInvoke_m444640204_gshared (Transform_1_t2118171515 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t2284023804 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4154496808_gshared (Transform_1_t1906025159 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m2110329656_gshared (Transform_1_t1906025159 * __this, int32_t ___key0, VirtualButtonData_t1223885651  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2110329656((Transform_1_t1906025159 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, VirtualButtonData_t1223885651  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, VirtualButtonData_t1223885651  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3375480339_gshared (Transform_1_t1906025159 * __this, int32_t ___key0, VirtualButtonData_t1223885651  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3375480339_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(VirtualButtonData_t1223885651_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m2901876014_gshared (Transform_1_t1906025159 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m803981588_gshared (Transform_1_t1058033362 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Invoke(TKey,TValue)
extern "C"  VirtualButtonData_t1223885651  Transform_1_Invoke_m574052764_gshared (Transform_1_t1058033362 * __this, int32_t ___key0, VirtualButtonData_t1223885651  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m574052764((Transform_1_t1058033362 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef VirtualButtonData_t1223885651  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, VirtualButtonData_t1223885651  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef VirtualButtonData_t1223885651  (*FunctionPointerType) (void* __this, int32_t ___key0, VirtualButtonData_t1223885651  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,Vuforia.VuforiaManagerImpl/VirtualButtonData>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m109519847_gshared (Transform_1_t1058033362 * __this, int32_t ___key0, VirtualButtonData_t1223885651  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m109519847_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(VirtualButtonData_t1223885651_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,Vuforia.VuforiaManagerImpl/VirtualButtonData>::EndInvoke(System.IAsyncResult)
extern "C"  VirtualButtonData_t1223885651  Transform_1_EndInvoke_m1197290474_gshared (Transform_1_t1058033362 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(VirtualButtonData_t1223885651 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3506255812_gshared (Transform_1_t1772107233 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m1135920892_gshared (Transform_1_t1772107233 * __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1135920892((Transform_1_t1772107233 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m652403149_gshared (Transform_1_t1772107233 * __this, int64_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m652403149_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m2279994258_gshared (Transform_1_t1772107233 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m59718966_gshared (Transform_1_t3836940203 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t818741072  Transform_1_Invoke_m2933042102_gshared (Transform_1_t3836940203 * __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2933042102((Transform_1_t3836940203 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t818741072  (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t818741072  (*FunctionPointerType) (void* __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1482372703_gshared (Transform_1_t3836940203 * __this, int64_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1482372703_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t818741072  Transform_1_EndInvoke_m2262549260_gshared (Transform_1_t3836940203 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t818741072 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3207773655_gshared (Transform_1_t795109283 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m1013242963_gshared (Transform_1_t795109283 * __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1013242963((Transform_1_t795109283 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1955297614_gshared (Transform_1_t795109283 * __this, int64_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1955297614_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m1055649173_gshared (Transform_1_t795109283 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m271886956_gshared (Transform_1_t3927277168 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Int64>::Invoke(TKey,TValue)
extern "C"  int64_t Transform_1_Invoke_m3525936996_gshared (Transform_1_t3927277168 * __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3525936996((Transform_1_t3927277168 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int64_t (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int64_t (*FunctionPointerType) (void* __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Int64>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1101679575_gshared (Transform_1_t3927277168 * __this, int64_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1101679575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Int64>::EndInvoke(System.IAsyncResult)
extern "C"  int64_t Transform_1_EndInvoke_m3184135298_gshared (Transform_1_t3927277168 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int64_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3373533409_gshared (Transform_1_t2356341726 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m1299483797_gshared (Transform_1_t2356341726 * __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1299483797((Transform_1_t2356341726 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3448776694_gshared (Transform_1_t2356341726 * __this, int64_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3448776694_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m3006940059_gshared (Transform_1_t2356341726 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m232902212_gshared (Transform_1_t743779247 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1436312919  Transform_1_Invoke_m1183831012_gshared (Transform_1_t743779247 * __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1183831012((Transform_1_t743779247 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1436312919  (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t1436312919  (*FunctionPointerType) (void* __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m655666259_gshared (Transform_1_t743779247 * __this, int64_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m655666259_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1436312919  Transform_1_EndInvoke_m229614702_gshared (Transform_1_t743779247 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t1436312919 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3927316327_gshared (Transform_1_t216544365 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Int64>::Invoke(TKey,TValue)
extern "C"  int64_t Transform_1_Invoke_m3042893651_gshared (Transform_1_t216544365 * __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3042893651((Transform_1_t216544365 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int64_t (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int64_t (*FunctionPointerType) (void* __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Int64>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3566819466_gshared (Transform_1_t216544365 * __this, int64_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3566819466_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Int64>::EndInvoke(System.IAsyncResult)
extern "C"  int64_t Transform_1_EndInvoke_m2544704049_gshared (Transform_1_t216544365 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int64_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4269627275_gshared (Transform_1_t1996915623 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m333092703_gshared (Transform_1_t1996915623 * __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m333092703((Transform_1_t1996915623 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m4016145208_gshared (Transform_1_t1996915623 * __this, int64_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m4016145208_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m3793097937_gshared (Transform_1_t1996915623 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2937663280_gshared (Transform_1_t2129470893 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m810218400_gshared (Transform_1_t2129470893 * __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m810218400((Transform_1_t2129470893 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1858167433_gshared (Transform_1_t2129470893 * __this, IntPtr_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1858167433_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m407226534_gshared (Transform_1_t2129470893 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Int32>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3843771734_gshared (Transform_1_t3646810779 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Int32>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t271247988  Transform_1_Invoke_m3442926198_gshared (Transform_1_t3646810779 * __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3442926198((Transform_1_t3646810779 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t271247988  (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t271247988  (*FunctionPointerType) (void* __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Int32>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1958180191_gshared (Transform_1_t3646810779 * __this, IntPtr_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1958180191_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Int32>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t271247988  Transform_1_EndInvoke_m551042604_gshared (Transform_1_t3646810779 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t271247988 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1035489375_gshared (Transform_1_t1152472943 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m1529026043_gshared (Transform_1_t1152472943 * __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1529026043((Transform_1_t1152472943 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2708840102_gshared (Transform_1_t1152472943 * __this, IntPtr_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2708840102_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m2656215677_gshared (Transform_1_t1152472943 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.IntPtr>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m963744174_gshared (Transform_1_t1584656104 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.IntPtr>::Invoke(TKey,TValue)
extern "C"  IntPtr_t Transform_1_Invoke_m2250487894_gshared (Transform_1_t1584656104 * __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2250487894((Transform_1_t1584656104 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef IntPtr_t (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef IntPtr_t (*FunctionPointerType) (void* __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.IntPtr>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1506524387_gshared (Transform_1_t1584656104 * __this, IntPtr_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1506524387_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.IntPtr>::EndInvoke(System.IAsyncResult)
extern "C"  IntPtr_t Transform_1_EndInvoke_m3407152920_gshared (Transform_1_t1584656104 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(IntPtr_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1237139657_gshared (Transform_1_t2713705386 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m2675095773_gshared (Transform_1_t2713705386 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2675095773((Transform_1_t2713705386 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m4270324442_gshared (Transform_1_t2713705386 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m4270324442_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m4121732147_gshared (Transform_1_t2713705386 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4075225366_gshared (Transform_1_t553649823 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t888819835  Transform_1_Invoke_m1023901590_gshared (Transform_1_t553649823 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1023901590((Transform_1_t553649823 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t888819835  (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t888819835  (*FunctionPointerType) (void* __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m345445247_gshared (Transform_1_t553649823 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m345445247_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t888819835  Transform_1_EndInvoke_m3664875948_gshared (Transform_1_t553649823 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t888819835 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.IntPtr>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2284483831_gshared (Transform_1_t2168890597 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.IntPtr>::Invoke(TKey,TValue)
extern "C"  IntPtr_t Transform_1_Invoke_m524403747_gshared (Transform_1_t2168890597 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m524403747((Transform_1_t2168890597 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef IntPtr_t (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef IntPtr_t (*FunctionPointerType) (void* __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.IntPtr>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2914101698_gshared (Transform_1_t2168890597 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2914101698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.IntPtr>::EndInvoke(System.IAsyncResult)
extern "C"  IntPtr_t Transform_1_EndInvoke_m1944338913_gshared (Transform_1_t2168890597 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(IntPtr_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3731381235_gshared (Transform_1_t2354279283 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m1111426439_gshared (Transform_1_t2354279283 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1111426439((Transform_1_t2354279283 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3901963208_gshared (Transform_1_t2354279283 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3901963208_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m275018665_gshared (Transform_1_t2354279283 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3569730739_gshared (Transform_1_t2990554165 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Boolean>::Invoke(TKey,TValue)
extern "C"  bool Transform_1_Invoke_m2906736839_gshared (Transform_1_t2990554165 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2906736839((Transform_1_t2990554165 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Boolean>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3826027984_gshared (Transform_1_t2990554165 * __this, Il2CppObject * ___key0, bool ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3826027984_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Transform_1_EndInvoke_m258407721_gshared (Transform_1_t2990554165 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1978472014_gshared (Transform_1_t2213854845 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m2509306846_gshared (Transform_1_t2213854845 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2509306846((Transform_1_t2213854845 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1167293475_gshared (Transform_1_t2213854845 * __this, Il2CppObject * ___key0, bool ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1167293475_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m2742732284_gshared (Transform_1_t2213854845 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m974062490_gshared (Transform_1_t339959515 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1174980068  Transform_1_Invoke_m4136847354_gshared (Transform_1_t339959515 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m4136847354((Transform_1_t339959515 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1174980068  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1174980068  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t1174980068  (*FunctionPointerType) (void* __this, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2640141359_gshared (Transform_1_t339959515 * __this, Il2CppObject * ___key0, bool ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2640141359_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1174980068  Transform_1_EndInvoke_m3779953636_gshared (Transform_1_t339959515 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t1174980068 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4273332630_gshared (Transform_1_t1854428742 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m3830249854_gshared (Transform_1_t1854428742 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3830249854((Transform_1_t1854428742 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, bool ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2082152109_gshared (Transform_1_t1854428742 * __this, Il2CppObject * ___key0, bool ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2082152109_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1709357192_gshared (Transform_1_t1854428742 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m353209818_gshared (Transform_1_t1676220171 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m719893226_gshared (Transform_1_t1676220171 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m719893226((Transform_1_t1676220171 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m786657825_gshared (Transform_1_t1676220171 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m786657825_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m664119620_gshared (Transform_1_t1676220171 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m583305686_gshared (Transform_1_t2343594867 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3716250094  Transform_1_Invoke_m1172879766_gshared (Transform_1_t2343594867 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1172879766((Transform_1_t2343594867 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3716250094  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3716250094  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t3716250094  (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2336029567_gshared (Transform_1_t2343594867 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2336029567_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3716250094  Transform_1_EndInvoke_m1025924012_gshared (Transform_1_t2343594867 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t3716250094 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1642784939_gshared (Transform_1_t699222221 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m2099058127_gshared (Transform_1_t699222221 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2099058127((Transform_1_t699222221 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3169382212_gshared (Transform_1_t699222221 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3169382212_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m7550125_gshared (Transform_1_t699222221 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2375232750_gshared (Transform_1_t1316794068 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m2777900342_gshared (Transform_1_t1316794068 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2777900342((Transform_1_t1316794068 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1856627011_gshared (Transform_1_t1316794068 * __this, Il2CppObject * ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1856627011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m2379644184_gshared (Transform_1_t1316794068 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2129956639_gshared (Transform_1_t427295924 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m2455579403_gshared (Transform_1_t427295924 * __this, Il2CppObject * ___key0, Nullable_1_t334943763  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2455579403((Transform_1_t427295924 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, Nullable_1_t334943763  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, Nullable_1_t334943763  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, Nullable_1_t334943763  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1988794314_gshared (Transform_1_t427295924 * __this, Il2CppObject * ___key0, Nullable_1_t334943763  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1988794314_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Nullable_1_t334943763_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m1232746697_gshared (Transform_1_t427295924 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Int32>>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3862666998_gshared (Transform_1_t3652704231 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Int32>>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1979316409  Transform_1_Invoke_m514070902_gshared (Transform_1_t3652704231 * __this, Il2CppObject * ___key0, Nullable_1_t334943763  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m514070902((Transform_1_t3652704231 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1979316409  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, Nullable_1_t334943763  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1979316409  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, Nullable_1_t334943763  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t1979316409  (*FunctionPointerType) (void* __this, Nullable_1_t334943763  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Int32>>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3575693983_gshared (Transform_1_t3652704231 * __this, Il2CppObject * ___key0, Nullable_1_t334943763  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3575693983_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Nullable_1_t334943763_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Int32>>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1979316409  Transform_1_EndInvoke_m895528780_gshared (Transform_1_t3652704231 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t1979316409 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1864940023_gshared (Transform_1_t2008331585 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>>::Invoke(TKey,TValue)
extern "C"  Nullable_1_t334943763  Transform_1_Invoke_m3781372707_gshared (Transform_1_t2008331585 * __this, Il2CppObject * ___key0, Nullable_1_t334943763  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3781372707((Transform_1_t2008331585 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Nullable_1_t334943763  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, Nullable_1_t334943763  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Nullable_1_t334943763  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, Nullable_1_t334943763  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Nullable_1_t334943763  (*FunctionPointerType) (void* __this, Nullable_1_t334943763  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3499650370_gshared (Transform_1_t2008331585 * __this, Il2CppObject * ___key0, Nullable_1_t334943763  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3499650370_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Nullable_1_t334943763_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>>::EndInvoke(System.IAsyncResult)
extern "C"  Nullable_1_t334943763  Transform_1_EndInvoke_m4154128609_gshared (Transform_1_t2008331585 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Nullable_1_t334943763 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1847768453_gshared (Transform_1_t67869821 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m419001697_gshared (Transform_1_t67869821 * __this, Il2CppObject * ___key0, Nullable_1_t334943763  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m419001697((Transform_1_t67869821 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, Nullable_1_t334943763  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, Nullable_1_t334943763  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Nullable_1_t334943763  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m172013252_gshared (Transform_1_t67869821 * __this, Il2CppObject * ___key0, Nullable_1_t334943763  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m172013252_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Nullable_1_t334943763_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m989945287_gshared (Transform_1_t67869821 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4161450529_gshared (Transform_1_t2260454664 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m2770612589_gshared (Transform_1_t2260454664 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2770612589((Transform_1_t2260454664 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3014766640_gshared (Transform_1_t2260454664 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m803975703_gshared (Transform_1_t2260454664 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2658320534_gshared (Transform_1_t3545401207 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t38854645  Transform_1_Invoke_m1976033878_gshared (Transform_1_t3545401207 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1976033878((Transform_1_t3545401207 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t38854645  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t38854645  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t38854645  (*FunctionPointerType) (void* __this, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3105433791_gshared (Transform_1_t3545401207 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t38854645  Transform_1_EndInvoke_m687617772_gshared (Transform_1_t3545401207 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t38854645 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3849972087_gshared (Transform_1_t1901028561 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m1224512163_gshared (Transform_1_t1901028561 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1224512163((Transform_1_t1901028561 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2122310722_gshared (Transform_1_t1901028561 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1237128929_gshared (Transform_1_t1901028561 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2161053645_gshared (Transform_1_t1173087252 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m3973223697_gshared (Transform_1_t1173087252 * __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3973223697((Transform_1_t1173087252 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2990576644_gshared (Transform_1_t1173087252 * __this, Il2CppObject * ___key0, uint16_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2990576644_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(UInt16_t986882611_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m392634387_gshared (Transform_1_t1173087252 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3551863574_gshared (Transform_1_t755467111 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t2631255257  Transform_1_Invoke_m2051902166_gshared (Transform_1_t755467111 * __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2051902166((Transform_1_t755467111 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2631255257  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2631255257  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t2631255257  (*FunctionPointerType) (void* __this, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3964891327_gshared (Transform_1_t755467111 * __this, Il2CppObject * ___key0, uint16_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3964891327_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(UInt16_t986882611_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t2631255257  Transform_1_EndInvoke_m3201392492_gshared (Transform_1_t755467111 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t2631255257 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2846172515_gshared (Transform_1_t813661149 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m3150503015_gshared (Transform_1_t813661149 * __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3150503015((Transform_1_t813661149 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2109839950_gshared (Transform_1_t813661149 * __this, Il2CppObject * ___key0, uint16_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2109839950_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(UInt16_t986882611_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m2931809245_gshared (Transform_1_t813661149 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.UInt16>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m652010423_gshared (Transform_1_t3406061761 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.UInt16>::Invoke(TKey,TValue)
extern "C"  uint16_t Transform_1_Invoke_m2472178915_gshared (Transform_1_t3406061761 * __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2472178915((Transform_1_t3406061761 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef uint16_t (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef uint16_t (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint16_t (*FunctionPointerType) (void* __this, uint16_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.UInt16>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1380725890_gshared (Transform_1_t3406061761 * __this, Il2CppObject * ___key0, uint16_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1380725890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(UInt16_t986882611_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.UInt16>::EndInvoke(System.IAsyncResult)
extern "C"  uint16_t Transform_1_EndInvoke_m1094807585_gshared (Transform_1_t3406061761 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(uint16_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3886908164_gshared (Transform_1_t4113099591 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m544618940_gshared (Transform_1_t4113099591 * __this, Il2CppObject * ___key0, Vector3_t2243707580  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m544618940((Transform_1_t4113099591 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, Vector3_t2243707580  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, Vector3_t2243707580  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, Vector3_t2243707580  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m629477401_gshared (Transform_1_t4113099591 * __this, Il2CppObject * ___key0, Vector3_t2243707580  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m629477401_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m2446839750_gshared (Transform_1_t4113099591 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Vector3>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m523981366_gshared (Transform_1_t657337123 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Vector3>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3888080226  Transform_1_Invoke_m1893645494_gshared (Transform_1_t657337123 * __this, Il2CppObject * ___key0, Vector3_t2243707580  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1893645494((Transform_1_t657337123 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3888080226  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, Vector3_t2243707580  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3888080226  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, Vector3_t2243707580  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t3888080226  (*FunctionPointerType) (void* __this, Vector3_t2243707580  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Vector3>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1553007199_gshared (Transform_1_t657337123 * __this, Il2CppObject * ___key0, Vector3_t2243707580  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1553007199_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Vector3>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3888080226  Transform_1_EndInvoke_m777737356_gshared (Transform_1_t657337123 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t3888080226 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4119223700_gshared (Transform_1_t3753673488 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m588307748_gshared (Transform_1_t3753673488 * __this, Il2CppObject * ___key0, Vector3_t2243707580  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m588307748((Transform_1_t3753673488 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, Vector3_t2243707580  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, Vector3_t2243707580  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Vector3_t2243707580  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1402666263_gshared (Transform_1_t3753673488 * __this, Il2CppObject * ___key0, Vector3_t2243707580  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1402666263_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1239076774_gshared (Transform_1_t3753673488 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3772004087_gshared (Transform_1_t3307931773 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,UnityEngine.Vector3>::Invoke(TKey,TValue)
extern "C"  Vector3_t2243707580  Transform_1_Invoke_m469561379_gshared (Transform_1_t3307931773 * __this, Il2CppObject * ___key0, Vector3_t2243707580  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m469561379((Transform_1_t3307931773 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Vector3_t2243707580  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, Vector3_t2243707580  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Vector3_t2243707580  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, Vector3_t2243707580  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Vector3_t2243707580  (*FunctionPointerType) (void* __this, Vector3_t2243707580  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,UnityEngine.Vector3>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m4121449538_gshared (Transform_1_t3307931773 * __this, Il2CppObject * ___key0, Vector3_t2243707580  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m4121449538_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.Vector3,UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  Vector3_t2243707580  Transform_1_EndInvoke_m3660930529_gshared (Transform_1_t3307931773 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Vector3_t2243707580 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m898157465_gshared (Transform_1_t2507364635 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m3013029325_gshared (Transform_1_t2507364635 * __this, Il2CppObject * ___key0, ProfileData_t1724666488  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3013029325((Transform_1_t2507364635 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ProfileData_t1724666488  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ProfileData_t1724666488  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, ProfileData_t1724666488  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3448866058_gshared (Transform_1_t2507364635 * __this, Il2CppObject * ___key0, ProfileData_t1724666488  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3448866058_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ProfileData_t1724666488_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m188839907_gshared (Transform_1_t2507364635 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2916375062_gshared (Transform_1_t2827528371 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3369039134  Transform_1_Invoke_m909098262_gshared (Transform_1_t2827528371 * __this, Il2CppObject * ___key0, ProfileData_t1724666488  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m909098262((Transform_1_t2827528371 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3369039134  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ProfileData_t1724666488  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3369039134  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ProfileData_t1724666488  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t3369039134  (*FunctionPointerType) (void* __this, ProfileData_t1724666488  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3175591935_gshared (Transform_1_t2827528371 * __this, Il2CppObject * ___key0, ProfileData_t1724666488  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3175591935_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ProfileData_t1724666488_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3369039134  Transform_1_EndInvoke_m1570680620_gshared (Transform_1_t2827528371 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t3369039134 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2088140547_gshared (Transform_1_t2147938532 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m519637335_gshared (Transform_1_t2147938532 * __this, Il2CppObject * ___key0, ProfileData_t1724666488  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m519637335((Transform_1_t2147938532 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ProfileData_t1724666488  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ProfileData_t1724666488  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, ProfileData_t1724666488  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3164846456_gshared (Transform_1_t2147938532 * __this, Il2CppObject * ___key0, ProfileData_t1724666488  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3164846456_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ProfileData_t1724666488_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m3041686233_gshared (Transform_1_t2147938532 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2014670519_gshared (Transform_1_t1183155725 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,Vuforia.WebCamProfile/ProfileData>::Invoke(TKey,TValue)
extern "C"  ProfileData_t1724666488  Transform_1_Invoke_m3366889123_gshared (Transform_1_t1183155725 * __this, Il2CppObject * ___key0, ProfileData_t1724666488  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3366889123((Transform_1_t1183155725 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef ProfileData_t1724666488  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, ProfileData_t1724666488  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef ProfileData_t1724666488  (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, ProfileData_t1724666488  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef ProfileData_t1724666488  (*FunctionPointerType) (void* __this, ProfileData_t1724666488  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,Vuforia.WebCamProfile/ProfileData>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m500565698_gshared (Transform_1_t1183155725 * __this, Il2CppObject * ___key0, ProfileData_t1724666488  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m500565698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key0;
	__d_args[1] = Box(ProfileData_t1724666488_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,Vuforia.WebCamProfile/ProfileData>::EndInvoke(System.IAsyncResult)
extern "C"  ProfileData_t1724666488  Transform_1_EndInvoke_m2319433825_gshared (Transform_1_t1183155725 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(ProfileData_t1724666488 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m97147873_gshared (Transform_1_t3129600301 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m1263589125_gshared (Transform_1_t3129600301 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1263589125((Transform_1_t3129600301 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1330432734_gshared (Transform_1_t3129600301 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1330432734_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(PIXEL_FORMAT_t3010530044_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m288602267_gshared (Transform_1_t3129600301 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3337008500_gshared (Transform_1_t2221772955 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t2141048052  Transform_1_Invoke_m738258324_gshared (Transform_1_t2221772955 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m738258324((Transform_1_t2221772955 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2141048052  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t2141048052  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2760658979_gshared (Transform_1_t2221772955 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2760658979_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(PIXEL_FORMAT_t3010530044_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t2141048052  Transform_1_EndInvoke_m2204805950_gshared (Transform_1_t2221772955 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t2141048052 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m925266811_gshared (Transform_1_t2770174198 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m3399302511_gshared (Transform_1_t2770174198 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3399302511((Transform_1_t2770174198 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3238894512_gshared (Transform_1_t2770174198 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3238894512_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(PIXEL_FORMAT_t3010530044_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m4101449681_gshared (Transform_1_t2770174198 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1408729447_gshared (Transform_1_t3091254947 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,Vuforia.Image/PIXEL_FORMAT>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m366087667_gshared (Transform_1_t3091254947 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m366087667((Transform_1_t3091254947 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,Vuforia.Image/PIXEL_FORMAT>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3515991146_gshared (Transform_1_t3091254947 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3515991146_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(PIXEL_FORMAT_t3010530044_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,Vuforia.Image/PIXEL_FORMAT>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m1228232497_gshared (Transform_1_t3091254947 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2897343013_gshared (Transform_1_t2910497288 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m134111705_gshared (Transform_1_t2910497288 * __this, TrackableIdPair_t1329355276  ___key0, PoseAgeEntry_t3432166560  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m134111705((Transform_1_t2910497288 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, TrackableIdPair_t1329355276  ___key0, PoseAgeEntry_t3432166560  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, TrackableIdPair_t1329355276  ___key0, PoseAgeEntry_t3432166560  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3407289780_gshared (Transform_1_t2910497288 * __this, TrackableIdPair_t1329355276  ___key0, PoseAgeEntry_t3432166560  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3407289780_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(TrackableIdPair_t1329355276_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(PoseAgeEntry_t3432166560_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m152644459_gshared (Transform_1_t2910497288 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2912521238_gshared (Transform_1_t2285374327 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t2423752437  Transform_1_Invoke_m1713490774_gshared (Transform_1_t2285374327 * __this, TrackableIdPair_t1329355276  ___key0, PoseAgeEntry_t3432166560  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1713490774((Transform_1_t2285374327 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2423752437  (*FunctionPointerType) (Il2CppObject *, void* __this, TrackableIdPair_t1329355276  ___key0, PoseAgeEntry_t3432166560  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t2423752437  (*FunctionPointerType) (void* __this, TrackableIdPair_t1329355276  ___key0, PoseAgeEntry_t3432166560  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m717873343_gshared (Transform_1_t2285374327 * __this, TrackableIdPair_t1329355276  ___key0, PoseAgeEntry_t3432166560  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m717873343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(TrackableIdPair_t1329355276_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(PoseAgeEntry_t3432166560_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t2423752437  Transform_1_EndInvoke_m3779468140_gshared (Transform_1_t2285374327 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t2423752437 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1345066737_gshared (Transform_1_t3293788450 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::Invoke(TKey,TValue)
extern "C"  PoseAgeEntry_t3432166560  Transform_1_Invoke_m1107381605_gshared (Transform_1_t3293788450 * __this, TrackableIdPair_t1329355276  ___key0, PoseAgeEntry_t3432166560  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1107381605((Transform_1_t3293788450 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef PoseAgeEntry_t3432166560  (*FunctionPointerType) (Il2CppObject *, void* __this, TrackableIdPair_t1329355276  ___key0, PoseAgeEntry_t3432166560  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef PoseAgeEntry_t3432166560  (*FunctionPointerType) (void* __this, TrackableIdPair_t1329355276  ___key0, PoseAgeEntry_t3432166560  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1508964964_gshared (Transform_1_t3293788450 * __this, TrackableIdPair_t1329355276  ___key0, PoseAgeEntry_t3432166560  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1508964964_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(TrackableIdPair_t1329355276_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(PoseAgeEntry_t3432166560_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::EndInvoke(System.IAsyncResult)
extern "C"  PoseAgeEntry_t3432166560  Transform_1_EndInvoke_m3952120147_gshared (Transform_1_t3293788450 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(PoseAgeEntry_t3432166560 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,Vuforia.VuforiaManager/TrackableIdPair>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1861109589_gshared (Transform_1_t1190977166 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,Vuforia.VuforiaManager/TrackableIdPair>::Invoke(TKey,TValue)
extern "C"  TrackableIdPair_t1329355276  Transform_1_Invoke_m939805297_gshared (Transform_1_t1190977166 * __this, TrackableIdPair_t1329355276  ___key0, PoseAgeEntry_t3432166560  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m939805297((Transform_1_t1190977166 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef TrackableIdPair_t1329355276  (*FunctionPointerType) (Il2CppObject *, void* __this, TrackableIdPair_t1329355276  ___key0, PoseAgeEntry_t3432166560  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef TrackableIdPair_t1329355276  (*FunctionPointerType) (void* __this, TrackableIdPair_t1329355276  ___key0, PoseAgeEntry_t3432166560  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,Vuforia.VuforiaManager/TrackableIdPair>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m270362696_gshared (Transform_1_t1190977166 * __this, TrackableIdPair_t1329355276  ___key0, PoseAgeEntry_t3432166560  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m270362696_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(TrackableIdPair_t1329355276_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(PoseAgeEntry_t3432166560_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry,Vuforia.VuforiaManager/TrackableIdPair>::EndInvoke(System.IAsyncResult)
extern "C"  TrackableIdPair_t1329355276  Transform_1_EndInvoke_m1480897735_gshared (Transform_1_t1190977166 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(TrackableIdPair_t1329355276 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3555575442_gshared (Transform_1_t2927905217 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m1256587466_gshared (Transform_1_t2927905217 * __this, TrackableIdPair_t1329355276  ___key0, PoseInfo_t1161658011  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1256587466((Transform_1_t2927905217 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, TrackableIdPair_t1329355276  ___key0, PoseInfo_t1161658011  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, TrackableIdPair_t1329355276  ___key0, PoseInfo_t1161658011  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1380819331_gshared (Transform_1_t2927905217 * __this, TrackableIdPair_t1329355276  ___key0, PoseInfo_t1161658011  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1380819331_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(TrackableIdPair_t1329355276_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(PoseInfo_t1161658011_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m2251431320_gshared (Transform_1_t2927905217 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2102029238_gshared (Transform_1_t32273707 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t153243888  Transform_1_Invoke_m2222759478_gshared (Transform_1_t32273707 * __this, TrackableIdPair_t1329355276  ___key0, PoseInfo_t1161658011  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2222759478((Transform_1_t32273707 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t153243888  (*FunctionPointerType) (Il2CppObject *, void* __this, TrackableIdPair_t1329355276  ___key0, PoseInfo_t1161658011  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t153243888  (*FunctionPointerType) (void* __this, TrackableIdPair_t1329355276  ___key0, PoseInfo_t1161658011  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2103714143_gshared (Transform_1_t32273707 * __this, TrackableIdPair_t1329355276  ___key0, PoseInfo_t1161658011  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2103714143_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(TrackableIdPair_t1329355276_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(PoseInfo_t1161658011_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t153243888  Transform_1_EndInvoke_m853620236_gshared (Transform_1_t32273707 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t153243888 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2260677277_gshared (Transform_1_t1040687830 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::Invoke(TKey,TValue)
extern "C"  PoseInfo_t1161658011  Transform_1_Invoke_m1374546105_gshared (Transform_1_t1040687830 * __this, TrackableIdPair_t1329355276  ___key0, PoseInfo_t1161658011  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1374546105((Transform_1_t1040687830 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef PoseInfo_t1161658011  (*FunctionPointerType) (Il2CppObject *, void* __this, TrackableIdPair_t1329355276  ___key0, PoseInfo_t1161658011  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef PoseInfo_t1161658011  (*FunctionPointerType) (void* __this, TrackableIdPair_t1329355276  ___key0, PoseInfo_t1161658011  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3070020174_gshared (Transform_1_t1040687830 * __this, TrackableIdPair_t1329355276  ___key0, PoseInfo_t1161658011  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3070020174_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(TrackableIdPair_t1329355276_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(PoseInfo_t1161658011_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::EndInvoke(System.IAsyncResult)
extern "C"  PoseInfo_t1161658011  Transform_1_EndInvoke_m2751109223_gshared (Transform_1_t1040687830 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(PoseInfo_t1161658011 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,Vuforia.VuforiaManager/TrackableIdPair>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2868591346_gshared (Transform_1_t1208385095 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,Vuforia.VuforiaManager/TrackableIdPair>::Invoke(TKey,TValue)
extern "C"  TrackableIdPair_t1329355276  Transform_1_Invoke_m1684249954_gshared (Transform_1_t1208385095 * __this, TrackableIdPair_t1329355276  ___key0, PoseInfo_t1161658011  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1684249954((Transform_1_t1208385095 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef TrackableIdPair_t1329355276  (*FunctionPointerType) (Il2CppObject *, void* __this, TrackableIdPair_t1329355276  ___key0, PoseInfo_t1161658011  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef TrackableIdPair_t1329355276  (*FunctionPointerType) (void* __this, TrackableIdPair_t1329355276  ___key0, PoseInfo_t1161658011  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,Vuforia.VuforiaManager/TrackableIdPair>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m56665023_gshared (Transform_1_t1208385095 * __this, TrackableIdPair_t1329355276  ___key0, PoseInfo_t1161658011  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m56665023_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(TrackableIdPair_t1329355276_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(PoseInfo_t1161658011_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo,Vuforia.VuforiaManager/TrackableIdPair>::EndInvoke(System.IAsyncResult)
extern "C"  TrackableIdPair_t1329355276  Transform_1_EndInvoke_m4184997356_gshared (Transform_1_t1208385095 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(TrackableIdPair_t1329355276 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3877051878_gshared (Transform_1_t2908250109 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m3734845782_gshared (Transform_1_t2908250109 * __this, TrackableIdPair_t1329355276  ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3734845782((Transform_1_t2908250109 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, TrackableIdPair_t1329355276  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, TrackableIdPair_t1329355276  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3927196739_gshared (Transform_1_t2908250109 * __this, TrackableIdPair_t1329355276  ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3927196739_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(TrackableIdPair_t1329355276_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Status_t4057911311_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m734562676_gshared (Transform_1_t2908250109 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4047550250_gshared (Transform_1_t2908871899 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3049497188  Transform_1_Invoke_m589798410_gshared (Transform_1_t2908871899 * __this, TrackableIdPair_t1329355276  ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m589798410((Transform_1_t2908871899 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3049497188  (*FunctionPointerType) (Il2CppObject *, void* __this, TrackableIdPair_t1329355276  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t3049497188  (*FunctionPointerType) (void* __this, TrackableIdPair_t1329355276  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m170097695_gshared (Transform_1_t2908871899 * __this, TrackableIdPair_t1329355276  ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m170097695_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(TrackableIdPair_t1329355276_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Status_t4057911311_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3049497188  Transform_1_EndInvoke_m2658234260_gshared (Transform_1_t2908871899 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t3049497188 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m447829889_gshared (Transform_1_t3917286022 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m2009610709_gshared (Transform_1_t3917286022 * __this, TrackableIdPair_t1329355276  ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2009610709((Transform_1_t3917286022 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, TrackableIdPair_t1329355276  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, TrackableIdPair_t1329355276  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3676348468_gshared (Transform_1_t3917286022 * __this, TrackableIdPair_t1329355276  ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3676348468_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(TrackableIdPair_t1329355276_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Status_t4057911311_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m2384721027_gshared (Transform_1_t3917286022 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,Vuforia.VuforiaManager/TrackableIdPair>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m378763558_gshared (Transform_1_t1188729987 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,Vuforia.VuforiaManager/TrackableIdPair>::Invoke(TKey,TValue)
extern "C"  TrackableIdPair_t1329355276  Transform_1_Invoke_m1721734334_gshared (Transform_1_t1188729987 * __this, TrackableIdPair_t1329355276  ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1721734334((Transform_1_t1188729987 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef TrackableIdPair_t1329355276  (*FunctionPointerType) (Il2CppObject *, void* __this, TrackableIdPair_t1329355276  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef TrackableIdPair_t1329355276  (*FunctionPointerType) (void* __this, TrackableIdPair_t1329355276  ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,Vuforia.VuforiaManager/TrackableIdPair>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m170449759_gshared (Transform_1_t1188729987 * __this, TrackableIdPair_t1329355276  ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m170449759_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(TrackableIdPair_t1329355276_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Status_t4057911311_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status,Vuforia.VuforiaManager/TrackableIdPair>::EndInvoke(System.IAsyncResult)
extern "C"  TrackableIdPair_t1329355276  Transform_1_EndInvoke_m1773329000_gshared (Transform_1_t1188729987 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(TrackableIdPair_t1329355276 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m370613445_gshared (Enumerator_t2766235847 * __this, Dictionary_2_t1079703083 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1079703083 *)L_0);
		Enumerator_t2399727785  L_1 = ((  Enumerator_t2399727785  (*) (Dictionary_2_t1079703083 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t1079703083 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m370613445_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1079703083 * ___host0, const MethodInfo* method)
{
	Enumerator_t2766235847 * _thisAdjusted = reinterpret_cast<Enumerator_t2766235847 *>(__this + 1);
	Enumerator__ctor_m370613445(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4017233928_gshared (Enumerator_t2766235847 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		int32_t L_1 = Enumerator_get_CurrentValue_m2708916123((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4017233928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2766235847 * _thisAdjusted = reinterpret_cast<Enumerator_t2766235847 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m4017233928(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m9625560_gshared (Enumerator_t2766235847 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2787013825((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m9625560_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2766235847 * _thisAdjusted = reinterpret_cast<Enumerator_t2766235847 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m9625560(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m2229103293_gshared (Enumerator_t2766235847 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m1631869499((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2229103293_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2766235847 * _thisAdjusted = reinterpret_cast<Enumerator_t2766235847 *>(__this + 1);
	Enumerator_Dispose_m2229103293(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3657937156_gshared (Enumerator_t2766235847 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3809319098((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m3657937156_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2766235847 * _thisAdjusted = reinterpret_cast<Enumerator_t2766235847 *>(__this + 1);
	return Enumerator_MoveNext_m3657937156(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m25368454_gshared (Enumerator_t2766235847 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3132015601 * L_1 = (KeyValuePair_2_t3132015601 *)L_0->get_address_of_current_3();
		int32_t L_2 = KeyValuePair_2_get_Value_m1136571287((KeyValuePair_2_t3132015601 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int32_t Enumerator_get_Current_m25368454_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2766235847 * _thisAdjusted = reinterpret_cast<Enumerator_t2766235847 *>(__this + 1);
	return Enumerator_get_Current_m25368454(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2988407410_gshared (Enumerator_t3383807694 * __this, Dictionary_2_t1697274930 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1697274930 *)L_0);
		Enumerator_t3017299632  L_1 = ((  Enumerator_t3017299632  (*) (Dictionary_2_t1697274930 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t1697274930 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2988407410_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1697274930 * ___host0, const MethodInfo* method)
{
	Enumerator_t3383807694 * _thisAdjusted = reinterpret_cast<Enumerator_t3383807694 *>(__this + 1);
	Enumerator__ctor_m2988407410(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1648049763_gshared (Enumerator_t3383807694 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentValue_m3562053380((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1648049763_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3383807694 * _thisAdjusted = reinterpret_cast<Enumerator_t3383807694 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1648049763(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m655633499_gshared (Enumerator_t3383807694 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m761796566((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m655633499_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3383807694 * _thisAdjusted = reinterpret_cast<Enumerator_t3383807694 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m655633499(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2369319718_gshared (Enumerator_t3383807694 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2243145188((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2369319718_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3383807694 * _thisAdjusted = reinterpret_cast<Enumerator_t3383807694 *>(__this + 1);
	Enumerator_Dispose_m2369319718(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1091131935_gshared (Enumerator_t3383807694 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2770956757((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1091131935_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3383807694 * _thisAdjusted = reinterpret_cast<Enumerator_t3383807694 *>(__this + 1);
	return Enumerator_MoveNext_m1091131935(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3006348140_gshared (Enumerator_t3383807694 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3749587448 * L_1 = (KeyValuePair_2_t3749587448 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m2897691047((KeyValuePair_2_t3749587448 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m3006348140_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3383807694 * _thisAdjusted = reinterpret_cast<Enumerator_t3383807694 *>(__this + 1);
	return Enumerator_get_Current_m3006348140(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Reflection.Emit.Label>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2456172699_gshared (Enumerator_t642593763 * __this, Dictionary_2_t3251028295 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t3251028295 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3251028295 *)L_0);
		Enumerator_t276085701  L_1 = ((  Enumerator_t276085701  (*) (Dictionary_2_t3251028295 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t3251028295 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2456172699_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3251028295 * ___host0, const MethodInfo* method)
{
	Enumerator_t642593763 * _thisAdjusted = reinterpret_cast<Enumerator_t642593763 *>(__this + 1);
	Enumerator__ctor_m2456172699(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Reflection.Emit.Label>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2983671754_gshared (Enumerator_t642593763 * __this, const MethodInfo* method)
{
	{
		Enumerator_t276085701 * L_0 = (Enumerator_t276085701 *)__this->get_address_of_host_enumerator_0();
		Label_t4243202660  L_1 = Enumerator_get_CurrentValue_m3952904993((Enumerator_t276085701 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Label_t4243202660  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2983671754_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t642593763 * _thisAdjusted = reinterpret_cast<Enumerator_t642593763 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2983671754(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Reflection.Emit.Label>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m750766582_gshared (Enumerator_t642593763 * __this, const MethodInfo* method)
{
	{
		Enumerator_t276085701 * L_0 = (Enumerator_t276085701 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2015547391((Enumerator_t276085701 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m750766582_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t642593763 * _thisAdjusted = reinterpret_cast<Enumerator_t642593763 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m750766582(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Reflection.Emit.Label>::Dispose()
extern "C"  void Enumerator_Dispose_m484450631_gshared (Enumerator_t642593763 * __this, const MethodInfo* method)
{
	{
		Enumerator_t276085701 * L_0 = (Enumerator_t276085701 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2214493861((Enumerator_t276085701 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m484450631_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t642593763 * _thisAdjusted = reinterpret_cast<Enumerator_t642593763 *>(__this + 1);
	Enumerator_Dispose_m484450631(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Reflection.Emit.Label>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1410782430_gshared (Enumerator_t642593763 * __this, const MethodInfo* method)
{
	{
		Enumerator_t276085701 * L_0 = (Enumerator_t276085701 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3018149748((Enumerator_t276085701 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1410782430_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t642593763 * _thisAdjusted = reinterpret_cast<Enumerator_t642593763 *>(__this + 1);
	return Enumerator_MoveNext_m1410782430(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Reflection.Emit.Label>::get_Current()
extern "C"  Label_t4243202660  Enumerator_get_Current_m1765296160_gshared (Enumerator_t642593763 * __this, const MethodInfo* method)
{
	{
		Enumerator_t276085701 * L_0 = (Enumerator_t276085701 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1008373517 * L_1 = (KeyValuePair_2_t1008373517 *)L_0->get_address_of_current_3();
		Label_t4243202660  L_2 = KeyValuePair_2_get_Value_m590593229((KeyValuePair_2_t1008373517 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Label_t4243202660  Enumerator_get_Current_m1765296160_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t642593763 * _thisAdjusted = reinterpret_cast<Enumerator_t642593763 *>(__this + 1);
	return Enumerator_get_Current_m1765296160(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1603410773_gshared (Enumerator_t457302414 * __this, Dictionary_2_t3065736946 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t3065736946 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3065736946 *)L_0);
		Enumerator_t90794352  L_1 = ((  Enumerator_t90794352  (*) (Dictionary_2_t3065736946 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t3065736946 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1603410773_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3065736946 * ___host0, const MethodInfo* method)
{
	Enumerator_t457302414 * _thisAdjusted = reinterpret_cast<Enumerator_t457302414 *>(__this + 1);
	Enumerator__ctor_m1603410773(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3236647928_gshared (Enumerator_t457302414 * __this, const MethodInfo* method)
{
	{
		Enumerator_t90794352 * L_0 = (Enumerator_t90794352 *)__this->get_address_of_host_enumerator_0();
		int32_t L_1 = Enumerator_get_CurrentValue_m1949022123((Enumerator_t90794352 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3236647928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t457302414 * _thisAdjusted = reinterpret_cast<Enumerator_t457302414 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3236647928(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3086482046_gshared (Enumerator_t457302414 * __this, const MethodInfo* method)
{
	{
		Enumerator_t90794352 * L_0 = (Enumerator_t90794352 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3676619405((Enumerator_t90794352 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3086482046_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t457302414 * _thisAdjusted = reinterpret_cast<Enumerator_t457302414 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3086482046(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::Dispose()
extern "C"  void Enumerator_Dispose_m3575335869_gshared (Enumerator_t457302414 * __this, const MethodInfo* method)
{
	{
		Enumerator_t90794352 * L_0 = (Enumerator_t90794352 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m3765489531((Enumerator_t90794352 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3575335869_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t457302414 * _thisAdjusted = reinterpret_cast<Enumerator_t457302414 *>(__this + 1);
	Enumerator_Dispose_m3575335869(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1629414154_gshared (Enumerator_t457302414 * __this, const MethodInfo* method)
{
	{
		Enumerator_t90794352 * L_0 = (Enumerator_t90794352 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3012296512((Enumerator_t90794352 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1629414154_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t457302414 * _thisAdjusted = reinterpret_cast<Enumerator_t457302414 *>(__this + 1);
	return Enumerator_MoveNext_m1629414154(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.TrackableBehaviour/Status>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3681121540_gshared (Enumerator_t457302414 * __this, const MethodInfo* method)
{
	{
		Enumerator_t90794352 * L_0 = (Enumerator_t90794352 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t823082168 * L_1 = (KeyValuePair_2_t823082168 *)L_0->get_address_of_current_3();
		int32_t L_2 = KeyValuePair_2_get_Value_m2492290363((KeyValuePair_2_t823082168 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int32_t Enumerator_get_Current_m3681121540_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t457302414 * _thisAdjusted = reinterpret_cast<Enumerator_t457302414 *>(__this + 1);
	return Enumerator_get_Current_m3681121540(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2172712953_gshared (Enumerator_t1918244050 * __this, Dictionary_2_t231711286 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t231711286 * L_0 = ___host0;
		NullCheck((Dictionary_2_t231711286 *)L_0);
		Enumerator_t1551735988  L_1 = ((  Enumerator_t1551735988  (*) (Dictionary_2_t231711286 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t231711286 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2172712953_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t231711286 * ___host0, const MethodInfo* method)
{
	Enumerator_t1918244050 * _thisAdjusted = reinterpret_cast<Enumerator_t1918244050 *>(__this + 1);
	Enumerator__ctor_m2172712953(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m248398268_gshared (Enumerator_t1918244050 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1551735988 * L_0 = (Enumerator_t1551735988 *)__this->get_address_of_host_enumerator_0();
		VirtualButtonData_t1223885651  L_1 = Enumerator_get_CurrentValue_m2040133495((Enumerator_t1551735988 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		VirtualButtonData_t1223885651  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m248398268_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1918244050 * _thisAdjusted = reinterpret_cast<Enumerator_t1918244050 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m248398268(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2984435500_gshared (Enumerator_t1918244050 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1551735988 * L_0 = (Enumerator_t1551735988 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m497205((Enumerator_t1551735988 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2984435500_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1918244050 * _thisAdjusted = reinterpret_cast<Enumerator_t1918244050 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2984435500(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Dispose()
extern "C"  void Enumerator_Dispose_m2702780945_gshared (Enumerator_t1918244050 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1551735988 * L_0 = (Enumerator_t1551735988 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2687880623((Enumerator_t1551735988 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2702780945_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1918244050 * _thisAdjusted = reinterpret_cast<Enumerator_t1918244050 *>(__this + 1);
	Enumerator_Dispose_m2702780945(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1213553088_gshared (Enumerator_t1918244050 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1551735988 * L_0 = (Enumerator_t1551735988 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2819456918((Enumerator_t1551735988 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1213553088_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1918244050 * _thisAdjusted = reinterpret_cast<Enumerator_t1918244050 *>(__this + 1);
	return Enumerator_MoveNext_m1213553088(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Current()
extern "C"  VirtualButtonData_t1223885651  Enumerator_get_Current_m2919251530_gshared (Enumerator_t1918244050 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1551735988 * L_0 = (Enumerator_t1551735988 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t2284023804 * L_1 = (KeyValuePair_2_t2284023804 *)L_0->get_address_of_current_3();
		VirtualButtonData_t1223885651  L_2 = KeyValuePair_2_get_Value_m2492481859((KeyValuePair_2_t2284023804 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  VirtualButtonData_t1223885651  Enumerator_get_Current_m2919251530_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1918244050 * _thisAdjusted = reinterpret_cast<Enumerator_t1918244050 *>(__this + 1);
	return Enumerator_get_Current_m2919251530(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2270221822_gshared (Enumerator_t452961318 * __this, Dictionary_2_t3061395850 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t3061395850 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3061395850 *)L_0);
		Enumerator_t86453256  L_1 = ((  Enumerator_t86453256  (*) (Dictionary_2_t3061395850 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t3061395850 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2270221822_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3061395850 * ___host0, const MethodInfo* method)
{
	Enumerator_t452961318 * _thisAdjusted = reinterpret_cast<Enumerator_t452961318 *>(__this + 1);
	Enumerator__ctor_m2270221822(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3271520495_gshared (Enumerator_t452961318 * __this, const MethodInfo* method)
{
	{
		Enumerator_t86453256 * L_0 = (Enumerator_t86453256 *)__this->get_address_of_host_enumerator_0();
		int32_t L_1 = Enumerator_get_CurrentValue_m1182545736((Enumerator_t86453256 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3271520495_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t452961318 * _thisAdjusted = reinterpret_cast<Enumerator_t452961318 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3271520495(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3259539291_gshared (Enumerator_t452961318 * __this, const MethodInfo* method)
{
	{
		Enumerator_t86453256 * L_0 = (Enumerator_t86453256 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m199703458((Enumerator_t86453256 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3259539291_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t452961318 * _thisAdjusted = reinterpret_cast<Enumerator_t452961318 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3259539291(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m808899306_gshared (Enumerator_t452961318 * __this, const MethodInfo* method)
{
	{
		Enumerator_t86453256 * L_0 = (Enumerator_t86453256 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m3531528168((Enumerator_t86453256 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m808899306_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t452961318 * _thisAdjusted = reinterpret_cast<Enumerator_t452961318 *>(__this + 1);
	Enumerator_Dispose_m808899306(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1846263727_gshared (Enumerator_t452961318 * __this, const MethodInfo* method)
{
	{
		Enumerator_t86453256 * L_0 = (Enumerator_t86453256 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3282539621((Enumerator_t86453256 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1846263727_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t452961318 * _thisAdjusted = reinterpret_cast<Enumerator_t452961318 *>(__this + 1);
	return Enumerator_MoveNext_m1846263727(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3488365209_gshared (Enumerator_t452961318 * __this, const MethodInfo* method)
{
	{
		Enumerator_t86453256 * L_0 = (Enumerator_t86453256 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t818741072 * L_1 = (KeyValuePair_2_t818741072 *)L_0->get_address_of_current_3();
		int32_t L_2 = KeyValuePair_2_get_Value_m3901703242((KeyValuePair_2_t818741072 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int32_t Enumerator_get_Current_m3488365209_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t452961318 * _thisAdjusted = reinterpret_cast<Enumerator_t452961318 *>(__this + 1);
	return Enumerator_get_Current_m3488365209(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m419014865_gshared (Enumerator_t1070533165 * __this, Dictionary_2_t3678967697 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t3678967697 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3678967697 *)L_0);
		Enumerator_t704025103  L_1 = ((  Enumerator_t704025103  (*) (Dictionary_2_t3678967697 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t3678967697 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m419014865_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3678967697 * ___host0, const MethodInfo* method)
{
	Enumerator_t1070533165 * _thisAdjusted = reinterpret_cast<Enumerator_t1070533165 *>(__this + 1);
	Enumerator__ctor_m419014865(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2712263668_gshared (Enumerator_t1070533165 * __this, const MethodInfo* method)
{
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentValue_m659195263((Enumerator_t704025103 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2712263668_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1070533165 * _thisAdjusted = reinterpret_cast<Enumerator_t1070533165 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2712263668(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m149043970_gshared (Enumerator_t1070533165 * __this, const MethodInfo* method)
{
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m598725905((Enumerator_t704025103 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m149043970_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1070533165 * _thisAdjusted = reinterpret_cast<Enumerator_t1070533165 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m149043970(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m942415041_gshared (Enumerator_t1070533165 * __this, const MethodInfo* method)
{
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m3446908287((Enumerator_t704025103 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m942415041_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1070533165 * _thisAdjusted = reinterpret_cast<Enumerator_t1070533165 *>(__this + 1);
	Enumerator_Dispose_m942415041(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2045321910_gshared (Enumerator_t1070533165 * __this, const MethodInfo* method)
{
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3139459564((Enumerator_t704025103 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m2045321910_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1070533165 * _thisAdjusted = reinterpret_cast<Enumerator_t1070533165 *>(__this + 1);
	return Enumerator_MoveNext_m2045321910(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1952080304_gshared (Enumerator_t1070533165 * __this, const MethodInfo* method)
{
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1436312919 * L_1 = (KeyValuePair_2_t1436312919 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m1203523270((KeyValuePair_2_t1436312919 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m1952080304_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1070533165 * _thisAdjusted = reinterpret_cast<Enumerator_t1070533165 *>(__this + 1);
	return Enumerator_get_Current_m1952080304(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4094254752_gshared (Enumerator_t4200435530 * __this, Dictionary_2_t2513902766 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t2513902766 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2513902766 *)L_0);
		Enumerator_t3833927468  L_1 = ((  Enumerator_t3833927468  (*) (Dictionary_2_t2513902766 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t2513902766 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m4094254752_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2513902766 * ___host0, const MethodInfo* method)
{
	Enumerator_t4200435530 * _thisAdjusted = reinterpret_cast<Enumerator_t4200435530 *>(__this + 1);
	Enumerator__ctor_m4094254752(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1738297215_gshared (Enumerator_t4200435530 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3833927468 * L_0 = (Enumerator_t3833927468 *)__this->get_address_of_host_enumerator_0();
		int32_t L_1 = Enumerator_get_CurrentValue_m2248732114((Enumerator_t3833927468 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1738297215_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4200435530 * _thisAdjusted = reinterpret_cast<Enumerator_t4200435530 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1738297215(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1672746039_gshared (Enumerator_t4200435530 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3833927468 * L_0 = (Enumerator_t3833927468 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1791350284((Enumerator_t3833927468 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1672746039_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4200435530 * _thisAdjusted = reinterpret_cast<Enumerator_t4200435530 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1672746039(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m2630138556_gshared (Enumerator_t4200435530 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3833927468 * L_0 = (Enumerator_t3833927468 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m3738173754((Enumerator_t3833927468 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2630138556_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4200435530 * _thisAdjusted = reinterpret_cast<Enumerator_t4200435530 *>(__this + 1);
	Enumerator_Dispose_m2630138556(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1519402707_gshared (Enumerator_t4200435530 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3833927468 * L_0 = (Enumerator_t3833927468 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3274679113((Enumerator_t3833927468 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1519402707_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4200435530 * _thisAdjusted = reinterpret_cast<Enumerator_t4200435530 *>(__this + 1);
	return Enumerator_MoveNext_m1519402707(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m577317777_gshared (Enumerator_t4200435530 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3833927468 * L_0 = (Enumerator_t3833927468 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t271247988 * L_1 = (KeyValuePair_2_t271247988 *)L_0->get_address_of_current_3();
		int32_t L_2 = KeyValuePair_2_get_Value_m2072891806((KeyValuePair_2_t271247988 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int32_t Enumerator_get_Current_m577317777_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4200435530 * _thisAdjusted = reinterpret_cast<Enumerator_t4200435530 *>(__this + 1);
	return Enumerator_get_Current_m577317777(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1320388337_gshared (Enumerator_t523040081 * __this, Dictionary_2_t3131474613 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t3131474613 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3131474613 *)L_0);
		Enumerator_t156532019  L_1 = ((  Enumerator_t156532019  (*) (Dictionary_2_t3131474613 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t3131474613 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1320388337_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3131474613 * ___host0, const MethodInfo* method)
{
	Enumerator_t523040081 * _thisAdjusted = reinterpret_cast<Enumerator_t523040081 *>(__this + 1);
	Enumerator__ctor_m1320388337(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2160496972_gshared (Enumerator_t523040081 * __this, const MethodInfo* method)
{
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentValue_m3147984131((Enumerator_t156532019 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2160496972_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t523040081 * _thisAdjusted = reinterpret_cast<Enumerator_t523040081 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2160496972(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1146954164_gshared (Enumerator_t523040081 * __this, const MethodInfo* method)
{
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1147555085((Enumerator_t156532019 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1146954164_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t523040081 * _thisAdjusted = reinterpret_cast<Enumerator_t523040081 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1146954164(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1062622161_gshared (Enumerator_t523040081 * __this, const MethodInfo* method)
{
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2117437651((Enumerator_t156532019 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1062622161_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t523040081 * _thisAdjusted = reinterpret_cast<Enumerator_t523040081 *>(__this + 1);
	Enumerator_Dispose_m1062622161(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1404007384_gshared (Enumerator_t523040081 * __this, const MethodInfo* method)
{
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m266619810((Enumerator_t156532019 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1404007384_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t523040081 * _thisAdjusted = reinterpret_cast<Enumerator_t523040081 *>(__this + 1);
	return Enumerator_MoveNext_m1404007384(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m419581838_gshared (Enumerator_t523040081 * __this, const MethodInfo* method)
{
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t888819835 * L_1 = (KeyValuePair_2_t888819835 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m544293807((KeyValuePair_2_t888819835 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m419581838_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t523040081 * _thisAdjusted = reinterpret_cast<Enumerator_t523040081 *>(__this + 1);
	return Enumerator_get_Current_m419581838(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m908409898_gshared (Enumerator_t809200314 * __this, Dictionary_2_t3417634846 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t3417634846 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3417634846 *)L_0);
		Enumerator_t442692252  L_1 = ((  Enumerator_t442692252  (*) (Dictionary_2_t3417634846 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t3417634846 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m908409898_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3417634846 * ___host0, const MethodInfo* method)
{
	Enumerator_t809200314 * _thisAdjusted = reinterpret_cast<Enumerator_t809200314 *>(__this + 1);
	Enumerator__ctor_m908409898(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2625473469_gshared (Enumerator_t809200314 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_get_CurrentValue_m4143929484((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2625473469_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t809200314 * _thisAdjusted = reinterpret_cast<Enumerator_t809200314 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2625473469(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2909592833_gshared (Enumerator_t809200314 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3115320746((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2909592833_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t809200314 * _thisAdjusted = reinterpret_cast<Enumerator_t809200314 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2909592833(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C"  void Enumerator_Dispose_m1323464986_gshared (Enumerator_t809200314 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2711120408((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1323464986_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t809200314 * _thisAdjusted = reinterpret_cast<Enumerator_t809200314 *>(__this + 1);
	Enumerator_Dispose_m1323464986(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1212551889_gshared (Enumerator_t809200314 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1856697671((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1212551889_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t809200314 * _thisAdjusted = reinterpret_cast<Enumerator_t809200314 *>(__this + 1);
	return Enumerator_MoveNext_m1212551889(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C"  bool Enumerator_get_Current_m2986380627_gshared (Enumerator_t809200314 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1174980068 * L_1 = (KeyValuePair_2_t1174980068 *)L_0->get_address_of_current_3();
		bool L_2 = KeyValuePair_2_get_Value_m1916631176((KeyValuePair_2_t1174980068 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  bool Enumerator_get_Current_m2986380627_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t809200314 * _thisAdjusted = reinterpret_cast<Enumerator_t809200314 *>(__this + 1);
	return Enumerator_get_Current_m2986380627(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3539306986_gshared (Enumerator_t3350470340 * __this, Dictionary_2_t1663937576 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t1663937576 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1663937576 *)L_0);
		Enumerator_t2983962278  L_1 = ((  Enumerator_t2983962278  (*) (Dictionary_2_t1663937576 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t1663937576 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3539306986_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1663937576 * ___host0, const MethodInfo* method)
{
	Enumerator_t3350470340 * _thisAdjusted = reinterpret_cast<Enumerator_t3350470340 *>(__this + 1);
	Enumerator__ctor_m3539306986(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1805365227_gshared (Enumerator_t3350470340 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		int32_t L_1 = Enumerator_get_CurrentValue_m2645962456((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1805365227_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3350470340 * _thisAdjusted = reinterpret_cast<Enumerator_t3350470340 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1805365227(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3294415347_gshared (Enumerator_t3350470340 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1132695838((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3294415347_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3350470340 * _thisAdjusted = reinterpret_cast<Enumerator_t3350470340 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3294415347(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m2532362830_gshared (Enumerator_t3350470340 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m401572848((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2532362830_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3350470340 * _thisAdjusted = reinterpret_cast<Enumerator_t3350470340 *>(__this + 1);
	Enumerator_Dispose_m2532362830(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2534596951_gshared (Enumerator_t3350470340 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m435964161((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m2534596951_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3350470340 * _thisAdjusted = reinterpret_cast<Enumerator_t3350470340 *>(__this + 1);
	return Enumerator_MoveNext_m2534596951(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2838387513_gshared (Enumerator_t3350470340 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3716250094 * L_1 = (KeyValuePair_2_t3716250094 *)L_0->get_address_of_current_3();
		int32_t L_2 = KeyValuePair_2_get_Value_m3699669100((KeyValuePair_2_t3716250094 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int32_t Enumerator_get_Current_m2838387513_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3350470340 * _thisAdjusted = reinterpret_cast<Enumerator_t3350470340 *>(__this + 1);
	return Enumerator_get_Current_m2838387513(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1378921111_gshared (Enumerator_t1613536655 * __this, Dictionary_2_t4221971187 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t4221971187 * L_0 = ___host0;
		NullCheck((Dictionary_2_t4221971187 *)L_0);
		Enumerator_t1247028593  L_1 = ((  Enumerator_t1247028593  (*) (Dictionary_2_t4221971187 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t4221971187 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1378921111_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t4221971187 * ___host0, const MethodInfo* method)
{
	Enumerator_t1613536655 * _thisAdjusted = reinterpret_cast<Enumerator_t1613536655 *>(__this + 1);
	Enumerator__ctor_m1378921111(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1317997224_gshared (Enumerator_t1613536655 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1247028593 * L_0 = (Enumerator_t1247028593 *)__this->get_address_of_host_enumerator_0();
		Nullable_1_t334943763  L_1 = Enumerator_get_CurrentValue_m1012672069((Enumerator_t1247028593 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Nullable_1_t334943763  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1317997224_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1613536655 * _thisAdjusted = reinterpret_cast<Enumerator_t1613536655 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1317997224(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3216819908_gshared (Enumerator_t1613536655 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1247028593 * L_0 = (Enumerator_t1247028593 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2206907019((Enumerator_t1247028593 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3216819908_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1613536655 * _thisAdjusted = reinterpret_cast<Enumerator_t1613536655 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3216819908(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::Dispose()
extern "C"  void Enumerator_Dispose_m1453665971_gshared (Enumerator_t1613536655 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1247028593 * L_0 = (Enumerator_t1247028593 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2832749025((Enumerator_t1247028593 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1453665971_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1613536655 * _thisAdjusted = reinterpret_cast<Enumerator_t1613536655 *>(__this + 1);
	Enumerator_Dispose_m1453665971(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3793414252_gshared (Enumerator_t1613536655 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1247028593 * L_0 = (Enumerator_t1247028593 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m688389906((Enumerator_t1247028593 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m3793414252_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1613536655 * _thisAdjusted = reinterpret_cast<Enumerator_t1613536655 *>(__this + 1);
	return Enumerator_MoveNext_m3793414252(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::get_Current()
extern "C"  Nullable_1_t334943763  Enumerator_get_Current_m4077586382_gshared (Enumerator_t1613536655 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1247028593 * L_0 = (Enumerator_t1247028593 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1979316409 * L_1 = (KeyValuePair_2_t1979316409 *)L_0->get_address_of_current_3();
		Nullable_1_t334943763  L_2 = KeyValuePair_2_get_Value_m2361283873((KeyValuePair_2_t1979316409 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Nullable_1_t334943763  Enumerator_get_Current_m4077586382_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1613536655 * _thisAdjusted = reinterpret_cast<Enumerator_t1613536655 *>(__this + 1);
	return Enumerator_get_Current_m4077586382(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3819430617_gshared (Enumerator_t3968042187 * __this, Dictionary_2_t2281509423 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		Enumerator_t3601534125  L_1 = ((  Enumerator_t3601534125  (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3819430617_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2281509423 * ___host0, const MethodInfo* method)
{
	Enumerator_t3968042187 * _thisAdjusted = reinterpret_cast<Enumerator_t3968042187 *>(__this + 1);
	Enumerator__ctor_m3819430617(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3933483934_gshared (Enumerator_t3968042187 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentValue_m402763047((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3933483934_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3968042187 * _thisAdjusted = reinterpret_cast<Enumerator_t3968042187 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3933483934(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2482663638_gshared (Enumerator_t3968042187 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3129803197((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2482663638_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3968042187 * _thisAdjusted = reinterpret_cast<Enumerator_t3968042187 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2482663638(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m4238653081_gshared (Enumerator_t3968042187 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m1905011127((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m4238653081_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3968042187 * _thisAdjusted = reinterpret_cast<Enumerator_t3968042187 *>(__this + 1);
	Enumerator_Dispose_m4238653081(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m335649778_gshared (Enumerator_t3968042187 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3349738440((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m335649778_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3968042187 * _thisAdjusted = reinterpret_cast<Enumerator_t3968042187 *>(__this + 1);
	return Enumerator_MoveNext_m335649778(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1976109889_gshared (Enumerator_t3968042187 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t38854645 * L_1 = (KeyValuePair_2_t38854645 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m1251901674((KeyValuePair_2_t38854645 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m1976109889_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3968042187 * _thisAdjusted = reinterpret_cast<Enumerator_t3968042187 *>(__this + 1);
	return Enumerator_get_Current_m1976109889(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1132884941_gshared (Enumerator_t2265475503 * __this, Dictionary_2_t578942739 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t578942739 * L_0 = ___host0;
		NullCheck((Dictionary_2_t578942739 *)L_0);
		Enumerator_t1898967441  L_1 = ((  Enumerator_t1898967441  (*) (Dictionary_2_t578942739 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t578942739 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1132884941_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t578942739 * ___host0, const MethodInfo* method)
{
	Enumerator_t2265475503 * _thisAdjusted = reinterpret_cast<Enumerator_t2265475503 *>(__this + 1);
	Enumerator__ctor_m1132884941(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m581780706_gshared (Enumerator_t2265475503 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1898967441 * L_0 = (Enumerator_t1898967441 *)__this->get_address_of_host_enumerator_0();
		uint16_t L_1 = Enumerator_get_CurrentValue_m3339719667((Enumerator_t1898967441 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint16_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m581780706_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2265475503 * _thisAdjusted = reinterpret_cast<Enumerator_t2265475503 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m581780706(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt16>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m101480946_gshared (Enumerator_t2265475503 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1898967441 * L_0 = (Enumerator_t1898967441 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1141574313((Enumerator_t1898967441 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m101480946_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2265475503 * _thisAdjusted = reinterpret_cast<Enumerator_t2265475503 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m101480946(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt16>::Dispose()
extern "C"  void Enumerator_Dispose_m2244457285_gshared (Enumerator_t2265475503 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1898967441 * L_0 = (Enumerator_t1898967441 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2601453635((Enumerator_t1898967441 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2244457285_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2265475503 * _thisAdjusted = reinterpret_cast<Enumerator_t2265475503 *>(__this + 1);
	Enumerator_Dispose_m2244457285(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt16>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m696545430_gshared (Enumerator_t2265475503 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1898967441 * L_0 = (Enumerator_t1898967441 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m4229610700((Enumerator_t1898967441 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m696545430_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2265475503 * _thisAdjusted = reinterpret_cast<Enumerator_t2265475503 *>(__this + 1);
	return Enumerator_MoveNext_m696545430(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt16>::get_Current()
extern "C"  uint16_t Enumerator_get_Current_m1144777792_gshared (Enumerator_t2265475503 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1898967441 * L_0 = (Enumerator_t1898967441 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t2631255257 * L_1 = (KeyValuePair_2_t2631255257 *)L_0->get_address_of_current_3();
		uint16_t L_2 = KeyValuePair_2_get_Value_m3658972159((KeyValuePair_2_t2631255257 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  uint16_t Enumerator_get_Current_m1144777792_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2265475503 * _thisAdjusted = reinterpret_cast<Enumerator_t2265475503 *>(__this + 1);
	return Enumerator_get_Current_m1144777792(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.Vector3>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3161733182_gshared (Enumerator_t3522300472 * __this, Dictionary_2_t1835767708 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t1835767708 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1835767708 *)L_0);
		Enumerator_t3155792410  L_1 = ((  Enumerator_t3155792410  (*) (Dictionary_2_t1835767708 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t1835767708 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3161733182_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1835767708 * ___host0, const MethodInfo* method)
{
	Enumerator_t3522300472 * _thisAdjusted = reinterpret_cast<Enumerator_t3522300472 *>(__this + 1);
	Enumerator__ctor_m3161733182(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1396268095_gshared (Enumerator_t3522300472 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3155792410 * L_0 = (Enumerator_t3155792410 *)__this->get_address_of_host_enumerator_0();
		Vector3_t2243707580  L_1 = Enumerator_get_CurrentValue_m3593693876((Enumerator_t3155792410 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Vector3_t2243707580  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1396268095_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3522300472 * _thisAdjusted = reinterpret_cast<Enumerator_t3522300472 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1396268095(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.Vector3>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2451736067_gshared (Enumerator_t3522300472 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3155792410 * L_0 = (Enumerator_t3155792410 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3146199818((Enumerator_t3155792410 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2451736067_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3522300472 * _thisAdjusted = reinterpret_cast<Enumerator_t3522300472 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2451736067(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.Vector3>::Dispose()
extern "C"  void Enumerator_Dispose_m2813853538_gshared (Enumerator_t3522300472 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3155792410 * L_0 = (Enumerator_t3155792410 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m498224468((Enumerator_t3155792410 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2813853538_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3522300472 * _thisAdjusted = reinterpret_cast<Enumerator_t3522300472 *>(__this + 1);
	Enumerator_Dispose_m2813853538(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.Vector3>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2172016871_gshared (Enumerator_t3522300472 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3155792410 * L_0 = (Enumerator_t3155792410 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2125092321((Enumerator_t3155792410 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m2172016871_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3522300472 * _thisAdjusted = reinterpret_cast<Enumerator_t3522300472 *>(__this + 1);
	return Enumerator_MoveNext_m2172016871(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.Vector3>::get_Current()
extern "C"  Vector3_t2243707580  Enumerator_get_Current_m3456750973_gshared (Enumerator_t3522300472 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3155792410 * L_0 = (Enumerator_t3155792410 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3888080226 * L_1 = (KeyValuePair_2_t3888080226 *)L_0->get_address_of_current_3();
		Vector3_t2243707580  L_2 = KeyValuePair_2_get_Value_m3480897294((KeyValuePair_2_t3888080226 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Vector3_t2243707580  Enumerator_get_Current_m3456750973_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3522300472 * _thisAdjusted = reinterpret_cast<Enumerator_t3522300472 *>(__this + 1);
	return Enumerator_get_Current_m3456750973(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2002289345_gshared (Enumerator_t3003259380 * __this, Dictionary_2_t1316726616 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t1316726616 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1316726616 *)L_0);
		Enumerator_t2636751318  L_1 = ((  Enumerator_t2636751318  (*) (Dictionary_2_t1316726616 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t1316726616 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2002289345_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1316726616 * ___host0, const MethodInfo* method)
{
	Enumerator_t3003259380 * _thisAdjusted = reinterpret_cast<Enumerator_t3003259380 *>(__this + 1);
	Enumerator__ctor_m2002289345(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1152917532_gshared (Enumerator_t3003259380 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2636751318 * L_0 = (Enumerator_t2636751318 *)__this->get_address_of_host_enumerator_0();
		ProfileData_t1724666488  L_1 = Enumerator_get_CurrentValue_m2118029427((Enumerator_t2636751318 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		ProfileData_t1724666488  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1152917532_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3003259380 * _thisAdjusted = reinterpret_cast<Enumerator_t3003259380 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1152917532(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m155444740_gshared (Enumerator_t3003259380 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2636751318 * L_0 = (Enumerator_t2636751318 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m4069406173((Enumerator_t2636751318 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m155444740_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3003259380 * _thisAdjusted = reinterpret_cast<Enumerator_t3003259380 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m155444740(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::Dispose()
extern "C"  void Enumerator_Dispose_m4230661281_gshared (Enumerator_t3003259380 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2636751318 * L_0 = (Enumerator_t2636751318 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m346153571((Enumerator_t2636751318 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m4230661281_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3003259380 * _thisAdjusted = reinterpret_cast<Enumerator_t3003259380 *>(__this + 1);
	Enumerator_Dispose_m4230661281(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m536014408_gshared (Enumerator_t3003259380 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2636751318 * L_0 = (Enumerator_t2636751318 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2696697426((Enumerator_t2636751318 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m536014408_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3003259380 * _thisAdjusted = reinterpret_cast<Enumerator_t3003259380 *>(__this + 1);
	return Enumerator_MoveNext_m536014408(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Current()
extern "C"  ProfileData_t1724666488  Enumerator_get_Current_m2996020126_gshared (Enumerator_t3003259380 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2636751318 * L_0 = (Enumerator_t2636751318 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3369039134 * L_1 = (KeyValuePair_2_t3369039134 *)L_0->get_address_of_current_3();
		ProfileData_t1724666488  L_2 = KeyValuePair_2_get_Value_m699551839((KeyValuePair_2_t3369039134 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  ProfileData_t1724666488  Enumerator_get_Current_m2996020126_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3003259380 * _thisAdjusted = reinterpret_cast<Enumerator_t3003259380 *>(__this + 1);
	return Enumerator_get_Current_m2996020126(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m668593521_gshared (Enumerator_t1775268298 * __this, Dictionary_2_t88735534 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t88735534 * L_0 = ___host0;
		NullCheck((Dictionary_2_t88735534 *)L_0);
		Enumerator_t1408760236  L_1 = ((  Enumerator_t1408760236  (*) (Dictionary_2_t88735534 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t88735534 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m668593521_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t88735534 * ___host0, const MethodInfo* method)
{
	Enumerator_t1775268298 * _thisAdjusted = reinterpret_cast<Enumerator_t1775268298 *>(__this + 1);
	Enumerator__ctor_m668593521(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2127871028_gshared (Enumerator_t1775268298 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1408760236 * L_0 = (Enumerator_t1408760236 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentValue_m1347854879((Enumerator_t1408760236 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2127871028_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1775268298 * _thisAdjusted = reinterpret_cast<Enumerator_t1775268298 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2127871028(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m8906282_gshared (Enumerator_t1775268298 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1408760236 * L_0 = (Enumerator_t1408760236 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3214492505((Enumerator_t1408760236 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m8906282_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1775268298 * _thisAdjusted = reinterpret_cast<Enumerator_t1775268298 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m8906282(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2294050025_gshared (Enumerator_t1775268298 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1408760236 * L_0 = (Enumerator_t1408760236 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m3604910775((Enumerator_t1408760236 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2294050025_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1775268298 * _thisAdjusted = reinterpret_cast<Enumerator_t1775268298 *>(__this + 1);
	Enumerator_Dispose_m2294050025(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m139596990_gshared (Enumerator_t1775268298 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1408760236 * L_0 = (Enumerator_t1408760236 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1380568772((Enumerator_t1408760236 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m139596990_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1775268298 * _thisAdjusted = reinterpret_cast<Enumerator_t1775268298 *>(__this + 1);
	return Enumerator_MoveNext_m139596990(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3067148225_gshared (Enumerator_t1775268298 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1408760236 * L_0 = (Enumerator_t1408760236 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t2141048052 * L_1 = (KeyValuePair_2_t2141048052 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m4144029831((KeyValuePair_2_t2141048052 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m3067148225_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1775268298 * _thisAdjusted = reinterpret_cast<Enumerator_t1775268298 *>(__this + 1);
	return Enumerator_get_Current_m3067148225(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1541783957_gshared (Enumerator_t2057972683 * __this, Dictionary_2_t371439919 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t371439919 * L_0 = ___host0;
		NullCheck((Dictionary_2_t371439919 *)L_0);
		Enumerator_t1691464621  L_1 = ((  Enumerator_t1691464621  (*) (Dictionary_2_t371439919 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t371439919 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1541783957_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t371439919 * ___host0, const MethodInfo* method)
{
	Enumerator_t2057972683 * _thisAdjusted = reinterpret_cast<Enumerator_t2057972683 *>(__this + 1);
	Enumerator__ctor_m1541783957(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2421552874_gshared (Enumerator_t2057972683 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1691464621 * L_0 = (Enumerator_t1691464621 *)__this->get_address_of_host_enumerator_0();
		PoseAgeEntry_t3432166560  L_1 = Enumerator_get_CurrentValue_m491091915((Enumerator_t1691464621 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		PoseAgeEntry_t3432166560  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2421552874_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2057972683 * _thisAdjusted = reinterpret_cast<Enumerator_t2057972683 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2421552874(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2268110914_gshared (Enumerator_t2057972683 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1691464621 * L_0 = (Enumerator_t1691464621 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1703409689((Enumerator_t1691464621 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2268110914_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2057972683 * _thisAdjusted = reinterpret_cast<Enumerator_t2057972683 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2268110914(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::Dispose()
extern "C"  void Enumerator_Dispose_m3235936213_gshared (Enumerator_t2057972683 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1691464621 * L_0 = (Enumerator_t1691464621 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m591512691((Enumerator_t1691464621 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3235936213_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2057972683 * _thisAdjusted = reinterpret_cast<Enumerator_t2057972683 *>(__this + 1);
	Enumerator_Dispose_m3235936213(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2047441542_gshared (Enumerator_t2057972683 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1691464621 * L_0 = (Enumerator_t1691464621 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3813949020((Enumerator_t1691464621 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m2047441542_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2057972683 * _thisAdjusted = reinterpret_cast<Enumerator_t2057972683 *>(__this + 1);
	return Enumerator_MoveNext_m2047441542(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::get_Current()
extern "C"  PoseAgeEntry_t3432166560  Enumerator_get_Current_m3189532360_gshared (Enumerator_t2057972683 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1691464621 * L_0 = (Enumerator_t1691464621 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t2423752437 * L_1 = (KeyValuePair_2_t2423752437 *)L_0->get_address_of_current_3();
		PoseAgeEntry_t3432166560  L_2 = KeyValuePair_2_get_Value_m252031535((KeyValuePair_2_t2423752437 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  PoseAgeEntry_t3432166560  Enumerator_get_Current_m3189532360_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2057972683 * _thisAdjusted = reinterpret_cast<Enumerator_t2057972683 *>(__this + 1);
	return Enumerator_get_Current_m3189532360(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3682789228_gshared (Enumerator_t4082431430 * __this, Dictionary_2_t2395898666 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t2395898666 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2395898666 *)L_0);
		Enumerator_t3715923368  L_1 = ((  Enumerator_t3715923368  (*) (Dictionary_2_t2395898666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t2395898666 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3682789228_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2395898666 * ___host0, const MethodInfo* method)
{
	Enumerator_t4082431430 * _thisAdjusted = reinterpret_cast<Enumerator_t4082431430 *>(__this + 1);
	Enumerator__ctor_m3682789228(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m624745849_gshared (Enumerator_t4082431430 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3715923368 * L_0 = (Enumerator_t3715923368 *)__this->get_address_of_host_enumerator_0();
		PoseInfo_t1161658011  L_1 = Enumerator_get_CurrentValue_m95183650((Enumerator_t3715923368 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		PoseInfo_t1161658011  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m624745849_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4082431430 * _thisAdjusted = reinterpret_cast<Enumerator_t4082431430 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m624745849(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1681422929_gshared (Enumerator_t4082431430 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3715923368 * L_0 = (Enumerator_t3715923368 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m531859192((Enumerator_t3715923368 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1681422929_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4082431430 * _thisAdjusted = reinterpret_cast<Enumerator_t4082431430 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1681422929(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m1595156764_gshared (Enumerator_t4082431430 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3715923368 * L_0 = (Enumerator_t3715923368 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2977499502((Enumerator_t3715923368 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1595156764_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4082431430 * _thisAdjusted = reinterpret_cast<Enumerator_t4082431430 *>(__this + 1);
	Enumerator_Dispose_m1595156764(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m66940849_gshared (Enumerator_t4082431430 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3715923368 * L_0 = (Enumerator_t3715923368 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m4104902027((Enumerator_t3715923368 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m66940849_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4082431430 * _thisAdjusted = reinterpret_cast<Enumerator_t4082431430 *>(__this + 1);
	return Enumerator_MoveNext_m66940849(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::get_Current()
extern "C"  PoseInfo_t1161658011  Enumerator_get_Current_m2272525103_gshared (Enumerator_t4082431430 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3715923368 * L_0 = (Enumerator_t3715923368 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t153243888 * L_1 = (KeyValuePair_2_t153243888 *)L_0->get_address_of_current_3();
		PoseInfo_t1161658011  L_2 = KeyValuePair_2_get_Value_m1229996980((KeyValuePair_2_t153243888 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  PoseInfo_t1161658011  Enumerator_get_Current_m2272525103_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4082431430 * _thisAdjusted = reinterpret_cast<Enumerator_t4082431430 *>(__this + 1);
	return Enumerator_get_Current_m2272525103(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1971000786_gshared (Enumerator_t2683717434 * __this, Dictionary_2_t997184670 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t997184670 * L_0 = ___host0;
		NullCheck((Dictionary_2_t997184670 *)L_0);
		Enumerator_t2317209372  L_1 = ((  Enumerator_t2317209372  (*) (Dictionary_2_t997184670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t997184670 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1971000786_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t997184670 * ___host0, const MethodInfo* method)
{
	Enumerator_t2683717434 * _thisAdjusted = reinterpret_cast<Enumerator_t2683717434 *>(__this + 1);
	Enumerator__ctor_m1971000786(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m514775589_gshared (Enumerator_t2683717434 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2317209372 * L_0 = (Enumerator_t2317209372 *)__this->get_address_of_host_enumerator_0();
		int32_t L_1 = Enumerator_get_CurrentValue_m3861338020((Enumerator_t2317209372 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m514775589_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2683717434 * _thisAdjusted = reinterpret_cast<Enumerator_t2683717434 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m514775589(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3523229681_gshared (Enumerator_t2683717434 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2317209372 * L_0 = (Enumerator_t2317209372 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2100056858((Enumerator_t2317209372 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3523229681_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2683717434 * _thisAdjusted = reinterpret_cast<Enumerator_t2683717434 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3523229681(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::Dispose()
extern "C"  void Enumerator_Dispose_m2249060810_gshared (Enumerator_t2683717434 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2317209372 * L_0 = (Enumerator_t2317209372 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m1953131992((Enumerator_t2317209372 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2249060810_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2683717434 * _thisAdjusted = reinterpret_cast<Enumerator_t2683717434 *>(__this + 1);
	Enumerator_Dispose_m2249060810(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1436514993_gshared (Enumerator_t2683717434 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2317209372 * L_0 = (Enumerator_t2317209372 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1170226423((Enumerator_t2317209372 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1436514993_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2683717434 * _thisAdjusted = reinterpret_cast<Enumerator_t2683717434 *>(__this + 1);
	return Enumerator_MoveNext_m1436514993(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3856955819_gshared (Enumerator_t2683717434 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2317209372 * L_0 = (Enumerator_t2317209372 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3049497188 * L_1 = (KeyValuePair_2_t3049497188 *)L_0->get_address_of_current_3();
		int32_t L_2 = KeyValuePair_2_get_Value_m1579783160((KeyValuePair_2_t3049497188 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int32_t Enumerator_get_Current_m3856955819_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2683717434 * _thisAdjusted = reinterpret_cast<Enumerator_t2683717434 *>(__this + 1);
	return Enumerator_get_Current_m3856955819(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m1016212932_gshared (ValueCollection_t4077730222 * __this, Dictionary_2_t1079703083 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection__ctor_m1016212932_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1079703083 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t1079703083 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1909364658_gshared (ValueCollection_t4077730222 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1909364658_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4184581491_gshared (ValueCollection_t4077730222 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4184581491_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m246642050_gshared (ValueCollection_t4077730222 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		int32_t L_1 = ___item0;
		NullCheck((Dictionary_2_t1079703083 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t1079703083 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1079703083 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1476685027_gshared (ValueCollection_t4077730222 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1476685027_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m374665661_gshared (ValueCollection_t4077730222 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t4077730222 *)__this);
		Enumerator_t2766235847  L_0 = ((  Enumerator_t2766235847  (*) (ValueCollection_t4077730222 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ValueCollection_t4077730222 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t2766235847  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m2486821337_gshared (ValueCollection_t4077730222 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	Int32U5BU5D_t3030399641* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (Int32U5BU5D_t3030399641*)((Int32U5BU5D_t3030399641*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		Int32U5BU5D_t3030399641* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((ValueCollection_t4077730222 *)__this);
		((  void (*) (ValueCollection_t4077730222 *, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((ValueCollection_t4077730222 *)__this, (Int32U5BU5D_t3030399641*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t1079703083 * L_4 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t1079703083 *)L_4);
		((  void (*) (Dictionary_2_t1079703083 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1079703083 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1079703083 * L_7 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2737799006 * L_11 = (Transform_1_t2737799006 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2737799006 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1079703083 *)L_7);
		((  void (*) (Dictionary_2_t1079703083 *, Il2CppArray *, int32_t, Transform_1_t2737799006 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t1079703083 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t2737799006 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1020497378_gshared (ValueCollection_t4077730222 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t4077730222 *)__this);
		Enumerator_t2766235847  L_0 = ((  Enumerator_t2766235847  (*) (ValueCollection_t4077730222 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ValueCollection_t4077730222 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t2766235847  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2046100031_gshared (ValueCollection_t4077730222 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2689217077_gshared (ValueCollection_t4077730222 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m3444889833_gshared (ValueCollection_t4077730222 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_ICollection_get_SyncRoot_m3444889833_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m2054124627_gshared (ValueCollection_t4077730222 * __this, Int32U5BU5D_t3030399641* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		Int32U5BU5D_t3030399641* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t1079703083 *)L_0);
		((  void (*) (Dictionary_2_t1079703083 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1079703083 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1079703083 * L_3 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		Int32U5BU5D_t3030399641* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2737799006 * L_7 = (Transform_1_t2737799006 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2737799006 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1079703083 *)L_3);
		((  void (*) (Dictionary_2_t1079703083 *, Int32U5BU5D_t3030399641*, int32_t, Transform_1_t2737799006 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t1079703083 *)L_3, (Int32U5BU5D_t3030399641*)L_4, (int32_t)L_5, (Transform_1_t2737799006 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t2766235847  ValueCollection_GetEnumerator_m494496494_gshared (ValueCollection_t4077730222 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		Enumerator_t2766235847  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m370613445(&L_1, (Dictionary_2_t1079703083 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3077228621_gshared (ValueCollection_t4077730222 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t1079703083 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t1079703083 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1079703083 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m882866357_gshared (ValueCollection_t400334773 * __this, Dictionary_2_t1697274930 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection__ctor_m882866357_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1697274930 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t1697274930 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1903672223_gshared (ValueCollection_t400334773 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1903672223_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3271993638_gshared (ValueCollection_t400334773 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3271993638_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3958350925_gshared (ValueCollection_t400334773 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t1697274930 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t1697274930 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1697274930 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m98888100_gshared (ValueCollection_t400334773 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m98888100_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1604400448_gshared (ValueCollection_t400334773 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t400334773 *)__this);
		Enumerator_t3383807694  L_0 = ((  Enumerator_t3383807694  (*) (ValueCollection_t400334773 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ValueCollection_t400334773 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t3383807694  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m2627730402_gshared (ValueCollection_t400334773 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((ValueCollection_t400334773 *)__this);
		((  void (*) (ValueCollection_t400334773 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((ValueCollection_t400334773 *)__this, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t1697274930 * L_4 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t1697274930 *)L_4);
		((  void (*) (Dictionary_2_t1697274930 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1697274930 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1697274930 * L_7 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t3939605346 * L_11 = (Transform_1_t3939605346 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t3939605346 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1697274930 *)L_7);
		((  void (*) (Dictionary_2_t1697274930 *, Il2CppArray *, int32_t, Transform_1_t3939605346 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t1697274930 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t3939605346 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1073215119_gshared (ValueCollection_t400334773 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t400334773 *)__this);
		Enumerator_t3383807694  L_0 = ((  Enumerator_t3383807694  (*) (ValueCollection_t400334773 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ValueCollection_t400334773 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t3383807694  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1325719984_gshared (ValueCollection_t400334773 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4041633470_gshared (ValueCollection_t400334773 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m3927965720_gshared (ValueCollection_t400334773 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_ICollection_get_SyncRoot_m3927965720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m1460341186_gshared (ValueCollection_t400334773 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t1697274930 *)L_0);
		((  void (*) (Dictionary_2_t1697274930 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1697274930 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1697274930 * L_3 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t3939605346 * L_7 = (Transform_1_t3939605346 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t3939605346 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1697274930 *)L_3);
		((  void (*) (Dictionary_2_t1697274930 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t3939605346 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t1697274930 *)L_3, (ObjectU5BU5D_t3614634134*)L_4, (int32_t)L_5, (Transform_1_t3939605346 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3383807694  ValueCollection_GetEnumerator_m520082450_gshared (ValueCollection_t400334773 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		Enumerator_t3383807694  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m2988407410(&L_1, (Dictionary_2_t1697274930 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m90930038_gshared (ValueCollection_t400334773 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t1697274930 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t1697274930 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1697274930 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Reflection.Emit.Label>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m3461572578_gshared (ValueCollection_t1954088138 * __this, Dictionary_2_t3251028295 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection__ctor_m3461572578_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3251028295 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t3251028295 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Reflection.Emit.Label>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1237476428_gshared (ValueCollection_t1954088138 * __this, Label_t4243202660  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1237476428_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Reflection.Emit.Label>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1332473421_gshared (ValueCollection_t1954088138 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1332473421_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Reflection.Emit.Label>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m226864652_gshared (ValueCollection_t1954088138 * __this, Label_t4243202660  ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t3251028295 * L_0 = (Dictionary_2_t3251028295 *)__this->get_dictionary_0();
		Label_t4243202660  L_1 = ___item0;
		NullCheck((Dictionary_2_t3251028295 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3251028295 *, Label_t4243202660 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3251028295 *)L_0, (Label_t4243202660 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Reflection.Emit.Label>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1826798401_gshared (ValueCollection_t1954088138 * __this, Label_t4243202660  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1826798401_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Reflection.Emit.Label>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3445681567_gshared (ValueCollection_t1954088138 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t1954088138 *)__this);
		Enumerator_t642593763  L_0 = ((  Enumerator_t642593763  (*) (ValueCollection_t1954088138 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ValueCollection_t1954088138 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t642593763  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Reflection.Emit.Label>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m4268955891_gshared (ValueCollection_t1954088138 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	LabelU5BU5D_t196522893* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (LabelU5BU5D_t196522893*)((LabelU5BU5D_t196522893*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		LabelU5BU5D_t196522893* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		LabelU5BU5D_t196522893* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((ValueCollection_t1954088138 *)__this);
		((  void (*) (ValueCollection_t1954088138 *, LabelU5BU5D_t196522893*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((ValueCollection_t1954088138 *)__this, (LabelU5BU5D_t196522893*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t3251028295 * L_4 = (Dictionary_2_t3251028295 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t3251028295 *)L_4);
		((  void (*) (Dictionary_2_t3251028295 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3251028295 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3251028295 * L_7 = (Dictionary_2_t3251028295 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2075247470 * L_11 = (Transform_1_t2075247470 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2075247470 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3251028295 *)L_7);
		((  void (*) (Dictionary_2_t3251028295 *, Il2CppArray *, int32_t, Transform_1_t2075247470 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t3251028295 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t2075247470 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Reflection.Emit.Label>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4006962744_gshared (ValueCollection_t1954088138 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t1954088138 *)__this);
		Enumerator_t642593763  L_0 = ((  Enumerator_t642593763  (*) (ValueCollection_t1954088138 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ValueCollection_t1954088138 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t642593763  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Reflection.Emit.Label>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m4192860789_gshared (ValueCollection_t1954088138 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Reflection.Emit.Label>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2877069675_gshared (ValueCollection_t1954088138 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Reflection.Emit.Label>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m22966243_gshared (ValueCollection_t1954088138 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_ICollection_get_SyncRoot_m22966243_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3251028295 * L_0 = (Dictionary_2_t3251028295 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Reflection.Emit.Label>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m3898196505_gshared (ValueCollection_t1954088138 * __this, LabelU5BU5D_t196522893* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t3251028295 * L_0 = (Dictionary_2_t3251028295 *)__this->get_dictionary_0();
		LabelU5BU5D_t196522893* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t3251028295 *)L_0);
		((  void (*) (Dictionary_2_t3251028295 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3251028295 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3251028295 * L_3 = (Dictionary_2_t3251028295 *)__this->get_dictionary_0();
		LabelU5BU5D_t196522893* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2075247470 * L_7 = (Transform_1_t2075247470 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2075247470 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3251028295 *)L_3);
		((  void (*) (Dictionary_2_t3251028295 *, LabelU5BU5D_t196522893*, int32_t, Transform_1_t2075247470 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t3251028295 *)L_3, (LabelU5BU5D_t196522893*)L_4, (int32_t)L_5, (Transform_1_t2075247470 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Reflection.Emit.Label>::GetEnumerator()
extern "C"  Enumerator_t642593763  ValueCollection_GetEnumerator_m2618682996_gshared (ValueCollection_t1954088138 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3251028295 * L_0 = (Dictionary_2_t3251028295 *)__this->get_dictionary_0();
		Enumerator_t642593763  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m2456172699(&L_1, (Dictionary_2_t3251028295 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Reflection.Emit.Label>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1251033675_gshared (ValueCollection_t1954088138 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3251028295 * L_0 = (Dictionary_2_t3251028295 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t3251028295 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t3251028295 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t3251028295 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour/Status>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m3664366612_gshared (ValueCollection_t1768796789 * __this, Dictionary_2_t3065736946 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection__ctor_m3664366612_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3065736946 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t3065736946 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour/Status>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2358931206_gshared (ValueCollection_t1768796789 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2358931206_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour/Status>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m526884159_gshared (ValueCollection_t1768796789 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m526884159_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour/Status>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m890560962_gshared (ValueCollection_t1768796789 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t3065736946 * L_0 = (Dictionary_2_t3065736946 *)__this->get_dictionary_0();
		int32_t L_1 = ___item0;
		NullCheck((Dictionary_2_t3065736946 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3065736946 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3065736946 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour/Status>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m825862607_gshared (ValueCollection_t1768796789 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m825862607_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour/Status>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2074712609_gshared (ValueCollection_t1768796789 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t1768796789 *)__this);
		Enumerator_t457302414  L_0 = ((  Enumerator_t457302414  (*) (ValueCollection_t1768796789 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ValueCollection_t1768796789 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t457302414  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour/Status>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m447602081_gshared (ValueCollection_t1768796789 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	StatusU5BU5D_t3152771798* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (StatusU5BU5D_t3152771798*)((StatusU5BU5D_t3152771798*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		StatusU5BU5D_t3152771798* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		StatusU5BU5D_t3152771798* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((ValueCollection_t1768796789 *)__this);
		((  void (*) (ValueCollection_t1768796789 *, StatusU5BU5D_t3152771798*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((ValueCollection_t1768796789 *)__this, (StatusU5BU5D_t3152771798*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t3065736946 * L_4 = (Dictionary_2_t3065736946 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t3065736946 *)L_4);
		((  void (*) (Dictionary_2_t3065736946 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3065736946 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3065736946 * L_7 = (Dictionary_2_t3065736946 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t551237730 * L_11 = (Transform_1_t551237730 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t551237730 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3065736946 *)L_7);
		((  void (*) (Dictionary_2_t3065736946 *, Il2CppArray *, int32_t, Transform_1_t551237730 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t3065736946 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t551237730 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour/Status>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m309128498_gshared (ValueCollection_t1768796789 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t1768796789 *)__this);
		Enumerator_t457302414  L_0 = ((  Enumerator_t457302414  (*) (ValueCollection_t1768796789 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ValueCollection_t1768796789 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t457302414  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour/Status>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3938561147_gshared (ValueCollection_t1768796789 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour/Status>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m68377053_gshared (ValueCollection_t1768796789 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour/Status>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m3948248397_gshared (ValueCollection_t1768796789 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValueCollection_System_Collections_ICollection_get_SyncRoot_m3948248397_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3065736946 * L_0 = (Dictionary_2_t3065736946 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour/Status>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m3757312127_gshared (ValueCollection_t1768796789 * __this, StatusU5BU5D_t3152771798* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t3065736946 * L_0 = (Dictionary_2_t3065736946 *)__this->get_dictionary_0();
		StatusU5BU5D_t3152771798* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t3065736946 *)L_0);
		((  void (*) (Dictionary_2_t3065736946 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3065736946 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3065736946 * L_3 = (Dictionary_2_t3065736946 *)__this->get_dictionary_0();
		StatusU5BU5D_t3152771798* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t551237730 * L_7 = (Transform_1_t551237730 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t551237730 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3065736946 *)L_3);
		((  void (*) (Dictionary_2_t3065736946 *, StatusU5BU5D_t3152771798*, int32_t, Transform_1_t551237730 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t3065736946 *)L_3, (StatusU5BU5D_t3152771798*)L_4, (int32_t)L_5, (Transform_1_t551237730 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour/Status>::GetEnumerator()
extern "C"  Enumerator_t457302414  ValueCollection_GetEnumerator_m294727156_gshared (ValueCollection_t1768796789 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3065736946 * L_0 = (Dictionary_2_t3065736946 *)__this->get_dictionary_0();
		Enumerator_t457302414  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m1603410773(&L_1, (Dictionary_2_t3065736946 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.TrackableBehaviour/Status>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3246808833_gshared (ValueCollection_t1768796789 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3065736946 * L_0 = (Dictionary_2_t3065736946 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t3065736946 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t3065736946 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t3065736946 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
