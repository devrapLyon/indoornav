﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Firebase_Database_Firebase_Database_Query2792659010.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Database.DatabaseReference
struct  DatabaseReference_t1167676104  : public Query_t2792659010
{
public:

public:
};

struct DatabaseReference_t1167676104_StaticFields
{
public:
	// System.Object Firebase.Database.DatabaseReference::SSync
	Il2CppObject * ___SSync_4;

public:
	inline static int32_t get_offset_of_SSync_4() { return static_cast<int32_t>(offsetof(DatabaseReference_t1167676104_StaticFields, ___SSync_4)); }
	inline Il2CppObject * get_SSync_4() const { return ___SSync_4; }
	inline Il2CppObject ** get_address_of_SSync_4() { return &___SSync_4; }
	inline void set_SSync_4(Il2CppObject * value)
	{
		___SSync_4 = value;
		Il2CppCodeGenWriteBarrier(&___SSync_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
