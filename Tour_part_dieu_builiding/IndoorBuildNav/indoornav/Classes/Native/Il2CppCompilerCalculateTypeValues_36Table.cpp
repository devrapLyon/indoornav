﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamARController2804466264.h"
#include "Vuforia_UnityExtensions_Vuforia_WordManager1585193471.h"
#include "Vuforia_UnityExtensions_Vuforia_WordResult1915507197.h"
#include "Vuforia_UnityExtensions_Vuforia_WordTemplateMode1097144495.h"
#include "Vuforia_UnityExtensions_Vuforia_WordAbstractBehavi2878458725.h"
#include "Vuforia_UnityExtensions_Vuforia_WordFilterMode695600879.h"
#include "Vuforia_UnityExtensions_Vuforia_WordList1278495262.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearCalibration2396922556.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearUserCalibrat626398268.h"
#include "Vuforia_UnityExtensions_U3CPrivateImplementationDe1486305137.h"
#include "Vuforia_UnityExtensions_U3CPrivateImplementationDet978476007.h"
#include "Firebase_Database_U3CModuleU3E3783534214.h"
#include "Firebase_Database_Firebase_Database_DatabaseError1067746743.h"
#include "Firebase_Database_Firebase_Database_DatabaseExcept1509032028.h"
#include "Firebase_Database_Firebase_Database_DatabaseRefere1167676104.h"
#include "Firebase_Database_Firebase_Database_DataSnapshot1287895350.h"
#include "Firebase_Database_Firebase_Database_DataSnapshot_E3246115362.h"
#include "Firebase_Database_Firebase_Database_DataSnapshot_E1612832105.h"
#include "Firebase_Database_Firebase_Database_InternalHelpers118531028.h"
#include "Firebase_Database_Firebase_Database_MutableData1171022152.h"
#include "Firebase_Database_Firebase_Database_Query2792659010.h"
#include "Firebase_Database_Firebase_Database_Query_Runnable1164582213.h"
#include "Firebase_Database_Firebase_Database_Query_Runnable2730666154.h"
#include "Firebase_Database_Firebase_Database_Query_U3CGetVa2125080077.h"
#include "Firebase_Database_Firebase_Database_Query_U3CAddLi3586663574.h"
#include "Firebase_Database_Firebase_Database_TransactionRes3107513211.h"
#include "Firebase_Database_Firebase_Database_ValueChangedEve929877234.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne1429582413.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne2339493528.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne1942506794.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne1747391121.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne3406713242.h"
#include "Firebase_Database_Firebase_Database_Internal_Connec671917741.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne1016574279.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne2292118836.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne1904999661.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne1099507887.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne2050960365.h"
#include "Firebase_Database_Firebase_Database_Internal_Connect67058648.h"
#include "Firebase_Database_Firebase_Database_Internal_Connec774816424.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne3017763353.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne3450828338.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne3816656261.h"
#include "Firebase_Database_Firebase_Database_Internal_Connec164694114.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne2407603808.h"
#include "Firebase_Database_Firebase_Database_Internal_Connec485289530.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne1244804617.h"
#include "Firebase_Database_Firebase_Database_Internal_Connec485289728.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne3966625496.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne2533788837.h"
#include "Firebase_Database_Firebase_Database_Internal_Connect98025219.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne1372375221.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne1111414532.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne1917770995.h"
#include "Firebase_Database_Firebase_Database_Internal_Connec754971594.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne3483854948.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne3887139478.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne4110307229.h"
#include "Firebase_Database_Firebase_Database_Internal_Connec265678593.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne3179440036.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne2341283196.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne2038207663.h"
#include "Firebase_Database_Firebase_Database_Internal_Connec470580281.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne1703758004.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_C496419158.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1326285479.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2891699520.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3600 = { sizeof (WebCamARController_t2804466264), -1, sizeof(WebCamARController_t2804466264_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3600[6] = 
{
	WebCamARController_t2804466264::get_offset_of_RenderTextureLayer_1(),
	WebCamARController_t2804466264::get_offset_of_mDeviceNameSetInEditor_2(),
	WebCamARController_t2804466264::get_offset_of_mFlipHorizontally_3(),
	WebCamARController_t2804466264::get_offset_of_mWebCamImpl_4(),
	WebCamARController_t2804466264_StaticFields::get_offset_of_mInstance_5(),
	WebCamARController_t2804466264_StaticFields::get_offset_of_mPadlock_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3601 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3602 = { sizeof (WordManager_t1585193471), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3603 = { sizeof (WordResult_t1915507197), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3604 = { sizeof (WordTemplateMode_t1097144495)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3604[3] = 
{
	WordTemplateMode_t1097144495::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3605 = { sizeof (WordAbstractBehaviour_t2878458725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3605[3] = 
{
	WordAbstractBehaviour_t2878458725::get_offset_of_mMode_10(),
	WordAbstractBehaviour_t2878458725::get_offset_of_mSpecificWord_11(),
	WordAbstractBehaviour_t2878458725::get_offset_of_mWord_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3606 = { sizeof (WordFilterMode_t695600879)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3606[4] = 
{
	WordFilterMode_t695600879::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3607 = { sizeof (WordList_t1278495262), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3608 = { sizeof (EyewearCalibrationProfileManager_t2396922556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3609 = { sizeof (EyewearUserCalibrator_t626398268), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3610 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305143), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3610[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3611 = { sizeof (__StaticArrayInitTypeSizeU3D24_t978476007)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D24_t978476007 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3612 = { sizeof (U3CModuleU3E_t3783534229), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3613 = { sizeof (DatabaseError_t1067746743), -1, sizeof(DatabaseError_t1067746743_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3613[5] = 
{
	DatabaseError_t1067746743_StaticFields::get_offset_of_ErrorReasons_0(),
	DatabaseError_t1067746743_StaticFields::get_offset_of_ErrorCodes_1(),
	DatabaseError_t1067746743::get_offset_of_U3CCodeU3Ek__BackingField_2(),
	DatabaseError_t1067746743::get_offset_of_U3CMessageU3Ek__BackingField_3(),
	DatabaseError_t1067746743::get_offset_of_U3CDetailsU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3614 = { sizeof (DatabaseException_t1509032028), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3615 = { sizeof (DatabaseReference_t1167676104), -1, sizeof(DatabaseReference_t1167676104_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3615[1] = 
{
	DatabaseReference_t1167676104_StaticFields::get_offset_of_SSync_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3616 = { sizeof (DataSnapshot_t1287895350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3616[2] = 
{
	DataSnapshot_t1287895350::get_offset_of__node_0(),
	DataSnapshot_t1287895350::get_offset_of_U3CReferenceU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3617 = { sizeof (Enumerable238_t3246115362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3617[2] = 
{
	Enumerable238_t3246115362::get_offset_of__enclosing_0(),
	Enumerable238_t3246115362::get_offset_of__iter_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3618 = { sizeof (Enumerator242_t1612832105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3618[2] = 
{
	Enumerator242_t1612832105::get_offset_of__enclosing_0(),
	Enumerator242_t1612832105::get_offset_of__iter_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3619 = { sizeof (InternalHelpers_t118531028), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3620 = { sizeof (MutableData_t1171022152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3620[2] = 
{
	MutableData_t1171022152::get_offset_of__holder_0(),
	MutableData_t1171022152::get_offset_of__prefixPath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3621 = { sizeof (Query_t2792659010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3621[4] = 
{
	Query_t2792659010::get_offset_of__orderByCalled_0(),
	Query_t2792659010::get_offset_of_Params_1(),
	Query_t2792659010::get_offset_of_Path_2(),
	Query_t2792659010::get_offset_of_Repo_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3622 = { sizeof (Runnable182_t1164582213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3622[2] = 
{
	Runnable182_t1164582213::get_offset_of__enclosing_0(),
	Runnable182_t1164582213::get_offset_of__registration_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3623 = { sizeof (Runnable192_t2730666154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3623[2] = 
{
	Runnable192_t2730666154::get_offset_of__enclosing_0(),
	Runnable192_t2730666154::get_offset_of__listener_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3624 = { sizeof (U3CGetValueAsyncU3Ec__AnonStorey0_t2125080077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3624[1] = 
{
	U3CGetValueAsyncU3Ec__AnonStorey0_t2125080077::get_offset_of_completionSource_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3625 = { sizeof (U3CAddListenerForSingleValueEventU3Ec__AnonStorey1_t3586663574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3625[3] = 
{
	U3CAddListenerForSingleValueEventU3Ec__AnonStorey1_t3586663574::get_offset_of_wrapper_0(),
	U3CAddListenerForSingleValueEventU3Ec__AnonStorey1_t3586663574::get_offset_of_handler_1(),
	U3CAddListenerForSingleValueEventU3Ec__AnonStorey1_t3586663574::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3626 = { sizeof (TransactionResult_t3107513211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3626[2] = 
{
	TransactionResult_t3107513211::get_offset_of__data_0(),
	TransactionResult_t3107513211::get_offset_of_U3CIsSuccessU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3627 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3628 = { sizeof (ValueChangedEventArgs_t929877234), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3628[2] = 
{
	ValueChangedEventArgs_t929877234::get_offset_of_U3CSnapshotU3Ek__BackingField_1(),
	ValueChangedEventArgs_t929877234::get_offset_of_U3CDatabaseErrorU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3629 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3629[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3630 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3630[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3631 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3632 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3632[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3633 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3634 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3635 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3635[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3636 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3636[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3637 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3637[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3638 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3639 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3639[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3640 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3641 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3641[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3642 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3643 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3644 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3645 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3645[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3646 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3646[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3647 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3647[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3648 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3648[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3649 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3649[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3650 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3650[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3651 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3651[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3652 = { sizeof (CompoundHash_t1429582413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3652[2] = 
{
	CompoundHash_t1429582413::get_offset_of__hashes_0(),
	CompoundHash_t1429582413::get_offset_of__posts_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3653 = { sizeof (Connection_t2339493528), -1, sizeof(Connection_t2339493528_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3653[6] = 
{
	Connection_t2339493528_StaticFields::get_offset_of__connectionIds_0(),
	Connection_t2339493528::get_offset_of__delegate_1(),
	Connection_t2339493528::get_offset_of__hostInfo_2(),
	Connection_t2339493528::get_offset_of__logger_3(),
	Connection_t2339493528::get_offset_of__conn_4(),
	Connection_t2339493528::get_offset_of__state_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3654 = { sizeof (DisconnectReason_t1942506794)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3654[3] = 
{
	DisconnectReason_t1942506794::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3655 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3656 = { sizeof (State_t1747391121)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3656[4] = 
{
	State_t1747391121::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3657 = { sizeof (ConnectionAuthTokenProvider_t3406713242), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3658 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3659 = { sizeof (ConnectionContext_t671917741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3659[6] = 
{
	ConnectionContext_t671917741::get_offset_of__authTokenProvider_0(),
	ConnectionContext_t671917741::get_offset_of__clientSdkVersion_1(),
	ConnectionContext_t671917741::get_offset_of__executorService_2(),
	ConnectionContext_t671917741::get_offset_of__logger_3(),
	ConnectionContext_t671917741::get_offset_of__persistenceEnabled_4(),
	ConnectionContext_t671917741::get_offset_of__userAgent_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3660 = { sizeof (ConnectionUtils_t1016574279), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3661 = { sizeof (HostInfo_t2292118836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3661[3] = 
{
	HostInfo_t2292118836::get_offset_of__host_0(),
	HostInfo_t2292118836::get_offset_of__namespace_1(),
	HostInfo_t2292118836::get_offset_of__secure_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3662 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3663 = { sizeof (PersistentConnection_t1904999661), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3664 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3665 = { sizeof (PersistentConnectionImpl_t1099507887), -1, sizeof(PersistentConnectionImpl_t1099507887_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3665[28] = 
{
	PersistentConnectionImpl_t1099507887_StaticFields::get_offset_of__connectionIds_0(),
	PersistentConnectionImpl_t1099507887::get_offset_of__authTokenProvider_1(),
	PersistentConnectionImpl_t1099507887::get_offset_of__context_2(),
	PersistentConnectionImpl_t1099507887::get_offset_of__delegate_3(),
	PersistentConnectionImpl_t1099507887::get_offset_of__executorService_4(),
	PersistentConnectionImpl_t1099507887::get_offset_of__hostInfo_5(),
	PersistentConnectionImpl_t1099507887::get_offset_of__interruptReasons_6(),
	PersistentConnectionImpl_t1099507887::get_offset_of__listens_7(),
	PersistentConnectionImpl_t1099507887::get_offset_of__logger_8(),
	PersistentConnectionImpl_t1099507887::get_offset_of__onDisconnectRequestQueue_9(),
	PersistentConnectionImpl_t1099507887::get_offset_of__outstandingPuts_10(),
	PersistentConnectionImpl_t1099507887::get_offset_of__requestCbHash_11(),
	PersistentConnectionImpl_t1099507887::get_offset_of__retryHelper_12(),
	PersistentConnectionImpl_t1099507887::get_offset_of__authToken_13(),
	PersistentConnectionImpl_t1099507887::get_offset_of__cachedHost_14(),
	PersistentConnectionImpl_t1099507887::get_offset_of__connectionState_15(),
	PersistentConnectionImpl_t1099507887::get_offset_of__currentGetTokenAttempt_16(),
	PersistentConnectionImpl_t1099507887::get_offset_of__firstConnection_17(),
	PersistentConnectionImpl_t1099507887::get_offset_of__forceAuthTokenRefresh_18(),
	PersistentConnectionImpl_t1099507887::get_offset_of__hasOnDisconnects_19(),
	PersistentConnectionImpl_t1099507887::get_offset_of__inactivityTimer_20(),
	PersistentConnectionImpl_t1099507887::get_offset_of__invalidAuthTokenCount_21(),
	PersistentConnectionImpl_t1099507887::get_offset_of__lastConnectionEstablishedTime_22(),
	PersistentConnectionImpl_t1099507887::get_offset_of__lastSessionId_23(),
	PersistentConnectionImpl_t1099507887::get_offset_of__lastWriteTimestamp_24(),
	PersistentConnectionImpl_t1099507887::get_offset_of__realtime_25(),
	PersistentConnectionImpl_t1099507887::get_offset_of__requestCounter_26(),
	PersistentConnectionImpl_t1099507887::get_offset_of__writeCounter_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3666 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3667 = { sizeof (ListenQuerySpec_t2050960365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3667[3] = 
{
	ListenQuerySpec_t2050960365::get_offset_of_Path_0(),
	ListenQuerySpec_t2050960365::get_offset_of__pathAsString_1(),
	ListenQuerySpec_t2050960365::get_offset_of_QueryParams_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3668 = { sizeof (OutstandingListen_t67058648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3668[4] = 
{
	OutstandingListen_t67058648::get_offset_of__hashFunction_0(),
	OutstandingListen_t67058648::get_offset_of_Query_1(),
	OutstandingListen_t67058648::get_offset_of_ResultCallback_2(),
	OutstandingListen_t67058648::get_offset_of__tag_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3669 = { sizeof (OutstandingPut_t774816424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3669[4] = 
{
	OutstandingPut_t774816424::get_offset_of__action_0(),
	OutstandingPut_t774816424::get_offset_of_OnComplete_1(),
	OutstandingPut_t774816424::get_offset_of__request_2(),
	OutstandingPut_t774816424::get_offset_of__sent_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3670 = { sizeof (OutstandingDisconnect_t3017763353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3670[4] = 
{
	OutstandingDisconnect_t3017763353::get_offset_of__action_0(),
	OutstandingDisconnect_t3017763353::get_offset_of__data_1(),
	OutstandingDisconnect_t3017763353::get_offset_of__path_2(),
	OutstandingDisconnect_t3017763353::get_offset_of_OnComplete_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3671 = { sizeof (ConnectionState_t3450828338)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3671[6] = 
{
	ConnectionState_t3450828338::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3672 = { sizeof (Runnable557_t3816656261), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3672[2] = 
{
	Runnable557_t3816656261::get_offset_of__enclosing_0(),
	Runnable557_t3816656261::get_offset_of__forceRefresh_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3673 = { sizeof (GetTokenCallback567_t164694114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3673[2] = 
{
	GetTokenCallback567_t164694114::get_offset_of__enclosing_0(),
	GetTokenCallback567_t164694114::get_offset_of__thisGetTokenAttempt_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3674 = { sizeof (ConnectionRequestCallback629_t2407603808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3674[1] = 
{
	ConnectionRequestCallback629_t2407603808::get_offset_of__onComplete_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3675 = { sizeof (ConnectionRequestCallback797_t485289530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3675[2] = 
{
	ConnectionRequestCallback797_t485289530::get_offset_of__enclosing_0(),
	ConnectionRequestCallback797_t485289530::get_offset_of__restoreStateAfterComplete_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3676 = { sizeof (ConnectionRequestCallback941_t1244804617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3676[5] = 
{
	ConnectionRequestCallback941_t1244804617::get_offset_of__action_0(),
	ConnectionRequestCallback941_t1244804617::get_offset_of__enclosing_1(),
	ConnectionRequestCallback941_t1244804617::get_offset_of__onComplete_2(),
	ConnectionRequestCallback941_t1244804617::get_offset_of__put_3(),
	ConnectionRequestCallback941_t1244804617::get_offset_of__putId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3677 = { sizeof (ConnectionRequestCallback993_t485289728), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3677[2] = 
{
	ConnectionRequestCallback993_t485289728::get_offset_of__enclosing_0(),
	ConnectionRequestCallback993_t485289728::get_offset_of__listen_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3678 = { sizeof (ConnectionRequestCallback1026_t3966625496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3678[1] = 
{
	ConnectionRequestCallback1026_t3966625496::get_offset_of__enclosing_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3679 = { sizeof (Runnable1097_t2533788837), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3679[1] = 
{
	Runnable1097_t2533788837::get_offset_of__enclosing_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3680 = { sizeof (RangeMerge_t98025219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3680[3] = 
{
	RangeMerge_t98025219::get_offset_of__optExclusiveStart_0(),
	RangeMerge_t98025219::get_offset_of__optInclusiveEnd_1(),
	RangeMerge_t98025219::get_offset_of__snap_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3681 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3682 = { sizeof (WebsocketConnection_t1372375221), -1, sizeof(WebsocketConnection_t1372375221_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3682[12] = 
{
	WebsocketConnection_t1372375221_StaticFields::get_offset_of__connectionId_0(),
	WebsocketConnection_t1372375221::get_offset_of__connectionContext_1(),
	WebsocketConnection_t1372375221::get_offset_of__delegate_2(),
	WebsocketConnection_t1372375221::get_offset_of__executorService_3(),
	WebsocketConnection_t1372375221::get_offset_of__logger_4(),
	WebsocketConnection_t1372375221::get_offset_of__conn_5(),
	WebsocketConnection_t1372375221::get_offset_of__connectTimeout_6(),
	WebsocketConnection_t1372375221::get_offset_of__everConnected_7(),
	WebsocketConnection_t1372375221::get_offset_of__frameReader_8(),
	WebsocketConnection_t1372375221::get_offset_of__isClosed_9(),
	WebsocketConnection_t1372375221::get_offset_of__keepAlive_10(),
	WebsocketConnection_t1372375221::get_offset_of__totalFrames_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3683 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3684 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3685 = { sizeof (WsClientTubesock_t1111414532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3685[2] = 
{
	WsClientTubesock_t1111414532::get_offset_of__enclosing_0(),
	WsClientTubesock_t1111414532::get_offset_of__ws_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3686 = { sizeof (Runnable49_t1917770995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3686[1] = 
{
	Runnable49_t1917770995::get_offset_of__enclosing_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3687 = { sizeof (Runnable64_t754971594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3687[2] = 
{
	Runnable64_t754971594::get_offset_of__enclosing_0(),
	Runnable64_t754971594::get_offset_of__str_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3688 = { sizeof (Runnable75_t3483854948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3688[2] = 
{
	Runnable75_t3483854948::get_offset_of__enclosing_0(),
	Runnable75_t3483854948::get_offset_of__logMessage_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3689 = { sizeof (Runnable86_t3887139478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3689[2] = 
{
	Runnable86_t3887139478::get_offset_of__e_0(),
	Runnable86_t3887139478::get_offset_of__enclosing_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3690 = { sizeof (Runnable171_t4110307229), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3690[1] = 
{
	Runnable171_t4110307229::get_offset_of__enclosing_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3691 = { sizeof (Runnable290_t265678593), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3691[1] = 
{
	Runnable290_t265678593::get_offset_of__enclosing_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3692 = { sizeof (DictionaryHelper_t3179440036), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3693 = { sizeof (RetryHelper_t2341283196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3693[10] = 
{
	RetryHelper_t2341283196::get_offset_of__executorService_0(),
	RetryHelper_t2341283196::get_offset_of__jitterFactor_1(),
	RetryHelper_t2341283196::get_offset_of__logger_2(),
	RetryHelper_t2341283196::get_offset_of__maxRetryDelay_3(),
	RetryHelper_t2341283196::get_offset_of__minRetryDelayAfterFailure_4(),
	RetryHelper_t2341283196::get_offset_of__random_5(),
	RetryHelper_t2341283196::get_offset_of__retryExponent_6(),
	RetryHelper_t2341283196::get_offset_of__currentRetryDelay_7(),
	RetryHelper_t2341283196::get_offset_of__lastWasSuccess_8(),
	RetryHelper_t2341283196::get_offset_of__scheduledRetry_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3694 = { sizeof (Runnable53_t2038207663), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3694[2] = 
{
	Runnable53_t2038207663::get_offset_of__enclosing_0(),
	Runnable53_t2038207663::get_offset_of__runnable_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3695 = { sizeof (Builder_t470580281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3695[6] = 
{
	Builder_t470580281::get_offset_of__logger_0(),
	Builder_t470580281::get_offset_of__service_1(),
	Builder_t470580281::get_offset_of__jitterFactor_2(),
	Builder_t470580281::get_offset_of__minRetryDelayAfterFailure_3(),
	Builder_t470580281::get_offset_of__retryExponent_4(),
	Builder_t470580281::get_offset_of__retryMaxDelay_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3696 = { sizeof (StringListReader_t1703758004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3696[6] = 
{
	StringListReader_t1703758004::get_offset_of__markedCharPos_0(),
	StringListReader_t1703758004::get_offset_of__markedStringListPos_1(),
	StringListReader_t1703758004::get_offset_of__strings_2(),
	StringListReader_t1703758004::get_offset_of__charPos_3(),
	StringListReader_t1703758004::get_offset_of__frozen_4(),
	StringListReader_t1703758004::get_offset_of__stringListPos_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3697 = { sizeof (CompoundWrite_t496419158), -1, sizeof(CompoundWrite_t496419158_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3697[2] = 
{
	CompoundWrite_t496419158_StaticFields::get_offset_of_Empty_0(),
	CompoundWrite_t496419158::get_offset_of__writeTree_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3698 = { sizeof (TreeVisitor88_t1326285479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3698[1] = 
{
	TreeVisitor88_t1326285479::get_offset_of__path_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3699 = { sizeof (TreeVisitor234_t2891699520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3699[2] = 
{
	TreeVisitor234_t2891699520::get_offset_of__exportFormat_0(),
	TreeVisitor234_t2891699520::get_offset_of__writes_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
