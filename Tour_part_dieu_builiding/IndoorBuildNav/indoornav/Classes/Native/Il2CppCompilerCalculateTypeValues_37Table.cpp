﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3626412043.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3486154757.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2291877717.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1468635474.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3004044241.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_4222917807.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2568473163.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1007998148.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1244308462.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3371926541.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3977814488.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3500234454.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_R498336269.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1577920156.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_R325891947.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_R354392331.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1896492884.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_R713238209.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2143512465.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1464745291.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3889844450.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_R456743155.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3030829553.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2987537869.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2584253343.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3961001445.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_R949104308.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1991632987.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3747052656.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_4079583710.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2704469169.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1373652811.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_4225245602.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2343739264.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2504102480.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_S504080338.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_S150939507.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2720557329.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_S528142079.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1108302320.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2640751814.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_S718437514.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1074667861.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1262884541.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_S812545846.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_S792342866.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_S436112508.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2808765505.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_S792342872.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2358426799.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_S476518459.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1363616158.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3330738428.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2439924210.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_4018228498.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2054883910.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_U388677579.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_V481127222.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_Va34821328.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_W736318673.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1802089450.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1781886473.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_W351426802.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3773541119.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3439463511.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2378640522.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_4207088346.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3598067969.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_O860860098.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1017554386.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1909004847.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1191528129.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_P325790784.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_V994563358.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_V686203200.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_V639587248.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3330191602.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_V732806402.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1195404398.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3304926575.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3660935884.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_Vie2009560.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_V656865134.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_V526937568.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3700 = { sizeof (Constants_t3626412043), -1, sizeof(Constants_t3626412043_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3700[4] = 
{
	Constants_t3626412043_StaticFields::get_offset_of_DotInfo_0(),
	Constants_t3626412043_StaticFields::get_offset_of_DotInfoServertimeOffset_1(),
	Constants_t3626412043_StaticFields::get_offset_of_DotInfoAuthenticated_2(),
	Constants_t3626412043_StaticFields::get_offset_of_DotInfoConnected_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3701 = { sizeof (Context_t3486154757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3701[16] = 
{
	Context_t3486154757::get_offset_of__sync_0(),
	Context_t3486154757::get_offset_of__forcedPersistenceManager_1(),
	Context_t3486154757::get_offset_of__frozen_2(),
	Context_t3486154757::get_offset_of__platform_3(),
	Context_t3486154757::get_offset_of__stopped_4(),
	Context_t3486154757::get_offset_of_AuthTokenProvider_5(),
	Context_t3486154757::get_offset_of_CacheSize_6(),
	Context_t3486154757::get_offset_of_EventTarget_7(),
	Context_t3486154757::get_offset_of_FirebaseApp_8(),
	Context_t3486154757::get_offset_of_LoggedComponents_9(),
	Context_t3486154757::get_offset_of_Logger_10(),
	Context_t3486154757::get_offset_of_LogLevel_11(),
	Context_t3486154757::get_offset_of_PersistenceEnabled_12(),
	Context_t3486154757::get_offset_of_PersistenceKey_13(),
	Context_t3486154757::get_offset_of_RunLoop_14(),
	Context_t3486154757::get_offset_of_UserAgent_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3702 = { sizeof (ConnectionAuthTokenProvider258_t2291877717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3702[1] = 
{
	ConnectionAuthTokenProvider258_t2291877717::get_offset_of__provider_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3703 = { sizeof (GetTokenCompletionListener261_t1468635474), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3703[1] = 
{
	GetTokenCompletionListener261_t1468635474::get_offset_of__callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3704 = { sizeof (DatabaseConfig_t3004044241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3704[1] = 
{
	DatabaseConfig_t3004044241::get_offset_of__sync_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3705 = { sizeof (EventRegistration_t4222917807), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3705[3] = 
{
	EventRegistration_t4222917807::get_offset_of__zombied_0(),
	EventRegistration_t4222917807::get_offset_of__isUserInitiated_1(),
	EventRegistration_t4222917807::get_offset_of__listener_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3706 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3707 = { sizeof (Path_t2568473163), -1, sizeof(Path_t2568473163_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3707[4] = 
{
	Path_t2568473163_StaticFields::get_offset_of_EmptyPath_0(),
	Path_t2568473163::get_offset_of__end_1(),
	Path_t2568473163::get_offset_of__pieces_2(),
	Path_t2568473163::get_offset_of__start_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3708 = { sizeof (Enumerator174_t1007998148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3708[3] = 
{
	Enumerator174_t1007998148::get_offset_of__enclosing_0(),
	Enumerator174_t1007998148::get_offset_of__offset_1(),
	Enumerator174_t1007998148::get_offset_of_U3CCurrentU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3709 = { sizeof (Repo_t1244308462), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3709[17] = 
{
	Repo_t1244308462::get_offset_of__connection_0(),
	Repo_t1244308462::get_offset_of__ctx_1(),
	Repo_t1244308462::get_offset_of__firebaseDatabase_2(),
	Repo_t1244308462::get_offset_of__dataLogger_3(),
	Repo_t1244308462::get_offset_of__eventRaiser_4(),
	Repo_t1244308462::get_offset_of__operationLogger_5(),
	Repo_t1244308462::get_offset_of__repoInfo_6(),
	Repo_t1244308462::get_offset_of__serverClock_7(),
	Repo_t1244308462::get_offset_of__transactionLogger_8(),
	Repo_t1244308462::get_offset_of__hijackHash_9(),
	Repo_t1244308462::get_offset_of__infoData_10(),
	Repo_t1244308462::get_offset_of__infoSyncTree_11(),
	Repo_t1244308462::get_offset_of__nextWriteId_12(),
	Repo_t1244308462::get_offset_of__onDisconnectTree_13(),
	Repo_t1244308462::get_offset_of__serverSyncTree_14(),
	Repo_t1244308462::get_offset_of__transactionQueueTree_15(),
	Repo_t1244308462::get_offset_of_DataUpdateCount_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3710 = { sizeof (Runnable67_t3371926541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3710[1] = 
{
	Runnable67_t3371926541::get_offset_of__enclosing_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3711 = { sizeof (TokenChangeListener79_t3977814488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3711[1] = 
{
	TokenChangeListener79_t3977814488::get_offset_of__enclosing_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3712 = { sizeof (ListenProvider106_t3500234454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3712[1] = 
{
	ListenProvider106_t3500234454::get_offset_of__enclosing_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3713 = { sizeof (Runnable111_t498336269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3713[3] = 
{
	Runnable111_t498336269::get_offset_of__enclosing_0(),
	Runnable111_t498336269::get_offset_of__onComplete_1(),
	Runnable111_t498336269::get_offset_of__query_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3714 = { sizeof (ListenProvider131_t1577920156), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3714[1] = 
{
	ListenProvider131_t1577920156::get_offset_of__enclosing_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3715 = { sizeof (RequestResultCallback139_t325891947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3715[2] = 
{
	RequestResultCallback139_t325891947::get_offset_of__enclosing_0(),
	RequestResultCallback139_t325891947::get_offset_of__onListenComplete_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3716 = { sizeof (RequestResultCallback168_t354392331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3716[2] = 
{
	RequestResultCallback168_t354392331::get_offset_of__enclosing_0(),
	RequestResultCallback168_t354392331::get_offset_of__write_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3717 = { sizeof (SparseSnapshotTreeVisitor559_t1896492884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3717[2] = 
{
	SparseSnapshotTreeVisitor559_t1896492884::get_offset_of__enclosing_0(),
	SparseSnapshotTreeVisitor559_t1896492884::get_offset_of__events_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3718 = { sizeof (TransactionStatus_t713238209)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3718[7] = 
{
	TransactionStatus_t713238209::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3719 = { sizeof (TransactionData_t2143512465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3719[12] = 
{
	TransactionData_t2143512465::get_offset_of_AbortReason_0(),
	TransactionData_t2143512465::get_offset_of_ApplyLocally_1(),
	TransactionData_t2143512465::get_offset_of_CurrentInputSnapshot_2(),
	TransactionData_t2143512465::get_offset_of_CurrentOutputSnapshotRaw_3(),
	TransactionData_t2143512465::get_offset_of_CurrentOutputSnapshotResolved_4(),
	TransactionData_t2143512465::get_offset_of_CurrentWriteId_5(),
	TransactionData_t2143512465::get_offset_of_Handler_6(),
	TransactionData_t2143512465::get_offset_of_Order_7(),
	TransactionData_t2143512465::get_offset_of_OutstandingHandler_8(),
	TransactionData_t2143512465::get_offset_of_Path_9(),
	TransactionData_t2143512465::get_offset_of_RetryCount_10(),
	TransactionData_t2143512465::get_offset_of_Status_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3720 = { sizeof (TreeVisitor778_t1464745291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3720[1] = 
{
	TreeVisitor778_t1464745291::get_offset_of__enclosing_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3721 = { sizeof (RequestResultCallback817_t3889844450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3721[4] = 
{
	RequestResultCallback817_t3889844450::get_offset_of__enclosing_0(),
	RequestResultCallback817_t3889844450::get_offset_of__path_1(),
	RequestResultCallback817_t3889844450::get_offset_of__queue_2(),
	RequestResultCallback817_t3889844450::get_offset_of__repo_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3722 = { sizeof (Runnable838_t456743155), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3722[2] = 
{
	Runnable838_t456743155::get_offset_of__snap_0(),
	Runnable838_t456743155::get_offset_of__txn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3723 = { sizeof (TreeVisitor907_t3030829553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3723[1] = 
{
	TreeVisitor907_t3030829553::get_offset_of__enclosing_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3724 = { sizeof (Runnable1019_t2987537869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3724[2] = 
{
	Runnable1019_t2987537869::get_offset_of__enclosing_0(),
	Runnable1019_t2987537869::get_offset_of__transaction_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3725 = { sizeof (Runnable1028_t2584253343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3725[3] = 
{
	Runnable1028_t2584253343::get_offset_of__callbackError_0(),
	Runnable1028_t2584253343::get_offset_of__snapshot_1(),
	Runnable1028_t2584253343::get_offset_of__transaction_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3726 = { sizeof (TreeVisitor1074_t3961001445), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3726[2] = 
{
	TreeVisitor1074_t3961001445::get_offset_of__enclosing_0(),
	TreeVisitor1074_t3961001445::get_offset_of__queue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3727 = { sizeof (TreeFilter1088_t949104308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3727[2] = 
{
	TreeFilter1088_t949104308::get_offset_of__enclosing_0(),
	TreeFilter1088_t949104308::get_offset_of__reason_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3728 = { sizeof (TreeVisitor1098_t1991632987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3728[2] = 
{
	TreeVisitor1098_t1991632987::get_offset_of__enclosing_0(),
	TreeVisitor1098_t1991632987::get_offset_of__reason_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3729 = { sizeof (Runnable1144_t3747052656), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3729[2] = 
{
	Runnable1144_t3747052656::get_offset_of__abortError_0(),
	Runnable1144_t3747052656::get_offset_of__transaction_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3730 = { sizeof (RepoInfo_t4079583710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3730[4] = 
{
	RepoInfo_t4079583710::get_offset_of_Host_0(),
	RepoInfo_t4079583710::get_offset_of_InternalHost_1(),
	RepoInfo_t4079583710::get_offset_of_Namespace_2(),
	RepoInfo_t4079583710::get_offset_of_Secure_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3731 = { sizeof (RepoManager_t2704469169), -1, sizeof(RepoManager_t2704469169_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3731[2] = 
{
	RepoManager_t2704469169_StaticFields::get_offset_of_Instance_0(),
	RepoManager_t2704469169::get_offset_of__repos_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3732 = { sizeof (ServerValues_t1373652811), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3733 = { sizeof (SparseSnapshotTreeVisitor40_t4225245602), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3733[2] = 
{
	SparseSnapshotTreeVisitor40_t4225245602::get_offset_of__resolvedTree_0(),
	SparseSnapshotTreeVisitor40_t4225245602::get_offset_of__serverValues_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3734 = { sizeof (ChildVisitor71_t2343739264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3734[2] = 
{
	ChildVisitor71_t2343739264::get_offset_of__holder_0(),
	ChildVisitor71_t2343739264::get_offset_of__serverValues_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3735 = { sizeof (SnapshotHolder_t2504102480), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3735[1] = 
{
	SnapshotHolder_t2504102480::get_offset_of__rootNode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3736 = { sizeof (SparseSnapshotTree_t504080338), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3736[2] = 
{
	SparseSnapshotTree_t504080338::get_offset_of__children_0(),
	SparseSnapshotTree_t504080338::get_offset_of__value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3737 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3738 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3739 = { sizeof (SparseSnapshotChildVisitor107_t150939507), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3739[2] = 
{
	SparseSnapshotChildVisitor107_t150939507::get_offset_of__prefixPath_0(),
	SparseSnapshotChildVisitor107_t150939507::get_offset_of__visitor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3740 = { sizeof (SyncPoint_t2720557329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3740[2] = 
{
	SyncPoint_t2720557329::get_offset_of__persistenceManager_0(),
	SyncPoint_t2720557329::get_offset_of__views_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3741 = { sizeof (SyncTree_t528142079), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3741[9] = 
{
	SyncTree_t528142079::get_offset_of__keepSyncedQueries_0(),
	SyncTree_t528142079::get_offset_of__listenProvider_1(),
	SyncTree_t528142079::get_offset_of__logger_2(),
	SyncTree_t528142079::get_offset_of__pendingWriteTree_3(),
	SyncTree_t528142079::get_offset_of__persistenceManager_4(),
	SyncTree_t528142079::get_offset_of__queryToTagMap_5(),
	SyncTree_t528142079::get_offset_of__tagToQueryMap_6(),
	SyncTree_t528142079::get_offset_of__nextQueryTag_7(),
	SyncTree_t528142079::get_offset_of__syncPointTree_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3742 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3743 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3744 = { sizeof (ListenContainer_t1108302320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3744[3] = 
{
	ListenContainer_t1108302320::get_offset_of__enclosing_0(),
	ListenContainer_t1108302320::get_offset_of_Tag_1(),
	ListenContainer_t1108302320::get_offset_of_View_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3745 = { sizeof (Callable149_t2640751814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3745[7] = 
{
	Callable149_t2640751814::get_offset_of__enclosing_0(),
	Callable149_t2640751814::get_offset_of__newData_1(),
	Callable149_t2640751814::get_offset_of__newDataUnresolved_2(),
	Callable149_t2640751814::get_offset_of__path_3(),
	Callable149_t2640751814::get_offset_of__persist_4(),
	Callable149_t2640751814::get_offset_of__visible_5(),
	Callable149_t2640751814::get_offset_of__writeId_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3746 = { sizeof (Callable170_t718437514), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3746[6] = 
{
	Callable170_t718437514::get_offset_of__children_0(),
	Callable170_t718437514::get_offset_of__enclosing_1(),
	Callable170_t718437514::get_offset_of__path_2(),
	Callable170_t718437514::get_offset_of__persist_3(),
	Callable170_t718437514::get_offset_of__unresolvedChildren_4(),
	Callable170_t718437514::get_offset_of__writeId_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3747 = { sizeof (Callable188_t1074667861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3747[5] = 
{
	Callable188_t1074667861::get_offset_of__enclosing_0(),
	Callable188_t1074667861::get_offset_of__persist_1(),
	Callable188_t1074667861::get_offset_of__revert_2(),
	Callable188_t1074667861::get_offset_of__serverClock_3(),
	Callable188_t1074667861::get_offset_of__writeId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3748 = { sizeof (Callable247_t1262884541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3748[3] = 
{
	Callable247_t1262884541::get_offset_of__enclosing_0(),
	Callable247_t1262884541::get_offset_of__newData_1(),
	Callable247_t1262884541::get_offset_of__path_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3749 = { sizeof (Callable259_t812545846), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3749[3] = 
{
	Callable259_t812545846::get_offset_of__changedChildren_0(),
	Callable259_t812545846::get_offset_of__enclosing_1(),
	Callable259_t812545846::get_offset_of__path_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3750 = { sizeof (Callable318_t792342866), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3750[2] = 
{
	Callable318_t792342866::get_offset_of__enclosing_0(),
	Callable318_t792342866::get_offset_of__path_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3751 = { sizeof (Callable330_t436112508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3751[2] = 
{
	Callable330_t436112508::get_offset_of__enclosing_0(),
	Callable330_t436112508::get_offset_of__tag_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3752 = { sizeof (Callable357_t2808765505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3752[4] = 
{
	Callable357_t2808765505::get_offset_of__enclosing_0(),
	Callable357_t2808765505::get_offset_of__path_1(),
	Callable357_t2808765505::get_offset_of__snap_2(),
	Callable357_t2808765505::get_offset_of__tag_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3753 = { sizeof (Callable378_t792342872), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3753[4] = 
{
	Callable378_t792342872::get_offset_of__changedChildren_0(),
	Callable378_t792342872::get_offset_of__enclosing_1(),
	Callable378_t792342872::get_offset_of__path_2(),
	Callable378_t792342872::get_offset_of__tag_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3754 = { sizeof (Callable399_t2358426799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3754[2] = 
{
	Callable399_t2358426799::get_offset_of__enclosing_0(),
	Callable399_t2358426799::get_offset_of__eventRegistration_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3755 = { sizeof (Callable506_t476518459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3755[4] = 
{
	Callable506_t476518459::get_offset_of__cancelError_0(),
	Callable506_t476518459::get_offset_of__enclosing_1(),
	Callable506_t476518459::get_offset_of__eventRegistration_2(),
	Callable506_t476518459::get_offset_of__query_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3756 = { sizeof (TreeVisitor688_t1363616158), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3756[1] = 
{
	TreeVisitor688_t1363616158::get_offset_of__enclosing_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3757 = { sizeof (NodeVisitor826_t3330738428), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3757[5] = 
{
	NodeVisitor826_t3330738428::get_offset_of__enclosing_0(),
	NodeVisitor826_t3330738428::get_offset_of__events_1(),
	NodeVisitor826_t3330738428::get_offset_of__operation_2(),
	NodeVisitor826_t3330738428::get_offset_of__resolvedServerCache_3(),
	NodeVisitor826_t3330738428::get_offset_of__writesCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3758 = { sizeof (Tag_t2439924210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3758[1] = 
{
	Tag_t2439924210::get_offset_of__tagNumber_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3759 = { sizeof (ThreadInitializer_t4018228498), -1, sizeof(ThreadInitializer_t4018228498_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3759[1] = 
{
	ThreadInitializer_t4018228498_StaticFields::get_offset_of_DefaultInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3760 = { sizeof (ThreadInitializer7_t2054883910), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3761 = { sizeof (UserWriteRecord_t388677579), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3761[5] = 
{
	UserWriteRecord_t388677579::get_offset_of__merge_0(),
	UserWriteRecord_t388677579::get_offset_of__overwrite_1(),
	UserWriteRecord_t388677579::get_offset_of__path_2(),
	UserWriteRecord_t388677579::get_offset_of__visible_3(),
	UserWriteRecord_t388677579::get_offset_of__writeId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3762 = { sizeof (ValidationPath_t481127222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3762[2] = 
{
	ValidationPath_t481127222::get_offset_of__parts_0(),
	ValidationPath_t481127222::get_offset_of__byteLength_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3763 = { sizeof (ValueEventRegistration_t34821328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3763[3] = 
{
	ValueEventRegistration_t34821328::get_offset_of__eventHandler_3(),
	ValueEventRegistration_t34821328::get_offset_of__repo_4(),
	ValueEventRegistration_t34821328::get_offset_of__spec_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3764 = { sizeof (WriteTree_t736318673), -1, sizeof(WriteTree_t736318673_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3764[4] = 
{
	WriteTree_t736318673_StaticFields::get_offset_of_DefaultFilter_0(),
	WriteTree_t736318673::get_offset_of__allWrites_1(),
	WriteTree_t736318673::get_offset_of__lastWriteId_2(),
	WriteTree_t736318673::get_offset_of__visibleWrites_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3765 = { sizeof (Predicate203_t1802089450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3765[3] = 
{
	Predicate203_t1802089450::get_offset_of__includeHiddenWrites_1(),
	Predicate203_t1802089450::get_offset_of__treePath_2(),
	Predicate203_t1802089450::get_offset_of__writeIdsToExclude_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3766 = { sizeof (Predicate372_t1781886473), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3767 = { sizeof (WriteTreeRef_t351426802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3767[2] = 
{
	WriteTreeRef_t351426802::get_offset_of__treePath_0(),
	WriteTreeRef_t351426802::get_offset_of__writeTree_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3768 = { sizeof (ZombieEventManager_t3773541119), -1, sizeof(ZombieEventManager_t3773541119_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3768[2] = 
{
	ZombieEventManager_t3773541119_StaticFields::get_offset_of_DefaultInstance_0(),
	ZombieEventManager_t3773541119::get_offset_of_GlobalEventRegistrations_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3769 = { sizeof (AckUserWrite_t3439463511), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3769[2] = 
{
	AckUserWrite_t3439463511::get_offset_of__affectedTree_3(),
	AckUserWrite_t3439463511::get_offset_of__revert_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3770 = { sizeof (ListenComplete_t2378640522), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3771 = { sizeof (Merge_t4207088346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3771[1] = 
{
	Merge_t4207088346::get_offset_of__children_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3772 = { sizeof (Operation_t3598067969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3772[3] = 
{
	Operation_t3598067969::get_offset_of_Path_0(),
	Operation_t3598067969::get_offset_of_Source_1(),
	Operation_t3598067969::get_offset_of_Type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3773 = { sizeof (OperationType_t860860098)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3773[5] = 
{
	OperationType_t860860098::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3774 = { sizeof (OperationSource_t1017554386), -1, sizeof(OperationSource_t1017554386_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3774[5] = 
{
	OperationSource_t1017554386_StaticFields::get_offset_of_User_0(),
	OperationSource_t1017554386_StaticFields::get_offset_of_Server_1(),
	OperationSource_t1017554386::get_offset_of__queryParams_2(),
	OperationSource_t1017554386::get_offset_of__source_3(),
	OperationSource_t1017554386::get_offset_of__tagged_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3775 = { sizeof (Source_t1909004847)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3775[3] = 
{
	Source_t1909004847::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3776 = { sizeof (Overwrite_t1191528129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3776[1] = 
{
	Overwrite_t1191528129::get_offset_of__snapshot_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3777 = { sizeof (NoopPersistenceManager_t325790784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3777[1] = 
{
	NoopPersistenceManager_t325790784::get_offset_of__insideTransaction_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3778 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3779 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3779[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3780 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3781 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3781[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3782 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3782[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3783 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3784 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3784[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3785 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3786 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3787 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3787[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3788 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3788[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3789 = { sizeof (CacheNode_t994563358), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3789[3] = 
{
	CacheNode_t994563358::get_offset_of__filtered_0(),
	CacheNode_t994563358::get_offset_of__fullyInitialized_1(),
	CacheNode_t994563358::get_offset_of__indexedNode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3790 = { sizeof (CancelEvent_t686203200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3790[3] = 
{
	CancelEvent_t686203200::get_offset_of__error_0(),
	CancelEvent_t686203200::get_offset_of__eventRegistration_1(),
	CancelEvent_t686203200::get_offset_of__path_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3791 = { sizeof (Change_t639587248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3791[5] = 
{
	Change_t639587248::get_offset_of__childKey_0(),
	Change_t639587248::get_offset_of__eventType_1(),
	Change_t639587248::get_offset_of__indexedNode_2(),
	Change_t639587248::get_offset_of__oldIndexedNode_3(),
	Change_t639587248::get_offset_of__prevName_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3792 = { sizeof (DataEvent_t3330191602), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3792[4] = 
{
	DataEvent_t3330191602::get_offset_of__eventRegistration_0(),
	DataEvent_t3330191602::get_offset_of__eventType_1(),
	DataEvent_t3330191602::get_offset_of__prevName_2(),
	DataEvent_t3330191602::get_offset_of__snapshot_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3793 = { sizeof (Event_t732806402), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3794 = { sizeof (EventType_t1195404398)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3794[6] = 
{
	EventType_t1195404398::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3795 = { sizeof (EventGenerator_t3304926575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3795[2] = 
{
	EventGenerator_t3304926575::get_offset_of__index_0(),
	EventGenerator_t3304926575::get_offset_of__query_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3796 = { sizeof (Comparator70_t3660935884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3796[1] = 
{
	Comparator70_t3660935884::get_offset_of__enclosing_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3797 = { sizeof (EventRaiser_t2009560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3797[2] = 
{
	EventRaiser_t2009560::get_offset_of__eventTarget_0(),
	EventRaiser_t2009560::get_offset_of__logger_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3798 = { sizeof (Runnable30_t656865134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3798[2] = 
{
	Runnable30_t656865134::get_offset_of__enclosing_0(),
	Runnable30_t656865134::get_offset_of__eventsClone_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3799 = { sizeof (QueryParams_t526937568), -1, sizeof(QueryParams_t526937568_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3799[8] = 
{
	QueryParams_t526937568_StaticFields::get_offset_of_DefaultParams_0(),
	QueryParams_t526937568::get_offset_of__index_1(),
	QueryParams_t526937568::get_offset_of__indexEndName_2(),
	QueryParams_t526937568::get_offset_of__indexEndValue_3(),
	QueryParams_t526937568::get_offset_of__indexStartName_4(),
	QueryParams_t526937568::get_offset_of__indexStartValue_5(),
	QueryParams_t526937568::get_offset_of__limit_6(),
	QueryParams_t526937568::get_offset_of__viewFrom_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
