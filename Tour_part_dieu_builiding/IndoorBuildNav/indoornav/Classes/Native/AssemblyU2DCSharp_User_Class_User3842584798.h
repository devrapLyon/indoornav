﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// User_Class/User
struct  User_t3842584798  : public Il2CppObject
{
public:
	// System.String User_Class/User::<firstName>k__BackingField
	String_t* ___U3CfirstNameU3Ek__BackingField_0;
	// System.String User_Class/User::<lastName>k__BackingField
	String_t* ___U3ClastNameU3Ek__BackingField_1;
	// System.String User_Class/User::<mobileNumber>k__BackingField
	String_t* ___U3CmobileNumberU3Ek__BackingField_2;
	// System.Single User_Class/User::<pos_x>k__BackingField
	float ___U3Cpos_xU3Ek__BackingField_3;
	// System.Single User_Class/User::<pos_y>k__BackingField
	float ___U3Cpos_yU3Ek__BackingField_4;
	// System.Single User_Class/User::<pos_z>k__BackingField
	float ___U3Cpos_zU3Ek__BackingField_5;
	// System.String User_Class/User::<Key>k__BackingField
	String_t* ___U3CKeyU3Ek__BackingField_6;
	// System.String User_Class/User::<user_level>k__BackingField
	String_t* ___U3Cuser_levelU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CfirstNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(User_t3842584798, ___U3CfirstNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CfirstNameU3Ek__BackingField_0() const { return ___U3CfirstNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CfirstNameU3Ek__BackingField_0() { return &___U3CfirstNameU3Ek__BackingField_0; }
	inline void set_U3CfirstNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CfirstNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfirstNameU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3ClastNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(User_t3842584798, ___U3ClastNameU3Ek__BackingField_1)); }
	inline String_t* get_U3ClastNameU3Ek__BackingField_1() const { return ___U3ClastNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3ClastNameU3Ek__BackingField_1() { return &___U3ClastNameU3Ek__BackingField_1; }
	inline void set_U3ClastNameU3Ek__BackingField_1(String_t* value)
	{
		___U3ClastNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClastNameU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CmobileNumberU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(User_t3842584798, ___U3CmobileNumberU3Ek__BackingField_2)); }
	inline String_t* get_U3CmobileNumberU3Ek__BackingField_2() const { return ___U3CmobileNumberU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CmobileNumberU3Ek__BackingField_2() { return &___U3CmobileNumberU3Ek__BackingField_2; }
	inline void set_U3CmobileNumberU3Ek__BackingField_2(String_t* value)
	{
		___U3CmobileNumberU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmobileNumberU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3Cpos_xU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(User_t3842584798, ___U3Cpos_xU3Ek__BackingField_3)); }
	inline float get_U3Cpos_xU3Ek__BackingField_3() const { return ___U3Cpos_xU3Ek__BackingField_3; }
	inline float* get_address_of_U3Cpos_xU3Ek__BackingField_3() { return &___U3Cpos_xU3Ek__BackingField_3; }
	inline void set_U3Cpos_xU3Ek__BackingField_3(float value)
	{
		___U3Cpos_xU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3Cpos_yU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(User_t3842584798, ___U3Cpos_yU3Ek__BackingField_4)); }
	inline float get_U3Cpos_yU3Ek__BackingField_4() const { return ___U3Cpos_yU3Ek__BackingField_4; }
	inline float* get_address_of_U3Cpos_yU3Ek__BackingField_4() { return &___U3Cpos_yU3Ek__BackingField_4; }
	inline void set_U3Cpos_yU3Ek__BackingField_4(float value)
	{
		___U3Cpos_yU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3Cpos_zU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(User_t3842584798, ___U3Cpos_zU3Ek__BackingField_5)); }
	inline float get_U3Cpos_zU3Ek__BackingField_5() const { return ___U3Cpos_zU3Ek__BackingField_5; }
	inline float* get_address_of_U3Cpos_zU3Ek__BackingField_5() { return &___U3Cpos_zU3Ek__BackingField_5; }
	inline void set_U3Cpos_zU3Ek__BackingField_5(float value)
	{
		___U3Cpos_zU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CKeyU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(User_t3842584798, ___U3CKeyU3Ek__BackingField_6)); }
	inline String_t* get_U3CKeyU3Ek__BackingField_6() const { return ___U3CKeyU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CKeyU3Ek__BackingField_6() { return &___U3CKeyU3Ek__BackingField_6; }
	inline void set_U3CKeyU3Ek__BackingField_6(String_t* value)
	{
		___U3CKeyU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CKeyU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3Cuser_levelU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(User_t3842584798, ___U3Cuser_levelU3Ek__BackingField_7)); }
	inline String_t* get_U3Cuser_levelU3Ek__BackingField_7() const { return ___U3Cuser_levelU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3Cuser_levelU3Ek__BackingField_7() { return &___U3Cuser_levelU3Ek__BackingField_7; }
	inline void set_U3Cuser_levelU3Ek__BackingField_7(String_t* value)
	{
		___U3Cuser_levelU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cuser_levelU3Ek__BackingField_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
