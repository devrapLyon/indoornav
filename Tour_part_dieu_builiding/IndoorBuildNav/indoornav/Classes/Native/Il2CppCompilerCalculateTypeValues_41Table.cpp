﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DatabaseCheck2175600645.h"
#include "AssemblyU2DCSharp_STEventHandler125466593.h"
#include "AssemblyU2DCSharp_STMenuOptions4254480554.h"
#include "AssemblyU2DCSharp_ObbComparison4173085576.h"
#include "AssemblyU2DCSharp_TextEventHandler4116109227.h"
#include "AssemblyU2DCSharp_QualityDialog1435260315.h"
#include "AssemblyU2DCSharp_TargetBuilderUIHandler2867400282.h"
#include "AssemblyU2DCSharp_UDTEventHandler3117429239.h"
#include "AssemblyU2DCSharp_UDTSettings3206912890.h"
#include "AssemblyU2DCSharp_VBMenuOptions3990510209.h"
#include "AssemblyU2DCSharp_VBSoccerballEventHandler3236383594.h"
#include "AssemblyU2DCSharp_VirtualButtonEventHandler735753253.h"
#include "AssemblyU2DCSharp_PanelShowHide775446595.h"
#include "AssemblyU2DCSharp_VuMarkEventHandler2972421956.h"
#include "AssemblyU2DCSharp_VuMarkHandler583794080.h"
#include "AssemblyU2DCSharp_VuMarkHandler_U3CShowPanelAfterU1558983881.h"
#include "AssemblyU2DCSharp_VuMarkTapHandler2561605313.h"
#include "AssemblyU2DCSharp_SamplesAboutScreenInfo1023079980.h"
#include "AssemblyU2DCSharp_SamplesConfigUI3549911149.h"
#include "AssemblyU2DCSharp_SamplesConfigUI_UIPresetsEnum1160230479.h"
#include "AssemblyU2DCSharp_SamplesLoadingScreen2658974353.h"
#include "AssemblyU2DCSharp_SamplesMainMenu2014309523.h"
#include "AssemblyU2DCSharp_SamplesMainMenu_MenuItem3182038446.h"
#include "AssemblyU2DCSharp_SamplesNavigationHandler2120161205.h"
#include "AssemblyU2DCSharp_SamplesTapHandler2866147728.h"
#include "AssemblyU2DCSharp_MoveCamera3193441886.h"
#include "AssemblyU2DCSharp_outlineShader244045327.h"
#include "AssemblyU2DCSharp_TouchCamera409509692.h"
#include "AssemblyU2DCSharp_Touch_zoom1547134311.h"
#include "AssemblyU2DCSharp_cameraController4112230091.h"
#include "AssemblyU2DCSharp_Change_View1623420308.h"
#include "AssemblyU2DCSharp_Markers3135928711.h"
#include "AssemblyU2DCSharp_Markers_Vumark_Class29598747.h"
#include "AssemblyU2DCSharp_Office_Class2060087375.h"
#include "AssemblyU2DCSharp_Office_Class_Salle3888431915.h"
#include "AssemblyU2DCSharp_User_Class3086874568.h"
#include "AssemblyU2DCSharp_User_Class_User3842584798.h"
#include "AssemblyU2DCSharp_Destination_script318603734.h"
#include "AssemblyU2DCSharp_Exit_Level1387234555.h"
#include "AssemblyU2DCSharp_Finder2554545268.h"
#include "AssemblyU2DCSharp_GetPos935983698.h"
#include "AssemblyU2DCSharp_ButtonListButton2298477596.h"
#include "AssemblyU2DCSharp_ButtonListControl933417769.h"
#include "AssemblyU2DCSharp_Key2293551073.h"
#include "AssemblyU2DCSharp_Users907518852.h"
#include "AssemblyU2DCSharp_IdOffice1887523751.h"
#include "AssemblyU2DCSharp_OfficeData3828749716.h"
#include "AssemblyU2DCSharp_RootObject1671561673.h"
#include "AssemblyU2DCSharp_ClickToAsync2031327609.h"
#include "AssemblyU2DCSharp_ClickToAsync_U3CLoadLevelWithBar1546663020.h"
#include "AssemblyU2DCSharp_LoadAdditive1235278820.h"
#include "AssemblyU2DCSharp_LoadOnClick2668983067.h"
#include "AssemblyU2DCSharp_TogglePanelButton2654886650.h"
#include "AssemblyU2DCSharp_Origin_scripts2339293569.h"
#include "AssemblyU2DCSharp_vumark_levid1745599613.h"
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour2431285219.h"
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour3077176941.h"
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour2091399712.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErro965510117.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventH870608571.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHan1082256726.h"
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler3809113141.h"
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour3495034315.h"
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour2654589389.h"
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayer852788525.h"
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterB3249343815.h"
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayer3656371703.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponen1383853028.h"
#include "AssemblyU2DCSharp_Vuforia_WSAUnityPlayer425981959.h"
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour2994129365.h"
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour3504654311.h"
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour3836044259.h"
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour966064926.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour4009935945.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTarget2111803406.h"
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour2405314212.h"
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour3400239837.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour3058161409.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour584991835.h"
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildin4184040062.h"
#include "AssemblyU2DCSharp_VRIntegrationHelper556656694.h"
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour3161817952.h"
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour2515041812.h"
#include "AssemblyU2DCSharp_Vuforia_VuMarkBehaviour2060629989.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviour359035403.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaConfiguration3823746026.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaRuntimeInitializa1850075444.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour2494532455.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventH1535150527.h"
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour3366478421.h"
#include "AssemblyU2DCSharp_dis_or450365292.h"
#include "AssemblyU2DCSharp_script_test1536385136.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236537.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3762068664.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4100 = { sizeof (DatabaseCheck_t2175600645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4100[3] = 
{
	DatabaseCheck_t2175600645::get_offset_of_errorCanvas_2(),
	DatabaseCheck_t2175600645::get_offset_of_errorTitle_3(),
	DatabaseCheck_t2175600645::get_offset_of_errorMessage_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4101 = { sizeof (STEventHandler_t125466593), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4101[3] = 
{
	STEventHandler_t125466593::get_offset_of_mReconstructionBehaviour_2(),
	STEventHandler_t125466593::get_offset_of_PropTemplate_3(),
	STEventHandler_t125466593::get_offset_of_SurfaceTemplate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4102 = { sizeof (STMenuOptions_t4254480554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4102[1] = 
{
	STMenuOptions_t4254480554::get_offset_of_mReconstructionBehaviour_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4103 = { sizeof (ObbComparison_t4173085576), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4104 = { sizeof (TextEventHandler_t4116109227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4104[14] = 
{
	TextEventHandler_t4116109227::get_offset_of_mLoupeWidth_2(),
	TextEventHandler_t4116109227::get_offset_of_mLoupeHeight_3(),
	TextEventHandler_t4116109227::get_offset_of_mBBoxLineWidth_4(),
	TextEventHandler_t4116109227::get_offset_of_mBBoxPadding_5(),
	TextEventHandler_t4116109227::get_offset_of_mBBoxColor_6(),
	TextEventHandler_t4116109227::get_offset_of_mDetectionAndTrackingRect_7(),
	TextEventHandler_t4116109227::get_offset_of_mBoundingBoxTexture_8(),
	TextEventHandler_t4116109227::get_offset_of_mBoundingBoxMaterial_9(),
	TextEventHandler_t4116109227::get_offset_of_mIsInitialized_10(),
	TextEventHandler_t4116109227::get_offset_of_mVideoBackgroundChanged_11(),
	TextEventHandler_t4116109227::get_offset_of_mSortedWords_12(),
	TextEventHandler_t4116109227::get_offset_of_mDisplayedWords_13(),
	TextEventHandler_t4116109227::get_offset_of_boundingBoxMaterial_14(),
	TextEventHandler_t4116109227::get_offset_of_textRecoCanvas_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4105 = { sizeof (QualityDialog_t1435260315), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4106 = { sizeof (TargetBuilderUIHandler_t2867400282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4106[3] = 
{
	TargetBuilderUIHandler_t2867400282::get_offset_of_mTargetBuilderCanvas_2(),
	TargetBuilderUIHandler_t2867400282::get_offset_of_mMenuAnim_3(),
	TargetBuilderUIHandler_t2867400282::get_offset_of_mVuforiaStarted_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4107 = { sizeof (UDTEventHandler_t3117429239), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4107[9] = 
{
	UDTEventHandler_t3117429239::get_offset_of_ImageTargetTemplate_2(),
	0,
	UDTEventHandler_t3117429239::get_offset_of_mTargetBuildingBehaviour_4(),
	UDTEventHandler_t3117429239::get_offset_of_mQualityDialog_5(),
	UDTEventHandler_t3117429239::get_offset_of_mObjectTracker_6(),
	UDTEventHandler_t3117429239::get_offset_of_mBuiltDataSet_7(),
	UDTEventHandler_t3117429239::get_offset_of_mFrameQuality_8(),
	UDTEventHandler_t3117429239::get_offset_of_mTargetCounter_9(),
	UDTEventHandler_t3117429239::get_offset_of_mTrackableSettings_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4108 = { sizeof (UDTSettings_t3206912890), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4108[1] = 
{
	UDTSettings_t3206912890::get_offset_of_mUDTEventHandler_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4109 = { sizeof (VBMenuOptions_t3990510209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4109[4] = 
{
	VBMenuOptions_t3990510209::get_offset_of_mVirtualButtonMaterial_5(),
	VBMenuOptions_t3990510209::get_offset_of_mImageTargetWood_6(),
	VBMenuOptions_t3990510209::get_offset_of_mVBPositionDict_7(),
	VBMenuOptions_t3990510209::get_offset_of_mVBScaleDict_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4110 = { sizeof (VBSoccerballEventHandler_t3236383594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4110[4] = 
{
	VBSoccerballEventHandler_t3236383594::get_offset_of_mSoccerball_2(),
	VBSoccerballEventHandler_t3236383594::get_offset_of_mIsRolling_3(),
	VBSoccerballEventHandler_t3236383594::get_offset_of_mTimeRolling_4(),
	VBSoccerballEventHandler_t3236383594::get_offset_of_mForce_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4111 = { sizeof (VirtualButtonEventHandler_t735753253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4111[4] = 
{
	VirtualButtonEventHandler_t735753253::get_offset_of_m_TeapotMaterials_2(),
	VirtualButtonEventHandler_t735753253::get_offset_of_m_VirtualButtonMaterial_3(),
	VirtualButtonEventHandler_t735753253::get_offset_of_mTeapot_4(),
	VirtualButtonEventHandler_t735753253::get_offset_of_mActiveMaterials_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4112 = { sizeof (PanelShowHide_t775446595), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4112[4] = 
{
	PanelShowHide_t775446595::get_offset_of__animator_2(),
	PanelShowHide_t775446595::get_offset_of__title_3(),
	PanelShowHide_t775446595::get_offset_of__id_4(),
	PanelShowHide_t775446595::get_offset_of__image_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4113 = { sizeof (VuMarkEventHandler_t2972421956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4113[1] = 
{
	VuMarkEventHandler_t2972421956::get_offset_of_mTrackableBehaviour_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4114 = { sizeof (VuMarkHandler_t583794080), -1, sizeof(VuMarkHandler_t583794080_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4114[9] = 
{
	VuMarkHandler_t583794080::get_offset_of_mIdPanel_2(),
	VuMarkHandler_t583794080::get_offset_of_mVuMarkManager_3(),
	VuMarkHandler_t583794080::get_offset_of_mClosestVuMark_4(),
	VuMarkHandler_t583794080::get_offset_of_mCurrentVuMark_5(),
	VuMarkHandler_t583794080::get_offset_of_distance_6(),
	VuMarkHandler_t583794080::get_offset_of_vuMarkId_7(),
	VuMarkHandler_t583794080::get_offset_of_Txt_action_8(),
	VuMarkHandler_t583794080_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
	VuMarkHandler_t583794080_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4115 = { sizeof (U3CShowPanelAfterU3Ec__Iterator0_t1558983881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4115[8] = 
{
	U3CShowPanelAfterU3Ec__Iterator0_t1558983881::get_offset_of_seconds_0(),
	U3CShowPanelAfterU3Ec__Iterator0_t1558983881::get_offset_of_vuMarkTitle_1(),
	U3CShowPanelAfterU3Ec__Iterator0_t1558983881::get_offset_of_vuMarkId_2(),
	U3CShowPanelAfterU3Ec__Iterator0_t1558983881::get_offset_of_vuMarkImage_3(),
	U3CShowPanelAfterU3Ec__Iterator0_t1558983881::get_offset_of_U24this_4(),
	U3CShowPanelAfterU3Ec__Iterator0_t1558983881::get_offset_of_U24current_5(),
	U3CShowPanelAfterU3Ec__Iterator0_t1558983881::get_offset_of_U24disposing_6(),
	U3CShowPanelAfterU3Ec__Iterator0_t1558983881::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4116 = { sizeof (VuMarkTapHandler_t2561605313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4116[1] = 
{
	VuMarkTapHandler_t2561605313::get_offset_of_VuMarkCard_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4117 = { sizeof (SamplesAboutScreenInfo_t1023079980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4117[2] = 
{
	SamplesAboutScreenInfo_t1023079980::get_offset_of_titles_0(),
	SamplesAboutScreenInfo_t1023079980::get_offset_of_descriptions_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4118 = { sizeof (SamplesConfigUI_t3549911149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4118[9] = 
{
	SamplesConfigUI_t3549911149::get_offset_of_UIPresets_2(),
	SamplesConfigUI_t3549911149::get_offset_of_SamplesTitle_3(),
	SamplesConfigUI_t3549911149::get_offset_of_ExtendedTracking_4(),
	SamplesConfigUI_t3549911149::get_offset_of_Autofocus_5(),
	SamplesConfigUI_t3549911149::get_offset_of_Flash_6(),
	SamplesConfigUI_t3549911149::get_offset_of_CameraGroup_7(),
	SamplesConfigUI_t3549911149::get_offset_of_DatasetGroup_8(),
	SamplesConfigUI_t3549911149::get_offset_of_VirtualButtonsGroup_9(),
	SamplesConfigUI_t3549911149::get_offset_of_SmartTerrainGroup_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4119 = { sizeof (UIPresetsEnum_t1160230479)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4119[11] = 
{
	UIPresetsEnum_t1160230479::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4120 = { sizeof (SamplesLoadingScreen_t2658974353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4120[2] = 
{
	SamplesLoadingScreen_t2658974353::get_offset_of_mChangeLevel_2(),
	SamplesLoadingScreen_t2658974353::get_offset_of_mUISpinner_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4121 = { sizeof (SamplesMainMenu_t2014309523), -1, sizeof(SamplesMainMenu_t2014309523_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4121[9] = 
{
	SamplesMainMenu_t2014309523::get_offset_of_AboutCanvas_2(),
	SamplesMainMenu_t2014309523::get_offset_of_AboutTitle_3(),
	SamplesMainMenu_t2014309523::get_offset_of_AboutDescription_4(),
	SamplesMainMenu_t2014309523_StaticFields::get_offset_of_isAboutScreenVisible_5(),
	SamplesMainMenu_t2014309523_StaticFields::get_offset_of_menuItem_6(),
	0,
	0,
	SamplesMainMenu_t2014309523::get_offset_of_aboutScreenInfo_9(),
	SamplesMainMenu_t2014309523_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4122 = { sizeof (MenuItem_t3182038446)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4122[11] = 
{
	MenuItem_t3182038446::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4123 = { sizeof (SamplesNavigationHandler_t2120161205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4123[1] = 
{
	SamplesNavigationHandler_t2120161205::get_offset_of_currentSceneName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4124 = { sizeof (SamplesTapHandler_t2866147728), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4124[4] = 
{
	0,
	SamplesTapHandler_t2866147728::get_offset_of_mTimeSinceLastTap_3(),
	SamplesTapHandler_t2866147728::get_offset_of_mMenuAnim_4(),
	SamplesTapHandler_t2866147728::get_offset_of_mTapCount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4125 = { sizeof (MoveCamera_t3193441886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4125[7] = 
{
	MoveCamera_t3193441886::get_offset_of_turnSpeed_2(),
	MoveCamera_t3193441886::get_offset_of_panSpeed_3(),
	MoveCamera_t3193441886::get_offset_of_zoomSpeed_4(),
	MoveCamera_t3193441886::get_offset_of_mouseOrigin_5(),
	MoveCamera_t3193441886::get_offset_of_isPanning_6(),
	MoveCamera_t3193441886::get_offset_of_isRotating_7(),
	MoveCamera_t3193441886::get_offset_of_isZooming_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4126 = { sizeof (outlineShader_t244045327), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4126[4] = 
{
	outlineShader_t244045327::get_offset_of_gameonj_current_2(),
	outlineShader_t244045327::get_offset_of__newmat_3(),
	outlineShader_t244045327::get_offset_of_newcolor_4(),
	outlineShader_t244045327::get_offset_of_changecolor_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4127 = { sizeof (TouchCamera_t409509692), -1, sizeof(TouchCamera_t409509692_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4127[11] = 
{
	TouchCamera_t409509692_StaticFields::get_offset_of_PanSpeed_2(),
	TouchCamera_t409509692_StaticFields::get_offset_of_ZoomSpeedTouch_3(),
	TouchCamera_t409509692_StaticFields::get_offset_of_ZoomSpeedMouse_4(),
	TouchCamera_t409509692_StaticFields::get_offset_of_BoundsX_5(),
	TouchCamera_t409509692_StaticFields::get_offset_of_BoundsZ_6(),
	TouchCamera_t409509692_StaticFields::get_offset_of_ZoomBounds_7(),
	TouchCamera_t409509692::get_offset_of_cam_8(),
	TouchCamera_t409509692::get_offset_of_lastPanPosition_9(),
	TouchCamera_t409509692::get_offset_of_panFingerId_10(),
	TouchCamera_t409509692::get_offset_of_wasZoomingLastFrame_11(),
	TouchCamera_t409509692::get_offset_of_lastZoomPositions_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4128 = { sizeof (Touch_zoom_t1547134311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4128[1] = 
{
	Touch_zoom_t1547134311::get_offset_of_perspectiveZoomSensitivity_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4129 = { sizeof (cameraController_t4112230091), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4129[1] = 
{
	cameraController_t4112230091::get_offset_of_panSpeed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4130 = { sizeof (Change_View_t1623420308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4130[13] = 
{
	Change_View_t1623420308::get_offset_of_obj_bats_2(),
	Change_View_t1623420308::get_offset_of_Origin_3(),
	Change_View_t1623420308::get_offset_of_MCamera_4(),
	Change_View_t1623420308::get_offset_of_ArCamera_5(),
	Change_View_t1623420308::get_offset_of_Txt_action_6(),
	Change_View_t1623420308::get_offset_of_txt_version_7(),
	Change_View_t1623420308::get_offset_of_Menu_Bdd_8(),
	Change_View_t1623420308::get_offset_of_isShowing_9(),
	Change_View_t1623420308::get_offset_of_destination_checked_10(),
	Change_View_t1623420308::get_offset_of_BtnPlan_11(),
	Change_View_t1623420308::get_offset_of_BtnNaviguer_12(),
	Change_View_t1623420308::get_offset_of_BtnExit_13(),
	Change_View_t1623420308::get_offset_of_Btn_Destination_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4131 = { sizeof (Markers_t3135928711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4131[1] = 
{
	Markers_t3135928711::get_offset_of_vm_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4132 = { sizeof (Vumark_Class_t29598747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4132[8] = 
{
	Vumark_Class_t29598747::get_offset_of_U3Cpos_vumark_xU3Ek__BackingField_0(),
	Vumark_Class_t29598747::get_offset_of_U3Cpos_vumark_yU3Ek__BackingField_1(),
	Vumark_Class_t29598747::get_offset_of_U3Cpos_vumark_zU3Ek__BackingField_2(),
	Vumark_Class_t29598747::get_offset_of_U3CRot_vumark_xU3Ek__BackingField_3(),
	Vumark_Class_t29598747::get_offset_of_U3CRot_vumark_yU3Ek__BackingField_4(),
	Vumark_Class_t29598747::get_offset_of_U3CRot_vumark_zU3Ek__BackingField_5(),
	Vumark_Class_t29598747::get_offset_of_U3CLevelID_markersU3Ek__BackingField_6(),
	Vumark_Class_t29598747::get_offset_of_U3CVuMarkIDU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4133 = { sizeof (Office_Class_t2060087375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4133[1] = 
{
	Office_Class_t2060087375::get_offset_of_NewSalle_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4134 = { sizeof (Salle_t3888431915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4134[6] = 
{
	Salle_t3888431915::get_offset_of_U3ClabelU3Ek__BackingField_0(),
	Salle_t3888431915::get_offset_of_U3Cpos_xU3Ek__BackingField_1(),
	Salle_t3888431915::get_offset_of_U3Cpos_yU3Ek__BackingField_2(),
	Salle_t3888431915::get_offset_of_U3Cpos_zU3Ek__BackingField_3(),
	Salle_t3888431915::get_offset_of_U3Coffice_levelU3Ek__BackingField_4(),
	Salle_t3888431915::get_offset_of_U3CKeyU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4135 = { sizeof (User_Class_t3086874568), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4135[1] = 
{
	User_Class_t3086874568::get_offset_of_NewUser_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4136 = { sizeof (User_t3842584798), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4136[8] = 
{
	User_t3842584798::get_offset_of_U3CfirstNameU3Ek__BackingField_0(),
	User_t3842584798::get_offset_of_U3ClastNameU3Ek__BackingField_1(),
	User_t3842584798::get_offset_of_U3CmobileNumberU3Ek__BackingField_2(),
	User_t3842584798::get_offset_of_U3Cpos_xU3Ek__BackingField_3(),
	User_t3842584798::get_offset_of_U3Cpos_yU3Ek__BackingField_4(),
	User_t3842584798::get_offset_of_U3Cpos_zU3Ek__BackingField_5(),
	User_t3842584798::get_offset_of_U3CKeyU3Ek__BackingField_6(),
	User_t3842584798::get_offset_of_U3Cuser_levelU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4137 = { sizeof (Destination_script_t318603734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4137[1] = 
{
	Destination_script_t318603734::get_offset_of_level_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4138 = { sizeof (Exit_Level_t1387234555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4138[1] = 
{
	Exit_Level_t1387234555::get_offset_of_exit_level_var_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4139 = { sizeof (Finder_t2554545268), -1, sizeof(Finder_t2554545268_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4139[17] = 
{
	Finder_t2554545268::get_offset_of__ArCamera_2(),
	Finder_t2554545268::get_offset_of__MainCamera_Scene_3(),
	Finder_t2554545268::get_offset_of__Vumark_4(),
	Finder_t2554545268::get_offset_of_spawnObject_5(),
	Finder_t2554545268::get_offset_of_arrowObj_6(),
	Finder_t2554545268::get_offset_of_lExit_7(),
	Finder_t2554545268::get_offset_of_Obj_dest_8(),
	Finder_t2554545268::get_offset_of_markers_9(),
	Finder_t2554545268_StaticFields::get_offset_of__Level_10(),
	Finder_t2554545268::get_offset_of_VumarkNumber_11(),
	Finder_t2554545268::get_offset_of_vumark_class_script_12(),
	Finder_t2554545268::get_offset_of_vumarkhandler_Script_13(),
	Finder_t2554545268::get_offset_of_change_view_script_14(),
	Finder_t2554545268::get_offset_of_Txt_action_15(),
	Finder_t2554545268::get_offset_of_Menu_Bdd_16(),
	Finder_t2554545268_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_17(),
	Finder_t2554545268_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4140 = { sizeof (GetPos_t935983698), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4140[1] = 
{
	GetPos_t935983698::get_offset_of_ArCam_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4141 = { sizeof (ButtonListButton_t2298477596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4141[8] = 
{
	ButtonListButton_t2298477596::get_offset_of_BtnTxt_2(),
	ButtonListButton_t2298477596::get_offset_of_BtnsList_3(),
	ButtonListButton_t2298477596::get_offset_of_Obj_dest_4(),
	ButtonListButton_t2298477596::get_offset_of_Txt_action_5(),
	ButtonListButton_t2298477596::get_offset_of_Menu_Bdd_6(),
	ButtonListButton_t2298477596::get_offset_of_tmpStr_7(),
	ButtonListButton_t2298477596::get_offset_of_BtnListControl_8(),
	ButtonListButton_t2298477596::get_offset_of_change_view_script_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4142 = { sizeof (ButtonListControl_t933417769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4142[9] = 
{
	ButtonListControl_t933417769::get_offset_of_app_2(),
	ButtonListControl_t933417769::get_offset_of_BtnTemplate_3(),
	ButtonListControl_t933417769::get_offset_of_buttons_4(),
	ButtonListControl_t933417769::get_offset_of_IDiscUser_5(),
	ButtonListControl_t933417769::get_offset_of_IDiscOffice_6(),
	ButtonListControl_t933417769::get_offset_of_UserScript_7(),
	ButtonListControl_t933417769::get_offset_of_OfficeScript_8(),
	ButtonListControl_t933417769::get_offset_of_valueUserColl_9(),
	ButtonListControl_t933417769::get_offset_of_valueOfficeColl_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4143 = { sizeof (Key_t2293551073), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4143[3] = 
{
	Key_t2293551073::get_offset_of_U3CfirstNameU3Ek__BackingField_0(),
	Key_t2293551073::get_offset_of_U3ClastNameU3Ek__BackingField_1(),
	Key_t2293551073::get_offset_of_U3CmobileNumberU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4144 = { sizeof (Users_t907518852), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4144[1] = 
{
	Users_t907518852::get_offset_of_U3CkeyU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4145 = { sizeof (IdOffice_t1887523751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4145[1] = 
{
	IdOffice_t1887523751::get_offset_of_U3ClableU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4146 = { sizeof (OfficeData_t3828749716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4146[1] = 
{
	OfficeData_t3828749716::get_offset_of_U3Cid_officeU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4147 = { sizeof (RootObject_t1671561673), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4147[2] = 
{
	RootObject_t1671561673::get_offset_of_U3CusersU3Ek__BackingField_0(),
	RootObject_t1671561673::get_offset_of_U3COfficeDataU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4148 = { sizeof (ClickToAsync_t2031327609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4148[3] = 
{
	ClickToAsync_t2031327609::get_offset_of_loadingbar_2(),
	ClickToAsync_t2031327609::get_offset_of_loadimage_3(),
	ClickToAsync_t2031327609::get_offset_of_async_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4149 = { sizeof (U3CLoadLevelWithBarU3Ec__Iterator0_t1546663020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4149[5] = 
{
	U3CLoadLevelWithBarU3Ec__Iterator0_t1546663020::get_offset_of_level_0(),
	U3CLoadLevelWithBarU3Ec__Iterator0_t1546663020::get_offset_of_U24this_1(),
	U3CLoadLevelWithBarU3Ec__Iterator0_t1546663020::get_offset_of_U24current_2(),
	U3CLoadLevelWithBarU3Ec__Iterator0_t1546663020::get_offset_of_U24disposing_3(),
	U3CLoadLevelWithBarU3Ec__Iterator0_t1546663020::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4150 = { sizeof (LoadAdditive_t1235278820), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4151 = { sizeof (LoadOnClick_t2668983067), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4151[1] = 
{
	LoadOnClick_t2668983067::get_offset_of_loadingImage_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4152 = { sizeof (TogglePanelButton_t2654886650), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4153 = { sizeof (Origin_scripts_t2339293569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4153[1] = 
{
	Origin_scripts_t2339293569::get_offset_of_level_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4154 = { sizeof (vumark_levid_t1745599613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4154[2] = 
{
	vumark_levid_t1745599613::get_offset_of_vumark_level_num_2(),
	vumark_levid_t1745599613::get_offset_of_marker_id_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4155 = { sizeof (BackgroundPlaneBehaviour_t2431285219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4156 = { sizeof (CloudRecoBehaviour_t3077176941), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4157 = { sizeof (CylinderTargetBehaviour_t2091399712), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4158 = { sizeof (DefaultInitializationErrorHandler_t965510117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4158[3] = 
{
	DefaultInitializationErrorHandler_t965510117::get_offset_of_mErrorText_2(),
	DefaultInitializationErrorHandler_t965510117::get_offset_of_mErrorOccurred_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4159 = { sizeof (DefaultSmartTerrainEventHandler_t870608571), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4159[3] = 
{
	DefaultSmartTerrainEventHandler_t870608571::get_offset_of_mReconstructionBehaviour_2(),
	DefaultSmartTerrainEventHandler_t870608571::get_offset_of_PropTemplate_3(),
	DefaultSmartTerrainEventHandler_t870608571::get_offset_of_SurfaceTemplate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4160 = { sizeof (DefaultTrackableEventHandler_t1082256726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4160[1] = 
{
	DefaultTrackableEventHandler_t1082256726::get_offset_of_mTrackableBehaviour_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4161 = { sizeof (GLErrorHandler_t3809113141), -1, sizeof(GLErrorHandler_t3809113141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4161[3] = 
{
	GLErrorHandler_t3809113141_StaticFields::get_offset_of_mErrorText_2(),
	GLErrorHandler_t3809113141_StaticFields::get_offset_of_mErrorOccurred_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4162 = { sizeof (HideExcessAreaBehaviour_t3495034315), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4163 = { sizeof (ImageTargetBehaviour_t2654589389), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4164 = { sizeof (AndroidUnityPlayer_t852788525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4164[6] = 
{
	0,
	0,
	AndroidUnityPlayer_t852788525::get_offset_of_mScreenOrientation_2(),
	AndroidUnityPlayer_t852788525::get_offset_of_mJavaScreenOrientation_3(),
	AndroidUnityPlayer_t852788525::get_offset_of_mFramesSinceLastOrientationReset_4(),
	AndroidUnityPlayer_t852788525::get_offset_of_mFramesSinceLastJavaOrientationCheck_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4165 = { sizeof (ComponentFactoryStarterBehaviour_t3249343815), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4166 = { sizeof (IOSUnityPlayer_t3656371703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4166[1] = 
{
	IOSUnityPlayer_t3656371703::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4167 = { sizeof (VuforiaBehaviourComponentFactory_t1383853028), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4168 = { sizeof (WSAUnityPlayer_t425981959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4168[1] = 
{
	WSAUnityPlayer_t425981959::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4169 = { sizeof (MaskOutBehaviour_t2994129365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4170 = { sizeof (MultiTargetBehaviour_t3504654311), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4171 = { sizeof (ObjectTargetBehaviour_t3836044259), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4172 = { sizeof (PropBehaviour_t966064926), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4173 = { sizeof (ReconstructionBehaviour_t4009935945), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4174 = { sizeof (ReconstructionFromTargetBehaviour_t2111803406), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4175 = { sizeof (SurfaceBehaviour_t2405314212), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4176 = { sizeof (TextRecoBehaviour_t3400239837), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4177 = { sizeof (TurnOffBehaviour_t3058161409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4178 = { sizeof (TurnOffWordBehaviour_t584991835), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4179 = { sizeof (UserDefinedTargetBuildingBehaviour_t4184040062), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4180 = { sizeof (VRIntegrationHelper_t556656694), -1, sizeof(VRIntegrationHelper_t556656694_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4180[12] = 
{
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftCameraMatrixOriginal_2(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightCameraMatrixOriginal_3(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftCamera_4(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightCamera_5(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftExcessAreaBehaviour_6(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightExcessAreaBehaviour_7(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftCameraPixelRect_8(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightCameraPixelRect_9(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftCameraDataAcquired_10(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightCameraDataAcquired_11(),
	VRIntegrationHelper_t556656694::get_offset_of_IsLeft_12(),
	VRIntegrationHelper_t556656694::get_offset_of_TrackableParent_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4181 = { sizeof (VideoBackgroundBehaviour_t3161817952), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4182 = { sizeof (VirtualButtonBehaviour_t2515041812), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4183 = { sizeof (VuMarkBehaviour_t2060629989), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4184 = { sizeof (VuforiaBehaviour_t359035403), -1, sizeof(VuforiaBehaviour_t359035403_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4184[1] = 
{
	VuforiaBehaviour_t359035403_StaticFields::get_offset_of_mVuforiaBehaviour_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4185 = { sizeof (VuforiaConfiguration_t3823746026), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4186 = { sizeof (VuforiaRuntimeInitialization_t1850075444), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4187 = { sizeof (WireframeBehaviour_t2494532455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4187[4] = 
{
	WireframeBehaviour_t2494532455::get_offset_of_lineMaterial_2(),
	WireframeBehaviour_t2494532455::get_offset_of_ShowLines_3(),
	WireframeBehaviour_t2494532455::get_offset_of_LineColor_4(),
	WireframeBehaviour_t2494532455::get_offset_of_mLineMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4188 = { sizeof (WireframeTrackableEventHandler_t1535150527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4188[1] = 
{
	WireframeTrackableEventHandler_t1535150527::get_offset_of_mTrackableBehaviour_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4189 = { sizeof (WordBehaviour_t3366478421), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4190 = { sizeof (dis_or_t450365292), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4191 = { sizeof (script_test_t1536385136), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4192 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305145), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4192[2] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DABA519E428DAD9CE185AEB6FA2FE954198E228E8_0(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D898C2022A0C02FCE602BF05E1C09BD48301606E5_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4193 = { sizeof (U24ArrayTypeU3D96_t3894236537)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D96_t3894236537 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4194 = { sizeof (U24ArrayTypeU3D24_t762068664)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D24_t762068664 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
