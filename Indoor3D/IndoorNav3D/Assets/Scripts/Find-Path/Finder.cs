﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Finder : MonoBehaviour {
	
	//OnDrawGizmosSelected

	void Update(){
		if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Stationary) 

		//if (Input.GetKeyDown(KeyCode.B))
		{
			OnDrawGiz ();
		}
	}
	void OnDrawGiz()
	{
		Transform target = GameObject.Find ("Destination").GetComponent<Transform>();
		NavMeshAgent nav;
		LineRenderer line;
		 nav= this.GetComponent<NavMeshAgent>();
	     if( nav == null || nav.path == null )
		    return;
//
		line = this.GetComponent<LineRenderer>();
//		if( line == null )
//		{
//			line = this.gameObject.AddComponent<LineRenderer>();
			line.material = new Material( Shader.Find( "Sprites/Default" ) ) { color = Color.yellow };
//			line.startWidth = 0.5f;
//			line.endWidth = 0.5f;
			//line.SetWidth( 0.5f, 0.5f );
			line.startColor=Color.yellow;
			line.endColor = Color.green;
			//line.SetPosition (-1, transform.position);
			//line.SetColors( Color.yellow, Color.yellowss );
		//}
		nav.SetDestination (target.position);
		nav.Stop ();
		var path = nav.path;
		if (path.corners.Length < 2)
			return ;
		
		line.positionCount=path.corners.Length ;

		for( int i = 0; i < path.corners.Length; i++ )
		{
			line.SetPosition( i, path.corners[ i ] );
		}

	}

}
