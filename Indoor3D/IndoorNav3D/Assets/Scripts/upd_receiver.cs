﻿using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using System.Collections.Generic;
using UnityEngine;

public class upd_receiver : MonoBehaviour {


	static UdpClient udp;
	Thread thread;
	List<double> receivedArray;
	//public float[] vuforia;
	Vector3 receive_pos;
	public Vector3 pos;
	public GameObject []Pos_Vumarks;
	Vumark_Class vm;
	Vector3 eularAngles;

	void Start () {
		int port = 5555;
		udp = new UdpClient (port);
		thread = new Thread (new ThreadStart (ThreadMethod));
		thread.Start ();
		Debug.Log ("listening on " + port);
		//receivedArray = JsonConvert.DeserializeObject<List<double>> ("[\"-0.103713155\"]");


		//receivedArray = JsonUtility.FromJson<List<double>> ("[\"-0.103713155\"]", List<double>); 
		//Debug.Log ("n=" + receivedArray.Count);
	}


	void OnApplicationQuit () {
		udp.Close ();
		thread.Abort ();
	}
	void Update(){
		this.GetComponent<Transform> ().transform.localPosition = new Vector3 (pos.x,pos.y,pos.z);
		this.GetComponent<Transform> ().localRotation = Quaternion.Euler (eularAngles);

		//Debug.Log ("accepted vmmmmmmm"+vm.VuMarkID);
		//GameObject.Find("ARCamera").GetComponent<Transform>().transform.position=new Vector3 (pos.x,pos.y,pos.z);

		//Condition for VumarkId , which for each id change the location of Vumark in the scene 
		switch (vm.VuMarkID){
		case "11":
			if (GameObject.Find("VuMark").GetComponent<Transform>().position == Pos_Vumarks [0].transform.position)
				Debug.Log ("Original position");
			else {
				GameObject.Find("VuMark").GetComponent<Transform>().position = Pos_Vumarks [0].transform.position;
				GameObject.Find("VuMark").GetComponent<Transform>().localRotation=Pos_Vumarks [0].transform.localRotation;
			}
			break;
		case "12":
			GameObject.Find("VuMark").GetComponent<Transform>().position = Pos_Vumarks [1].transform.position;
			GameObject.Find("VuMark").GetComponent<Transform>().localRotation=Pos_Vumarks [1].transform.localRotation;
			break;
		case "13":
			//this.transform.position = Pos_Vumarks [2].transform.position;
			break;
		default:
			break;

		}
	}

	private void ThreadMethod () {
		while (true) {
			IPEndPoint RemoteIpEndPoint = new IPEndPoint (IPAddress.Any, 0);
			byte[] receiveBytes = udp.Receive (ref RemoteIpEndPoint); 
			string receivedData = Encoding.ASCII.GetString (receiveBytes);
			Debug.Log (receivedData);
			//pos = (Vector3)JsonConvert.DeserializeObject<Vector3> (receivedData);
			vm=(Vumark_Class)JsonConvert.DeserializeObject<Vumark_Class>(receivedData);
			pos = new Vector3 (vm.cam_x, vm.cam_y, vm.cam_z);
			eularAngles = new Vector3 (vm.camR_x,vm.camR_y,vm.camR_z);

//			switch (vm.VuMarkID){
//			case "11":
//				Debug.Log ("111111111111111111111");
////				if (GameObject.Find("VuMark").GetComponent<Transform>().position == GameObject.Find("Cube").GetComponent<Transform>().position)
////					Debug.Log ("Original position");
////				else {
////					GameObject.Find ("VuMark").GetComponent<Transform> ().position = GameObject.Find ("Cube").GetComponent<Transform> ().position;
////					//this.transform.localRotation=Pos_Vumarks [0].transform.localRotation;
////				}
//				break;
//			case "12":
//				Debug.Log ("22222222222222222");
////				GameObject.Find ("VuMark").GetComponent<Transform> ().position = GameObject.Find ("Cube (1)").GetComponent<Transform> ().position;
//				//this.transform.localRotation=Pos_Vumarks [1].transform.localRotation;
//				break;
//			case "13":
//				//this.transform.position = Pos_Vumarks [2].transform.position;
//				break;
//			default:
//				break;
//
//			}



			//pos = new Vector3 (pos.x, pos.y, pos.z);
		
			//vuforia =(Vector3) JsonConvert.DeserializeObject<Vector3> (receivedData);
			//float a = pos.x;
			//Debug.Log ("Receive data from mac pos"+ a);
			//var _Data = JsonConvert.DeserializeObject <List<data_pos>>(receivedData);
//			foreach (data_pos pos in _Data)
//			{
//
//				Debug.Log ("X is "+pos.x.ToString());
//			}
//			//receivedArray = JsonConvert.DeserializeObject<List<double>> (receivedData);
			//vuforia = JsonConvert.DeserializeObject<float[]> (receivedData);
			//Debug.Log ("n=" + _Data.Count + "  - n2=" + vuforia.Length);
			//Debug.Log ("n=" + receivedArray.Count + "  - n2=" + vuforia.Length);
			//pos = new Vector3 (vuforia [0], vuforia [1], vuforia [2]);

		}
	}
}
public class Vumark_Class{
	//Camera Position
	public float cam_x{ get; set;}
	public float cam_y{ get; set;}
	public float cam_z{ get; set;}

	//Camera Rotation
	public float camR_x{ get; set;}
	public float camR_y{ get; set;}
	public float camR_z{ get; set;}
	//VuMarkID
	public string VuMarkID{ get; set;}
}


// from http://stackoverflow.com/questions/37131742/how-to-use-udp-with-unity-methods