﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Distance : MonoBehaviour {
	public float distance;
	Vector3 a;
	Vector3 b;
	// Use this for initialization
	void Start () {
		a= GameObject.Find ("VuMark").GetComponent<Transform> ().position;
		b= GameObject.Find ("Capsule").GetComponent<Transform> ().position;
	}
	
	// Update is called once per frame
	void Update () {
		a= GameObject.Find ("VuMark").GetComponent<Transform> ().position;
		b= GameObject.Find ("Capsule").GetComponent<Transform> ().position;
		distance = Vector3.Distance(a,b);
		Debug.Log ("Distance"+distance);
	}
}
