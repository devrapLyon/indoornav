# Indoor Nav

How to help user to navigate inside enterprise ?
How to find a room or a person ?

Indoor Nav is a prototype to test an indoor Navigation for Android and iOS mobiles.
This project use Reality Augmented to see navigation.

[See the video of presentation](https://www.youtube.com/watch?v=4wce_wZ6rwk)

**Note:** it's a beta version, Last update on the Git, IndoorNav support Multifloor navigation.


For this prototype we use : 

- [Vuforia] to read markers positioned in the building, and locate the user by ID of theses markers ([link to login into Vuforia target manager](https://developer.vuforia.com/user/login) )
- [Unity 3D] to code naviagation app with RA and 3D
- [Sketchup] to modelize a 3D plan for a 2D plan of building
- [Orange Nectarine] to publish iOS mobile app.


## Contributors

- Mohammad Kabiri : main developer. Trainee from March to August 2017.
- Laurent Souchet : coach of Mohammad
- Joaquin Keller : Orange specialist of 3D and RA/RV universe
- Maxime Jouin : Orange specialist of 3D and RA/RV universe


## How to test

### Install mobile app (Android or iOS)

[Upload **Google Android** app mobile](https://play.google.com/store/apps/details?id=com.devraplyon.indoornav) to navigate in Tour Part Dieu (9e stage)

![Or scan this QR Code for download mobile app Android](./docs/QRCodeMobileAppAndroid.png)

[Upload **Apple iOS** app mobile](https://is.gd/wbFaoz)

![Or scan this QR code for download mobile app iOS](./docs/QRCodeMobileAppIOS.png)

When you start iOS app, if you have a message like this *Développeur Enterprise non approuvé", see [this article](https://support.apple.com/fr-fr/HT204460) 


### Use case : I'm on 904 office and I want to get out


Click "Naviguer" button to start scaning of markers.

You should download and print marker and put it on the floor.Click here to download first marker [**First Marker**](./docs/vumarkOrange_11.pdf) 

Scan this first marker : it's office 904, where you start.

You'll see an left arrow to indicate direction.


![Scan the marker of 904 desk](./docs/marker_desk_904.png)

Clic "Plan" button to see where you are on the plan.


![Scan the marker of 904 desk](./docs/Plan_path.png)

You will see the path on the plan, green color  


Scan Second marker of the way to get out : it's office 901
Click here to download second marker [**Second Marker**](./docs/vumarkOrange_12(1).pdf)

You'il see an another arrow to indicate direction.

![Scan the marker of 904 desk](./docs/vumarkOrange_12.png)


Click "Plan" button to see where you are.


 

## Others indoor navigation projects in the world

- [Airport Britannique](http://fr.ubergizmo.com/2017/05/27/aeroport-britannique-beacons-bluetooth-navigation-realite-augmentee.html?utm_content=bufferb0655&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer). Localication with Beacons.


## Tutorials

Orange Tutorials

- Le wiki : http://orange-unity-web.orange-labs.fr/wiki/index.php/Main_Page
- Le tuto pour bien débuter sur Unity : http://orange-unity-web.orange-labs.fr/wiki/index.php/D%C3%A9butants
- Le tuto pour bien débuter sur la RA avec Unity : http://orange-unity-web.orange-labs.fr/wiki/index.php/La_r%C3%A9alit%C3%A9_augment%C3%A9e_pour_les_nuls
- 

## How the code is structured

- The main project:"Tour-part-Dieu-building", it includes all the main core project.

- "Vuforia-Distance", which has a function to get your location, using Vuforia marker and send your position information to another application to show your position in space at realtime. we have been used UDP protocol to send datas.

- "Indoor3D/IndoorNav3D", receive the data, from "Vuforia-Distance" application, and show you, your location on the scene.

- "ScreenMenuTest", a test Unity project to show, how we can use the menus on the Unity.

- "Tour-PartDieu-Orange ", This Unity project, show you how we can use path-Finding algorithm to draw the path between the elements on the scene or our model.


## How to deploy app to mobile Android, iOS

### How to deploy iOS mobile app to Nectarine, Orange store.

#### Generate and resign ipa file

From Unity, generate ipa with com.orange in prefix of package name.

The first time, if we have not a connexion to Jenkins, send a access to Rémi Groult <remi.groult@orange.com> with your CUID.
The password is **changeme**.

Then, when you have an access, resign IPA with [Jenkins job](http://c-hod-osmose2.rd.francetelecom.fr:3003/view/iOS/job/iOS-Resign-Manual/) in iOS tab, job **iOS-Resign-Manuel

Clic *build with parameters* top left window. 

Upload IPA file to resign

Clic *Build*

Waiting star of new job

When job finished, download ipa file in *Arfacts du build*

#### Deploy to Nectarine, Orange store

Console [Nectarine] must be used to deploy app mobile.

First time, create create an account, then add a new app (clic on + sign).

Verify you are *Application Administrators* of application.

Verify to select *Latest Build*

To have all options to add a version, top right clic *ACCOUNT* list and select *Display Admin Options*

![Display Admin Options](./docs/nectarine-top-right-admin-menu.png)

 

## Contacts

- Laurent Souchet

    - email: <laurent.souchet@orange.com>
    - Twitter: [canardair](https://twitter.com/canardair)


- Mohammad Kabiri 

    - e-mail: <mo.kabiiri@gmail.com>, 
    - LinkedIn: [mkabiri](https://www.linkedin.com/in/mkabiri/) 


Please feel free to contact us, for any informations.


All rights reserved, Copyright (©) 2017 by the Orange Company.


------

[Nectarine]:https://nectarine.orange-labs.fr/web/#/apps
[Sketchup]:http://www.sketchup.com/fr
[Orange Nactarine]:https://nectarine.orange-labs.fr/web/index.html#/home
[Unity 3D]:https://unity3d.com/fr
[Vuforia]:https://www.vuforia.com/


iOS dev
https://is.gd/cMc7Xa

iOS prod
https://is.gd/oYOpBF