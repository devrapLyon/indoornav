﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Touch_zoom : MonoBehaviour {
	public float perspectiveZoomSensitivity;

	//;public float orthographicZoomSensitivity;
	// Use this for initialization

	// Update is called once per frame
	void Update () {
		if (Input.touchCount == 3) 
		{
			//Touch touch0 = Input.GetTouch(0);
			//Touch touch1 = Input.GetTouch(1);

			Vector2 touch0_prevPos = Input.GetTouch(0).position - Input.GetTouch(0).deltaPosition;
			Vector2 touch1_prevPos = Input.GetTouch(1).position - Input.GetTouch(1).deltaPosition;

			float prev_TouchDeltaMag = (touch0_prevPos - touch1_prevPos).magnitude;
			float current_TouchDeltaMag = (Input.GetTouch(0).position - Input.GetTouch(1).position).magnitude;

			float deltaMagDiff = prev_TouchDeltaMag - current_TouchDeltaMag;

			//if the camera is orthographic view
			//this.GetComponent<Camera> ().orthographicSize += deltaMagDiff * orthographicZoomSensitivity;

			//if the camera is in perspective view
			this.GetComponent<Camera> ().fieldOfView += deltaMagDiff * perspectiveZoomSensitivity;

			//if the camera is in orthographic view
			//this.GetComponent<Camera> ().orthographicSize = Mathf.Max(this.GetComponent<Camera> ().orthographicSize, 0.1f);

			//if the camera is in perspective view
			this.GetComponent<Camera> ().fieldOfView= Mathf.Clamp( this.GetComponent<Camera> ().fieldOfView, 0.1f, 179.9f);
	}
}
}